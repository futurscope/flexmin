# Flexmin Asset Source Files

This folder is the source folder for the flexmin asset zip files.

Use the `.\update.sh` script to get the current script and config files off 
this system to use as source assets.

## system (folder)

### setup.sh (script)

`setup.sh` is the script used to prepare the system for Flexmin, this script
is called when you run the `flexmin install` command after installig the
PyPi package.

This script also relies on these scripts

- `setup_pathcheck.sh` _checks where required paths are on the system and sets
  environment variables appropriately._
- `setup_prereq_checks.sh` _checks for certain prerequisites and outputs details
  of missing prerequisites and sets environment varaible `abandon` to yes if
  setup should be abandoned._
