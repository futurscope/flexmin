# -------------------------------
# name: List Configurations
# tags: Py4Web
# info: Lists available-site config files for selection and viewing.
# ------------------------------

# Main Script
# --------------

if [ -z ${FM_PY4WEB_ROOT} ] || [ ! -d ${FM_PY4WEB_ROOT} ]
then
  echo "title: Py4Web - Folder Not Found"
  echo "note: >"
  echo "  Please ensure the flexmin parameter FM_PY4WEB_ROOT is defined," 
  echo "  and the folder exists and has your Py4Web applications in it."
  exit 1
fi

# get location of NGINX configs (NGINX_AVAIL, and NGINX_ENABLED)
source ./helpers/nginx_configs.sh
    
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  # ask user to select a file
  echo "title: Py4Web Applications"
  echo "display:"
  echo "  - text: |"
  echo "      List of py4web applications in ${FM_PY4WEB_ROOT} including:"
  echo "      - uWSGI status (has a matching config file been set up)"
  echo "      - NGINX status (has a location file been set up with the py4web app name in it (incl. static folder) , available)"
  echo "  - note: >"
  echo "      WSGI check looks for matching uWSGI ini file or any ini file with a chdir entry pointing to the application folder."
  echo "  - table:"
  echo "      header: [Application, WSGI*, NGINX*]"
  echo "      delimiter_row: '\n'"
  echo "      delimiter: '\s'"
  echo "      show_sub: ['_',' ']"
  echo "      data: |"
  for nc in `ls -d ${FM_PY4WEB_ROOT}/*/ | grep -vE '/_.*/$' | sed -r 's/^.*\/([^/]+)\/$/\1/'`
  do
    ./helpers/uwsgi_configs.sh $nc >/dev/null
    if [ $? -eq 0 ]
    then
      uwsgi='ready*green'
    else
      if ./helpers/uwsgi_configs.sh ${FM_PY4WEB_ROOT} >/dev/null
      then
        uwsgi='ready*green'
      else
        uwsgi='not_found*red'
      fi
    fi
    if grep -r "location /${nc}/static" ${NGINX_AVAIL}/* >/dev/null 2>&1
    then
      nginx='ready*green'
    else
      nginx='not_found*red'
    fi
    if grep -r "location /${nc}/static" ${NGINX_LOC_AVAIL}/* >/dev/null 2>&1
    then 
      nginx='ready*green'
    fi
    # [ -f ${NGINX_AVAIL}/$nc ] && nginx='ready*green' || nginx='not_found*red'
    #[ -f ${NGINX_LOC_AVAIL}/py4web_$nc ] && nginx='ready*green' || nginx='not_found*red'
    echo "        ${nc} ${uwsgi} ${nginx}"
  done
  echo "  - form:"
  echo "      title: Create new py4web app"
  echo "      fields:"
  echo "        - label: App name"
  echo "          info: Name of app folder and URL part"
  echo "          pattern: '[a-zA-Z0-9_.]+'"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  echo "      submit: Add app"
  
  
elif [ "$2" == "create" ]
then
  echo "title: Creating ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  cd ${FM_PY4WEB_ROOT}
  py4web new_app -Y ${FM_PY4WEB_ROOT} ${1}
  cd -
  echo "Done"
  
  
elif [ "$2" == "enable" ]
then
  echo "title: Enabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  ln -s ${AVAIL}${1} ${ENABLED}${1}
  echo "Enabled"
elif [ "$2" == "disable" ]
then
  echo "title: Disabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  rm ${ENABLED}${1}
  echo "Disabled"
elif [ "$2" == "delete" ]
then
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  # remove link file and original
  echo "---"
  echo "Disabling and deleting"
  rm ${ENABLED}${2}
  rm ${AVAIL}${2}
elif [ "$2" == "view" ]
then
  echo "title: View Configuration - ${1}"
  echo "method: popup"
  echo "---"
  if [ -r ${AVAIL}${1} ]
  then
    cat ${AVAIL}/${1}
  else
    THIS_USER=$(whoami)
    echo "ERROR: ${THIS_USER} does not have permission to open the file."
  fi
fi
