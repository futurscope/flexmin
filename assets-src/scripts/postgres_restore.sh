# -------------------
# name: Restore
# tags: PostgreSQL
# info: Lists PostgreSQL DBs for Backup
# ---------------------

# Check whether postgress installed and initialised
./helpers/postgres_check.sh || exit
./helpers/backup_check.sh || exit

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Backup Files"
  echo "table:"
  echo "  actions: '?restore.!delete'"
  echo "  header: [Filename,DateTime, Size]"
  echo "  delimiter: ';'"
  echo "  data: |"
  cd ${FM_BACKUP}
  find ./ -name "*.pgdump.gz" -readable -printf "%p;%Tc;%s\n" | sed -r 's/^\.\///' | sed 's/^/    /'
  #ls -all -D "%Y-%m-%d %H:%M:%S" *.pgdump.gz | grep -v -e "^total" -e "\.$" | awk '{print("    ",$8,"\t",$5,"\t",$6,"\t",$7)}'

elif [ "$2" == "restore" ]
then
  echo "title: Select database"
  echo "method: popup"
  echo "note:"
  echo "  Existing contents of the selected database will be deleted and replaced"
  echo "  with the restored data, use with caution."
  echo "form:"
  echo "  submit: Restore Database"
  echo "  fields:"
  echo "    - label: Backup file"
  echo "      type: text_long"
  echo "      default: ${1}"
  echo "    - type: hidden"
  echo "      default: restore_db"
  echo "    - label: Destination Database"
  echo "      delimiter: '\n'"
  echo "      select: |"
  q="SELECT d.datname FROM pg_database d WHERE d.datistemplate = false;"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "${q}" | sed s'/^/       /'

elif [ "$2" == "restore_db" ]
then
  echo "title: Restoring to ${3}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  echo "Clearing contents of ${3}"
  ./helpers/postgres_recreatedb.sh "${3}"
  echo "Restoring backup ${1}"
  echo "To database ${3}"
  # Make sure postgres user account has access to backup path
  gunzip -c ${FM_BACKUP}/${1} | ${FM_PSQL} ${3} -U ${FM_PSQL_SU}
  #sudo -u ${FM_PSQL_SU} gunzip -c ${FM_BACKUP}/${1} | ${FM_PSQL} ${3}
  echo "Done."
fi
