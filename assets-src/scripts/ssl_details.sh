# -----------------------------
# name: Certificate Details
# tags: Certificates
# info: Lists available certificate files and displays details
#   of selected certificate if possible
# -----------------------------


# FM_SSL_ROOT=/usr/local/etc/ssl/

# Main Script
# --------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Certificate List"
  echo "display:"
  if [ -d /etc/letsencrypt/live ] && [ $(command -v certbot) ]
  then
    echo "  - table:"
    echo "      title: Let's Encrypt Certificates"
    echo "      actions: ?cb+detail.!cb+delete"
    echo "      header: [Name, Expiry]"
    echo "      delimiter: ', '"
    echo "      data: |"
    certbot certificates 2>&1 | grep -e 'Name:' -e 'Expiry' | sed -r 's/^[^:]+\:[ ]+//' | sed -r 's/ [0-9]+[^(]+/ /' | sed -E '/./{H;$!d} ; x ; s/([a-z]{2})\n/\1, /g' | grep -v '^$' | sed -r 's/^/        /'
  fi
  if [ -d ${FM_SSL_ROOT} ]
  then
    echo "  - table:"
    echo "      title: Other Certificates"
    echo "      header: [Certificate, Expiry]"
    echo "      actions: '?view.!delete'"
    echo "      delimiter: ';'"
    echo "      data: |"
    cd ${FM_SSL_ROOT}
    find ./ \( -name "*.crt" -o -name "*.key" \) | sed -r 's/^\.\///' | sort | while read -r line
    do
      case $line in
        *.crt|*.csr )
          end_date=$(openssl x509 -in ${FM_SSL_ROOT}/$line -noout -enddate | sed -e 's/notAfter=//')
          echo "        $line;$end_date"
          ;;
        * )
          echo "        $line;No expiry found"
          ;;
      esac
    done
    cd - >/dev/null
  fi


# --------------------------------------------
# LetsEncrypt certficate actions
# --------------------------------------------

elif [ "$2" == "cb detail" ]
then
    echo "title: 'Certificate : ${1}'"
    echo "method: popup"
    echo "---"
    certbot certificates --cert-name ${1}
    
elif [ "$2" == "cb delete" ]
then
    echo "title: 'Deleting Certificate : ${1}'"
    echo "method: popup"
    echo "---"
    #certbot delete --cert-name ${1}
    echo "Not really!"
    
# --------------------------------------------
# Other certficate type actions
# --------------------------------------------

elif [ "$2" == "delete" ]
then
    echo "title: Deleting ${1}"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: DELETE"
    echo "---"
    rm ${FM_SSL_ROOT}/${1}
    echo "Deleted"
    
elif [ "$2" == "view" ]
then
    echo "title: 'Certificate : ${1}'"
    echo "method: popup"
    echo "---"
    # cert file specified by first parameter
    #echo "debug: |"
    #set | sed 's/^/  /'
    case "$1" in
    *.crt )
        openssl x509 -in ${FM_SSL_ROOT}/$1 -text -noout
        ;;
    *.csr )
        openssl req -in ${FM_SSL_ROOT}/$1 -text -noout -verify
        ;;
    * )
        echo "Can only show info for .crt and .csr files."
        ;;
    esac

elif [ "$2" == "raw_text" ]
then
    case "$1" in
    *.crt )
        cat ${FM_SSL_ROOT}/$1
        ;;
    *.csr )
        cat ${FM_SSL_ROOT}/$1
        ;;
    * )
        echo "Can only show .crt and .csr files."
        ;;
    esac
fi
