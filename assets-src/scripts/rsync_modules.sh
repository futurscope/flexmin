# ----------------------
# name: Modules
# tags: Rsync
# info: Rsync Module Administration
# -------------------------

# Main Script
# ------------------

if [ ! -f $FM_RSYNCD_CONF ]
then
  echo "title: Rsync Modules"
  echo "text: Rsync Daemon not installed."
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Rsync Modules"
  echo "display:"
  echo "  - note: |"
  echo "      Rysnc modules represent a configuration for syncing data in a "
  echo "      particular path, with various settings specific to the module."
  echo "      Modules defined here relate to the Rsync Daemon (Service) not"
  echo "      individual users which must be configured elsewhere."
  echo "  - table:"
  echo "      header: [Module,Path]"
  echo "      actions: 'info.!delete'"
  echo "      delimiter: ','"
  echo "      data: |"
  conf=`cat ${FM_RSYNCD_CONF}`
  for mod in `cat ${FM_RSYNCD_CONF} | sed -n 's/^\[\(.*\)\]/\1/p'`
  do
    # sed 1 select all text between and including [mod] and any other []
    # sed 2 matches a path entry and repalces it with the path only, and only returns matched lines
    path=`echo "${conf}" | sed -n "/\[${mod}\]/,/\[.*\]/p" | sed -nr 's/^path[ ]*\=[ ]*(.*)/\1/p'`
    echo "        ${mod},${path}"
  done
  echo "  - form:"
  echo "      title: Create Rsync Module"
  echo "      submit: Create User"
  echo "      fields:"
  echo "        - label: Name"
  echo "          type: text"
  echo "          pattern: '[\w\-\_]+'"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Description"
  echo "          type: text"
  echo "        - label: Path"
  echo "          info: Path to folder to be synchronised with Rsync"
  echo "          type: text"
  echo "        - label: Read Only"
  echo "          select: ['yes','no']"
  echo "        - label: Users"
  echo "          info: |"
  echo "            Leave blank for no user authentication, add comma separated"
  echo "            list of rsync users if user based authentication is required"
  echo "          type: text"
elif [ "$2" == "info" ]
then
  echo "title: Module Info ${1}"
  echo "method: popup"
  echo "---"
  # sed 1 selects the text between [module] and the end of the file or the next [] entry
  # sed 2 removes the second [] entry by removing [] from line 2 onwards only
  cat ${FM_RSYNCD_CONF} | sed -n "/\[${1}\]/,/\[.*\]/p" | sed -r '2,$ s/^\[.*\]//'

elif [ "$2" == "create" ]
then
  echo "title: Adding Rsync Module"
  echo "method: popup"
  echo "category: CREATE"
  if cat ${FM_RSYNCD_CONF} | grep -q "^\[${1}\]" >/dev/null
  then
    echo "text: Aborting, module ${1} already exists."
    exit 1
  elif [ -z "$4" ]
  then
    echo "text: Aborting, path must be specified"
    exit 1
  else
    echo "next_action: reload_pane"
    echo "---"
    echo "" >> ${FM_RSYNCD_CONF}  # make sure we start with an empty line
    echo "" >> ${FM_RSYNCD_CONF}  # make sure we start with an empty line
    echo "[${1}]" >> ${FM_RSYNCD_CONF}
    echo "comment = ${3}" >> ${FM_RSYNCD_CONF}
    echo "path = ${4}" >> ${FM_RSYNCD_CONF}
    echo "read only = ${5}" >> ${FM_RSYNCD_CONF}
    if [ -n "$6" ]
    then
      echo "auth users = ${6}" >> ${FM_RSYNCD_CONF}
    fi
    echo "Module ${2} added."
  fi
fi
