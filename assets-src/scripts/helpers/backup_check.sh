# Script to check if PostgreSQL installed before running any postgresql task scripts

if [ -d ${FM_BACKUP} ] && [ $(cat /etc/passwd | grep backup) ]
then
  exit 0
else  # postgres appears not to be installed
  echo "title: Backup"
  echo "note: |"
  echo "  A backup user and backup home folder does not exist. Please create a user called 'backup'"
  echo "  and ensure it has the folder /home/backup created in order to use flexmin backup features."
  exit 1
fi
