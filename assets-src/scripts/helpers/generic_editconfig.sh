# Generic Edit Configuration File Script

# Must have these settings in calling script
# name=<name of application>
# app_name=<application command>
# edit_mode=<text edit mode>
# conf_path_1=<first configuration file path>
# conf_path_2=<second configuration file path>

# Check application installed, exit if not
command -v ${app_name} >/dev/null 2>&1 && installed='yes'
if [ ! "$installed" == "yes" ]
then
  echo "title: ${name} Configuration"
  echo "note: ${name} not found on this machine."
  exit 1
fi

# Find the configuration file, exit if not found
if [ -f ${conf_path_1} ]
then
  config=$conf_path_1
elif [ -f ${conf_path_2} ]
then
  config=$conf_path_2
else
  echo "title: ${name} Configuration"
  echo "note: ${name} configuration file not found on this machine."
  exit 1
fi

# Main script logic
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Edit Configuration"
  echo "edit:"
  echo "  filename: ${config}"
  echo "  mode: ${edit_mode}"
  echo "  data: |"
  cat ${config} | sed 's/^/    /'

elif [ "$2" == "save" ]
then
  echo "title: Saving ${config}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "---"
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 ${config}
  echo "Saved"
fi
 
