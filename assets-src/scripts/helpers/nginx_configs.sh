# Identify NGINX config folders

if [ -d /usr/local/etc/nginx/sites-available ]
then
  NGINX_AVAIL=/usr/local/etc/nginx/sites-available
  NGINX_ENABLED=/usr/local/etc/nginx/sites-enabled
elif [ -d /etc/nginx/sites-available ]
then
  NGINX_AVAIL=/etc/nginx/sites-available
  NGINX_ENABLED=/etc/nginx/sites-enabled
fi

if [ -d /usr/local/etc/nginx/locations-available ] && [ -d /usr/local/etc/nginx/locaitons-enabled ]
then
  NGINX_LOC_AVAIL=/usr/local/etc/nginx/locations-available
  NGINX_LOC_ENABLED=/usr/local/etc/nginx/locations-enabled
elif [ -d /etc/nginx/locations-available ] && [ -d /etc/nginx/locations-enabled ]
then
  NGINX_LOC_AVAIL=/etc/nginx/locations-available
  NGINX_LOC_ENABLED=/etc/nginx/locations-enabled
fi

