import ftplib
import ssl
import sys
import io

if __name__ == '__main__':
    
    if len(sys.argv) == 8:
        action = sys.argv[1]
        host = sys.argv[2]
        user = sys.argv[3]
        password = sys.argv[4]
        domain = sys.argv[5]
        filename = sys.argv[6]
        content = sys.argv[7]
    else:
        print("Usage:")
        print("  certbot_ftp [create|delete] <ftp_host> <username> <password> <domain> <filename> <file_content>")
        print("")
        exit()
    
    context = ssl.create_default_context()  # get system certificate authorities etc to enable validation of server cert

    #context.check_hostname=False  # hostname won't match
    #ftp = ftplib.FTP_TLS(host='ftp.creativeedgedigital.co.uk', user='creativeedge', passwd='<password>', context=context)

    # Set up security context and connect
    context.check_hostname=True  # hostname will match
    ftp = ftplib.FTP_TLS(host=host, user=user, passwd=password, context=context)

    # enter directory for the domain, exit if fail
    try:
        res = ftp.cwd(domain)  # switch to directory for the domain we are managing
        print("result: " + res)
    except:
        ftp.quit()
        exit(1)

    # enter .well-known directory, make it if we fail
    try:
        res = ftp.cwd('.well-known')
        print("result: " + res)
        cwd = '.well-known'
    except:
        if action == 'create':
            res = ftp.mkd('.well-known')
            print("result: " + res)
            res = ftp.cwd('.well-known')
            print("result: " + res)
            cwd = '.well-known'
        
    # enter the acme-challenge directory, make it if this fails
    try:
        res = ftp.cwd('acme-challenge')
        print("result: " + res)
        cwd = 'acme-challenge'
    except:
        if action == 'create':
            res = ftp.mkd('acme-challenge')
            print("result: " + res)
            res = ftp.cwd('acme-challenge')
            print("result: " + res)
            cwd = 'acme-challenge'
        
    # Create or delete challenge file
    if action == 'create':
        f = io.BytesIO(bytes(content,'utf-8'))
        res = ftp.storlines("STOR " + filename, f)
        print("result: " + res)
    elif action == 'delete' and cwd == 'acme-challenge':
        res = ftp.delete(filename)
    
    # list directory contents to confirm creation and quit
    ftp.dir()
    
    if action == 'delete':

        if cwd == 'acme-challenge':
            res = ftp.sendcmd('CDUP')
            print("result: " + res)
            try:
                res = ftp.rmd('acme-challenge')
            except:
                res = "Could not remove acme-challenge directory, it may not be empty."
            print("result: " + res)
            cwd = '.well-known'
    
        if cwd == '.well-known':
            res = ftp.sendcmd('CDUP')
            print("result: " + res)
            try:
                res = ftp.rmd('.well-known')
            except:
                res = "Could not remove .well-known directory, it may not be empty."
            print("result: " + res)

    ftp.quit()  # proper log out from ftp
