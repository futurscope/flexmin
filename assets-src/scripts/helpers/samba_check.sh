# Script to check if Samba installed before running any postgresql task scripts

if [ ! $(command -v samba) ]
then
  echo "title: Samba"
  echo "note: |"
  echo "  Samba does not appear to be installed."
  exit 1
else
  if [ -z $FM_SAMBA_CONFIG ]
  then
    [ -f /etc/samba/smbd.conf ] && FM_SAMBA_CONFIG=/etc/samba/smbd.conf
    [ -f /etc/samba/smb.conf ] && FM_SAMBA_CONFIG=/etc/samba/smb.conf
    if [ ! -z $FM_SAMBA_CONFIG ]
    then
      # make a note of the missing samba parameter
      export note="FM_SAMBA_CONFIG not defined, assumed to be ${FM_SAMBA_CONFIG}"
      echo -e "\nFM_SAMBA_CONFIG='${FM_SAMBA_CONFIG}'" >> ${FM_CONFIGS}/flexmin.local.conf
    else
      echo "title: Samba"
      echo "note: |"
      echo "  You have not specified a location for the Samba configuration file in your"
      echo "  Flexmin parameters. The configuration file could not be found."    
      exit 1
    fi
  fi
fi
