# Add or modify a cron entry using these parameters
# 'command' 'daily' 'hh:mm'

cron_set () {
  echo "params (${1}) (${2}) (${3}) (${4})"
  hh=`echo $3 | sed 's/:[0-9,]*//'`
  mm=`echo $3 | sed 's/[0-9,]*://'`
  ch="#min   hour    mday    month  wday    command"
  if [ "$2" == "daily" ]
  then
    ce="${mm}     ${hh}      *       *      *       ${1}"
  else
    ce=""
  fi
  crontab -l 2>/dev/null >/dev/null
  if [ $? -ne 0 ]  # if user (root) crontab doesn't already exist
  then
    echo "Creating new crontab file"
    # prep temp new crontab file with a header
    echo "SHELL=/bin/sh" > ${FM_TASK_TMP}crontab.tmp
    echo "PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin" >> ${FM_TASK_TMP}crontab.tmp
    echo "${ch}" >> ${FM_TASK_TMP}crontab.tmp
    # add entry for this task
    echo "${ce}" >> ${FM_TASK_TMP}crontab.tmp
    # load temp crontab to live
    cat ${FM_TASK_TMP}crontab.tmp | crontab -
    # clear out temp file
    rm ${FM_TASK_TMP}crontab.tmp
  elif crontab -l | grep -i -q "${1}"  # check if this script already scheduled
  then
    echo "Updating existing entry in crontab"
    # command already scheduled take it out first
    crontab -l | grep -v "${1}" > ${FM_TASK_TMP}crontab.tmp
    # add new entry
    echo "${ce}" >> ${FM_TASK_TMP}crontab.tmp
    # load temp crontab file to main crontab
    cat ${FM_TASK_TMP}crontab.tmp | crontab -
    # clear out temp file
    rm ${FM_TASK_TMP}crontab.tmp
  else  # crontab exists, but entry for this task not there
    echo "Adding entry to crontab"
    echo "${ce}"
    # create temp file with current crontab
    crontab -l > ${FM_TASK_TMP}crontab.tmp
    # add new entry for this task
    echo "${ce}" >> ${FM_TASK_TMP}crontab.tmp
    # load temp crontab to live
    cat ${FM_TASK_TMP}crontab.tmp | crontab -
    # remove temp crontab
    rm ${FM_TASK_TMP}crontab.tmp
  fi
}
