# Generic Config File Select and Edit Script
# ------------------------------------------
# Lists all matching files by default (looks in subfolders of specified path)
# Allows option to view, edit or delete found files (configurable)
# Includes option to create new configs from template, simply include 
# a templates_path definition and filter to select suitable templates
# templates will have {NAME} replaced with the name of the file provided

# name='uWSGI Logs'   # used in titles
# folder_path='/var/log/uwsgi'   # path of the configuration files to list
# file_pattern="*.log"      # specifies what filenames to list as configuration files in path
# path_exclude="/\w+-enabled/"  # regex for excluding file paths from present list
# file_ext=".nmconnection"           # what extension to put on newly created files
# edit_mode="ini"   # specify edit mode for ACE editor
# create="yes"       # allow creating new configuration files
# create_info=""     # info for name of file to create
# prefix_separate="yes"  # specify 'name' separately from the filename
# filename_pattern=""      # pattern for accepting filename 
# create_default=""  # default value to put in file name
# create_pattern=""  # pattern for accepting file name/path value
# create_filename="newfile"   # specify a filename (user doesn't get a choice)
# yaml="yes"         # is file a flexmin YAML that generates the real file?
# delete="yes"       # should we allow delete of existing files
# edit="yes"         # should we provide an edit option
# primary_file='/etc/dnsmasq.conf'   # allow specify a file outside folder_path to include
# list_headers="[Filename,Modified,Size]"  # headers for the list of matched files
# list_format="%p;%Tc;%s\n"   # From find command's printf format options
# templates_path=$FM_CONFIG_TMPLT
# templates_filter="*.yaml.template"

# decide whether a delete action is to be added to the actions column
delete_action=""
[ "$delete" == "yes" ] && delete_action=".!delete"
edit_action=""
[ "$edit" == "yes" ] && edit_action=".~edit"
[ -z "$list_format" ] && list_format="%P\n"
[ -z "$list_headers" ] && list_headers="[Filename]"
[ -z "$list_title" ] && list_title="Edit Configurations"

# ---- Default Action ----
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then 
  [ ! -z "$name" ] && echo "title: $name"
  echo "display:"
  echo "  - table:"
  echo "      title: ${list_title}"
  echo "      header: ${list_headers}"
  echo "      actions: ?view${edit_action}${delete_action}"
  echo "      delimiter: ;"
  echo "      data: |"
  #find $folder_path/ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/        /'
  cd "${folder_path}"
  if [ ! -z "${primary_file}" ]
  then
    find $(dirname ${primary_file}) -name "$(basename ${primary_file})" -readable -printf "$list_format" | sed -r 's/^\.\///' | sed 's/^/        /'
  fi
  if [ -z "${path_exclude}" ]
  then
    find ./ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /'
  else
    find ./ -name "${file_pattern}" -readable -printf "$list_format" | grep -vE "${path_exclude}" | sed -r 's/^\.\///' | sort | sed 's/^/        /'
  fi
  cd - >/dev/null
  
  # if create_filename specified, but filename already exists, exit without presenting form
  if [ "$create" == "yes" ] && [ ! -z $create_filename ] && [ -f ${folder_path}/${create_filename}${file_ext} ]
  then
    exit 0
  fi
  
  # if allowed to create files insert form for this
  if [ "$create" == "yes" ]
  then
    echo "  - form:"
    echo "      title: Create new configuration"
    echo "      fields:"
        
    # Handle option to specify a name
    if [ -z $create_filename ]
    then
      [ -z "$create_info" ] && create_info="Name of configuration, will also be used in place of {NAME} in template file, and used for filename if filename not specified"
      [ -z "$create_pattern" ] && create_pattern="[a-zA-Z0-9_]+"
      # let end user choose file name
      echo "        - label: Name"
      echo "          info: ${create_info}"
      echo "          pattern: '${create_pattern}'"
      echo "          default: '${create_default}'"
      echo "          type: text"
    else
      # specific filename set
      echo "        - type: hidden"
      echo "          default: '${create_filename}'"
    fi

    # Action parameter
    echo "        - type: hidden"
    echo "          default: create"
    
    # Provide option to specify filename separately from name
    if [ ! -z "$prefix_option" ] && [ "$prefix_option" == "yes" ]
    then
      echo "        - label: File prefix"
      echo "          selectformat: linesep"
      echo "          select: |2"
      echo "            "
      echo "$prefix_list" | while read -r line
      do
        echo "            $line"
      done
      echo "          info: ${prefix_info}"
      echo "          type: text"
      echo "          default: same"
      #[ ! -z "$filename_pattern" ] && echo "          pattern: '${filename_pattern}'"
    else
      # no prefix option then include blank prefix as a default
      echo "        - type: hidden"
      echo "          default: ''"
    fi    
    
    # if templates path specified, insert input element to let user select template
    if [ ! -z $templates_path ]
    then
        echo "        - label: Using Template"
        echo "          selectformat: linesep"
        echo "          select: |"
        find ${templates_path}/ -name "${templates_filter}" | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/            /'
    fi
    echo "      submit: Create Configuration File"
  fi
  
  
       
# ---- Default Action on Selected Item ----
elif [ $# -eq 1 ]
then
  echo "title: Edit ${1}"
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: ${edit_mode}"
  echo "  data: |"
  cat ${folder_path}/"${1}" | sed 's/^/    /'

# ---- View Action ----
elif [ "$2" == "view" ]
then
  if [ ! -z "${view_tail}" ]
  then
    echo "title: View ${1} (last $view_tail lines)"
  else
    echo "title: View ${1}"
  fi
  echo "method: popup"
  echo "code: |2"  # explicit indent specified to allow for extra spaces on first line
  # if tail length specified in view_tail parameter then use tail command
  if [ ! -z "${view_tail}" ]
  then
    tail -n $view_tail ${folder_path}/"${1}" | sed -e "s/\x1b//g" | sed 's/^/  /'
    #    ^ get contents                        ^ replace known control char used by py4web in some output
  else
    cat ${folder_path}/"${1}" | sed -e "s/\x1b//g" | sed 's/^/  /'
    #    ^ get contents         ^ replace known control char used by py4web in some output
  fi
  
# ---- Save Action ----
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 ${folder_path}/"${1}"
  [ "${yaml}" == "yes" ] && flexmin yamlconfig "${1}"
  if [ ! -z "${save_nextaction}" ]
  then
    echo "fragmenty_request: ${save_nextaction}"
  fi
  echo "Saved"
       
# ---- Create Action ----
elif [ "$2" == "create" ]  # 1=name 2=create 3=prefix 4=template
then
  echo "title: Creating ${1}${file_ext}"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "---"
  prefix="$3"
  name="$1"
  if [ -z $4 ]  # if template name not specified in form
  then
    echo "Creating empty file at: ${folder_path}/${prefix}${name}${file_ext}"
    touch ${folder_path}/"${prefix}${name}${file_ext}"
  else   # template name spaecified grab this template for new file
    echo "Creating new file at: ${folder_path}/${prefix}${name}${file_ext}"  
    cp ${templates_path}/${4} ${folder_path}/"${prefix}${name}${file_ext}"
    # replace NAME and FILENAME parameters in template
    sed -i "s/{NAME}/${name//\//\\\/}/" ${folder_path}/"${prefix}${name}${file_ext}"
    sed -i "s/{FILENAME}/${prefix}${name//\//\\\/}/" ${folder_path}/"${prefix}${name}${file_ext}"
    # if this is a yaml file then assumes a flexmin yaml config and tries to process it
    if grep -q ".yaml" <<< "${file_pattern}"
    then
      # if processing yaml is specified then run the yamconfig parser, provide filename and {NAME} parameter
      [ "${yaml}" == "yes" ] && flexmin yamlconfig "${prefix}${name}${file_ext}"
    fi
  fi
  echo "Created"
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "---"
  # copy saved file to destination
  rm ${folder_path}/"${1}"
  echo "Deleted"
  [ "${yaml}" == "yes" ] && echo "The native application configuration file will still exist, please find this and delete it."
  
fi
