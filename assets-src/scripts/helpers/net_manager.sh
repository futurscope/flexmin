# Check what network adaptor configuration method is being used
network_srv="dhcpcd.* systemd-networkd.*"

if command -v nmcli >/dev/null 2>&1
then
    # Network Manager being used
    name='Network Manager'
    folder_path='/etc/NetworkManager/system-connections'
    file_pattern="*.nmconnection"
    file_ext=".nmconnection"
    list_conn=$(nmcli -t -g NAME connection show)
    ip_opt="['ipv4','ipv6']"
else
    # Check with systemd to see what network management services might be running
    networkd=$(systemctl list-unit-files ${network_srv} --type=service --no-legend --no-pager --state enabled | sed -r 's/\s.*$//')
    if [ -z "$networkd" ]
    then
        echo "title: Network Management"
        echo "note: No network configuration method recognised"
        exit 1
    elif [ "$networkd" == "systemd-networkd.service" ]
    then
        # Configure for networkd configuration files
        name='Systemd Networkd'
        folder_path='/etc/systemd/network'
        file_pattern="*"
        file_ext=""
        ip_opt="['ipv4','ipv6']"
        list_conn=$(ip link | grep -oE '[0-9]+: [^:]+' | sed -r 's/[0-9]+: //')
    elif [ "$networkd" == "dhcpcd.service" ]
    then
        # Configure for networkd configuration files
        name='Dhcpcd'
        folder_path='/etc/'
        file_pattern="dhcpcd.conf"
        file_ext=".conf"
        ip_opt="['ipv4','ipv6']"
        list_conn="ip link | grep -oE '[0-9]+: [^:]+' | sed -r 's/[0-9]+: //'"
    fi
fi 
