# Script to recreate a web2py application
# parameters zipfile

ZIPFILE="${1}"
# get user and group owner of parent application folder
OWNER=`ls -ld ${FM_WEB2PY_ROOT}/applications | awk '{print $3":"$4}'`
PERMS="rwxr--r--"

#mkdir -p ${FM_WEB2PY_ROOT}/applications/  # make sure folder exists
if [ -d ${FM_WEB2PY_ROOT}/applications ]
then
  unzip -d ${FM_WEB2PY_ROOT}/applications/ $ZIPFILE
  cd ${FM_WEB2PY_ROOT}/applications
  echo "Changing ownership to ${OWNER}"
  chown ${OWNER} `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+' | cut -w -f5`
  echo "Changing permissions to ${PERMS}"
  # convert mod string to number
  modint=`echo ${PERMS} | sed -e 's/rwx/7/g' -e 's/rw-/6/g' -e 's/r-x/5/g' -e 's/r--/4/g' -e 's/-wx/3/g' -e 's/-w-/2/g' -e 's/--x/1/g' -e 's/---/0/g'` 
  # chmod files and folders
  chmod ${modint} `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+.*$' | cut -w -f5`  # | grep -E '[ ]+[0-9]+.*[^\/]$' | cut -w -f5`
  # folders need x access
  # chmod modify folders only (only paths ending with /) 
  chmod ugo+x `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+.*[\/]$' | cut -w -f5`
  # some folders need write access
  # rather complicated way to identify top level folder from zip archive
  appfolder=`unzip -l $ZIPFILE | grep -E -m1 '[ ]+[0-9]+.*[\/]$' | cut -w -f5 | sed 's/\/.*$//'`
  cd ${appfolder}
  echo "Adding write access to group for some folders:"
  echo "- cache"
  chmod -R g+w ./cache
  echo "- databases"
  chmod -R g+w ./databases
  echo "- errors"
  chmod -R g+w ./errors
  echo "- sessions"
  chmod -R g+w ./sessions
else
  echo "Destination folder does not exist, aborting."
fi
