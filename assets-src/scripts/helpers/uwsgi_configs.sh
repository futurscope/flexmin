# List uWSGI config files, if parameter specified use this to restrict search
FM_UWSGI_ROOT=/etc/uwsgi
exit_code=1
if [ -d "${FM_UWSGI_ROOT}" ]
then
  cd ${FM_UWSGI_ROOT}
  if [ -z $1 ]
  then
    find ./ -name '*.ini' | sed -r -e 's/^\.\///' # | sed -r -e 's/^([^/]*)\//\1+/'
    exit_code=$?
  else
    if [ -d $1 ]
    then
      # look for ini file with this directory
      if grep -r -E "chdir[ ]?=[ ]?$1" >/dev/null 2>&1
      then
        exit_code=0
      fi
    else
      # look for .ini file with matching name
      find ./ -name "$1.ini" | sed -r -e 's/^\.\///' | grep $1
      [ $? -eq 0 ] && exit_code=0
    fi
  fi
  cd - > /dev/null
fi
exit $exit_code
 
