# usage ./helpers/set_folder_Perms.sh owner_str perm_str folder
# where perm_str in the form rwxr-xr--
# where own_str in the form root:www
# folder must be absolute path

if [ -d ${3} ]
then
  FOLDER=$3
  # convert mod string to number
  [ "$2" != "" ] && MODINT=`echo $2 | sed -e 's/rwx/7/g' \
                                          -e 's/rw-/6/g' \
                                          -e 's/r-x/5/g' \
                                          -e 's/r--/4/g' \
                                          -e 's/-wx/3/g' \
                                          -e 's/-w-/2/g' \
                                          -e 's/--x/1/g' \
                                          -e 's/---/0/g'`
  # convert mod string to number for directories
  # (ensure x is included if r or w are included)
  [ "$2" != "" ] && DIRINT=`echo $2 | sed -e 's/rwx/7/g' \
                                          -e 's/rw-/7/g' \
                                          -e 's/r-x/5/g' \
                                          -e 's/r--/5/g' \
                                          -e 's/-wx/3/g' \
                                          -e 's/-w-/3/g' \
                                          -e 's/--x/1/g' \
                                          -e 's/---/0/g'` 
  [ "$1" != "" ] && OWN=${1}
  echo "Setting ownership to $OWN"
  chown -R $OWN $FOLDER
  echo "Setting file permissions to $MODINT"
  for p in $(find $FOLDER -type f) # files
  do
    chmod $MODINT $p
  done
  echo "Setting directory permissions to $DIRINT"
  for p in $(find $FOLDER -type d) # files
  do
    chmod $DIRINT $p
  done
else
  echo "Folder ($3) not found."
fi