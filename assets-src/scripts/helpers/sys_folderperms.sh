echo "state: ${1}"
echo "file: ${2}"
state=${1}
file=${2}

# find /usr/local/www/web2py -type d -path */* ! -path */databases/* ! -path */cache/* ! -path */errors/* ! -path */sessions/*

apply () {
    # works but is slow and disk intensive as it processes each file individually!
    echo "apply path: ${path}"
    echo "apply include: ${include}"
    echo "apply exclude: ${exclude}"
    echo "apply owner: ${owner}"
    if [ -n "$path" ] &&\
       [ -n "$include" ] &&\
       [ -n "$owner" ] &&\
       [ -n "$modint" ]
    then
        user=${owner%:*}  # user is owner with :* removed from end
        echo "user: ${user}"
        group=${owner#*:} # group is owner with *: removed from beginning
        echo "group: ${group}"
        echo "dirint: ${dirint}"
        
        # assemble find parameter string that will look only for files that 
        # match the criteria and do not have the correct ownership and permissions already
        # files (ffcmd) and directories (fdcmd) are handled differently
        fdcmd="$path -type d \( ${include} \) ${exclude} \( ! -user ${user} -o ! -group ${group} -o ! -perm ${dirint} \)"
        ffcmd="$path -type f \( ${include} \) ${exclude} \( ! -user ${user} -o ! -group ${group} -o ! -perm ${modint} \)"
        
        # find matching directories and apply owenrship and permissions to those 
        # that do not have correct settings already
        echo "find ${fdcmd}"
        dirnum=0
        for p in $(eval find $fdcmd) # "${exclude}")
        do
            dirnum=`expr $dirnum + 1`
            # path enclosed in "" to ensure handling of odd characters in path
            eval chown ${owner} "\"${p}\""
            eval chmod ${dirint} "\"${p}\""
        done
        echo "Modified $dirnum directories"

        # find matching files and apply owenrship and permissions to those 
        # that do not have correct settings already
        echo "find ${ffcmd}"
        filenum=0
        for p in $(eval find $ffcmd) # "${exclude}")
        do
            filenum=`expr $filenum + 1`
            # path enclosed in "" to ensure handling of odd characters in path
            eval chown ${owner} "\"${p}\""
            eval chmod ${modint} "\"${p}\""
        done
        echo "Modified $filenum files"
    fi
}

IFS=$'\n'
for line in `cat ${FM_CONFIGS}/${2}`
do
  #echo "Process: ${line}"
  if [ "${line#[}" != "$line" ] # section header
  then
    apply
    path=""
    include=""   #'-path "*/*"'
    exclude=""
    modint=""
    owner=""
    section=`echo "$line" | sed -r 's/^\[ *([^]]+) *\].*/\1/'`
    echo "---------------------------------"
    echo "section: ${section}"
    echo "---------------------------------"
  elif [ "${line#path =}" != "$line" ]
  then
    #echo "Picked up path"
    path="${line#path = }"
    eval path=${path}
    echo "path: ${path}"
  elif [ "${line#default = }" != "$line" ] || [ "${line#${state} = }" != "$line" ]
  then
    if [ "${line#default = }" != "$line" ]
    then
      echo "Picked up default settings"
      entry=${line#default = }
    else
      echo "Picked up ${state} settings"
      entry=${line#${state} = }
    fi
    echo "Evaluating ownership and permissions"
    owner=${entry% *}
    modint=`echo ${entry#* } | sed -e 's/rwx/7/g' \
                                   -e 's/rw-/6/g' \
                                   -e 's/r-x/5/g' \
                                   -e 's/r--/4/g' \
                                   -e 's/-wx/3/g' \
                                   -e 's/-w-/2/g' \
                                   -e 's/--x/1/g' \
                                   -e 's/---/0/g'`
   # work out directory mod paramter (implied x where r or w)
    dirint=`echo ${entry#* } | sed -e 's/rwx/7/g' \
                                   -e 's/rw-/7/g' \
                                   -e 's/r-x/5/g' \
                                   -e 's/r--/5/g' \
                                   -e 's/-wx/3/g' \
                                   -e 's/-w-/3/g' \
                                   -e 's/--x/1/g' \
                                   -e 's/---/0/g'`
    echo "Owner: ${owner}"
    echo "Permissions: ${modint}"
    echo "Folder Permission: ${dirint}"
  elif [ "${line#exclude = }" != "$line" ] 
  then
    addexc=`echo "${line#exclude = }" | sed 's/\*/\\\\*/g'` # escape * to prevent expansion
    echo "adding exclusion: ${addexc}"
    exclude="${exclude} ! -path ${addexc}"
  elif [ "${line#include = }" != "$line" ] 
  then
    addinc=`echo "${line#include = }" | sed 's/\*/\\\\*/g'` # escape * to prevent expansion
    echo "adding inclusion: ${addinc}"
    if [ -n "$include" ]
    then
      # subsequent includes need or to match any include condition
      include="${include} -o -path ${addinc}"
    else
      include="-path ${addinc}"
    fi
  fi
done
# end of file apply settings from final section
apply
