# ---------------------------------------
# name: Generic Yaml Configuration Script
# ---------------------------------------
# Extended functionality for ini file output:
# - Shows groups defined in the ini file (optional, usefult for Samba)
# - Shows existing ini file without all comments (optional)
#
# Needs the following parameters specified before calling
# name=<name>
# app_name='<app_executable>'     # used to check app is installed
# config='<path_to_config_file>'  # actual app config path
# config_yaml='<name_of_flexmin_yaml_config_file>'  # flexmin yaml config path
# [optional] list_groups='yes'           # if we want to list the groups add this and specify all groups_ options
# [optional] groups_title='Samba Shares' # title of section shwoing ini groupss
# [optional] groups_column='Share'       # title of column showing groups names
# [optional] groups_excl='global|homes'   # exclude these group names list of names separated by |
#
# If the config_yaml file doesn't exist, this script will attempt to
# obtain an identical file from the config templates path (with .templates added)
# to use as a basis for editing a new file.


# Check application installed, exit if not
command -v ${app_name} >/dev/null 2>&1 && installed='yes'
if [ ! "$installed" == "yes" ]
then
  echo "title: ${name} Configuration"
  echo "note: ${name} not found on this machine."
  exit 1
fi

# Main Script
# --------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: ${name} Configuration"
  echo "display:"
  [ ! -z "$note" ] && echo "  - note: '${note}'"
  echo "  - note: |"
  echo "      WARNING: If there is an existing configuration, be sure that you"
  echo "      do not create a new yaml configuration that overwrites any important"
  echo "      part of the existing configuration."
  echo "  - table:"
  echo "      title: Edit YAML Source File"
  echo "      actions: '~edit'"
  echo "      object_col: 0"
  echo "      header: [Config]"
  echo "      data: |"
  echo "        ${config_yaml}"
  
  if [ ! -z $list_groups ]
  then
    echo "  - table:"
    echo "      title: $groups_title"
    echo "      header: [$groups_column]"
    echo "      data: |"
    cat ${config} | grep -E '^\s*\[.*\]\s*' | sed -r 's/^\s*\[\s*([a-zA-Z0-9_ ]+?)\s*\]\s*$/\1/' | grep -v -E "$groups_excl" | while read -r nc
    do
      echo "        ${nc}"
    done
  fi

    
  if [ -f ${config} ]
  then
    echo "  - title: Existing $name Config (${config})"
    echo "  - note: Excluding lines commented out to keep this short."
    echo "  - text: |"
    # list config, excluding blank and commented lines for brevity
    cat ${config} | grep -v '^#' | grep -v '^;' | grep -v '^\s*$' | sed -r 's/^\[/\n\[/' | sed 's/^/      /'
  else
    echo "  - note: No existing ${name} configuration file found"
  fi
  
# Edit yaml config file if one is specified, try and get a file from templates
# this yaml config file does not already exist in flexmin config folder.
elif [ $# -eq 1 ]
then
  echo "title: Edit ${name} Configuration ${1}"
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: yaml"
  echo "  data: |"
  if [ "$1" == "$config_yaml" ] && [ ! -f ${FM_CONFIGS}/${1} ]
  then
    cp ${FM_CONFIG_TMPLT}/${1}.template ${FM_CONFIGS}/${1}
  fi
  [ -f ${FM_CONFIGS}/${1} ] && cat ${FM_CONFIGS}/${1} | sed 's/^/    /'

# Save edited file and generate destination config file
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 ${FM_CONFIGS}/${1}
  flexmin yamlconfig ${1}
  echo "Saved"
fi
