# Script to recreate a PostgreSQL database (emptying database contents)
# only parameter is the name of the database
# currently only preservves the database name and owner on recreation

if command -v ${FM_PSQL} >/dev/null 2>&1
then
  # postgres installed
  echo "Checking owner"
  q="SELECT u.usename FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid) WHERE d.datname = '${1}';"
  #echo "query: ${q}"
  owner=`${FM_PSQL} -U ${FM_PSQL_SU} -t -c "${q}" | sed -e 's/^[ ]*//' -e 's/[ ]*$//'`
  echo "Owner is ${owner}"
  echo "Forcing deletion of existing database"
  q1="UPDATE pg_database SET datallowconn = 'false' WHERE datname = '${1}';"
  q2="SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${1}';"
  q3="DROP DATABASE ${1};"
  ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q1}" | sed s'/^/  /'
  ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q2}" | sed s'/^/  /'
  ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q3}" | sed s'/^/  /'
  echo "Creating new database ${1} with owner ${owner}"
  ${FM_PSQL} -U ${FM_PSQL_SU} -c "CREATE DATABASE ${1} WITH OWNER = ${owner};" 2>&1
else
  echo "PostgreSQL not installed."
  exit 1
fi
