# General certificate script checker
# Checks we know where the root of SSL certificates folder is
# Doesn't cover lletscencrypt certificate folder as that has it's 
# own specific structure that we shouldn't mess with.

if command -v openssl >/dev/null 2>&1
then
  :
else
  echo "title: Certificates"
  echo "note: openssl not found on this system, it is needed for certificate management."
  exit 1
fi

if [ -z "$FM_SSL_ROOT" ] || [ ! -d "$FM_SSL_ROOT" ]
then
  [ -d /etc/ssl ] && FM_SSL_ROOT='/etc/ssl'
  [ -d /usr/local/etc/ssl ] && FM_SSL_ROOT='/usr/local/etc/ssl'
fi
 
