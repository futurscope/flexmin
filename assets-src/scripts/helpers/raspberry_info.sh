#!/bin/bash
# Script: my-pi-temp.sh
# Purpose: Display the ARM CPU and GPU  temperature of Raspberry Pi 2/3 
# Author: Vivek Gite <www.cyberciti.biz> under GPL v2.x+
# -------------------------------------------------------
cpu=$(</sys/class/thermal/thermal_zone0/temp)
echo "-------------------------------------------"
echo -e "GPU Temperature:\n$(/opt/vc/bin/vcgencmd measure_temp)"
echo ""
echo -e "CPU Temperature:\n$((cpu/1000))'C"
echo ""
echo "GPU Version:"
/usr/bin/vcgencmd version

