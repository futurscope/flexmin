if command -v certbot >/dev/null 2>&1
then
  certbot --version >/dev/hull 2>&1
  if [ $? -gt 0 ]
  then
    echo "title: Certbot"
    echo "note: Certbot is not correctly installed on this system, please check installation."
    exit 1
  fi
else
  echo "title: Certbot"
  echo "note: Certbot is not installed on this system, install using your system package installer or using 'sudo pip install certbot'"
  exit 1
fi

