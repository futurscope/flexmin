# Script to check if PostgreSQL installed before running any postgresql task scripts

if command -v hg >/dev/null 2>&1
then
  # mercurial installed
  exit 0
else  # postgres appears not to be installed
  echo "title: Mercurial"
  echo "note: |"
  echo "  Mercurial does not appear to be installed on this system. "
  echo "  Please install, before you can use these features."
  exit 1
fi
