# Script to check if PostgreSQL installed before running any postgresql task scripts

if command -v ${FM_PSQL} >/dev/null 2>&1
then
  # postgres installed
  check=`sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "select version();" 2>/dev/null`
  if [ $? -eq 0 ]
  then
    # postgres initialised
    postgres_conf=`sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "select distinct sourcefile from pg_settings"`
    exit 0
  else
    # initialise postgres
    echo "title: PostgreSQL"
    echo "text: |"
    echo "  PostgreSQL does not appear to be running. Check the following:"
    echo "  - PostgreSQL is installed"
    echo "  - PostgreSQL is initialised (initdb command)"
    echo "  - PostgreSQL service is enabled and running"
    echo "  - The PostgreSQL su account is correctly defined in the FM_PSQL_SU parameter"
    echo "  - PostgreSQL is configured to allow the local postgres user to connect using the 'peer' method"
    exit 1
  fi
else  # postgres appears not to be installed
  echo "title: PostgreSQL"
  echo "note: |"
  echo "  PostgreSQL does not appear to be installed. Please install, and then "
  echo "  enable and initialise in the system services before use."
  exit 1
fi
