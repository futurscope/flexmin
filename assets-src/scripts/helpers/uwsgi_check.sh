# Identify NGINX config folders

if [ -d "${FM_UWSGI_ROOT}" ]
then

    if [ -d ${FM_UWSGI_ROOT}/apps-available ] && [ -d ${FM_UWSGI_ROOT}/apps-enabled ]
    then
      UWSGI_AVAIL=${FM_UWSGI_ROOT}/apps-available
      UWSGI_ENABLED=${FM_UWSGI_ROOT}/apps-enabled
    elif [ -d ${FM_UWSGI_ROOT}/vassals ]
    then
      UWSGI_ENABLED="${FM_UWSGI_ROOT}/vassals"
      UWSGI_AVAILABLE="${FM_UWSGI_ROOT}/vassals"
    fi

else
    echo "title: uWSGI Configuration"
    echo "note: uWSGI configuration folder ${FM_UWSGI_ROOT} not found."
    exit 1

fi
