# Script to check if PostgreSQL installed before running any postgresql task scripts

if command -v django-admin >/dev/null 2>&1
then
  # django installed
  export django_version=`python -c "import django; print django.get_version()" 2>/dev/null`
else  # postgres appears not to be installed
  echo "title: Django"
  echo "note: |"
  echo "  Django does not appear to be installed."
  exit 1
fi
