# -------------------------------
# name: List Configurations
# tags: NGINX
# info: Lists available-site config files for selection and viewing.
# ------------------------------

# Main Script
# --------------

# set up 
source ./helpers/nginx_configs.sh

if [ $? -gt 0 ] 
then
  echo "title: NGINX - Directories not found"
  echo "text: >"
  echo "  Please create sites-available and sites-enabled folders"
  echo "  in /user/local/etc/nginx or /etc/nginx"
fi
    
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  # ask user to select a file
  echo "title: NGINX Overview"
  echo "display:"
  #echo "  - text: |"
  #echo "      Configurations stored at:"
  #echo "        ${NGINX_AVAIL}"
  #echo "        ${NGINX_ENABLED}"
  #[ ! -z ${NGINX_LOC_AVAIL} ] && echo "        ${NGINX_LOC_AVAIL}"
  #[ ! -z ${NGINX_LOC_ENABLED} ] && echo "        ${NGINX_LOC_ENABLED}"
  echo "  - note: |"
  echo "      Where configuration files are included in a specific site, please prefix the location"
  echo "      files with <sitename>_ where <sitename> also matches the NGINX config filename."
  echo "      This makes the link between locations and sites clear."  
  echo "  - table:"
  echo "      title: Available Configurations"
  echo "      header: [Type, Configuration, Includes, Status*]"
  echo "      delimiter_row: '\n'"
  echo "      delimiter: ';'"
  echo "      actions: per_row"
  echo "      object_col: 1"
  echo "      second_col: 0"   # param_3 will be from this column
  echo "      data: |"
  
  for nc in `find ${NGINX_AVAIL} -mindepth 1 -maxdepth 1 -name "*"`
  do
    ng_inc=$(grep -vE '^\s*#' ${nc} | grep -o -E 'include[ ]*[^\;]*' | grep -v "_params" | sed 's/include\s*//')
    if [ -f ${NGINX_ENABLED}${nc/$NGINX_AVAIL/} ]
    then
      echo "        Site;${nc/$NGINX_AVAIL\//};$ng_inc;enabled*green;disable.!delete.?view"
    else
      echo "        Site;${nc/$NGINX_AVAIL\//};$ng_inc;disabled*red;enable.!delete.?view"
    fi
  done
  
  if [ ! -z ${NGINX_LOC_AVAIL} ] && [ ! -z ${NGINX_LOC_ENABLED} ]
  then
    for nc in `find ${NGINX_LOC_AVAIL}/ -mindepth 1 -maxdepth 1 -name "*"`
    do
        ng_inc=$(grep -o -E 'include[ ]*[^\;]*' ${nc} | grep -v "_params" | sed 's/include\s*//')
        if [ -f ${NGINX_LOC_ENABLED}/${nc/$NGINX_LOC_AVAIL/} ]
        then
          echo "        Loc;${nc/$NGINX_LOC_AVAIL\//};$ng_inc;enabled*green;disable.!delete.?view"
        else
          echo "        Loc;${nc/$NGINX_LOC_AVAIL\//};$ng_inc;disabled*red;enable.!delete.?view"
        fi
    done
  fi

  echo "  - table:"
  echo "      title: Server and Domain Names"
  echo "      header: [Configuration, Names, Ports, Status*]"
  echo "      last_right: yes"
  echo "      delimiter_row: '\n'"
  echo "      cell_newline: ','"
  echo "      delimiter: ';'"
  echo "      object_col: 1"
  echo "      second_col: 0"   # param_3 will be from this column
  echo "      data: |"
  
  for nc in `find ${NGINX_AVAIL} -mindepth 1 -maxdepth 1 -name "*"`
  do
    ng_name=$(grep -vE '^\s*#' ${nc} | grep -o -E 'server_name[ ]*[^\;]*' | sed 's/server_name\s*//' | sed 's/ /,/g')
    ng_ports=$(grep -vE '^\s*#' ${nc}| grep -aoE 'listen [^;]*' | paste -sd ',' - | sed -r "s/listen\s*([^,]+)/\1/g")
    if [ -f ${NGINX_ENABLED}${nc/$NGINX_AVAIL/} ]
    then
      echo "        ${nc/$NGINX_AVAIL\//};$ng_name;$ng_ports;enabled*green"
    else
      echo "        ${nc/$NGINX_AVAIL\//};$ng_name;$ng_ports;disabled*red"
    fi
  done

  echo "  - table:"
  echo "      title: Shared Roots"
  echo "      header:" # [Config File, Socket]"
  echo "        - Configuration"
  echo "        - Root Path"
  echo "      delimiter: ';'"
  echo "      data: |"
  grep -rw -E "^[^#a-zA-Z0-9]*root" ${FM_NGINX_CONFIG}/ \
       | sed "s|$FM_NGINX_CONFIG/||" \
       | sed -r 's/\:[ ]*root[ ]*([^;]*);/;\1/' | sed 's/^/        /' \
       | tac  # reverse order so sites-available appears before locations-available
  
  echo "  - table:"
  echo "      title: uWSGI and Proxy Pass Settings"
  echo "      header:" # [Config File, Socket]"
  echo "        - Configuration"
  echo "        - Socket"
  echo "      delimiter: ';'"
  echo "      data: |"
  grep -rw -E "^[^#]*(uwsgi|proxy)_pass" ${FM_NGINX_CONFIG}/ \
       | sed "s|$FM_NGINX_CONFIG/||" \
       | sed -r 's/\:[ ]*((uwsgi|proxy)_pass[^;]*)/;\1/' | sed 's/^/        /' \
       | tac  # reverse order so sites-available appears before locations-available
  
elif [ "$2" == "enable" ]
then
  # 1=name 2=enable 3=type
  echo "title: Enabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  [ "$3" = "Site" ] && ln -s ${NGINX_AVAIL}/${1} ${NGINX_ENABLED}/${1}
  [ "$3" = "Loc" ] && ln -s ${NGINX_LOC_AVAIL}/${1} ${NGINX_LOC_ENABLED}/${1}
  echo "Enabled. NGINX reload triggered"
  systemctl reload nginx.service
  
elif [ "$2" == "disable" ]
then
  # 1=name 2=disable 3=type
  echo "title: Disabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  [ "$3" = "Site" ] && rm ${NGINX_ENABLED}/${1}
  [ "$3" = "Loc" ] && rm ${NGINX_LOC_ENABLED}/${1}
  echo "Disabled. NGINX reload triggered"
  systemctl reload nginx.service
  
elif [ "$2" == "delete" ]
then
  # 1=name,2=delete 3=type
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  # remove link file and original
  echo "---"
  echo "Disabling and deleting"
  if [ "$3" == "Site" ]
  then
    [ -f ${NGINX_ENABLED}/${1} ] && rm ${NGINX_ENABLED}/${1}
    rm ${NGINX_AVAIL}/${1}
  fi
  if [ "$3" == "Loc" ]
  then
    [ -f ${NGINX_LOC_ENABLED}/${1} ] && rm ${NGINX_LOC_ENABLED}/${1}
    rm ${NGINX_LOC_AVAIL}/${1}
  fi
  echo "Done."
  echo "Attempting NGINX reload"
  systemctl reload nginx.service
  
elif [ "$2" == "view" ]
then
  # 1=view 2=type 3=name
  echo "title: View Configuration - ${1}"
  echo "method: popup"
  echo "---"
  if [ "$3" == "Site" ]
  then
    if [ -r ${NGINX_AVAIL}/$1 ]
    then
        cat ${NGINX_AVAIL}/$1
    else
        THIS_USER=$(whoami)
        echo "ERROR: ${THIS_USER} does not have permission to open the file."
    fi
   fi
  if [ "$3" == "Loc" ]
  then
    if [ -r ${NGINX_LOC_AVAIL}/$1 ]
    then
        cat ${NGINX_LOC_AVAIL}/$1
    else
        THIS_USER=$(whoami)
        echo "ERROR: ${THIS_USER} does not have permission to open the file."
    fi
   fi
   
fi
