# -------------------------------
# name: Edit Configurations
# tags: uWSGI
# info: Lists available uWSGI config files for editing
# ------------------------------

# Check whether samba installed and initialised
source ./helpers/samba_check.sh || exit

name=Samba
app_name='samba'
config=$FM_SAMBA_CONFIG  # actual app config path
config_yaml='samba.yaml'  # flexmin yaml config path
list_groups='yes'
groups_title='Samba Shares'
groups_column='Share'
groups_excl='global'   # exclude these group names

# include generic handler here
source ./helpers/generic_yaml_editconfig.sh
