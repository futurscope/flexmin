# --------------------
# name: Flexmin Login
# info: Dummy Login Task
# -----------------------

# No tag: entry in the section above, so not listed in task menu.
# -*- coding: utf-8 -*-

def run(vars,fm_vars,lines=None):
    if lines == None:
        retval = True  # return text
        lines = []
    else:
        retval = False # output list supplied, just append to that

    """
    If login is successful, this task will return confirmation to popup
    along with what to do next (as provided by initial login form (input name 1).
    """
    lines.append("title: Flexmin Login")
    lines.append("method: popup")
    lines.append("next_action: " + str(vars[0]))
    lines.append("text: |")
    lines.append("  Login Successful.")
    lines.append("  Press Esc to close this window.")
    lines.append("  Other recent successful logins: {}".format(str(fm_vars['FM_LOGIN_SUCCESS'])))

    # Return output to action function
    if retval:
        return '\n'.join(lines)
    else:
        return None
