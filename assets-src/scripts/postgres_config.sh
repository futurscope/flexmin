# -------------------
# name: Users
# tags: PostgreSQL
# info: Lists PostgreSQL Users and allows deletion
# ---------------------

# Check whether postgress installed and initialised

if command -v ${FM_PSQL} >/dev/null 2>&1
then
  pg_v=$(${FM_PSQL} -V | sed -r 's/[^0-9]*([0-9]+)\.[0-9]*.*/\1/')
  [ -f /etc/postgresql/$pg_v/main/pg_hba.conf ] && folder_path="/etc/postgresql/$pg_v/main"
  [ -f /var/lib/postgres/data/pg_hba.conf ] && folder_path='/var/lib/postgres/data'
else
    echo "title: PostgreSQL Configuration Not Found"
    echo "note: |"
    echo "  The pg_hba.conf file could not be found. Check PostgreSQL is installed,"
    echo "  and check where the configuration files are stored."
fi

name='PostgreSQL Configurations'
#folder_path=$FM_CONFIGS
#templates_path=''
file_pattern="*.conf"
edit_mode="ini"
edit="yes"

source ./helpers/generic_viewedit_list.sh
exit
