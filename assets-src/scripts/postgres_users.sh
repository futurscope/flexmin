# -------------------
# name: Users
# tags: PostgreSQL
# info: Lists PostgreSQL Users and allows deletion
# ---------------------

# Check whether postgress installed and initialised
./helpers/postgres_check.sh || exit

echo "title: Users"
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "table:"
  echo "  actions: '!delete.?reset_pw'"
  echo "  header: [DB]"
  echo "  delimiter: '\s'"
  echo "  data: |"
  # list users, except main postgresql super user (don't want to delete that!)
  q="SELECT u.usename FROM pg_user u WHERE u.usename != '${FM_PSQL_SU}';"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "${q}" | sed s'/^/   /'

  echo "form:"
  echo "  title: Create New User"
  echo "  fields:"
  echo "    - label: Username"
  echo "      type: text"
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Password"
  echo "      type: password"
  echo "  submit: Create New PostgreSQL User"

else
  if [ "$2" == "reset_pw" ]
  then
    echo "title: 'Reset ${1} password'"
    echo "method: popup"
    echo "form:"
    echo "  fields:"
    echo "    - type: hidden"
    echo "      default: ${1}"
    echo "    - type: hidden"
    echo "      default: chgpass"  
    echo "    - label: Enter Password"
    echo "      type: password"
    echo "    - label: Retype Password"
    echo "      type: password"
    echo "  submit: Reset Password"

  elif [ "$2" == "chgpass" ]
  then
    echo "title: 'Changing ${1} password'"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: CHANGE"
    echo "---"
    if [ "$3" == "$4" ]
    then
      q="ALTER USER ${1} WITH PASSWORD '$3';"
      sudo -u ${FM_PSQL_SU} ${FM_PSQL} -c "${q}"
      echo "Password changed."
    else
      echo "ERROR: Passwords do not match, reset failed."
    fi

  elif [ "$2" == "delete" ]
  then
    echo "title: 'Deleting User : ${1}'"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: DELETE"
    echo "---"
    q="DROP USER ${1};"
    sudo -u ${FM_PSQL_SU} ${FM_PSQL} -c "${q}"
    
  elif [ "$2" == "create" ]
  then
    echo "title: 'Creating User : ${1}'"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: CREATE"
    echo "---"
    sudo -u ${FM_PSQL_SU} ${FM_PSQL} -c "CREATE USER ${1} WITH PASSWORD '${3}';" 2>&1
  fi
fi
  
