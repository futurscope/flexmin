# --------------------
# name: Edit Menu
# tags: Flexmin
# info: Edit the local menu file (yaml)
# --------------------------

# -*- coding: utf-8 -*-

import os, shutil
import flexmin.utils.yamlconfigs as yamlconfigs
import glob

def run(vars,fm_vars,lines=None):
    if lines == None:
        retval = True  # return text
        lines = ["title: Edit Menu"]
    else:
        lines.append("title: Edit Menu")
        retval = False # output list supplied, just append to that
        
    f = os.path.join(fm_vars['FM_CONFIGS'], 'menu.yaml')
    
    if len(vars) == 1 and vars[0] == '~default~': # No parameters supplied present form

        # return file details and content for editing
        # should be using lines.append() below
        lines.append("edit:")
        lines.append("  filename: menu.yaml")
        lines.append("  mode: yaml")
        lines.append("  data: |")
        #lines.append("  " + f)
        if not os.path.isfile(f):
            f = os.path.join(fm_vars['FM_DEFAULT_CONFIGS'], 'menu.yaml')
        with open(f,'r') as original:
            for line in original:
                lines.append("    " + line.strip('\n'))

    elif (len(vars) > 2) and (vars[1] == 'save'):  # save changes

        lines.append("method: popup")
        lines.append('category: CHANGE')
        lines.append('next_action: reload_pane')
        lines.append('...')
        # Save file content supplied in parameter 3 (file)

        shutil.copyfile(fm_vars['FM_SCRIPT_PARAM']+'_3',f)
        lines.append('fragmenty_request: menu.html')
        lines.append('Saved')
        x = yamlconfigs.Config(f, fm_vars)
        x.native()

    # Return output to action fucntion
    if retval:
        return '\n'.join(lines)
    else:
        return None
