# -----------------------------
# name: User Keys
# tags: SSH
# info: List User Keys for SSH
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: User SSH Keys"
  echo "note: |"
  echo "  User SSH Keys are used for user accounts on this machine to connect"
  echo "  to other machines using SSH securely, without a password if desired."
  echo "  Here you can generate a public and private key. You should then click"
  echo "  in info to see the key details including the public key entry which"
  echo "  should be pasted into the users authorized_keys file (SSH: User "
  echo "  Configurations) on the remote machine ."
  echo "table:"
  echo "  actions: [info,'!delete']"
  echo "  header: [Path, User]"
  echo "  delimiter: ','"
  echo "  data: |"
  find /home/ /root/ -name "id_rsa" -print 2> /dev/null \
       | sed 's/\([^\/]*\)\/id_rsa/\1\/id_rsa,\1/' \
       | sed 's/^/    /'
  echo "form:"
  echo "  title: Create User Keys"
  echo "  fields:"
  echo "    - label: User"
  echo "      delimiter: '\n'"
  echo "      select: |"
  cd /home/
  ls | sed 's/^/        /'
  echo "root" | sed 's/^/        /'
  cd - >/dev/null
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Key Passphrase"
  echo "      info: Leave blank if no passphrase needed."
  echo "      type: password"
  echo "  submit: Create User SSH Keys"

elif [ "$2" == "delete" ]
then
  echo "title: Deleting User SSH Keys"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  echo "---"
  # try the delete and handle failure with neat yaml compatible message
  { rm -d ${1} || echo "ERROR: Private Key Delete failed."; } 2>/dev/null
  { rm -d ${1}.pub || echo "ERROR: Public Key Delete failed."; } 2>/dev/null
  echo "Done."
  
elif [ "$2" == "info" ]
then
  echo "title: SSH Key Info"
  echo "method: popup"
  echo "---"
  echo "Public Key"
  echo "-----------"
  cat ${1}.pub
  echo ""
  echo ""
  echo "Signatures (Private then Public)"
  echo "--------------------------------"
  ssh-keygen -l -f ${1}
  ssh-keygen -l -f ${1}.pub
  
elif [ "$2" == "create" ]
then
  echo "title: Creating User SSH Private and Public Keys"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  if [ "${1}" == "root" ]
  then
    userhome="/root"
    usergroup="wheel"
  else
    userhome="/home/${1}"
    usergroup="${1}"
  fi
  mkdir -p ${userhome}/.ssh
  echo "Setting required ownership and permissions on /home/${1}"
  chown ${1}:${usergroup} ${userhome}
  chmod 700 ${userhome}
  echo "Setting required ownership and permissions on /home/${1}/.ssh"
  chown ${1}:${usergroup} ${userhome}/.ssh
  chmod 700 ${userhome}/.ssh
  echo "Creating SSH Keys"
  cd ${userhome}/.ssh/
  ssh-keygen -t rsa -f ${userhome}/.ssh/id_rsa -N "${3}"
  chown ${1}:${usergroup} ${userhome}/.ssh/id_rsa*
  chmod 600 ${userhome}/.ssh/id_rsa*
fi
