# ----------------------
# name: Users
# tags: System
# info: Shows all user details in a table.
# -------------------------

# Main Script
# ------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: User Management"
  echo "display:"
  echo "  - table:"
  echo "      title: Existing User Accounts"
  echo "      delimiter: ':'"
  echo "      header: [username,-pw,uid,gid,desc,home,shell]"
  echo "      actions: '?reset_pw.!delete'"
  echo "      data: |"
  cat /etc/passwd | grep -E '^.*:.*:[0-9]{4}.*' | grep -v -E '^.*:.*:65534.*' | sed 's/^/        /'
  echo "  - form:"
  echo "      title: Create New User"
  echo "      fields:"
  echo "        - label: Username"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Password"
  echo "          type: password"
  echo "        - label: Re-type password"
  echo "          type: password"
  echo "        - label: Primary Group"
  echo "          type: text"
  echo "          default: 'same as username'"
  echo "        - label: Additional Groups"
  echo "          info: Comma or space separated list of groups. 'wheel' allows switch to root."
  echo "          type: text"
  echo "      submit: Create User"
  
elif [ "$2" == "create" ]
then
  echo "title: Creating user ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  if [ "$5" == "same as username" ] 
  then
    echo "Creating user ${1}"
    useradd -m ${1}
  else
    grp=$5
    echo "Creating group ${grp}"
    groupadd ${grp}
    echo "Creating user ${2}"
    useradd -m ${1} -g ${grp}
  fi
  if [ "$3" != "" ]
  then
    if [ "$3" == "$4" ]
    then
      echo "Setting password"
      passwd ${1} << EOD
${3}
${3}
EOD
    else
      echo "Passwords do not match, please set password on new user."
    fi
  else
    echo "No password specified!"
  fi
  
elif [ "$2" == "reset_pw" ]
then
  echo "title: Reset password for ${1}"
  echo "method: popup"
  echo "form:"
  echo "  submit: Change Password"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: ${1}"  # username
  echo "    - type: hidden"
  echo "      default: change_pw"
  echo "    - label: Password"
  echo "      type: password"
  echo "    - label: Retype Password"
  echo "      type: password"
  
elif [ "$2" == "change_pw" ]
then
  echo "title: Changing password for ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  if [ "$3" == "$4" ]
  then
    passwd ${1} << EOD
${3}
${3}
EOD
    echo "Password changed"
  else
    echo "Error - passwords did not match"
  fi
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting user ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  #echo "status: DELETING"
  echo "..."
  echo "Deleting user ${1} and the home directory"
  userdel -r ${1}
  echo "Deleted. Check groups in case user group needs deleting."
fi
