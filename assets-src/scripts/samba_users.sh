# -------------------
# name: Users
# tags: PostgreSQL
# info: Lists PostgreSQL Users and allows deletion
# ---------------------

# Check whether samba installed and initialised
./helpers/samba_check.sh || exit

echo "title: Samba Users"
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "table:"
  echo "  actions: per_row"
  echo "  header: [User, Samba Status]"
  echo "  delimiter: ':'"
  echo "  data: |"
  # list users, except main postgresql super user (don't want to delete that!)
  #pdbedit -L | sed s'/^/   /'
  for nc in `cat /etc/passwd | grep -E '^.*:.*:[0-9]{4}.*' | grep -v -E '^.*:.*:65534.*' | sed 's/\:.*$//'`
  do
    pdbedit -u ${nc} &> /dev/null
    if [ $? -eq 0 ]
    then
      echo "    ${nc}:enabled*green:?view.?reset_pw.?delete"
    else
      echo "    ${nc}:disabled*red:"
    fi
  done

  echo "note: |"
  echo "  Samba users are defined separately from system user accounts, but "
  echo "  depend on the system accounts to define access permissions of the"
  echo "  Samba user on this system. You need to create Samba users to enable"
  echo "  a system account user to access files via Samba, the Samba account "
  echo "  name must match the system account name, and the Samba account "
  echo "  password is set separately from the system account password."
  echo "form:"
  echo "  title: Create New User"
  echo "  fields:"
  echo "    - label: Username"
  echo "      type: text"
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Password"
  echo "      type: password"
  echo "    - label: Re-type Password"
  echo "      type: password"
  echo "  submit: Create New Samba User"

else
  if [ "$2" == "view" ]
  then
    echo "title: 'User Details: ${1}'"
    echo "method: popup"
    echo "---"
    pdbedit -L -v -u ${1}
    
  elif [ "$2" == "reset_pw" ]
  then
    echo "title: Reset password for ${1}"
    echo "method: popup"
    echo "form:"
    echo "  fields:"
    echo "    - type: hidden"
    echo "      default: create"
    echo "    - type: hidden"
    echo "      default: reset_pw_confirm"
    echo "    - label: Password"
    echo "      type: password"
    echo "    - label: Re-type Password"
    echo "      type: password"
    echo "  submit: Reset Password"
    
  elif [ "$2" == "reset_pw_confirm" ]
  then
    echo "title: Reset Password for ${1}"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: CHANGE"
    echo "---"
    (echo "$3"; echo "$4") | smbpasswd -s ${1}
    [ $? -eq 0 ] && echo "Password reset successfully." || echo "Password rset failed."

  elif [ "$2" == "delete" ]
  then
    echo "title: 'Deleting user : ${1}'"
    echo "method: popup"
    echo "note: This will delete the Samba account only, delete the system account separately if necessary."
    echo "form:"
    echo "  fields:"
    echo "    - type: hidden"
    echo "      default: ${1}"
    echo "    - type: hidden"
    echo "      default: delete_confirm"
    echo "  submit: Delete Samba Account"

  elif [ "$2" == "delete_confirm" ]
  then
    echo "title: 'Deleting user : ${1}'"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "---"
    smbpasswd -x ${1}
    echo "Samba account deleted."

  elif [ "$2" == "create" ]
  then
    if grep -E "^${1}:" /etc/passwd >/dev/null 2>&1
    then
      if [ "$3" == "$4" ]
      then
        echo "title: 'Creating User : ${1}'"
        echo "method: popup"
        echo "next_action: reload_pane"
        echo "category: CREATE"
        echo "..."
        (echo "$3"; echo "$4") | smbpasswd -s -a ${1}
        echo "Created samba user ${1}"
      else
        echo "error: passwords do not match."
        echo "next_action: reload_pane"
      fi
    else
      echo "error: |"
      echo "  User not found on this system. Please ensure you are creating a"
      echo "  Samba account matching an existing system user account name."
      echo "next_action: reload_pane"
    fi
  fi
fi
  
