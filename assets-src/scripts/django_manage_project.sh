# ----------------------
# name: Projects
# tags: Django
# info: Django Project Management
# -------------------------

# Main Script
# ------------------

NL=$'\n'
# Check whether postgress installed and initialised
./helpers/django_check.sh || exit
django_version=`python -c "import django; print django.get_version()" 2>/dev/null`

#PROJECT=`cat /etc/rc.conf | grep "djangod_path" | sed -e 's/djangod_path=\"\(.*\)\"/\1/' | xargs basename`
PROJECT=media_library

if [ -z  $PROJECT ]
then
  echo "title: Django Project"
  echo "text: djangod_path is not defined in /etc/rc.conf."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ ! -z $PROJECT ]
then
  # Manage Apps within specified project
  [ -z $PROJECT ] && PROJECT=$2
  echo "title: Django Project - ${PROJECT}"
  echo "display:"
  echo "- form:"
  echo "    submit: Create App"
  echo "    fields:"
  echo "    - type: hidden"  # 1 = create_app
  echo "      default: create_app"
  echo "    - type: hidden"  # 2 = project
  echo "      default: ${PROJECT}"
  echo "    - label: App Name"  # 3 = app name
  echo "      info: |"
  echo "        Avoid using python library names such as django or test etc."
  echo "      type: text"
  echo "    - label: Folder ownership"  # 4 = owner string
  echo "      info: In the form user:group"
  echo "      type: text"
  echo "      pattern: '^\w+:\w+$'"
  echo "      default: 'root:www'"
  echo "    - label: Folder permissions" # 5 = permission string
  echo "      info: In the form rwxr-xr--"
  echo "      type: text"
  echo "      pattern: '[r-][w-][x-][r-][w-][x-][r-][w-][x-]'"
  echo "      default: 'rwxrw-r--'"
  echo "- form:"
  echo "    title: Create Super User (${PROJECT})"
  echo "    submit: Create Super User"
  echo "    fields:"
  echo "    - type: hidden"  # 1 = command
  echo "      default: create_superuser"
  echo "    - type: hidden"
  echo "      default: ${PROJECT}"  # 2 = project
  echo "    - label: Username"  # 3 = username
  echo "      type: text"
  echo "    - label: Email Address"  # 4 = email
  echo "      type: text"
  echo "    - label: Password"  # 5 = password
  echo "      type: password"
  echo "- table:"
  echo "    title: Local Apps"
  echo "    delimiter: '\s+'"
  echo "    actions: per_row"
  echo "    header: [Application, Status]"
  echo "    data: |"
  cd ${FM_DJANGO_ROOT}/${PROJECT}
  # List projects apps
  for a in */
  do
      if [ "${a%/}" != "$PROJECT" ]
      then
        echo "      ${a%/} unknown make_migrations.!delete_app"
      fi
  done
  cd - >/dev/null # switch back to original folder
  
elif [ "$1" == "create_superuser" ]
then
  echo "title: Create Super User"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  echo "Creating super user ${3}..."
  echo "--------------------------------------------------------------"
  echo "python manage.py shell"
  echo "> from django.contrib.auth.models import User"
  echo "> User.objects.create_superuser(\"${3}\", \"${4}\", \"${5}\")"
  echo "--------------------------------------------------------------"
  cd ${FM_DJANGO_ROOT}/${2}
  python ./manage.py shell << END
from django.contrib.auth.models import User
User.objects.create_superuser("${3}", "${4}", "${5}")
END
  echo "Done."

elif [ "$2" == "make_migrations" ]
then
  echo "title: Make Migrations ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  # split path into project and app
  APP=$1
  cd ${FM_DJANGO_ROOT}/${PROJECT}
  echo "-------------------------------------------"
  echo "python manage.py makemigrations $APP"
  echo "-------------------------------------------"
  python manage.py makemigrations $APP
  echo "Done."
  
elif [ "$2" == "delete_app" ]
then
  # TODO check other stuff like databases
  echo "title: Deleting App ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "..."
  if [ -d ${FM_DJANGO_ROOT}/${1} ]
  then
    APP=$1
    echo "Deleting app ${APP} from ${PROJECT}"
    echo "Deleting folder ${FM_DJANGO_ROOT}/${PROJECT}/${APP}"
    cd ${FM_DJANGO_ROOT}/${PROJECT}
    rm -R ./${APP}
    echo "Removing app ${APP} from ${PROJECT} settings.py"
    NEWSETTINGS=`cat ./${PROJECT}/settings.py | grep -v "'${APP}',"`
    echo "$NEWSETTINGS" > ./${PROJECT}/settings.py
  fi
  echo "Deleted."

elif [ "$2" == "create_app" ]
then
  # Install Web2Py
  echo "title: Creating App"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "..."
  PROJECT=$1
  APP=$3
  OWN=$4
  PERM=$5
  if [ -d ${FM_DJANGO_ROOT}/${PROJECT}/${3} ]
  then
    echo "Error, app '${3}' already exists in project ${1}"
  else
    echo "Creating new app ${APP} in project ${PROJECT}"
    echo "-------------------------------------------"
    echo "django-admin startapp $APP"
    echo "-------------------------------------------"
    cd ${FM_DJANGO_ROOT}/${PROJECT}
    django-admin startapp ${APP} && echo "Created"
    # Insert INSTALL_APPS entry into projects settings.py
    echo "Inserting app into INSTALLED_APPS list in settings.py"
    NEWSETTINGS=`awk '$0 == ")" && previous == "INSTALLED_APPS" {
                       print "    -CREATED_APP_HERE-"; previous = "" } 
                     {print}
                       $0 ~ /^INSTALLED_APPS/ {
                     previous = "INSTALLED_APPS"}' ./${PROJECT}/settings.py`
    echo "${NEWSETTINGS}" | sed s/-CREATED_APP_HERE-/\'${APP}\',/ > ./${PROJECT}/settings.py
    cd -
    ./helpers/folder_set_perms.sh $OWN $PERM ${FM_DJANGO_ROOT}/${PROJECT}/${APP}
  fi


else
  echo "${0} - No recognised inputs found, no action taken."
fi
