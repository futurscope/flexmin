# -------------------------------
# name: Edit Configurations
# tags: uWSGI
# info: Lists available uWSGI config files for editing
# ------------------------------

# Main Script
# --------------

name='NGINX Logs'   # used in titles
folder_path='/var/log/nginx'   # path of the configuration files to list
file_pattern="*.log"      # specifies what filenames to list as configuration files in path
list_headers="[Log,Modified,Size]"
list_format="%P;%Tc;%s\n" # file_ext=".nmconnection"           # what extension to put on newly created files
# edit_mode="ini"   # specify edit mode for ACE editor
# create="yes"       # allow creating new configuration files
# yaml="yes"         # is file a flexmin YAML that generates the real file?
delete="yes"       # should we allow delete of existing files
# edit="yes"         # should we provide an edit option
view_tail=100

source ./helpers/generic_viewedit_list.sh
