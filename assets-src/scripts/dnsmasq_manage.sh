name='DNSMASQ Configuration'   # used in titles
folder_path='/etc/dnsmasq'   # path of the configuration files to list
file_pattern="*.conf"      # specifies what filenames to list as configuration files in path
file_ext=".conf"           # what extension to put on newly created files
edit_mode="ini"   # specify edit mode for ACE editor
create="yes"       # allow creating new configuration files
config="/etc/dnsmasq.conf"  # for showing current config
# yaml="yes"         # is file a flexmin YAML that generates the real file?
# delete="yes"       # should we allow delete of existing files
edit="yes"         # should we provide an edit option
primary_file='/etc/dnsmasq.conf'   # allow specify a file outside folder_path to include
list_headers="[Filename,Modified,Size]"  # headers for the list of matched files
list_format="%p;%Tc;%s\n"   # From find command's printf format options
# templates_path=$FM_CONFIG_TMPLT
# templates_filter="*.yaml.template"

# Ensure 'standard' folders and files in place
[ ! -d ${folder_path} ] && mkdir -p ${folder_path}
[ ! -f ${folder_path}/address.conf ] && touch ${folder_path}/address.conf
[ ! -f ${folder_path}/dhcp.conf ] && touch ${folder_path}/dhcp.conf
[ ! -f ${folder_path}/tftp.conf ] && touch ${folder_path}/tftp.conf

# decide whether a delete action is to be added to the actions column
delete_action=""
[ "$delete" == "yes" ] && delete_action=".!delete"
edit_action=""
[ "$edit" == "yes" ] && edit_action=".~edit"
[ -z "$list_format" ] && list_format="%P\n"
[ -z "$list_headers" ] && list_headers="[Filename]"

# ---- Default Action ----
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then 
  echo "title: $name"
  echo "display:"
  echo "  - table:"
  echo "      title: Edit Configurations"
  echo "      header: ${list_headers}"
  echo "      actions: ?view${edit_action}${delete_action}"
  echo "      delimiter: ;"
  echo "      data: |"
  cd /etc
  find ./ -name "dnsmasq.conf" -readable -printf "$list_format" | sed -r 's/^\.\///' | sed 's/^/        /'
  find ./ -path "*/dnsmasq/*.conf" -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /'
  cd - >/dev/null
  
  if [ -f ${config} ]
  then
    echo "  - title: Existing $name Config (${config})"
    echo "  - note: Excluding lines commented out to keep this short."
    echo "  - text: |"
    # list config, excluding blank and commented lines for brevity
    cat ${config} | grep -v '^#' | grep -v '^\s*$' | sed -r 's/^\[/\n\[/' | sed 's/^/      /'
  else
    echo "  - note: No existing ${name} configuration file found"
  fi
  
  if [ -f "${folder_path}/address.conf" ]
  then
    echo "  - table:"
    echo "      title: Fixed host resolution (address.conf)"
    echo "      header: ['name','address']"
    echo "      actions: '!delete'"
    echo "      delimiter: ';'"
    echo "      data: |"
    cat "${folder_path}/address.conf" | sed -r 's/address=\///' | sed -r 's/\//;/g' | sed -r 's/^/        /'
  fi
  
  echo "  - form:"
  echo "      title: Create new host entry"
  echo "      fields:"
  echo "        - label: Hostname"
  echo "          info: hostname entry e.g. www.example.com"
  echo "          pattern: '[a-zA-Z0-9_.]+'"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: IP Address"
  echo "          info: IP address of host"
  echo "          pattern: '[a-fA-F0-9:.]+'"
  echo "          type: text"
  echo "      submit: Add entry"
       
# ---- Default Action on Selected Item ----
elif [ $# -eq 1 ]
then
  echo "title: Edit ${1}"
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: ${edit_mode}"
  echo "  data: |"
  cat /etc/"${1}" | sed 's/^/    /'

# ---- View Action ----
elif [ "$2" == "view" ]
then
  echo "title: View ${1}"
  echo "method: popup"
  echo "code: |2"  # explicit indent specified to allow for extra spaces on first line
  cat /etc/"${1}" | sed -e "s/\x1b//g" | sed 's/^/  /'
  
# ---- Save Action ----
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "---"
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 /etc/"${1}"
  [ "${yaml}" == "yes" ] && flexmin yamlconfig "${1}"
  echo "Saved"
       
# ---- Create Action ----
elif [ "$2" == "create" ]  # 1=hostname 2=create 3=IP address
then
  echo "title: Creating New Host Entry"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "---"
  echo "Adding entry for host ${1}"
  if grep "/${1}/" ${folder_path}/address.conf >/dev/null
  then
    echo "An entry for this host already exists, please remove the existing entry first."
    echo "Aborted"
  else
    echo "address=/${1}/${3}" >> ${folder_path}/address.conf
    echo "Created"
  fi
  dnsmasq --test
  [ $? -eq 0 ] && echo "Restarting dnsmasq" && systemctl restart dnsmasq
  echo "Finished"
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "---"
  # copy saved file to destination
  echo "Deleting entry for host ${1}"
  grep -v "/${1}/" ${folder_path}/address.conf > ${folder_path}/address.tmp
  rm ${folder_path}/address.conf
  mv ${folder_path}/address.tmp ${folder_path}/address.conf
  echo "Deleted"
  
fi

