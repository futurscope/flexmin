# ----------------------
# name: Projects
# tags: Django
# info: Django Project Management
# -------------------------

# Main Script
# ------------------

NL=$'\n'
# Check whether django installed
./helpers/django_check.sh || exit
django_version=`python -c "import django; print django.get_version()" 2>/dev/null`

#PROJECT=`cat /etc/rc.conf | grep "djangod_path" | sed -e 's/djangod_path=\"\(.*\)\"/\1/' | xargs basename`


if [ -z  ${FM_DJANGO_ROOT} ]
then
  echo "title: Django"
  echo "text: FM_DJANGO_ROOT is not defined, please define this in Flexmin parameters."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -d ${FM_DJANGO_ROOT} ]
then
  echo "title: Create Django Project - ${django_version}"
  echo "display:"
  echo "  - form:"
  echo "      submit: Create Project"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Name"
  echo "          info: |"
  echo "            Avoid using python library names such as django or test etc."
  echo "          type: text"
  echo "  - table:"
  echo "      title: Local Django Projects"
  echo "      delimiter: '\s+'"
  echo "      actions: per_row"
  echo "      header: [Application, Status]"
  echo "      data: |"
  cd ${FM_DJANGO_ROOT}
  for a in */
  do
      echo "        ${a%/} unknown make_migrations.migrate.!delete"
  done
  cd - >/dev/null  # switch back to original folder
  
elif [ "$2" == "make_migrations" ]
then
  echo "title: Migrate Project - ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  PROJECT=$1
  cd ${FM_DJANGO_ROOT}/${PROJECT}
  echo "Making migration for project ${PROJECT}"
  python3 manage.py makemigrations
  echo "Done."

elif [ "$2" == "migrate" ]
then
  echo "title: Migrate Project - ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  PROJECT=$1
  cd ${FM_DJANGO_ROOT}/${PROJECT}
  echo "Migrating project ${PROJECT}"
  python3 manage.py migrate
  echo "Done."
  
elif [ "$2" == "delete" ]
then
  # TODO check other stuff like databases
  echo "title: Deleting Project ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "..."
  if [ -d ${FM_DJANGO_ROOT}/${1} ]
  then
    echo "Deleting project ${1}"
    rm -R ${FM_DJANGO_ROOT}/${1}
  fi
  echo "Deleted."
  
elif [ "$2" == "create" ]
then
  # Install Web2Py
  echo "title: Creating Django Project"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "..."
  cd ${FM_DJANGO_ROOT}
  if [ -d ${FM_DJANGO_ROOT}/${1} ]
  then
    echo "Error, project '${1}' already exists."
  else
    echo "Creating new project ${1}"
    django-admin startproject ${1} && echo "Created"
  fi
  
else
  echo "${0} - No recognised inputs found, no action taken."
fi
