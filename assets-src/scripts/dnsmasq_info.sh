echo "title: DNSMASQ Information"
echo "display:"

[ -f /var/lib/misc/dnsmasq.leases ] && lease_file='/var/lib/misc/dnsmasq.leases'
[ -f /var/db/dnsmasq.leases ] && lease_file='/var/db/dnsmasq.leases'
if [ ! -z "${lease_file}" ]
then
  echo "  - table:"
  echo "      title: DHCP Leases"
  echo "      header: ['Expires','Mac address', 'IP address','Name','Other']"
  echo "      delimiter: ';'"
  echo "      data: |2"
  cat ${lease_file} | sed 's/ /;/g' | while read -r nc
  do
    date=$(date -d @${nc%%;*})
    echo $nc | sed "s/${nc%%;*}/${date}/" | sed 's/^/        /'
  done
else
  echo "  - note: DNSMASQ lease file not found"
fi


[ -f /var/log/dnsmasq.log ] && log_file='/var/log/dnsmasq.log'
if [ ! -z "${log_file}" ]
then
  echo "  - title: DNSMASQ Log File (Last 30 entries)"
  echo "  - code: |2"
  tail -n 30 ${log_file} | sed 's/^/      /'
else
  echo "  - note: DNSMASQ Log file not found"
fi
