# -----------------------------
# name: Adaptor Configuration
# tags: Network
# info: Lists network interfaces to select which one view config.
# -----------------------------

# Main Script
# ---------------

echo "title: IP4 Config"
#echo 1: $1
#echo 2: $2
NL=$'\n'  # for newline character

if [ "$#" -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "display:"
  echo "  - note: |"
  echo "      Changes made to the network configuration here will not take place until"
  echo "      the system is restarted. The manual configuration defaults to the"
  echo "      IP address currently allocated, which might be assigned using DHCP"
  echo "  - table:"
  echo "      title: Network Addresses"
  echo "      header: [Adaptor, Network Address]"
  echo "      delimiter: ','"
  echo "      actions: per_row" #Set_to_DHCP.Set_to_Manual"
  echo "      data: |"
  # list network adaptors, and convert to item per line output
  ip -o address show | sed -r 's/[0-9]+:[ ]+([^ ]+)[ ]+inet[6]?[ ]([^ ]+).*/\1,\2,?info.delete/g' | sed -e 's/^/        /'
  echo "  - table:"
  echo "      title: Mac Addresses"
  echo "      header: [Adaptor, Mac Address]"
  echo "      delimiter: ','"
  #echo "      actions: per_row" #Set_to_DHCP.Set_to_Manual"
  echo "      data: |"
  # list network adaptors, and convert to item per line output
  ip -o link show | sed -r 's/[0-9]+:[ ]+([^:]+):.*link\/[a-z]+ ([0-9a-fA-F:]+).*/\1,\2/' | sed -e 's/^/        /'
  echo "  - form:"
  echo "      title: Add address to adaptor"
  echo "      submit: Save Address"
  echo "      fields:"
  echo "        - label: Network Adapter"
  echo "          delimiter: '\n'"
  echo "          select: |"
  ip link show | grep -v ' lo: ' | grep -E '^[0-9]: ' | sed -r 's/^[0-9]+: ([^:]+):.*$/\1/' | sed 's/^/            /'
  echo "        - type: hidden"
  echo "          default: add_ip"
  echo "        - label: IP4 Address and subnet"
  echo "          type: text"
  echo "          pattern: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}'"
  echo "          info: Must be in the form x.x.x.x/y where x is number between 0-255 and y is 8-32"
  

elif [ "$2" == "info" ]
then
  echo "title: 'Current Status of ${1}'"
  echo "method: popup"
  echo "---"
  ip -s -s link ls $1

  
elif [ "$2" == "add_ip" ]
then
  echo "title: 'Adding IP address to device ${1}'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "---"
  if [ "${1}" == "" ]
  then
    echo "Error: Device not specified!"
  else
    echo "Adding IP address ${3}"
    ip addr add "${3}" dev ${1}
  fi
  echo "Done."

  
elif [ "$2" == "delete" ]
then
  echo "title: 'Deleting ${3} from device ${1}'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "---"
  echo "Deleting address"
  ip addr del "${3}" dev ${1}
  echo "Done."
  
# TODO Get manual configuration option below working
# --------------------------------------------------
  
# Adapter and Manual IP4 config specified
elif [ "$#" -eq 2 ] && [ "$1" == "manual" ]
then
  #if grep -i "^ifconfig_$1 *= *\"*dhcp" /etc/rc.conf > /dev/null
  #then
  #  CTYPE=DHCP
  #elif grep -i "^ifconfig_$1 *= *\"[0-9]\{1,3\}\.[0-9]\{1,3\}\." /etc/rc.conf > /dev/null
  #then
    CTYPE=Manual
    IP_ADDRESS=`ifconfig $2 inet | awk '/inet/ {printf("%s\n", $2); }'`
    IP_ADDRESS=${IP_ADDRESS:="''"}  # set to empty '' if no value
    NETMASK=`ifconfig $2 inet | awk '/inet/ {printf("%s\n", $4); }' | xargs printf "%x"`
    if [ -n "$NETMASK" ]
    then
      HEX1=`echo $NETMASK | cut -c1-2`
      HEX2=`echo $NETMASK | cut -c3-4`
      HEX3=`echo $NETMASK | cut -c5-6`
      HEX4=`echo $NETMASK | cut -c7-8`
      MASK=`echo $((0x${HEX1})).$((0x${HEX2})).$((0x${HEX3})).$((0x${HEX4}))`
    else
      MASK="''"
    fi
    
    ROUTER=`netstat -rn -f inet | awk '/default/ {printf("%s\n", $2);}'`
  #fi
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "form:"
  echo "  submit: Save Manual Configuration"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: $2"
  echo "    - type: hidden"
  echo "      default: ${CTYPE}"
  echo "    - label: IP4 Address"
  echo "      type: text"
  echo "      default: ${IP_ADDRESS}"
  echo "      pattern: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'"
  echo "      info: Must be in the form x.x.x.x where x is number between 0-255"
  echo "    - label: Subnet Mask"
  echo "      type: text"
  echo "      default: ${MASK}"
  echo "      pattern: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'"
  echo "      info: Must be in the form x.x.x.x where x is number between 0-255"
  echo "    - label: Gateway (for other interfaces also)"
  echo "      info: This gateway will be the default for all interfaces"
  echo "      type: text"
  echo "      default: ${ROUTER}"
  echo "      pattern: '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'"
  echo "      info: Must be in the form x.x.x.x where x is number between 0-255"
  echo "    - label: With Effect"
  echo "      info: |"
  echo "        Choose whether you want the chnage to take effect now or when "
  echo "        the system next reboots."
  echo "      select: [reboot,now]"

# Adapter and DHCP config specified
elif [ "$#" -eq 2 ] && [ "$1" == "dhcp" ]
then
    echo "title: Switching to DHCP"
    echo "method: popup"
    echo "category: CHANGE"
    echo "next_action: reload_pane"
    echo "---"
  
    RE_IPOUT="^ifconfig_${2} *=.*"
    RE_IPIN="ifconfig_${2}=\"dhcp\""
    RE_DROUT="^defaultrouter *=.*"

    # swap ifconfig_adapter entry 
    if grep -i "${RE_IPOUT}" /etc/rc.conf > /dev/null
    then
      newrc1=`sed "s/${RE_IPOUT}/${RE_IPIN}/" /etc/rc.conf` # > ${FM_TASK_TMP}/rc.conf1
    else
      newrc1=`cat /etc/rc.conf` # > ${FM_TASK_TMP}/rc.conf1
      newrc1="${newrc1}${NL}${RE_IPIN}" # >> ${FM_TASK_TMP}/rc.conf1
    fi

    # remove 'defaultrouter=' line as DHCP will take care of this
    # NOTE: what if DHCP doesn't pickup a gateway?
    #       other adapters might then be without a gateway
    if echo "${newrc1}" | grep -i "${RE_DROUT}" >  /dev/null
    then
      newrc2=`echo "${newrc1}" | sed "/^defaultrouter *=.*/d"`
    else
      #cat /tmp/rc.conf1 > ${FM_TASK_TMP}/rc.conf2
      #echo "${RE_DRIN}" >> ${FM_TASK_TMP}/rc.conf2
      newrc2="${newrc1}${NL}${RE_DRIN}"
    fi

    # put new configuration into effect
    echo "${newrc2}" > /etc/rc.conf  # replace existing rc.conf with new version
    #cp ${FM_TASK_TMP}/rc.conf2 /etc/rc.conf
    #rm ${FM_TASK_TMP}/rc.conf2 ${FM_TASK_TMP}/rc.conf1
    # reconfigure current IP now permanent IP config updated.
    # ifconfig $1 inet $3 netmask $4
    # route delete default
    # route add default $5
    # any easy precautions to take if this fails?
    # do current switch first and test before making it permanent?
    # can ping gateway if that is defined
    echo "Configuration of $2 to DHCP saved."



# All parameters for a manual IP config provided 
elif [ "$#" -eq 6 ]
then
  # 5 parameters provided adapter(1), type(2), ip(3), mask(4), router(5)
  echo "title: Switching to Manual"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "---"
  if [ "$2" == "Manual" ]
  then
    RE_IPOUT="^ifconfig_$1 *=.*"
    RE_IPIN="ifconfig_$1=\"$3 netmask $4\""
    RE_DROUT="^defaultrouter *=.*"
    RE_DRIN="defaultrouter=\"$5\""

    if grep -i "^ifconfig_$1 *=.*" /etc/rc.conf > /dev/null
    then
      newrc1=`sed "s/^ifconfig_$1 *=.*/ifconfig_$1=\"$3 netmask $4\"/" /etc/rc.conf` # > ${FM_TASK_TMP}/rc.conf1
    else
      newrc1=`cat /etc/rc.conf` # > ${FM_TASK_TMP}/rc.conf1
      newrc1="${newrc1}${NL}${RE_IPIN}" # >> ${FM_TASK_TMP}/rc.conf1
    fi

    if echo "${newrc1}" | grep -i "${RE_DROUT}" >  /dev/null
    then
      newrc2=`echo "${newrc1}" | sed "s/^defaultrouter *=.*/defaultrouter=\"$5\"/"`
    else
      #cat ${FM_TASK_TMP}/rc.conf1 > ${FM_TASK_TMP}/rc.conf2
      #echo "${RE_DRIN}" >> ${FM_TASK_TMP}/rc.conf2
      newrc2="${newrc1}${NL}${RE_DRIN}"
    fi

    #cp ${FM_TASK_TMP}/rc.conf2 /etc/rc.conf
    #rm ${FM_TASK_TMP}/rc.conf2 ${FM_TASK_TMP}/rc.conf1
    echo "${newrc2}" > /etc/rc.conf
    # reconfigure current IP now permanent IP config updated.
    # ifconfig $1 inet $3 netmask $4
    # route delete default
    # route add default $5
    # any easy precautions to take if this fails?
    # do current switch first and test before making it permanent?
    # can ping gateway if that is defined
    echo "Configuration for $1 saved."

  fi
else
  echo "text: Unknown status"
  echo "status: ERROR"
fi
 
