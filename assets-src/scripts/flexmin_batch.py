# ---------------
# name: Task Packages
# tags: FlexPac
# info: Local flexmin packages available to run
# ---------------------

# -*- coding: utf-8 -*-

# Python Web2Py script, needs to return text output (yaml) and a boolean indicating whether this 
# task has finished (True) or whether more need to be done by the flexmin service version of the script (False)
import os, glob, zipfile

def run(vars,fm_vars,lines=None):
    if lines == None:
        retval = True  # return text
        lines = []
    else:
        retval = False # output list supplied, just append to that
         
    if len(vars) == 0:
        lines.append("title: Local Packages")
        lines.append("table:")
        lines.append("  actions: [description,tasks,'!run','!delete']")
        lines.append("  header: [Package]")
        lines.append("  show_sub: ['(\/.*\/|\/(?=[^\/]))','']")
        lines.append("  data: |")
        g = os.path.join(fm_vars['FM_BACKUP'],'*.flexpac.zip')
        nc = glob.glob(g)
        for f in nc:
            if os.path.basename(f)[0] != "_":
                lines.append("    " + os.path.abspath(f))
    elif len(vars) == 2 and vars[0] == "delete":
        zfile = vars[1]
        filename = os.path.basename(zfile)
        lines.append("title: Package Delete - " + filename)
        lines.append("method: popup")
        lines.append("next_action: reload_pane")
        lines.append("text: |")
        os.remove(zfile)
        lines.append("  Deleted")
    elif len(vars) == 2 and vars[0] == "tasks":
        zfile = vars[1]
        filename = os.path.basename(zfile)
        lines.append("title: Listing Package Tasks - " + filename)
        lines.append("method: popup")
        lines.append("---")
        with zipfile.ZipFile(zfile,"r") as z:
            try:
                with z.open('tasks') as f:
                    for line in f:
                        lines.append(line.strip('\n'))
            except:
                lines.append("  ERROR - Could not access tasks file in zip archive.")
    elif len(vars) == 2 and vars[0] == "description":
        zfile = vars[1]
        filename = os.path.basename(zfile)
        lines.append("title: Package Description - " + filename)
        lines.append("method: popup")
        lines.append("---")
        with zipfile.ZipFile(zfile,"r") as z:
            try:
                with z.open('readme.txt') as f:
                    for line in f:
                        lines.append(line.strip('\n'))
            except:
                lines.append("  ERROR - Could not access readme.txt file in zip archive.")

    # Return output to action function
    if retval:
        return '\n'.join(lines)
    else:
        return None
    
