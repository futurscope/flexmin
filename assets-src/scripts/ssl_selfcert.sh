# -----------------------------
# name: Self-Signed Certificate
# tags: Certificates
# info: Create a new self signed certificate
# ------------------------------------

# Main Script
# -----------
THIS_SCRIPT=`basename $0`

source ./helpers/cert_check.sh

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Create Self-Signed Certificate"
  echo "note: |"
  echo "  Please fill out all fields (using None, if field does not apply)."
  echo "  No / in any fields please."
  echo "form:"
  echo "  submit: Create Certificate"
  echo "  fields:"
  echo "    - label: CN (e.g. www.example.com)"
  echo "      pattern: '^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\.)+[A-Za-z]{2,6}$'"
  echo "      type: text"
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Duration (Days)"
  echo "      type: text"
  echo "    - label: Country"
  echo "      type: text"
  echo "      pattern: '[A-Z]{2}'"
  echo "    - label: State"
  echo "      type: text"
  echo "    - label: Locality"
  echo "      type: text"
  echo "    - label: Organisation"
  echo "      type: text"
  echo "    - label: Organisation Unit"
  echo "      type: text"

elif [ "$2" == "create" ]
then
  echo "title: Creating Self-Signed Certificate "
  echo "method: popup"
  echo "category: CREATE"
  echo "..."
  if [ -w ${FM_SSL_ROOT} ]
  then
    # Create private key and certificate
    openssl req -nodes -sha256 -x509 -newkey rsa:2048 -keyout ${FM_SSL_ROOT}/${1}.key \
      -out ${FM_SSL_ROOT}/${1}.crt -days $3 -subj "/C=$4/ST=$5/L=$6/O=$7,/OU=$8/CN=$1" 2>&1 | sed -r 's/^/> /'

    # show resulting certificate
    echo "Public Certificate:"
    cat ${FM_SSL_ROOT}/$1.crt

    # Set restricted permissions on new private keys and any existing keys
    # chown root:wheel ${FM_SSL_ROOT}/*.key
    chmod 400 ${FM_SSL_ROOT}/*.key
  else
    echo "ERROR: Permission to ${FM_SSL_ROOT} denied."
  fi
fi



