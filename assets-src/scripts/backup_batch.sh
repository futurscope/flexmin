# ---------------
# name: Backup Packages
# tags: FlexPac
# info: Local Backup packages available to run
# ---------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Backup Packages"
  echo "display:"
  echo "  - table:"
  echo "      actions: [description,tasks,'!run','!delete']"
  echo "      header: [Package]"
  echo "      show_sub: ['(\/.*\/|\/(?=[^\/]))','']"
  echo "      data: |"
  cd ${FM_BACKUP}
  for bp in *.backup.flexpac.zip
  do
    if [ "${bp}" != "*.backup.flexpac.zip" ]
    then
      echo "        ${bp}"
    fi
  done
  echo "  - table:"
  echo "      title: Restore Packages"
  echo "      actions: [description,tasks,'!run','!delete']"
  echo "      header: [Package]"
  echo "      show_sub: ['(\/.*\/|\/(?=[^\/]))','']"
  echo "      data: |"
  cd ${FM_BACKUP}
  for bp in *.restore.flexpac.zip
  do
    if [ "${bp}" != "*.restore.flexpac.zip" ]
    then
      echo "        ${bp}"
    fi
  done
  echo "  - form:"
  echo "      title: Schedule Backup"
  echo "      submit: Schedule Backup"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: cron"
  echo "        - label: Frequency"
  echo "          select: [daily]"
  echo "        - label: Time (hh:mm)"
  echo "          type: text"
  echo "          pattern: '\d\d:\d\d'"
  echo "  - note: |"
  echo "      This scheduled backup will run all backup packages listed above"
elif [ "$1" == "cron" ]
then
  # import cron helper functions
  . ./helpers/cron.sh
  sfn=`basename ${0}`
  echo "title: 'Scheduling Backup'"
  echo "method: popup"
  echo "status: CHANGING"
  echo "---"
  cron_set "${FM_SCRIPTS}/common.sh 0 ${0} backup" "${2}" "${3}"
  echo "Scheduled"
elif [ "$2" == "delete" ]
then
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "---"
  rm ${FM_BACKUP}/${1}
  echo "Done"
elif [ "$2" == "description" ]
then
  echo "title: Description ${1}"
  echo "method: popup"
  echo "---"
  unzip -p -x tasks ${FM_BACKUP}/${2}
elif [ "$2" == "tasks" ]
then
  echo "title: Tasks ${1}"
  echo "method: popup"
  echo "---"
  unzip -p -x *.txt ${FM_BACKUP}/${1}
elif [ "$2" == "run" ]
then
  # Web GUI based version of backup task below
  echo "title: Running ${1}"
  echo "method: popup"
  echo "..."
  cd $(dirname ${FM_SCRIPTS})  # main flexmin service folder
  pwd
  # this is a bit messy as we are using flexmin_srv.py to run itself
  python flexmin_srv.py run ${FM_BACKUP}/${1}
  echo "Done."
elif [ "$2" == "backup" ]
then
  # CRON/scheduled version of run task above
  # This should only be run scheduled, designed to log useful data
  echo "title: Running ${1}"
  echo "method: popup"
  echo "..."
  cd ${FM_BACKUP}
  for bp in *.backup.flexpac.zip
  do
    date >> ${FM_LOGROOT}/backup_batch.log
    echo "Job: ${bp}" >> ${FM_LOGROOT}/backup_batch.log
    cd $(dirname ${FM_SCRIPTS})  # main flexmin service folder
    # flexmin_srv will log package output to it's own log file, this script
    # handles output to backup_batch.log, this output should be more concise
    python flexmin_srv.py run ${FM_BACKUP}/${bp} \
        | grep -r "^#" \
        | sed 's/^/  /'>> ${FM_LOGROOT}/backup_batch.log
    cd -
    date >> ${FM_LOGROOT}/backup_batch.log
    echo "Done." >> ${FM_LOGROOT}/backup_batch.log
    echo "" >> ${FM_LOGROOT}/backup_batch.log
  done
fi
