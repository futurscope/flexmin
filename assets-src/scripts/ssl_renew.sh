# -----------------------------
# name: Certificate Renewal
# tags: Certificates
# info: Renew any existing certificate.
# ------------------------------------

# Main Script
# -----------
THIS_SCRIPT=`basename $0`

echo "title: Test - Certificate Renewal"
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "note: |"
  echo "  This is an experimental form for testing textbox and large"
  echo "  parameter inputs to scripts (via params/ files). Does not do anything"
  echo "form:"
  echo "  submit: Renew"
  echo "  fields:"
  echo "    - label: fqdn"
  echo "      type: text"
  echo "    - label: cert"
  echo "      type: textbox"
else
  echo "title: Dummy process"
  echo "method: popup"
  echo "---"
  echo "saved - $1"
  cat ${FM_SCRIPT_PARAM}_2
fi

# Renewal request for existing ssl certificate
# openssl x509 -x509toreq -in server.crt -out server_renewreq.pem -signkey server.key


