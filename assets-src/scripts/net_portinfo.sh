# -----------------------------
# name: Open Port List
# tags: Network
# info: List Open Network Ports
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Network Ports"
  echo "display:"
  echo "  - table:"
  echo "      title: IP4 Ports"
  echo "      header: [Netid,State,Recv-Q,Send-Q,Local Address:Port,Peer Address:Port,Process]"
  # delimiter \t+|\s+ splits row by tabs or spaces (one or more of either)
  echo "      delimiter: '\t+|\s+'"
  echo "      data: |"
  # sed 1d skips the 1st line which has the headers
  ss -tunpl4 | sed 1d | sed 's/^/        /'
  echo "  - table:"
  echo "      title: IP6 Ports"
  echo "      header: [Netid,State,Recv-Q,Send-Q,Local Address:Port,Peer Address:Port,Process]"
  echo "      delimiter: '\t+|\s+'"
  echo "      data: |"
  ss -tunpl6H | sed 's/^/        /'
fi
