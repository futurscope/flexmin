# ----------------------
# name: Applications
# tags: Web2py
# info: Web2pY Application Management
# -------------------------

# Main Script
# ------------------
NL=$'\n'


if [ -z  ${FM_WEB2PY_ROOT} ]
then
  echo "title: Web2py"
  echo "text: FM_WEB2PY_ROOT is not defined, please define this in Flexmin parameters."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -f ${FM_WEB2PY_ROOT}/VERSION ]
then
  echo "title: Install Web2Py Application"
  echo "display:"
  echo "  - form:"
  echo "      submit: Install Application"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: install"
  echo "        - label: source"
  echo "          info: |"
  echo "            Any URI recognised by fetch command e.g. "
  echo "            ftp://myserver.com/folder/myfile or "
  echo "            http://myserver.com/folder/myfile"
  echo "          type: text"
  #echo "        - label: Web user account"
  #echo "          info: |"
  #echo "            User account used to run web2py applications. Defaults to"
  #echo "            group owner of web2py/applications directory."
  #echo "            This is needed to set releavtn permissions on extracted files."
  #echo "          type: text"
  # get owner (group) of existing application folder ($3 would get user)
  #group=`ls -ld ${FM_WEB2PY_ROOT}/applications | awk '{print $4}'` 
  #echo "          default: ${group}"
  echo "  - table:"
  echo "      title: Local Web2py Applications"
  echo "      delimiter: '\s+'"
  echo "      actions: per_row"
  echo "      header: [Application, Status]"
  echo "      data: |"
  cd ${FM_WEB2PY_ROOT}/applications
  for a in */
  do
      echo "        ${a%/} unknown !delete"
  done
  cd -  # switch back to original folder
  echo "  - note: |"
  echo "      The backup option allows you to backup a zipped copy of your web2py"
  echo "      application. This backup excludes local data as the backup is for"
  echo "      the application, not the data."
  echo "      The database, cache, errors, sessions, and uploads folder contents"
  echo "      are excluded from the zipped file. These folder are included as"
  echo "      empty folders though."
elif [ "$1" == "delete" ]
then
  echo "title: Deleting ${2}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "---"
  if [ -f ${FM_BACKUP}/${2} ]
  then
    echo "Deleting backup file ${2}"
    rm -R ${FM_BACKUP}/${2}
  else
    echo "Deleting application ${2}"
    rm -R ${FM_WEB2PY_ROOT}/applications/${2}
  fi
  echo "Deleted."
elif [ "$1" == "install" ]
then
  # Install Web2Py
  echo "title: Installing Web2py Application"
  echo "method: popup"
  echo "category: INSTALL"
  echo "next_action: reload_pane"
  echo "---"
  if [ "${2%%/*}" == "." ]  # if text up to first / is .
  then
    src=${FM_REMOTE_SOURCE}/${2#*/}   # add default remote source to specified source
  else
    src=$2
  fi
  echo "Installing from ${src}"
  echo "Downloading to temporary file ${FM_TASK_TMP}${FM_TASKID}.zip"
  ZIPFILE=${FM_TASK_TMP}/${FM_TASKID}.zip  # destination for zip file download
  fetch -o $ZIPFILE ${src}  # download zip file
  if [ $? -ne 0 ]
  then
    echo "Download failed, aborting."
  else
    ./helpers/web2py_appinstall.sh $ZIPFILE
  fi
  # remove zip file after install
  if [ -f $ZIPFILE ]
  then
    rm $ZIPFILE
  fi
else
  echo "${0} - No recognised inputs found, no action taken."
fi
