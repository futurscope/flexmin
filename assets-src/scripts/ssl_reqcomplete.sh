# -----------------------------
# name: Complete Request
# tags: Certificates
# info: Save certifcate for previously created request.
# ------------------------------------

# Main Script
# -----------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Complete Request"
  echo "note: |"
  echo "  If intermediate certificates are provided, paste these into the "
  echo "  text box after the server/host certificate"
  echo "form:"
  echo "  submit: Complete"
  echo "  fields:"
  echo "    - label: Request"
  echo "      selectformat: spacesep"
  echo "      select: |"
  for f in ${FM_SSL_ROOT}/*.csr
  do
    if [ "$f" != "${FM_SSL_ROOT}/*.csr" ]  # check some files found first
    then
      if [ ! -f ${f%*.*}.crt ]  # if no matching crt exists for this csr file
      then
        # add to list of outstanding CSR requests.
        echo "        ${f}" | sed "s%${FM_SSL_ROOT}/%% ; s/\.csr//"
      fi
    fi
  done
  echo "    - label: Certificate"
  echo "      type: textbox" 

else
  echo "title: 'Completing Certificate Request : ${1}'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  echo "Saved - $1"
  cat ${FM_SCRIPT_PARAM}_2 > ${FM_SSL_ROOT}/$1.crt
  echo "certfile ${SSLROOT}/$1.crt"
  echo "------------------------------------------"
  cat ${FM_SSL_ROOT}/$1.crt
fi

# Renewal request for existing ssl certificate
# openssl x509 -x509toreq -in server.crt -out server_renewreq.pem -signkey server.key


