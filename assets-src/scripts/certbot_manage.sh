# check certbot installed, show message and end script if not.
source ./helpers/certbot_check.sh

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Certificate Management - CertBot"
  echo "display:"
  echo "  - table:"
  echo "      actions: ?detail.?renew.!delete"
  echo "      header: [Name, Expiry]"
  echo "      delimiter: ', '"
  echo "      data: |"
  ssl_type='letsencrypt'
  certbot certificates 2>&1 | grep -e 'Name:' -e 'Expiry' | sed -r 's/^[^:]+\:[ ]+//' | sed -r 's/ [0-9]+[^(]+/ /' | sed -E '/./{H;$!d} ; x ; s/([a-z]{2})\n/\1, /g' | grep -v '^$' | sed -r 's/^/        /'

  echo "  - title: Request new certificate"
  echo "  - note: |"
  echo "      This process makes a request to LetsEncrypt for a certificate."
  echo "      The LetsEncrypt Terms of Service are automatically accepted, and"
  echo "      the IP address is logged. The email address provided is used for"
  echo "      the request."
  echo "  - note: |"
  echo "      This certificate request assumes: that you have an FTP server, "
  echo "      that a folder matching the domain exists in the root folder, and"
  echo "      requests to the specified domain will be served from this folder."
  echo "  - form:"
  echo "      fields:"
  echo "        - label: FQDN"
  echo "          info: Enter full domain name here e.g. www.example.com"
  echo "          pattern: '[a-zA-Z0-9._]+'"
  echo "          type: text_long"
  echo "        - type: hidden"
  echo "          default: create_cert"
  echo "        - label: Email Address"
  echo "          type: text"
  if [ -f ${FM_CONFIGS}/certbot_cli.ini ]
  then
    # set default value based on existing email address in ini file
    cat ${FM_CONFIGS}/certbot_cli.ini | grep -E '^email = ' | sed -r 's/^email = ([^#]*).*$/          default: \1/'
  fi
  echo "        - label: FTP Server"
  echo "          type: text_long"
  echo "        - label: FTP Username"
  echo "          type: text"
  echo "        - label: FTP Password"
  echo "          type: password"

elif [ "$2" == "delete" ]
then
  echo "title: Deleting Certificate"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "---"
  echo "Deleting ${1}"
  certbot delete --cert-name ${1}

  
elif [ "$2" == "detail" ]
then
  echo "title: Certficate Detail"
  echo "method: popup"
  echo "---"
  certbot certificates --cert-name ${1}
  
elif [ "$2" == "renew" ]
then
  echo "title: Certficate Detail"
  echo "method: popup"
  echo "form:"
  echo "  title: Renew $1"
  echo "  fields:"
  echo "  - type: hidden"
  echo "    default: $1"
  echo "  - type: hidden"
  echo "    default: renew_cert"
  echo "  - label: Schedule Daily Renewal (hh:mm)"
  echo "    info: If a time is specified here then a scheduled job is set up instead of renewing immediately."
  echo "    type: text"
  echo "    default: now"
  echo "  - label: Email Address"
  echo "    type: text"
  if [ -f ${FM_CONFIGS}/certbot_cli.ini ]
  then
    # set default value based on existing email address in ini file
    cat ${FM_CONFIGS}/certbot_cli.ini | grep -E '^email = ' | sed -r 's/^email = ([^#]*).*$/    default: \1/'
  fi
  echo "  - label: FTP Server"
  echo "    type: text_long"
  echo "  - label: FTP Username"
  echo "    type: text"
  echo "  - label: FTP Password"
  echo "    type: password"
  echo "  submit: Renew Certificate"

elif [ "$2" == "renew_cert" ] && [ "$3" == "now" ]
then
  echo "title: Renewing certificate from LetsEncrypt"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  echo "Renewing certificate for: ${1}"
  sed -i -r "s/^email = .*$/email = ${4}/" ${FM_CONFIGS}/certbot_cli.ini
  echo "Using the following pre-defined parameters:"
  cat ${FM_CONFIGS}/certbot_cli.ini | grep -vE '^(#.*|\s*)$' | sed 's/^/  /'
  echo ""
  export CB_FTPSRV="$5"
  export CB_FTPUSR="$6"
  export CB_FTPPAS="$7"
  
  certbot renew --cert-name ${1} -c ${FM_CONFIGS}/certbot_cli.ini --non-interactive
  
  result=$?
  if [ $result -eq 0 ]
  then
    echo "Certificate renewal succeeded"
  else
    echo "Certificate renewal failed: $result"
  fi
    
  
  # Assuming NGINX service needs a reload after certificate updates
  sleep 1
  systemctl reload nginx.service


elif [ "$2" == "renew_cert" ] && [ "$3" == "cron" ]
then
  logtime=$(date +"%Y-%m-%dT%H:%M:%S")
  # Check if certificate within 30 days of expiring, or expired
  certbot certificates -d $1 | grep -E "^.*Expiry.*VALID:[^0-9]*(((2[0-9]|1[0-9]|[0-9]) days)|EXPIRED).*$"
  if [ $? -eq 0 ]
  then
    # Valid for 29 days or less, try and renew
    echo "$logtime" >> $FM_TASK_LOG
    echo "Certificate $1 ready for renewal" >> $FM_TASK_LOG
    cat ${FM_CONFIGS}/private/certbot_${1} | while mapfile -t -n 3 blocks && ((${#blocks[@]}))
    do
      export CB_FTPSRV=${blocks[0]}
      export CB_FTPUSR=${blocks[1]}
      export CB_FTPPAS=${blocks[2]}
      certbot renew --cert-name ${1} -c ${FM_CONFIGS}/certbot_cli.ini --non-interactive >> $FM_TASK_LOG 2>&1
    done
    echo "" >> $FM_TASK_LOG
  fi


elif [ "$2" == "renew_cert" ]
then
  echo "title: Scheduling certificate renewal from LetsEncrypt"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  
  echo "Scheduling renewal for: ${1}"
  . ./helpers/cron.sh  
  cron_set "${FM_SCRIPTS}/common.sh 0 '${0}' ${1} ${2} cron" "daily" "${3}"
  
  echo "Saving credential details needed for scheduled task"
  echo "  filename: ${FM_CONFIGS}/private/certbot_${1}"
  echo "${5}" > ${FM_CONFIGS}/private/certbot_${1}
  echo "${6}" >> ${FM_CONFIGS}/private/certbot_${1}
  echo "${7}" >> ${FM_CONFIGS}/private/certbot_${1}

    
elif [ "$2" == "create_cert" ]
then
  echo "title: Requesting certificate from LetsEncrypt"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  echo "Requesting certificate for: ${1}"
  sed -i -r "s/^email = .*$/email = ${3}/" ${FM_CONFIGS}/certbot_cli.ini
  echo "Using the following pre-defined parameters:"
  cat ${FM_CONFIGS}/certbot_cli.ini | grep -vE '^(#.*|\s*)$' | sed 's/^/  /'
  echo ""
  export CB_FTPSRV="$4"
  export CB_FTPUSR="$5"
  export CB_FTPPAS="$6"
  
  certbot certonly -c ${FM_CONFIGS}/certbot_cli.ini --non-interactive -d ${1}

fi


