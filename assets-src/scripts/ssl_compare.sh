# --------------------------
# name: Certificate Comparison
# tags: Certificates
# info: Generates md5 sum to check which certificates go together
# -----------------------------

# Main Script
# -----------

echo "title: Certificate Comparison"
echo "note: |"
echo "  The table below shows the checksum for each certificate's modulus."
echo "  If the checksum is the same, then the certificates match."
echo "table:"
echo "  delimiter: ':'"
echo "  header: [Cert File,Checksum]"
echo "  data: |"
cd ${FM_SSL_ROOT}
for f in *
do
  if [ -r "$f" ]
  then
    case "$f" in
      *.crt )
        openssl x509 -noout -modulus -in $f | openssl md5 | sed "s%^(stdin)= %    $f:%"
        ;;
      *.key )
        openssl rsa -noout -modulus -in $f | openssl md5 | sed "s%^(stdin)= %    $f:%"
        ;;
      *.csr )
        openssl req -noout -modulus -in $f | openssl md5 | sed "s%^(stdin)= %    $f:%"
        ;;
    esac
  else
    echo "    $f:Cannot read this file, check permissions for user ${FM_USER}"
  fi
done
cd - >/dev/null 2>&1
