# -----------------------------
# name: Folder Permissions
# tags: Filesystem
# info: Enforce folder ownership and permissions on specific locations
# ------------------------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Folder Permissions"
  echo "display:"
  echo "  - note: |"
  echo "      Permission sets are files that can be used to define what owners"
  echo "      and permissions files in various locations on the system should"
  echo "      have. Each set can define a number of different states (must "
  echo "      include a state called default) each with different permissions."
  echo "      here you can edit the folders and their permissions and switch the"
  echo "      permissions between the available states."
  echo "  - note: |"
  echo "      The 'defaul' permissions should be a restrictive and secure set, if"
  echo "      a more relaxed set of permissions is needed at times then this should"
  echo "      be defined under a new state name other than default."
  echo "  - note: |"
  echo "      Please use separate"
  echo "      sets (files) where possible for more granular control of permissions."
  echo "  - table:"
  echo "      header: [Permission Set]"
  echo "      delimiter: ','"
  echo "      actions: per_row" 
  echo "      data: |"
  cd ${FM_CONFIGS}
  # use find to ensure nothing listed if no match
  for f in $(find ./ -type f -name '*.perms.ini')
  do 
    actions='~edit.!delete'
    # get states value (must be . separated to fit actions format)
    states=`cat ${f} | grep '^states = ' \
                     | sed -r 's/^states.=.([A-Za-x0-9.]+)[[:space:]]*$/\1/'`
    actions="default.${states}.${actions}"
    # ${f#*/} strips path info from find result
    echo "        ${f#*/},${actions}"
  done
  cd - >/dev/null
  echo "  - form:"
  echo "      title: New Permission Set"
  echo "      submit: Make Set"
  echo "      fields:"
  echo "        - label: Name"
  echo "          info: No need to add .perms.ini to the name, this will be added automatically"
  echo "          type: text"
  echo "          pattern: '[\w_-\.]+'"
  echo "  - form:"
  echo "      title: Permission Reset"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: cron"
  echo "        - label: Frequency"
  echo "          select: [now,daily]"
  echo "        - label: Time (hh:mm)"
  echo "          type: text"
  echo "      submit: Reset"
  echo "  - note: |"
  echo "      The permission reset will reset all permission sets listed above"
  echo "      to the default permissions. This can be a time consuming process"
  echo "      that may slow down the system due to the volume of file operations"

elif [ $# -eq 1 ]
then
  if [ "${1%.perms.ini}" != "$1" ]
  then  # ends with .perms.ini leave it
    filename="$1"
  else  # no perms.ini add it to the end
    filename="${1}.perms.ini"
  fi
  echo "title: Editing Permission Set ${filename}"
  echo "note: |"
  echo "  Edit permissions set file. Each set must have a [ section name ] along"
  echo "  with path, default, and include values at a minimum. If states other"
  echo "  than default are defined, then a states entry is required listing "
  echo "  other states separated by a ."
  echo "  into separate sets (files) so that changes and be configured and "
  echo "  applied in a more granular way."
  echo "edit:"
  echo "  filename: ${filename}"
  echo "  mode: ini"
  echo "  data: |"
  { cat ${FM_CONFIGS}/${filename} || echo ""; } 2>/dev/null | sed 's/^/    /'

elif [ "$2" == 'save' ]
then
  echo "title: Saving Permissions Set ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  cp ${FM_SCRIPT_PARAM}_3 ${FM_CONFIGS}/${1} 2>/dev/null   # save file
  echo "Saved ${1}"
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting Permissions Set ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  # try the delete and handle failure with neat yaml compatible message
  { rm -Rd ${FM_CONFIGS}/${1} || echo "ERROR: Delete failed."; } 2>/dev/null
  echo "${1} Deleted."

elif [ $# -eq 2 ] && [ -f "${FM_CONFIGS}/${1}" ]
then
  echo "title: Setting permissions to ${2}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  echo "Switching to state ${2} for set ${1}"
  ./helpers/sys_folderperms.sh $2 $1

elif [ "$1" == "cron" ]
then
  if [ "$2" == "now" ]
  then
    for f in $(find ${FM_CONFIGS} -type f -name '*.perms.ini')
    do 
      echo "Processing $f" >> ${FM_TASK_LOG}
      f=`basename $f`
      ./helpers/sys_folderperms.sh default $f
    done
  elif [ "$2" == "scheduled" ]
  then
    date "+%Y-%m-%d %H:%M" >> ${FM_TASK_LOG}
    echo "Setting permissions to default" >> ${FM_TASK_LOG}
    for f in $(find ${FM_CONFIGS}/ -type f -name '*.perms.ini')
    do
      echo "Processing $f" >> ${FM_TASK_LOG}
      f=`basename $f`
      ./helpers/sys_folderperms.sh default $f >> ${FM_TASK_LOG}
    done
    echo "------------------" >> ${FM_TASK_LOG}
    echo "" >> ${FM_TASK_LOG}
    echo "" >> ${FM_TASK_LOG}
  else
    # import cron helper functions
    . ./helpers/cron.sh
    echo "title: Cron Entry"
    echo "method: popup"
    echo "text: |"
    cron_set "${FM_SCRIPTS}/common.sh 0 '${0}' cron scheduled" "${2}" "${3}" | sed 's/^/  /'
    echo "  Scheduled"
    echo "category: CHANGE"
  fi
fi
