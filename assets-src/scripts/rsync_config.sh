# ----------------------
# name: Configuration
# tags: Rsync
# info: Rsync Daemon configuration
# -------------------------

# Main Script
# ------------------

FM_RSYNCD_CONF="/usr/local/etc/rsync/rsyncd.conf"

if [ ! -f $FM_RSYNCD_CONF ]
then
  echo "title: Rsync Daemon"
  echo "text: Rsync Daemon not installed."
  
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Rsync Daemon Configuration"
  echo "display:"
  echo "  - note: |"
  echo "      This is where the Rsync Daemon (Service) can be configured. "
  echo "  - note: |"
  echo "      WARNING:"
  echo "      Rsync network traffic is not encrypted, except for the authentication"
  echo "      stage. However, there is a known brute force exploit on this encryption."
  echo "      For Rsync over SSH see Rsync: User configurations, and "
  echo "      SSH: User Configurations."
  echo "  - edit:"
  echo "      filename: ${FM_RSYNCD_CONF}"
  echo "      mode: ini"
  echo "      data: |"
  { cat ${FM_RSYNCD_CONF} || echo ""; } 2>/dev/null | sed 's/^/        /'
elif [ "$2" == 'save' ]
then
  echo "title: Saving Rsync Daemon Configuration"
  echo "method: popup"
  echo "category: CHANGE"
  echo "---"
  mkdir $(dirname ${FM_RSYNCD_CONF}) 2>/dev/null # make sure folder exists
  cp ${FM_SCRIPT_PARAM}_3 ${FM_RSYNCD_CONF} 2>/dev/null   # save file
  echo "Saved ${1}"
fi
