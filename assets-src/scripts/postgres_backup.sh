# -------------------
# name: Backup
# tags: PostgreSQL
# info: Lists PostgreSQL DBs for Backup
# ---------------------

# Check whether postgress installed and initialised
./helpers/postgres_check.sh || exit
./helpers/backup_check.sh || exit

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Databases"
  echo "table:"
  echo "  header: [Database, Owner]"
  echo "  actions: 'backup'"
  echo "  delimiter: '\s*\|\s*'"  # trims spaces between delimiter as well
  echo "  data: |"
  q="SELECT d.datname, u.usename FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid AND d.datistemplate = false);"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "${q}" | sed s'/^/   /'

  echo "form:"
  echo "  title: Schedule Database Backup"
  echo "  submit: Schedule Backup"
  echo "  fields:"
  echo "    - label: Database Name"
  echo "      delimiter: '\n'"
  echo "      select: |"
  q="SELECT d.datname FROM pg_database d WHERE d.datistemplate = false;"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "${q}" | sed s'/^/       /'
  echo "    - type: hidden"
  echo "      default: cron"
  echo "    - label: Frequency"
  echo "      select: daily"
  echo "      delimiter: '\s'"
  echo "    - label: Time (hh:mm)"
  echo "      type: date"

elif [ "$2" == "backup" ]
then
  echo "title: Backing Up Database ${1}"
  echo "method: popup"
  echo "comment: Processing postgres database ${1}"
  echo "category: BACKUP"
  echo "next_action: reload_pane"
  # register equivalent restore command
  DATETIMESTAMP=`date "+%Y%m%d_%H%M%S"`
  backupfile="${1}_${DATETIMESTAMP}.pgdump.gz"
  echo "restore: postgres_restore.sh restore_db ${backupfile} ${1}"
  # NOTE: Backup up stuff is not considered a system change
  #       no status = no log of this action
  echo "..."
  sudo -u ${FM_PSQL_SU} pg_dump ${1} | gzip > ${FM_BACKUP}/${backupfile} 
  #2>&1 | sed s'/^/  /'
  echo "Done."
  du -h ${FM_BACKUP}/${backupfile}

elif [ "$2" == "cron" ]
then
  # import cron helper functions
  . ./helpers/cron.sh
  sfn=`basename ${0}`
  echo "title: 'Scheduling Backup : ${1}'"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "---"
  cron_set "${FM_SCRIPTS}/common.sh 0 '${0}' '${1}' 'backup' " "${3}" "${4}"
  echo "Scheduled"
fi
