# -------------------
# name: Lighttpd Configuration
# tags: Lighttpd
# info: Edit main Lighttpd Configuration file
# ---------------------

# Settings
name='Lighttp Server'
app_name='lighttpd'
edit_mode=json
conf_path_1='/etc/lighttpd/lighttpd.conf'
conf_path_2='/etc/lighttpd/lighttpd.conf'

source ./helpers/generic_editconfig.sh

