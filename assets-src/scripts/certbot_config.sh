name='Certbot Configuration'   # used in titles
folder_path='/etc/flexmin'   # path of the configuration files to list
file_pattern="certbot_cli.ini"      # specifies what filenames to list as configuration files in path
file_ext=".ini"           # what extension to put on newly created files
edit_mode="ini"   # specify edit mode for ACE editor
create="yes"       # allow creating new configuration files
create_filename="certbot_cli"   # force a specific filename on the end user
# yaml="yes"         # is file a flexmin YAML that generates the real file?
# delete="yes"       # should we allow delete of existing files
edit="yes"         # should we provide an edit option
# primary_file='/etc/dnsmasq.conf'   # allow specify a file outside folder_path to include
list_headers="[Filename,Modified,Size]"  # headers for the list of matched files
list_format="%p;%Tc;%s\n"   # From find command's printf format options
templates_path=$FM_CONFIG_TMPLT
templates_filter="*.certbot_ini.template" 

# check certbot installed, show message and end script if not.
source ./helpers/certbot_check.sh

source ./helpers/generic_viewedit_list.sh

