

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
    echo "title: Disk Mount"
    echo "display:"
    #echo "  - title: Disk Partitions"
    #echo "  - code: |2"
    #lsblk -o UUID,NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL,MODEL | sed 's/^/      /'
    
    echo "  - table:"
    echo "      title: Mounted"
    echo "      header: [dev, mountpoint, type]"
    echo "      actions: ?m+details.unmount"
    echo "      second_col: 1"
    echo "      delimiter: ';'"
    echo "      data: |2"
    mount | grep -E "^/" | grep -v "/var/" | while read -r line
    do
      sed -r 's/^([^ ]+) on (.+) type ([^ ]+) \(([^)]+)\)[ ]*$/        \1;\2;\3/' <<< "$line"
    done
    
    echo "  - table:"
    echo "      title: Auto-mount list (fstab)"
    echo "      header: [dev, mountpoint, type]"
    echo "      delimiter: ';'"
    echo "      actions: ?fs+details.!fs+delete"
    echo "      second_col: 0"
    echo "      data: |2"
    cat /etc/fstab | grep -vE "^#" | while read -r line
    do
      sed -r 's/^([^ \t]+)[ \t]+([^ \t]+)[ \t]+([^ \t]+)[ \t]+([^ \t]+).*$/        \1;\2;\3/' <<< "$line"
    done

    echo "  - form:"
    echo "      fields:"
    echo "      - type: hidden"
    echo "        default: mount_all"
    echo "      submit: Mount All"
    
    echo "  - table:"
    echo "      title: Disk Identifiers"
    echo "      delimiter: ';'"
    echo "      second_col: 3"
    echo "      actions: ?automount"
    echo "      header: [UUID, Dev, Label, Type]"
    echo "      data: |2"
    blkid -o full | grep -v "/dev/loop" | while read -r line
    do
      dev=$(grep -oE '^[^:]+' <<< "$line")
      uuid=$(grep -oE ' UUID="([^"]+)' <<< "$line" | sed 's/ UUID="//')
      type=$(grep -oE ' TYPE="([^"]+)' <<< "$line" | sed 's/ TYPE="//')
      label=$(grep -oE ' LABEL="([^"]+)' <<< "$line" | sed 's/ LABEL="//')
      [ "$label" == "$line" ] && label=""
      echo "        $uuid;$dev;$label;$type"
    done

    echo "  - title: Add network mount"
    echo "  - form:"
    echo "      fields:"
    echo "      - label: Path"
    echo "        type: text"
    echo "      - type: hidden"
    echo "        default: add_mount_m"
    echo "      - label: Type"
    echo "        type: text"
    echo "        default: cifs"
    echo "      - label: Options"
    echo "        type: text"
    echo "        default: 'username=Anonymous,password=,ro,iocharset=utf8,file_mode=0744,dir_mode=0755'"
    echo "      - label: Mountpoint"
    echo "        type: text"
    echo "        info: 'Usual in the form /mnt/my_mount_folder'"
    echo "      submit: Add to automount list (fstab)"

    
    
elif [ $# -eq 1 ] && [ "$1" == "mount_all" ]
then
    echo "title: Mounting Drives"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "..."
    mount -a
    echo "Done"

elif [ $# -eq 3 ] && [ "$2" == "unmount" ]
then
    echo "title: Unmount Drive"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "---"
    echo "drive: $1"
    echo "mountpoint: $3"
    umount $3
    echo "Done."
    
elif [ "$2" == "m details" ]
then
    echo "title: Mount Details"
    echo "method: popup"
    echo "---"
    mount | grep -v "^[ ]*#" | grep "$1" | while read -r line
    do
      sed -r 's/^([^ ]+) on (.+) type ([^ ]+) \(([^)]+)\)[ ]*$/dev: \1\nmountpoint: \2\ntype: \3/' <<< "$line"
      echo "other:"
      sed -r 's/^[^(]+\(([^)]+)\)[ ]*$/\1/' <<< "$line" | sed -r 's/,/\n/g' | sed -r 's/^/  /'
    done

elif [ "$2" == "fs details" ]
then
    echo "title: FSTAB Details"
    echo "method: popup"
    echo "---"
    cat /etc/fstab | grep -v "^[ ]*#" | grep "$1" | while read -r line
    do
      sed -r 's/^([^ ]+)[ ]+([^ ]+)[ ]+([^ ]+)[ ]+([^ ]+).*$/dev: \1\nmountpoint: \2\ntype: \3/' <<< "$line"
      echo "other:"
      sed -r 's/^([^ ]+)[ ]+([^ ]+)[ ]+([^ ]+)[ ]+([^ ]+).*$/\4/' <<< "$line" | sed -r 's/,/\n/g' | sed -r 's/^/  /'
    done
    
elif [ "$2" == "fs delete" ]
then
    echo "title: Modifying fstab"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "..."
    echo "Removing entry for ${3}"
    grep -v "$3" /etc/fstab | uniq > ${FM_TASK_TMP}/fstab_new
    mv ${FM_TASK_TMP}/fstab_new /etc/fstab
    echo "Done"


elif [ $# -eq 3 ] && [ "$2" == "automount" ]
then
    echo "title: Add auto-mount (fstab) entry"
    echo "method: popup"
    if [ "$3" == "cifs" ]
    then
        # remote, networked mount
        path=""
        type="$3"
        options="username=Anonymous,password='',ro,iocharset=utf8,file_mode=0744,dir_mode=0755"
        mountpoint="/mnt/netdrive"
    else
        # local disk mount
        line=$(blkid -o full | grep "$1")
        dev=$(grep -oE '^[^:]+' <<< "$line")
        path="UUID="$(sed -r 's/^.*\s+UUID="([^"]+)".*$/\1/' <<< "$line")
        type=$(sed -r 's/^.*\s+TYPE="([^"]+)".*$/\1/' <<< "$line")
        label=$(sed -r 's/^.*\s+LABEL="([^"]+)".*$/\1/' <<< "$line")
        [ "$label" == "$line" ] && label=""
        options="defaults,auto,users,rw,nofail"
        [ "$type" == "ntfs" ] && options="${options},umask=000"
        [ "$type" == "vfat" ] && options="${options},umask=000"
        mountpoint="/mnt/usbdrive"
    fi
    #echo "UUID=${uuid} /mnt/usbdrive ${type} ${options} 0 0"
    echo "form:"
    echo "  fields:"
    echo "  - type: hidden"
    echo "    default: $1"
    echo "  - type: hidden"
    echo "    default: add_mount"
    echo "  - label: Path"
    echo "    type: text_long"
    echo "    pattern: '^(//[\w\d.]+/[\w\d.]+|^UUID=[0-9a-fA-F-]+)'"
    echo "    default: '${path}'"
    echo "  - label: FS Type"
    echo "    type: text"
    echo "    default: ${type}"
    echo "  - label: Mount Options"
    echo "    type: text_long"
    echo "    default: ${options}"
    echo "  - label: Mountpoint"
    echo "    type: text"
    echo "    default: ${mountpoint}"
    echo "    pattern: '^/mnt/.+'"
    echo "  submit: Create Automount Entry"

elif [ "$2" == "add_mount" ]
then
    echo "title: Modifying fstab"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "..."
    echo "Path: $3"
    echo "Type: $4"
    echo "Options: $5"
    echo "Mountpoint: $6"
    if [[ $(grep " ${6} " /etc/fstab) ]]
    then
      echo ""
      echo "Aborted: Mountpoint already in use"
      exit 1
    fi
    if [[ $(grep "${3}" /etc/fstab) ]]
    then
      echo ""
      echo "Aborted: Entry already exists for this path in fstab."
      exit 1
    else
      echo "Adding entry:"
      echo "${3} ${6} ${4} ${5} 0 0" >> /etc/fstab
      echo "Making sure mountpoint exists"
      mkdir -p ${6}
      [ $? -eq 0 ] && echo "Succeeded" || echo "Failed"
    fi

elif [ "$2" == "add_mount_m" ]
then
    # Manual 1st entry from form
    echo "title: Modifying fstab"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "..."
    echo "Path: $1"
    echo "Type: $3"
    echo "Options: $4"
    echo "Mountpoint: $5"
    if [[ $(grep "${1}" /etc/fstab) ]]
    then
      echo "Aborted: Entry already exists for this disk in fstab."
    else
      echo "Adding entry:"
      echo "${1} ${5} ${3} ${4} 0 0" >> /etc/fstab
      echo "Making sure mountpoint exists"
      mkdir -p ${5}
      [ $? -eq 0 ] && echo "Succeeded" || echo "Failed"
    fi
fi

