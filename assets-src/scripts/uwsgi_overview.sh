# -------------------------------
# name: List Configurations
# tags: NGINX
# info: Lists available-site config files for selection and viewing.
# ------------------------------

# Main Script
# --------------

# set up 
source ./helpers/uwsgi_check.sh
    
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then

  # ask user to select a file
  echo "title: uWSGI Overview"
  echo "display:"

  # -------- Primary Configurations ------------
  echo "  - table:"
  echo "      title: Primary Configurations"
  echo "      header: [Configuration, My Path!0]"
  echo "      delimiter_row: '\n'"
  echo "      delimiter: ';'"
  echo "      actions: per_row"
  echo "      data: |"
  for uc in `find ${FM_UWSGI_ROOT} -mindepth 1 -maxdepth 1 -type f -name "*"`
  do
    logfile=$(grep -r "^logto" ${uc} | sed 's/^logto\s*=\s*//')
    if [ ! -z "${logfile}" ]
    then
      echo "        ${uc/$FM_UWSGI_ROOT\//};;!delete.?view.?view_log"
    else
      echo "        ${uc/$FM_UWSGI_ROOT\//};;!delete.?view"
    fi
  done
  
  # -------- Optional Configurations ------------
  if [ "$UWSGI_AVAIL" != "$UWSGI_ENABLED" ]  # using available/enabled pattern
  then
    echo "  - table:"
    echo "      title: Optional Configurations"
    echo "      header: [Configuration, Path!0, Status*]"
    echo "      second_col: 1"
    echo "      delimiter_row: '\n'"
    echo "      delimiter: ';'"
    echo "      actions: per_row"
    echo "      data: |"
    for uc in `find ${UWSGI_AVAIL} -mindepth 1 -maxdepth 1 -name "*"`
    do
      logfile=$(grep -r "^logto" ${uc} | sed 's/^logto\s*=\s*//')
      if [ ! -z "${logfile}" ]
      then
        actions="!delete.?view.?view_log"
      else
        actions="!delete.?view"
      fi
      #uw_inc=$(grep -vE '^\s*#' ${uc} | grep -o -E 'include[ ]*[^\;]*' | grep -v "_params" | sed 's/include\s*//')
      if [ -f ${UWSGI_ENABLED}${uc/$UWSGI_AVAIL/} ]
      then
        echo "        ${uc/$UWSGI_AVAIL\//};/${UWSGI_AVAIL/$FM_UWSGI_ROOT\//};enabled*green;reload.disable.${actions}"
      else
        echo "        ${uc/$UWSGI_AVAIL\//};/${UWSGI_AVAIL/$FM_UWSGI_ROOT\//};disabled*red;enable.${actions}"
      fi
    done
  fi
  
  # -------- Scoket Configuration Info ------------
  echo "  - table:"
  echo "      title: uWSGI Socket Configuration"
  echo "      header: [Config File, Socket]"
  echo "      delimiter: ';'"
  echo "      data: |"
  grep -rw -E "^[\t\s]*socket" ${FM_UWSGI_ROOT}/ \
       | sed "s|$FM_UWSGI_ROOT/||" \
       | sed -r 's/\:[\t\s]*socket[ ]*=[ ]*/;/' | sed 's/^/        /'
       
elif [ "$2" == "view_log" ]
then
  echo "title: View Log (Last 150 lines)"
  echo "method: popup"
  echo "---"
  if [ -f "${FM_UWSGI_ROOT}/${1}" ]
  then
    logfile=$(grep -r "^logto" "${FM_UWSGI_ROOT}/${1}" | sed 's/^logto\s*=\s*//')
  elif [ -f "${UWSGI_AVAIL}/${1}" ]
  then
    logfile=$(grep -r "^logto" "${UWSGI_AVAIL}/${1}" | sed 's/^logto\s*=\s*//')
  fi
  if [ -r ${logfile} ]
  then
      tail -n 150 ${logfile}
  else
      echo "ERROR: cannot read ${logfile}"
  fi   
  
elif [ "$2" == "enable" ]
then
  # 1=name 2=enable
  echo "title: Enabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  ln -s ${UWSGI_AVAIL}/${1} ${UWSGI_ENABLED}/${1}
  echo "Enabled"
  
elif [ "$2" == "disable" ]
then
  # 1=name 2=disable
  echo "title: Disabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  rm ${UWSGI_ENABLED}/${1}
  echo "Disabled"

elif [ "$2" == "reload" ]
then
  # 1=name 2=disable
  echo "title: Reloading ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  # echo "Closing vassal by disabling it"
  # rm ${UWSGI_ENABLED}/${1}
  # sleep 2
  echo "Restarting vassal by updating it's configuration file"
  touch ${UWSGI_AVAIL}/${1}
  sleep 1
  # ln -s ${UWSGI_AVAIL}/${1} ${UWSGI_ENABLED}/${1}
  echo "Done"
  
elif [ "$2" == "delete" ]
then
  # 1=name,2=delete 
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  # remove link file and original
  echo "---"
  echo "Disabling and deleting"
  [ -f ${UWSGI_ENABLED}/${1} ] && rm ${UWSGI_ENABLED}/${1}
  rm ${UWSGI_AVAIL}/${1}
  echo "Done."
  
elif [ "$2" == "view" ]
then
  # 1=view 2=type 3=name
  echo "title: View Configuration - ${1}"
  echo "method: popup"
  echo "---"
  if [ -r ${FM_UWSGI_ROOT}$3/$1 ]
  then
      cat ${FM_UWSGI_ROOT}$3/$1
  else
      THIS_USER=$(whoami)
      echo "ERROR: ${THIS_USER} does not have permission to open the file."
  fi   
fi
