# ----------------------
# name: Configuration
# tags: Web2py
# info: Main web2py configuration
# -------------------------

# Main Script
# ------------------

if [ -z  ${FM_WEB2PY_ROOT} ]
then
  echo "title: Web2py"
  echo "text: FM_WEB2PY_ROOT is not defined, please define this in Flexmin parameters."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -f ${FM_WEB2PY_ROOT}/VERSION ]
then
  echo "title: Web2py"
  echo "display:"
  echo "  - text: |"
  echo "      Web2py Version:"
  cat ${FM_WEB2PY_ROOT}/VERSION | sed 's/^/      /'
  echo "  - form:"
  echo "      title: Reset Web2py Password"
  echo "      submit: Set Password"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: reset_pw"
  echo "        - label: New Password"
  echo "          type: password"
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  # Web2Py not installed, show form for install options
  echo "title: Web2py Installation"
  echo "display:"
  echo "  - text: |"
  echo "      Web2Py location: ${FM_WEB2PY_ROOT}"
  echo "      Web2Py is not installed, select install source below."
  echo "  - form:"
  echo "      long: yes"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: install"
  echo "        - label: Source"
  echo "          info: HTTP/FTP download url for zip file"
  echo "          type: text"
  echo "          default: ${FM_REMOTE_SOURCE}/web2py_[version].zip"
  echo "        - label: Version (e.g. 2.9.10)"
  echo "          type: text"
  echo "          default: 2.9.10"
  echo "        - label: Ownership"
  echo "          info: user:group ownership as used by chown command"
  echo "          type: text"
  echo "          default: root:www"
  echo "        - label: Permission"
  echo "          info: Permission in the form rwxrw-r-- as used by chmod"
  echo "          type: text"
  echo "          default: rwxrwxr--"
  echo "      submit: Download and Install"
fi

if [ "$1" == "reset_pw" ]
then
  # reset wen2py admin console password
  echo "title: Changing web2py admin password"
  echo "method: popup"
  echo "category: CHANGE"
  echo "---"
  cd ${FM_WEB2PY_ROOT}
  python <<END
import gluon.main
gluon.main.save_password("${2}",443)
END
  echo "Changed"
  
  
elif [ "$1" == "install" ]
then
  # Install Web2Py
  echo "title: Installing Web2py"
  echo "method: popup"
  echo "category: INSTALL"
  echo "next_action: reload_pane"
  echo "---"
  if [ "${2%%/*}" == "." ]  # if text up to first / is .
  then
    SRC=${FM_REMOTE_SOURCE}/${2#*/}   # add default remote source to specified source
  else
    SRC=$2
  fi
  SRC=`echo $SRC | sed "s/\[version\]/${3}/"`
  echo "Installing from ${src}"
  echo "Downloading to temporary file ${FM_TASK_TMP}/${FM_TASKID}.zip"
  ZIPFILE=${FM_TASK_TMP}/${FM_TASKID}.zip  # destination for zip file download
  fetch -o $ZIPFILE $SRC  # download zip file
  if [ $? -ne 0 ]
  then
    echo "Download failed, aborting."
  else
    # convert mod string to number
    modint=`echo $5 | sed -e 's/rwx/7/g' -e 's/rw-/6/g' -e 's/r-x/5/g' -e 's/r--/4/g' -e 's/-wx/3/g' -e 's/-w-/2/g' -e 's/--x/1/g' -e 's/---/0/g'` 
    mkdir ${FM_WEB2PY_ROOT}  # make sure folder exists
    chown ${4} ${FM_WEB2PY_ROOT} # give correct ownership
    chmod ${modint} ${FM_WEB2PY_ROOT}  # give correct perms
    if [ -d ${FM_WEB2PY_ROOT} ]
    then
      unzip -q -d ${FM_WEB2PY_ROOT} $ZIPFILE
      cd ${FM_WEB2PY_ROOT}
      echo "Changing ownership"
      chown ${4} `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+' | cut -w -f5`
      echo "Changing permissions"
      # chmod files and folders
      chmod ${modint} `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+.*$' | cut -w -f5`  # | grep -E '[ ]+[0-9]+.*[^\/]$' | cut -w -f5`
      # folders need x access
      # chmod modify folders only (only paths ending with /) 
      chmod ugo+x `unzip -l -q $ZIPFILE | grep -E '[ ]+[0-9]+.*[\/]$' | cut -w -f5`
      
      # Move contents of extracted subfolder into web2py root folder
      folder=$(dirname `find ${FM_WEB2PY_ROOT} -name "VERSION"`)
      mv ${folder}/* ${FM_WEB2PY_ROOT}/
      
    else
      echo "Destination folder does not exist, aborting."
    fi
  fi
  if [ -f $ZIPFILE ]
  then
    rm $ZIPFILE
  fi
fi
