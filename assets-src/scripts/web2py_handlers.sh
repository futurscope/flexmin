# ----------------------
# name: Handlers
# tags: Web2py
# info: Main web2py configuration
# -------------------------

# Main Script
# ------------------

if [ -z  ${FM_WEB2PY_ROOT} ]
then
  echo "title: Web2py"
  echo "text: FM_WEB2PY_ROOT is not defined, please define this in Flexmin parameters."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -f ${FM_WEB2PY_ROOT}/VERSION ]
then
  echo "title: Web2py Handlers"
  echo "display:"
  echo "  - note: |"
  echo "      web2py handlers are used to integrate web2py into other web server"
  echo "      frameworks. See web2py site documentation for more details."
  echo "  - table:"
  echo "      delimiter: '\s+'"
  echo "      actions: per_row"
  echo "      header: [Handler, Status]"
  echo "      data: |"
  cd ${FM_WEB2PY_ROOT}/handlers
  for h in *handler.py
  do
    if [ -f ../${h} ]
    then
      echo "        ${h} ready*green disable"
    else
      echo "        ${h} available*red enable"
    fi
  done
  cd -  # switch back to original folder
else
  echo "title: Web2py"
  echo "text: Web2py does not seem to be installed."
fi

# Actions
# -------

if [ "$1" == "enable" ]
then
  # enable handler
  echo "title: Enabling ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  cp ${FM_WEB2PY_ROOT}/handlers/${2} ${FM_WEB2PY_ROOT}/
  echo "Enabled"
elif [ "$1" == "disable" ]
then
  # disable handler
  echo "title: Enabling ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  rm ${FM_WEB2PY_ROOT}/${2}
  echo "Disabled"
fi
