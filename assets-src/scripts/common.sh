#!/bin/bash

if [ $# -eq 0 ]
then
  echo "Usage:"
  echo "  ./common.sh taskid scriptname scriptparameters"
  exit 1
fi

# ----------------------------------
# General Flexmin parameters:
# These parameters are also made available to python in
# flexmin_srv.py class TaskServer init function. Update that
# location also if these are changed or added to.
# ---------------------------------------------
export FM_SCRIPTS=$(dirname $0)
servicedir=$(dirname ${FM_SCRIPTS})
export FM_SERVICE_ROOT="$servicedir"

# look in standard FreeBSD application configuration folder first
[ -z "${FM_CONFIGS}" ] && [ -d "/usr/local/etc/flexmin/" ] && export FM_CONFIGS="/usr/local/etc/flexmin"

# then look in a more linux like path for configs
[ -z "${FM_CONFIGS}" ] && [ -d "/etc/flexmin/" ] && export FM_CONFIGS="/etc/flexmin"

# if .usr/local/etc/flexmin does not exist then use configs folder within flexmin app folder/
[ -z "${FM_CONFIGS}" ] && export FM_CONFIGS="${servicedir}/configs"

# get Flexmin parameters from FM_CONFIGS path
#cat "${FM_CONFIGS}/flexmin.conf" | grep -E ^[A-Z]+.*=.* | \
#while read line
#do
#  export $line
#done
set -o allexport
source "${FM_CONFIGS}/flexmin.conf"
source "${FM_CONFIGS}/flexmin.local.conf" >/dev/null 2>&1
set +o allexport

[ -z "${FM_TASK_DATA}" ] && export FM_TASK_DATA="${FM_LOGROOT}/task_data"
[ -z "${FM_DEFAULT_CONFIGS}" ] && export FM_DEFAULT_CONFIGS="${FM_CONFIGS}/config-defaults"


# ----------------------------------
# Task specific parameters:
# These parameters are also made available to python in
# flexmin_srv.py class Task init function. Update that
# location also if these are changed or added to.
# --------------------------------------

export FM_SCRIPT=`basename $2 | sed 's/\.sh$//'`

# assume parameter files in flexmin application's private task data folder for now
export FM_PARAMS="${FM_TASK_DATA}/task${1}"

# look for task??? folder in the flexmin tmp package? folder
for p in $(find ${FM_TASK_TMP}/ -type d -path */package*/task${1})
do
  # Use parameters from task tmp folder if they exist here.
  # This allows the unzipping of a package containing a task log and task
  # parameter files to the FM_TASK_TMP folder and re-running a set of tasks
  export FM_PARAMS="${p}"
done

export FM_TASKID=$1
export FM_SCRIPT_PARAM=${FM_PARAMS}/${FM_SCRIPT}_param
export FM_USER=$(whoami)
# -------------------------

# Carry out tasks common for all scripts here
# -------------------------------------------
# get scriptname and then base (without .sh extension)
scriptname=$(basename "$2")
FM_SCRIPTLOG="${scriptname%.*}.log"
export FM_TASK_LOG="${FM_LOGROOT}/${FM_SCRIPTLOG}"

# Advertise existence of log for this task if it exists
#if [ -f ${TASK_LOG} ]
#then
#  echo "logfile: ${FM_SCRIPTLOG}"
#fi

# switch to correct folder and run specified script
# --------------------------------------------------
cd ${FM_SCRIPTS}
this_script=$2
shift  # remove first parameter (taskid)
shift  # remove next parameter (script name)
$this_script "$@"  # call specified script with remaining parameters
