# ----------------------
# name: Users
# tags: Rsync
# info: Rsync User Administration
# -------------------------

# Main Script
# ------------------

# Make sure parameters defined, with default value if not defined elsewhere
[ -z $FM_RSYNCD_SECRETS ] && FM_RSYNC_SECRETS="/usr/local/etc/rsync/rsyncd.secrets"

if [ ! -f $FM_RSYNCD_CONF ]
then
  echo "title: Rsync Users"
  echo "text: Rsync Daemon not installed."
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Rsync Users"
  echo "display:"
  echo "  - note: |"
  echo "      This is where Rsync users can be added to the Rsync secrets file."
  echo "      Rsync user username and passwords are stored here in plain text."
  echo "      In the default path only root user and wheel groups should have"
  echo "      access to read or write to this file."
  echo "  - form:"
  echo "      title: Create Rsync User"
  echo "      submit: Create User"
  echo "      fields:"
  echo "        - label: Username"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Password"
  echo "          type: password"
  echo "    table:"
  echo "      header: [User]"
  echo "      actions: 'reset_pw.!delete'"
  echo "      data: |"
  if [ -f ${FM_RSYNCD_SECRETS} ]
  then
    cat ${FM_RSYNCD_SECRETS} | sed -e 's/:.*$//' -e 's/^/        /'
  fi
elif [ "$2" == 'reset_pw' ]
then
  echo "title: Reset Password - ${1}"
  echo "method: popup"
  echo "form:"
  echo "  submit: Change Password"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: change_pw"
  echo "    - type: hidden"
  echo "      default: ${1}"  # username
  echo "    - label: Password"
  echo "      type: password"
  echo "    - label: Retype Password"
  echo "      type: password"
  
elif [ "$2" == "change_pw" ]
then
  echo "title: Changing Password - ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  if [ "$3" == "$4" ]
  then
    secrets=`cat ${FM_RSYNCD_SECRETS} | sed "s/^${1}:.*$/${1}:${3}/"`
    echo "${secrets}" > ${FM_RSYNCD_SECRETS}
    echo "Password changed"
  else
    echo "Error - passwords did not match"
  fi
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting - ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "..."
  secrets=`cat ${FM_RSYNCD_SECRETS} | grep -v "^${1}:"`
  echo "${secrets}" > ${FM_RSYNCD_SECRETS}
  echo "${1} deleted."
  
elif [ "$2" == 'create' ]
then
  echo "title: Adding Rsync User"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "---"
  
  # make sure file exists
  if [ ! -f ${FM_RSYNCD_SECRETS} ]
  then
    touch ${FM_RSYNCD_SECRETS}
    chown root:wheel ${FM_RSYNCD_SECRETS}  # assuming rsyncd will run as root
    chmod 600 ${FM_RSYNCD_SECRETS}         # only root has any access
  fi
  
  if cat ${FM_RSYNCD_SECRETS} | grep -q "^${1}:" >/dev/null
  then
    echo "Aborting, user ${1} already exists."
  else
    echo "${1}:${3}" >> ${FM_RSYNCD_SECRETS}
    echo "User ${1} added."
  fi
fi
