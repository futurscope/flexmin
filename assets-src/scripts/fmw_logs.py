# --------------------
# name: General Task History
# tags: FlexPac
# info: Log of all tasks completed via Flexmin
# ------------------------------------------------------------------------------
# -*- coding: utf-8 -*-

# 'Long' task based version of fmw_logs.py in the Flexmin application modules folder
# This is a special case of a script that need to run as a long task outside
# web2py and within web2py.

# -*- coding: utf-8 -*-
import os, shutil, yamlconfigs, time, zipfile

def run(vars,fm_vars,lines=None):
    if vars[0] == 'package':
        # vars[1] is list of task ids
        # vars[2] is filename
        # vars[3] (texbox therefore stored as a file) is description
        # do action
        if lines == None:
            retval = True  # return text
            lines = []
        else:
            retval = False # output list supplied, just append to that
        lines.append("title: Package Actions")
        lines.append("method: popup")
        lines.append("next_action: reload_pane")
        lines.append("...")  # end of yaml content
        descriptpath = fm_vars['SCRIPT_PARAM'] + '_4'
        #lines.append(fm_vars['FMW_SHORT_OUT'])
        taskids = vars[1].split()
        lines.append(vars[0] + " " + str(taskids))
        if len(vars) > 2:
            package = vars[2] + ".flexpac.zip"
        else:
            package = "package" + str(fm_vars['FM_TASKID']) + ".zip"
        zfile = os.path.join(fm_vars['FM_BACKUP'],package)
        zf = zipfile.ZipFile(zfile,"w")
        tmptasks = os.path.join(fm_vars['FM_TASK_TMP'],"task" + str(fm_vars['FM_TASKID']),'tasks') 
        with open(tmptasks,"w") as tt:
            tt.write(fm_vars['FMW_SHORT_OUT'])
        zf.write(tmptasks,'tasks')  # write task command list
        zf.write(descriptpath,'readme.txt')  # write description
        os.remove(tmptasks)  # remove temporary file
        for t in taskids:
            taskpath = os.path.join(fm_vars['FM_TASK_DATA'],'task' + str(t))
            lines.append("Zipping: " + taskpath)
            if os.path.exists(taskpath):
                relpath = os.path.basename(os.path.normpath(taskpath))
                files = os.listdir(taskpath)
                for f in files:
                    fp = os.path.join(taskpath,f)
                    rp = os.path.join(relpath,f)
                    if os.path.isfile(fp):
                        lines.append("Zipping: " + rp)
                        zf.write(fp,rp)
        lines.append("Closing zip file: " + zfile)
        zf.close()
        
        
    elif vars[0] == 'delete':
        if lines == None:
            retval = True  # return text
            lines = []
        else:
            retval = False # output list supplied, just append to that
        lines.append("title: Delete Actions and Data")
        lines.append("method: popup")
        lines.append("next_action: reload_pane")
        lines.append("...")  # end of yaml content
        lines.append(fm_vars['FMW_SHORT_OUT']) # report on first bit
        taskids = vars[1].split()
        for t in taskids:
            taskpath = os.path.join(fm_vars['FM_TASK_DATA'],'task' + str(t))
            if os.path.exists(taskpath):
                shutil.rmtree(taskpath)
                lines.append("Deleting: " + taskpath)
            else:
                lines.append("Not exist: " + taskpath)
        
    else:
        lines.append("title: Long Log Task")
        lines.append("method: popup")
        lines.append("---")  # end of yaml content
        lines.append("No action taken.")
        
 
