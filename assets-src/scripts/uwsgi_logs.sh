# -------------------------------
# name: Edit Configurations
# tags: uWSGI
# info: Lists available uWSGI config files for editing
# ------------------------------

# Main Script
# --------------

if [ -z $FM_UWSGI_ROOT ]
then
  echo "title: uWSGI Configuration"
  echo "text: |"
  echo "  Error - FM_UWSGI_ROOT is not configured"
  exit 1
fi

name='uWSGI Logs'   # used in titles
folder_path='/var/log/uwsgi'   # path of the configuration files to list
file_pattern="*.log"      # specifies what filenames to list as configuration files in path
# file_ext=".nmconnection"           # what extension to put on newly created files
# edit_mode="ini"   # specify edit mode for ACE editor
# create="yes"       # allow creating new configuration files
# yaml="yes"         # is file a flexmin YAML that generates the real file?
delete="yes"       # should we allow delete of existing files
view_tail=100
list_title="Available Logs"
list_headers="[Log,Modified,Size]"
list_format="%P;%Tc;%s\n"
# edit="yes"         # should we provide an edit option

source ./helpers/generic_viewedit_list.sh
