# -----------------------------
# name: Import Key and Cert
# user: root
# tags: Certificates
# info: Import a private key and public certificate from pfx
# ------------------------------------

# Main Script
# -----------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Import Key and Certificate"
  echo "form:"
  echo "  submit: Import"
  echo "  fields:"
  echo "    - label: PFX File"
  echo "      type: upload"
  echo "    - label: Filename"
  echo "      type: text"
  echo "    - label: password"
  echo "      type: password"

else
  echo "title: 'Importing Key and Certificate : ${2}'"
  echo "method: popup"
  echo "category: CREATE"
  echo "..."
  # extract certificate to password protected private key
  openssl pkcs12 -in ${FM_SCRIPT_PARAM}_1 -nocerts -passin pass:"${3}" -passout pass:"${3}" -out ${FM_SSL_ROOT}/$2.tmp -nodes
  # remove password
  openssl rsa -in ${FM_SSL_ROOT}/$2.tmp -passin pass:"${3}" -out ${SSL_ROOT}/$2.key
  rm ${FM_SSL_ROOT}/$2.tmp
  openssl pkcs12 -in ${FM_SCRIPT_PARAM}_1 -nokeys -passin pass:${3} -out ${FM_SSL_ROOT}/$2.crt
  ls -all ${FM_SCRIPT_PARAM}* | sed 's/^/  /'
  ls -all ${FM_SSL_ROOT}/* | sed 's/^/  /'
fi
