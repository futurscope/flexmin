# -----------------------------
# name: User Configurations
# tags: Rsync
# info: List User Configurations for Rsync
# -----------------------------

# Main Script
# ---------------

if ! command -v rsync >/dev/null 2>&1
then
  echo "title: User Rsync Configurations"
  echo "text: Rsync not installed."
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: User Rsync Configurations"
  echo "note: |"
  echo "  User Rsync Configurations allow Rsync over SSH for that user login. "
  echo "  You must first ensure that the relevant user is permitted to connect"
  echo "  via SSH. Also, use the User SSH set up for key based access with no "
  echo "  passwords if desired."
  echo "table:"
  echo "  actions: [info,edit,'!delete']"
  echo "  header: [Path, User]"
  echo "  delimiter: ','"
  echo "  data: |"
  find /home/ -name "rsyncd.conf" -print 2> /dev/null | sed 's/\([^\/]*\)\/rsyncd\.conf/\1\/rsyncd.conf,\1/' | sed 's/^/    /'
  echo "form:"
  echo "  title: Create User Config"
  echo "  fields:"
  echo "    - label: User"
  echo "      delimiter: '\n'"
  echo "      select: |"
  cd /home/
  ls | sed 's/^/        /'
  cd -
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Rsync Configuration"
  echo "      type: textbox"
  echo "      default: |"
  { cat ${FM_DEFAULT_CONFIGS}/user_rsyncd.conf || echo ""; } | sed 's/^/        /'
  echo "  submit: Create User Rsync Config"

elif [ "$2" == "edit" ]
then
  echo "title: Editing Rsync Config ${1}"
  echo "method: popup"
  echo "note: |"
  echo "  This is the Rsync configuration file for an user account. Changes made "
  echo "  here will only affect connections made via SSH using this users login."
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: ini"
  echo "  data: |"
  { cat ${1} || echo ""; } 2>/dev/null | sed 's/^/    /'
  
elif [ "$2" == 'save' ]
then
  echo "title: Saving Rsync Config ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  mkdir $(dirname ${2}) 2>/dev/null # make sure folder exists
  cp ${FM_SCRIPT_PARAM}_3 ${2} 2>/dev/null   # save file
  echo "Saved ${2}"

elif [ "$2" == "delete" ]
then
  echo "title: Deleting User Rsync Config"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  echo "---"
  # try the delete and handle failure with neat yaml compatible message
  { rm -Rd ${1} || echo "ERROR: Delete failed."; } 2>/dev/null
  echo "${1} Deleted."
  
elif [ "$2" == "info" ]
then
  echo "title: Rsync Info"
  echo "method: popup"
  echo "---"
  cat ${1}
  
elif [ "$2" == "create" ]
then
  echo "title: Creating User Rsync Configuration"
  echo "method: popup"
  if [ -n "$1" ]
  then
      echo "next_action: reload_pane"
      echo "category: CREATE"
      echo "..."
      mkdir -p /home/${1}
      echo "Creating rsyncd.conf file"
      cp ${FM_SCRIPT_PARAM}_3 /home/${1}/rsyncd.conf
      chown ${1}:${1} /home/${1}/rsyncd.conf
      chmod 600 /home/${1}/rsyncd.conf
  else
      echo "text: No user selected! Please select a user."
  fi
fi
