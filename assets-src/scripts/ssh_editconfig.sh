# -----------------------------
# name: Service Configuration
# tags: SSH
# info: Edit SSHD Configuration
# -----------------------------

# Main Script
# ---------------
FM_SSHD_CONF=/etc/ssh/sshd_conf

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: SSHD Configuration"
  echo "display:"
  echo "  - note: |"
  if [ -f ${FM_SSHD_CONF} ]
  then
    contents=`cat ${FM_SSHD_CONF} | sed 's/^/        /'`
    echo "      This is your current SSHD configuration file"
  elif [ -f ${FM_DEFAULT_CONFIGS}/sshd_conf ]
  then
    echo "      This a default SSHD configuration file supplied by Flexmin."
    echo "      None of these settings are currently in force, so saving this"
    echo "      file may have unintended consequences. Please check and "
    echo "      understand these settings before saving."
    contents=`cat ${FM_DEFAULT_CONFIGS}/sshd_conf | sed 's/^/        /'`
  else
    echo "      No existing SSHD configuration found at ${FM_SSHD_CONF}."
    echo "      You might want to check your FM_SSHD_CONF setting is correct,"
    echo "      or add such a setting to Flexmin parameters if this path is not"
    echo "      the correct path for your system."
    contents=`echo "" | sed 's/^/        /'`
  fi
  echo "  - edit:"
  echo "      filename: ${FM_SSHD_CONF}"
  echo "      mode: ini"
  echo "      data: |"
  echo "${contents}"
  
elif [ "$2" == 'save' ]
then
  echo "title: Saving SSHD Configuration"
  echo "method: popup"
  echo "category: CHANGE"
  echo "---"
  mkdir $(dirname ${FM_SSHD_CONF}) 2>/dev/null # make sure folder exists
  cp ${FM_SCRIPT_PARAM}_3 ${FM_SSHD_CONF} 2>/dev/null   # save file
  echo "Saved ${1}"
fi
