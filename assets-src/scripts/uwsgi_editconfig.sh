# -------------------------------
# name: Edit Configurations
# tags: uWSGI
# info: Lists available uWSGI config files for editing
# ------------------------------

# Main Script
# --------------

if [ -z $FM_UWSGI_ROOT ]
then
  echo "title: uWSGI Configuration"
  echo "text: |"
  echo "  Error - FM_UWSGI_ROOT is not configured"
  exit 1
fi

source ./helpers/uwsgi_check.sh

name='uWSGI Configuration'
folder_path=$FM_UWSGI_ROOT
templates_path=$FM_CONFIG_TMPLT  # allows selection of a template file
templates_filter="uwsgi.*.template"
file_pattern="*.ini"
path_exclude="\w+-enabled/"
#filename_separate="yes"   # specify 'name' separately from the filename
#filename_pattern="(${UWSGI_AVAIL##*/}/\w+|\w+)"
file_ext=".ini"
edit_mode="ini"
create="yes"
create_info="In emperor mode most configurations would be in vassals or apps-available folder so ensure your filename begins with the suggested folder. Do not specify a file extension, ${file_ext} is added automatically"
create_default="${UWSGI_AVAIL##*/}/"
create_pattern="(${UWSGI_AVAIL##*/}/\w+|\w+)"
#yaml="yes"
edit="yes"
delete="yes"

source ./helpers/generic_viewedit_list.sh

       
