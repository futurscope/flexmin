# -----------------------------
# name: User Configurations
# tags: SSH
# info: List User Configurations for SSH
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: User SSH Configurations"
  echo "display:"
  echo "  - note: |"
  echo "      User SSH Configurations allow SSH connections for that user using"
  echo "      public key authentication instead of passwords. These configurations"
  echo "      hold the list of public keys (edit auth) of those authorised to "
  echo "      connect to SSH using key based access."
  echo "      They also hold the public keys of known hosts (edit known) which"
  echo "      are used to verify the identity of the host being connected to."
  echo "      These known hosts need to be added to root's list as most actions"
  echo "      are carried out by root, absence of the known host entry will"
  echo "      cause a rsync client action to fail."
  echo "  - note: |"
  echo "      The remote user needs to generate a public and private key pair"
  echo "      (SSH: User Keys), the public key"
  echo "      should be pasted into the authorized_keys file using the 'edit' option"
  echo "      below."
  echo "      WARNING: This is recommended only for local accounts with limited"
  echo "      access."
  echo "  - table:"
  echo "      actions: per_row"
  echo "      header: [User, Config Path]"
  echo "      delimiter: ','"
  echo "      data: |"
  for f in $(find /home/ /root/ -name ".ssh" -print 2> /dev/null) # \
                #    | sed 's/\([^\/]*\)\/\.ssh/\1\/.ssh,\1/')
  do
    act="~view"
    [ -f "${f}/authorized_keys" ] && act="${act}.edit_auth"
    [ -f "${f}/known_hosts" ] && act="${act}.edit_known"
    #act=${act#.}
    user=`echo "${f}" | sed -r 's/(\/home\/|\/)([^\/]*)\/\.ssh/\2/'`
    echo "        ${user},${f},${act}"
  done
                    #| sed 's/^/        /'
  echo "  - form:"
  echo "      title: Create User Config"
  echo "      fields:"
  echo "        - label: User"
  echo "          delimiter: '\n'"
  echo "          select: |"
  cd /home/
  ls | sed 's/^/            /'
  echo "            root"
  cd - >/dev/null
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Authorized Keys"
  echo "          type: textbox"
  echo "      submit: Create User SSH Config"

# ----------------------------------
# User Config Details 
# ----------------------------------
elif [ $# -eq 1 ]
then
  echo "title: User ${1}"
  echo "display:"
  if [ -f /home/${1}/.ssh/known_hosts ]
  then
    echo "  - table:"
    echo "      title: Known Hosts"
    echo "      actions: '?view_key.!delete_key'"
    echo "      object_col: 0"   # column 0 passed as parameter #1
    echo "      second_col: 1"   # column 1 passed as parameter #3
    echo "      header: [Name (Address), Key Type]"
    echo "      delimiter: ' '"
    echo "      data: |"
    cat /home/${1}/.ssh/known_hosts | sed -r 's/^([^ ]*\s[^ ]*).*$/\1/' | sed -r 's/^/        /'
    echo "  - form:"
    echo "      title: Add new host key"
    echo "      submit: Scan Host and Add Key"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: ${1}"
    echo "        - type: hidden"
    echo "          default: add_host_key"
    echo "        - label: Host Address"
    echo "          info: Host FQDN or IP Address"
    echo "          type: text"
  else
    echo "  - note: No known hosts file found for $1"
  fi
  if [ -f /home/${1}/.ssh/authorized_keys ]
  then
    echo "  - title: Authorized Keys"
    echo "  - text: |2"
    cat /home/${1}/.ssh/authorized_keys | sed 's/^/      /'
  fi


# ------------------------------------
# Table Actions  1=Col0 2=action 3=Col1
# ------------------------------------
elif [ "$2" == "view_key" ]  # $1 = <user>/<host_entry>
then
  host=${1#*/}
  user=${1%/*}
  echo "title: Key for host $host"
  echo "method: popup"
  echo "---"
  echo "user: ${user}"
  echo "host: ${host}"
  echo "key type: ${3}"
  echo "key:"
  cat /home/${user}/.ssh/known_hosts | grep "${host} ${3}" | sed -r 's/^([^ ]*\s[^ ]*)\s*([^ ]*)$/\2/' | sed -r 's/.{60}/&\n/g'

elif [ "$2" == "delete_key" ]  # $1 = <user>/<host_entry>
then
  host=${1#*/}
  user=${1%/*}
  echo "title: Key for host $host"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "---"
  echo "Deleting host entry $host for key type ${3}"
  sed -i "/$host $3/d" /home/${user}/.ssh/known_hosts
  echo "Deleted."
  # cat /home/${user}/.ssh/known_hosts | grep -v "${host} " | sed -r 's/^([^ ]*\s[^ ]*)\s*([^ ]*)$/\2/' | sed -r 's/.{60}/&\n/g'

# ----------------------------------------
# Form Actions
# ----------------------------------------

elif [ "$2" == "add_host_key" ]
then
  echo "title: Adding host ${3} to ${1}'s Known Host List"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  # [ "$1" == "root" ] && ssh-keyscan 192.168.1.78 >> /root/.ssh/known_hosts
  [ "$1" != "root" ] && ssh-keyscan ${3} >> /home/${1}/.ssh/known_hosts

elif [ "$2" == "edit_auth" ]
then
  echo "title: Editing authorized_key file for ${1}"
  echo "method: popup"
  echo "note: |"
  echo "  This is the authorized_keys file. Paste any extra public keys from "
  echo "  client machines here to allow remote connections without passwords."
  echo "edit:"
  echo "  filename: /home/${1}/.ssh/authorized_keys"
  echo "  mode: text"
  echo "  data: |2"
  { cat /home/${1}/.ssh/authorized_keys || echo ""; } 2>/dev/null | sed 's/^/    /'

elif [ "$2" == "edit_known" ]
then
  echo "title: Editing SSH Known Hosts ${1}"
  echo "method: popup"
  echo "note: |"
  echo "  This is the authorized_keys file. Paste any extra public keys from "
  echo "  client machines here to allow remote connections without passwords."
  echo "edit:"
  echo "  filename: /home/${1}/.ssh/known_hosts"
  echo "  mode: text"
  echo "  data: |"
  { cat /home/${1}/.ssh/known_hosts || echo ""; } 2>/dev/null | sed 's/^/    /'

elif [ "$2" == 'save' ]
then
  [ ! -z ${4} ] && filepath="${4}"
  echo "title: Saving ${filepath}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  if [ -z ${filepath} ]
  then
    echo "Filepath not found to save file"
  else
    mkdir $(dirname ${filepath}) 2>/dev/null # make sure folder exists
    cp ${FM_SCRIPT_PARAM}_3 ${filepath} 2>/dev/null   # save file
    echo "Saved ${filepath}"
  fi
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting User SSH Config"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  echo "---"
  # try the delete and handle failure with neat yaml compatible message
  { rm -Rd ${1} || echo "ERROR: Delete failed."; } 2>/dev/null
  echo "${1} Deleted."

elif [ "$2" == "info" ]
then
  echo "title: SSH Info"
  echo "method: popup"
  echo "---"
  cd ${1}
  echo "Authorized Keys"
  echo "---------------"
  [ -f authorized_keys ] && cat authorized_keys
  echo ""
  echo ""
  echo "Known Hosts"
  echo "---------------"
  cat known_hosts
  cd - >/dev/null

elif [ "$2" == "create" ]
then
  echo "title: Creating User SSH Configuration"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  mkdir -p /home/${1}/.ssh
  echo "Setting required ownership and permissions on /home/${1}"
  chown ${1}:${1} /home/${1}
  chmod 700 /home/${1}
  echo "Setting required ownership and permissions on /home/${1}/.ssh"
  chown ${1}:${1} /home/${1}/.ssh
  chmod 700 /home/${1}/.ssh
  echo "Creating authorized_keys file"
  cp ${FM_SCRIPT_PARAM}_3 /home/${1}/.ssh/authorized_keys
  chown ${1}:${1} /home/${1}/.ssh/authorized_keys
  chmod 600 /home/${1}/.ssh/authorized_keys
fi
