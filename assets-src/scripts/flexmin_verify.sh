# Verifies and adjusts Flexmin Parameters

if [ $# -eq 1 ] && [ "$1" == '~default~' ]
then
  echo "title: List Parameters"
  echo "table:"
  echo "  header: [Parameter, Value, Confirmed*]"
  echo "  delimiter: ';'"
  #echo "  show_sub: ['_','&nbsp;']"
  echo "  data: |"
  set | grep '^FM_' | while read -r line
  #cat /etc/flexmin/flexmin.conf | grep '^FM_' | while read -r line
  do
    name=${line%%=*}
    value=${line##*=}
    unquoted=$(eval echo $value)
    path=$unquoted
    #echo "path=$path"
    if [[ "$path" =~ ^/.* ]] 
    then
      [ -d $path ] && echo "    ${name};${value};folder exists*green" && continue
      [ -f $path ] && echo "    ${name};${value};file exists*green" && continue
      echo "    ${name};${value};path missing*red"
    else
      case $name in
        FM_PSQL)
          if command -v $unquoted >/dev/null 2>&1
          then
            echo "    ${name};${value};command*green"
          else
            echo "    ${name};${value};no command*red"
          fi
          ;;
        FM_PSQL_SU|FM_WEB_USER)
          if cat /etc/passwd | grep -E "^$unquoted:" >/dev/null 2>&1
          then
            echo "    ${name};${value};user exists*green"
          else
            echo "    ${name};${value};no user*red"
          fi
          ;;
        FM_SERVICE_PATTERN|FM_TEST|FM_REMOTE_SOURCE)
          echo "    ${name};${value};unverifiable*amber"
          ;;
        FM_TASKID|FM_SCRIPT_PARAM|FM_SCRIPT|FM_PARAMS|FM_TASK_LOG)
          # omit parameters that make no sense to verify within this task
          :
          ;;
        *)
          echo "    ${name};${value};unconfirmed*red"
      esac
    fi
  done
fi
