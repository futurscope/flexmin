# ----------------------
# name: Disk Space
# tags: Filesystem
# info: Shows used and free space on system disks.
# -------------------------

# Main Script
# ------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Disk Space"
  echo "table:"
  echo "  separate: Filesystem"
  echo "  delimiter: '\s+'"
  echo "  header: [Filesystem,Total (MB)!2,Used (MB)!2,Available (MB)!2,Used (%)!2,Mountpoint!1]"
  echo "  data: |"
  df -hm | sed '1d ; /^devfs /d ; s/^/    /'
  echo "form:"
  echo "  title: Schedule Disk Space Check"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: cron"
  echo "    - label: Frequency"
  echo "      select: daily"
  echo "      delimiter: '\s'"
  echo "    - label: Time (hh:mm)"
  echo "      type: text"
  echo "  submit: Schedule Check"

elif [ "$1" == "cron" ]
then
  # import cron helper functions
  . ./helpers/cron.sh
  echo "title: Cron Entry"
  echo "method: popup"
  echo "category: CHANGE"
  echo "---"
  cron_set "${FM_SCRIPTS}/common.sh 0 '${0}' log" "${2}" "${3}"
  echo "Scheduled"
  
else
  logtime=$(date +"%Y-%m-%dT%H:%M:%S")
  if [ ! -f ${FM_TASK_LOG} ]
  then
    echo "title: Disk Space History" > $FM_TASK_LOG
    echo "table:" >> $FM_TASK_LOG
    echo "  separate: Mountpoint" >> $FM_TASK_LOG
    echo "  graph: [Used (KB),Available (KB)]" >> $FM_TASK_LOG
    echo "  title: Mountpoint %separate%" >> $FM_TASK_LOG
    echo "  delimiter: '\s+'" >> $FM_TASK_LOG
    echo "  header: [DateTime,Filesystem,Total (KB),Used (KB),Available (KB),Used (%),Mountpoint]" >> $FM_TASK_LOG
    echo "  data: |" >> $FM_TASK_LOG
  fi
  # trim to last 29 days entries first, allows for 12 hours variation in job time
  awk -vDate=`date -d'now-29 days-12 hour-5 minute' +%Y-%m-%dT%H:%M:%S` '$1 > Date {print $0}' $FM_TASK_LOG >  $FM_TASK_LOG.tmp
  rm $FM_TASK_LOG
  mv $FM_TASK_LOG.tmp $FM_TASK_LOG
  # add todays entry to make a orund 30 days entries
  df -hk | grep -vE "(tmpfs |/dev/loop[0-9]|/run/media/)" | sed "1d ; /^devfs /d ; s/^/    ${logtime} /" >> $FM_TASK_LOG
fi
