# -----------------------------
# name: Create Key and Request
# tags: Certificates
# info: Create a new private key and matching certificate request.
# ------------------------------------

# Main Script
# -----------
THIS_SCRIPT=`basename $0`


if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Create Certificate Key and Request"
  echo "note: |"
  echo "  Please fill out all fields (using None, if field does not apply)."
  echo "  No / in any fields please."
  echo "form:"
  echo "  submit: Create Request"
  echo "  fields:"
  echo "    - label: CN (e.g. www.example.com)"
  echo "      type: text"
  echo "    - label: Country"
  echo "      type: text"
  echo "    - label: State"
  echo "      type: text"
  echo "    - label: Locality"
  echo "      type: text"
  echo "    - label: Organisation"
  echo "      type: text"
  echo "    - label: Organisation Unit"
  echo "      type: text"

else
  echo "title: Creating certificate key and request"
  echo "method: popup"
  echo "category: CREATE"
  echo "..."
  if [ -w ${FM_SSL_ROOT} ]
  then
    # Create private key and certificate
    # TODO check for existing file and abort or clear other matching filenames to
    # ensure no clash
    openssl req -out ${FM_SSL_ROOT}/$1.csr -new -sha256 -newkey rsa:2048 -nodes -keyout ${SSL_ROOT}/$1.key \
      -subj "/C=$2/ST=$3/L=$4/O=$5,/OU=$6/CN=$1"

    # show resulting certificate request
    cat ${FM_SSL_ROOT}/$1.csr

    # Set restricted permissions on new private keys and any existing keys
    # chown root:wheel ${FM_SSL_ROOT}/*.key
    chmod 400 ${FM_SSL_ROOT}/*.key
  else
    echo "ERROR: Permission to ${FM_SSL_ROOT} denied."
  fi
fi



