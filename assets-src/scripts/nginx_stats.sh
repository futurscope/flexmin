# -------------------------------
# name: Statistics (Not Ready)
# tags: NGINX
# info: Lists available-site config files for selection and viewing.
# ------------------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then

elif [ "$1" == "hour" ]
then
  hour=$2
  cat /var/log/nginx/server/access.log \
      | grep "09\/Jun\/2015:${hour}" \
      | cut -c26-42 | uniq -c | cut -w -f 2 \
      | py --ji -l \
          'str(min(l)) + " " +\
          str(numpy.percentile(l,25)) + " " +\
          str(numpy.percentile(l,50)) + " " +\
          str(numpy.percentile(l,75)) + " " +\
          str(max(l))'
fi
