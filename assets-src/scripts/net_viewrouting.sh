# -----------------------------
# name: Routing Configuration
# tags: Network
# info: Displays network routing information (netstat -r)
# -----------------------------

# Main Script
# ---------------

echo "title: Network Routing (Internet)"
echo "---"
ip route list

