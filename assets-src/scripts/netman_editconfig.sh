# Edit Network Adaptor Configuration

edit="yes"
edit_mode="ini"
create="no"
yaml="no"
delete="no"
network_srv='systemd-networkd.* dhcpcd.*'


# Check what network adaptor configuration method is being used
if command -v nmcli >/dev/null 2>&1
then
    # Network Manager being used
    name='Network Manager'
    folder_path='/etc/NetworkManager/system-connections'
    file_pattern="*.nmconnection"
    file_ext=".nmconnection"
else
    # Check with systemd to see what network management services might be running
    networkd=$(systemctl list-unit-files ${network_srv} --type=service --no-legend --no-pager --state enabled | sed -r 's/\s.*$//')
    if [ -z "$networkd" ]
    then
        echo "title: Network Management"
        echo "note: No network configuration method recognised"
        exit 1
    elif [ "$networkd" == "systemd-networkd.service" ]
    then
        # Configure for networkd configuration files
        name='Systemd Networkd'
        folder_path='/etc/systemd/network'
        file_pattern="*"
        file_ext=""
    elif [ "$networkd" == "dhcpcd.service" ]
    then
        # Configure for networkd configuration files
        name='Systemd dhcpcd'
        folder_path='/etc/'
        file_pattern="dhcpcd.conf"
        file_ext=".conf"
    fi
fi

source ./helpers/generic_viewedit_list.sh
