# -------------------------------
# name: Edit Configurations
# tags: NGINX
# info: Lists available NGINX config files for editing
# ------------------------------

# Generic Config File Select and Edit Script
# ------------------------------------------
# Includes option to create new configs from template, simply include 
# a templates_path definition and filter to select suitable templates
# templates will have {NAME} replaced with the name of the file provided


# name='uWSGI Logs'   # used in titles
# folder_path='/var/log/uwsgi'   # path of the configuration files to list
# file_pattern="*.log"      # specifies what filenames to list as configuration files in path
# file_ext=".nmconnection"           # what extension to put on newly created files
# edit_mode="ini"   # specify edit mode for ACE editor
# create="yes"       # allow creating new configuration files
# yaml="yes"         # is file a flexmin YAML that generates the real file?
# delete="yes"       # should we allow delete of existing files
# edit="yes"         # should we provide an edit option
# list_headers="[Filename,Modified,Size]"  # headers for the list of matched files
# list_format="%p;%Tc;%s\n"   # From find command's printf format options default is "%p\n"

name='NGINX Configurations'
folder_path=$FM_CONFIGS
templates_path=$FM_CONFIG_TMPLT  # allows selection of a template file
#templates_path=''
templates_filter="*.nginx.yaml.template"
prefix_option="yes"   # allow prefix to be chosen for filenames
prefix_info="If creating a location file for including within a site, select the prefix specific to the site's include directive. Will be used with name to replace {FILENAME} in the selected template."
file_pattern="*.nginx.yaml"
file_ext=".nginx.yaml"
edit_mode="yaml"
create="yes"
yaml="yes"
edit="yes"
delete="yes"

prefix_list=`grep -h "locations-enabled/" /etc/nginx/sites-available/* | sed -r "s/^.*locations-enabled\/([^*]+)\*\;/\1/"`


source ./helpers/generic_viewedit_list.sh
exit

# ---- Default Action ----
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then 
  echo "title: $name Configuration"
  echo "display:"
  echo "  - table:"
  echo "      title: Edit Configurations"
  echo "      header: [Configuration]"
  echo "      actions: ~edit.!delete"
  echo "      row_delimiter: '\s+'"
  echo "      data: |"
  find $config_path/ -name '*.nginx.yaml' | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/        /'
  
  echo "  - form:"
  echo "      title: Create new configuration"
  echo "      fields:"
  echo "        - label: Name"
  echo "          info: Name of configuration, will also be used in place of {NAME} in template file"
  echo "          pattern: '[a-zA-Z0-9_]+'"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  if [ ! -z $templates_path ]
  then
    echo "        - label: Using Template"
    echo "          selectformat: linesep"
    echo "          select: |"
    find ${templates_path}/ -name "${templates_filter}" | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/            /'
  fi
  echo "      submit: Create Configuration File"
       
# ---- Default Action on Selected Item ----
elif [ $# -eq 1 ]
then
  echo "title: Edit $name Configuration ${1}"
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: yaml"
  echo "  data: |"
  cat ${config_path}/${1} | sed 's/^/    /'
  
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "---"
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 ${config_path}/${1}
  flexmin yamlconfig ${1}
  echo "Saved"
       
elif [ "$2" == "create" ]  # 1=newfile 2=create 3=template
then
  echo "title: Creating ${1}"
  echo "method: popup"
  echo "category: CREATE"
  echo "next_action: reload_pane"
  echo "---"
  if [ -z $3 ]
  then
    touch ${config_path}/${1}${config_ext}
  else
    cp ${templates_path}/${3} ${config_path}/${1}${config_ext}
    sed -i "s/{NAME}/${1}/" ${config_path}/${1}${config_ext}
    # if this is a yaml file then assumes a flexmin yaml config and tries to process it
    if grep -q ".yaml" <<< "${config_ext}"
    then
      flexmin yamlconfig ${1}${config_ext}
    fi
  fi
  echo "Created"
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting ${1}"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "---"
  # copy saved file to destination
  rm ${config_path}/${1}
  echo "Deleted"
  echo "The native application configuration file will still exist, please find this and delete it."
  
fi
