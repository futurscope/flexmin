# -------------------------------
# name: List Configurations
# tags: NGINX
# info: Lists available-site config files for selection and viewing.
# ------------------------------

# Main Script
# --------------

# set up 
source ./helpers/nginx_configs.sh

if [ $? -gt 0 ] 
then
  echo "title: NGINX - Directories not found"
  echo "text: >"
  echo "  Please create sites-available and sites-enabled folders"
  echo "  in /user/local/etc/nginx or /etc/nginx"
fi
    
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  # ask user to select a file
  echo "title: NGINX Certificates"
  echo "display:"
  echo "  - table:"
  echo "      title: Server and Domain Names"
  echo "      header: [Configuration, Names, Ports, Status]"
  echo "      last_right: yes"  # right align last column in table
  echo "      cell_newline: ','"
  echo "      delimiter: ';'"
  echo "      object_col: 1"
  echo "      second_col: 0"   # param_3 will be from this column
  echo "      data: |"  
  for nc in `find ${NGINX_AVAIL} -mindepth 1 -maxdepth 1 -name "*"`
  do
    ng_name=$(grep -vE '^\s*#' ${nc} | grep -o -E 'server_name[ ]*[^\;]*' | sed 's/server_name\s*//' | sed 's/ /,/g')
    ng_ports=$(grep -vE '^\s*#' ${nc} | grep -aoE 'listen [^;]*' | paste -sd ',' - | sed 's/listen\s*//g')
    if [ -f ${NGINX_ENABLED}${nc/$NGINX_AVAIL/} ]
    then
      echo "        ${nc/$NGINX_AVAIL\//};$ng_name;$ng_ports;enabled*green"
    else
      echo "        ${nc/$NGINX_AVAIL\//};$ng_name;$ng_ports;disabled*red"
    fi
  done

  echo "  - table:"
  echo "      title: Certificate Referral"
  echo "      header: [Config,NGINX Directive,Certificate]"
  echo "      delimiter: '[ :;]+'"
  echo "      data: |"
  cd ${NGINX_AVAIL}
  grep -vE '^\s*#' * | grep -oE '[^ ]+:\s+ssl_certificate(_key)?\s+[^ ;]+' | sed 's/^/        /'

  echo "  - table:"
  echo "      title: Certificates"
  echo "      actions: '!remove'"
  echo "      header: [NGINX Certificate, Target]"
  echo "      delimiter: ' -> '"
  echo "      data: |"
  ls -al ${FM_NGINX_CONFIG}/certificates | grep -oE '([a-zA-Z0-9_.-]+) -> ([a-zA-Z0-9\/_.-]+)$' | sed -r 's/^/        /'
  
  echo "  - table:"
  echo "      title: Available Certificates"
  echo "      header: [Certificate File]"
  echo "      actions: '?view.?add'"
  echo "      delimiter: ' -> '"
  echo "      data: |"
  if [ ! -z "${FM_LETSENCRYPT_LIVE}" ] && [ -d ${FM_LETSENCRYPT_LIVE} ]
  then
    find ${FM_LETSENCRYPT_LIVE} -name "*.pem" | sed -r 's/^/        /'
  fi
  if [ ! -z "${FM_SSL_ROOT}" ] && [ -d ${FM_SSL_ROOT} ]
  then
    find ${FM_SSL_ROOT} \( -name "*.crt" -o -name "*.key" \) | sed -r 's/^/        /'
  fi

  
elif [ "$2" == "add" ]
then
  # 1=name 2=enable 3=type
  echo "title: Add Certficate"
  echo "method: popup"
  echo "form:"
  echo "  fields:"
  echo "  - label: Target"
  echo "    selectformat: linesep"
  echo "    select: |"
  echo "      /$1"
  echo "    default: '/$1'"
  echo "  - type: hidden"
  echo "    default: add_confirm"
  echo "  - label: NGINX filename"
  echo "    info: Filename to appear in the NGINX certificates folder"
  echo "    type: text"
  echo "    pattern: '[a-zA-Z0-9._-]+'"
  echo "  submit: Add to NGINX Certificates"
  
elif [ "$2" == "add_confirm" ]
then
  # 1=target 2=add_confirm 3=name
  echo "title: Adding ${3}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  echo "Adding /${1} to NGINX certificates folder"
  ln -s /${1} ${FM_NGINX_CONFIG}/certificates/${3}
  echo "Done"
  
elif [ "$2" == "remove" ]
then
  # 1=name,2=delete 3=type
  echo "title: Removing Link"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  # remove link file and original
  echo "---"
  echo "Removing link ${1}"
  rm ${FM_NGINX_CONFIG}/certificates/${1}
  echo "Done."
  
elif [ "$2" == "view" ]
then
  # 1=path 2=view
  echo "title: View Certificate"
  echo "method: popup"
  echo "---"
  # Check we are getting valid paths within our SSL folders
  if [[ /$1 == $(realpath ${FM_LETSENCRYPT_LIVE})* ]] || [[ /$1 == $(realpath ${FM_SSL_ROOT})* ]]
  then
    echo "file: /${1}"
    echo ""
    cat /${1}
  else
    echo "Error: Invalid path" && exit 1
  fi
fi
