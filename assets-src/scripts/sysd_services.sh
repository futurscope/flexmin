# -----------------------------
# name: Systemd
# tags: System
# info: Control Services
# -----------------------------

if [ -z "$FM_SERVICE_PATTERN" ]
then
  svc_pat="nginx.* *.uwsgi.* uwsgi-* flexmin.* flexmin-web.* postgresql.* smb.* dnsmasq.* circus.* atop.*"
else
  svc_pat="${FM_SERVICE_PATTERN}"
fi

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Systemd Services"
  echo "display:"
  echo "  - note: Using systemctl with pattern $svc_pat "
  echo "  - note: Set FM_SERVICE_PATTERN parameter to control which services are listed here. "
  echo "  - table:"
  echo "      title: Running Services"
  echo "      actions: per_row"
  echo "      long: yes"
  echo "      delimiter: ';'"
  echo "      header: [Service, Load*, Active*, Sub*]"
# echo "      header: [Service, Load, Active, Sub, Description]"
  echo "      data: |"
  #echo ${svc_pat}
  actions='restart.?status'
  systemctl list-units $svc_pat --type=service --no-legend --no-pager --plain | while read -r line
  do
    echo "$line" | grep nginx.service | sed -r "s/^([^ ]+) +([^ ]+) +([^ ]+) +([^ ]+) +(.*)$/\1;\2;\3;\4;reload.${actions}/g" | sed -r 's/;(loaded|active|running)/;\1*green/g' | sed -r 's/;(disabled|failed)/;\1*red/g' | sed -r 's/^/        /'
    echo "$line" | grep -v nginx.service | sed -r "s/^([^ ]+) +([^ ]+) +([^ ]+) +([^ ]+) +(.*)$/\1;\2;\3;\4;${actions}/g" | sed -r 's/;(loaded|active|running)/;\1*green/g' | sed -r 's/;(disabled|failed)/;\1*red/g' | sed -r 's/^/        /'
  done

  echo "  - table:"
  echo "      title: Available Services"
  echo "      actions: per_row"
  echo "      long: yes"
  echo "      delimiter: ';'"
  echo "      header: [Service, State*]"
# echo "      header: [Service, Load, Active, Sub, Description]"
  echo "      data: |"    
  #svc_pat="nginx.* *.uwsgi.* uwsgi-* flexmin.* flexmin-web.* postgresql.* smb.* dnsmasq.*"
  systemctl list-unit-files $svc_pat --type=service --no-legend --no-pager --state enabled,disabled | while read -r line
  do
    echo "$line" | sed -e 's/^\([^ ]*\)[ ]*/        \1;/' -e 's/;enabled.*$/;enabled*green;stop.start.disable/' -e 's/;disabled.*$/;disabled*red;enable/' | grep -E -w 'enabled|disabled'
  done
  #systemctl list-units --type=service  --no-legend --no-pager | sed 's/^\([^ ]*\)[ ]*\([^ ]*\)[ ]*\([^ ]*\)[ ]*\([^ ]*\)[ ]*/        \1;\2;\3;\4;/'
  
  echo "  - form:"
  echo "      title: Daemon Reload"
  echo "      submit: Systemd Daemon Reload"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: reload"
  

elif [ "$1" == "reload" ]
then 
  echo "title: Systemd Daemon Reload"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "---"
  systemctl daemon-reload
  echo "Done."
  
elif [ "$2" == "enable" ]
then
  echo "title: Enabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  systemctl enable ${1}
  echo "Done."
  
elif [ "$2" == "disable" ]
then
  echo "title: Disabling ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  systemctl disable ${1}
  echo "Done."
  
elif [ "$2" == "restart" ]
then
  echo "title: Restarting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  echo "Restarting..."
  sleep 1
  systemctl restart ${1}
  echo "Done."

elif [ "$2" == "reload" ]
then
  # Service reload is effecting a change but not making a permanent change
  echo "title: Reloading ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  echo "Reloading..."
  sleep 1
  systemctl reload ${1}
  echo "Done."

elif [ "$2" == "start" ]
then
  echo "title: Starting ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  echo "Starting..."
  systemctl start ${1}
  echo "Done."
  
elif [ "$2" == "stop" ]
then
  echo "title: Stopping ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  echo "Stopping..."
  systemctl stop ${1}
  echo "Done."
  
elif [ "$2" == "status" ]
then
  echo "title: Status for ${1}"
  echo "method: popup"
  echo "..."
  systemctl status ${1} --no-pager
  
fi
