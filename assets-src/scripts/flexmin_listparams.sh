# -----------------------------
# name: List Parameters
# tags: Flexmin
# info: List All Flexmin Parameters, included those calculated automatically in the background
# -----------------------------

# Main Script
# ---------------

echo "title: List Parameters"
echo "note: |"
echo "  These are the parameters available for use in Flexmin scripts. "
echo "  Many of these parameters may need changing to match the various "
echo "  directory paths for your specific system."
echo "  You can edit these parameters using the Edit Parameters option."
echo "table:"
echo "  header: [Parameter, Value]"
echo "  delimiter: '='"
echo "  data: |"
set | grep '^FM_' | sed 's/^/    /'
