# taskd status and control helper

name="flexmin"
rc_enable="flexmin_enable"
desc="Flexmin Server"
xcmds=".restart"
rc_params=""

# check required software and/or service script is installed, 
# if not, then exit with error (no output)
[ -f /usr/local/etc/rc.d/${name} ] || exit 1 2>&1

if [ $# -eq 0 ]
then
  opt="enable"
  startup="disabled*red"
  grep -q -i -E "${rc_enable}=\"?yes\"?\s*" /etc/rc.conf && opt="disable" && startup="enabled*green"
  cmd="start"
  status="stopped*red"
  service ${name} onestatus | grep -q -i 'is running' && status="running*green" && cmd="stop"
  echo "${name}, ${desc}, ${startup}, ${status}, ${opt}.${cmd}${xcmds}"
elif [ "$1" == "params" ]
then
  echo "${rc_params}"
else
  service ${name} $1
fi