# postgresql status and control helper

name="postgresql"
rc_enable="postgresql_enable"
desc="PostgreSQL Database Server"
xcmds=".restart"
rc_params=""

# check required software and/or service script is installed, 
# if not, then exit with error (no output)
( command -v ${FM_PSQL} >/dev/null &&\
  [ -f /usr/local/etc/rc.d/${name} ] ) || exit 1 2>&1

if [ $# -eq 0 ]
then
  opt="enable"
  #startup="disabled*red"
  grep -q -i -E "${rc_enable}=\"?yes\"?\s*" /etc/rc.conf && opt="disable" && startup="enabled*green" && enabled="yes"
  cmd="start"
  status="stopped*red"
  if [ "$enabled" != "yes" ]
  then
    # not enabled yet, this must be done first, present enable option
    echo "${name}, ${desc}, ${startup}, unavailable, enable"
  else
    # enabled, check whether initialised
    find /usr/local/pgsql -name "PG_VERSION" | grep "PG_VERSION" >/dev/null
    if [ $? -ne 0 ] 
    then
      # still a problem, not initialised, present initdb option
      echo "${name}, ${desc}, ${startup}, unavailable, initdb"
    else
      # ready for normal service operation
      service ${name} onestatus | grep -q -i 'is running' && status="running*green" && cmd="stop"
      echo "${name}, ${desc}, ${startup}, ${status}, ${opt}.${cmd}${xcmds}"
    fi
  fi
elif [ "$1" == "params" ]
then
  echo "${rc_params}"
else
  service ${name} $1
fi