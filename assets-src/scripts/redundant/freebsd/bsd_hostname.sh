# name: Hostname
# tags: Network
# info: Show or Set Hostname
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 0 ]
then
  echo "title: System Hostname"
  echo "text: |"
  hostname | sed 's/^/  /'
  echo "form:"
  echo "  title: Set Hostname"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: set"
  echo "    - label: New Hostname"
  echo "      type: text"
  echo "  submit: Set Hostname"
elif [ "$1" == "set" ]
then
  echo "title: Changing hostname"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  if [ -n $2 ]
  then
    if cat /etc/rc.conf | grep -i -q '^hostname='
    then
      # entry already exists, modify it, sed uses : for replace commadn instead of / to allow for paths in sub string
      newrc=`cat /etc/rc.conf | sed "s:^hostname=.*:hostname=\"${2}\":"`
      echo "${newrc}" > /etc/rc.conf
      echo "Replaced hostname entry"
    else
      # add entry to end of rc.conf
      echo "hostname=\"${2}\"" >> /etc/rc.conf
      echo "Created hostname entry"
    fi
    hostname "$2"
    echo "Note: some system components may need a restart to use new hostname. A system restart is suggested to ensure all components reflect new hostname."
  fi
fi
