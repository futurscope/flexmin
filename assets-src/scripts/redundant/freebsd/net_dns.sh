# ---------------------
# name: DNS Configuration
# tags: Network
# info: Display DNS server details
# --------------------

# Main Script
# ------------

echo "title: DNS Configuration"
echo "---"
cat /etc/resolv.conf
