# -----------------------------
# name: Open Port List
# tags: Network
# info: List Open Network Ports
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 0 ]
then
  echo "title: Open IP4 Ports"
  echo "display:"
  echo "  - table:"
  echo "      header: [User,Command,PID,FD,Protocol,Local,LPort,Foreign,FPort]"
  echo "      delimiter: '\t+|\s+|:'"
  echo "      data: |"
  sockstat -4l | grep -v -E "USER" | sed 's/^/        /'
  echo "  - table:"
  echo "      title: Open IP6 Ports"
  echo "      header: [User,Command,PID,FD,Protocol,Local,LPort,Foreign,FPort]"
  echo "      delimiter: '\t+|\s+|:'"
  echo "      data: |"
  sockstat -6l | grep -v -E "USER" | sed 's/^/        /'
fi
