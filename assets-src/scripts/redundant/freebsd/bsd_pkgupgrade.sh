# ----------------------
# name: Package Upgrade
# tags: System
# info: Checks installed packages for security updates
# -------------------------

# Main Script
# ------------------

if [ $# -eq 0 ]
then
    echo "title: Package Upgrade"
    echo "display:"
    echo "  - form:"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: audit"
    echo "      submit: Audit Packages"
    echo "  - table:"
    echo "      title: Package Upgrades"
    echo "      delimiter: ':'"
    echo "      header: [Package,Installed,Available]"
    echo "      actions: [check,'!upgrade']"
    echo "      data: |"
    upgradeinfo=`pkg upgrade -n`
    upgrades=`echo "${upgradeinfo}" | sed -nr "/^Installed.*UPGRADED:/,/^\s*$/p"`
    echo "${upgrades}" | grep "^[[:space:]]" | sed 's/->/:/;s/^[[:space:]]\{1,\}/        /'
    echo "  - table:"
    echo "      title: Package Reinstalls"
    echo "      delimiter: ':'"
    echo "      header: [Package,Installed,Comment]"
    echo "      actions: [check,'!reinstall']"
    echo "      data: |"
    reinstalls=`echo "${upgradeinfo}" | sed -nr "/^Installed.*REINSTALLED:/,/^\s*$/p"`
    echo "${reinstalls}" | grep "^[[:space:]]" | sed -r "s/-([0-9\-\.]+)/:\1/" | sed -r "s/[[:space:]]*([^\( ]+)[[:space:]]*\((.*)\)/\1:\2/" | sed 's/^/        /'

    #pkg upgrade -n | grep "^[[:space:]]" | sed 's/->/:/;s/^[[:space:]]\{1,\}/        /'
elif [ "$1" == "audit" ]
then
    echo "title: Audit Packages"
    echo "method: popup"
    echo "..."
    pkg audit
    if [ $? -eq 65 ]  # /var/db/pkg/vuln.xml does not exist
    then
      pkg audit -F
    fi
elif [ "$1" == "check" ]
then
    echo "title: Upgrade Check (No Action)"
    echo "method: popup"
    echo "..."
    pkg upgrade -n ${2}
elif [ "$1" == "reinstall" ]
then
    echo "title: Reinstall"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: UPGRADE"
    echo "..."
    pkg install ${2}
elif [ "$1" == "upgrade" ]
then
    echo "title: Upgrade"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: UPGRADE"
    echo "..."
    pkg upgrade ${2}
fi
