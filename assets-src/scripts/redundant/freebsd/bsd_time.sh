# name: Date and Time
# tags: System
# info: Show or Set Date and Time
# -----------------------------

# Main Script
# ---------------


# if parameters are 'timezone' Region Country
if [ "$1" == "timezone" ]
then
  if [ -f /etc/localtime ]
  then
      rm /etc/localtime
  fi
  ln -s /usr/share/zoneinfo/${2}/${3} /etc/localtime
fi

if [ $# -eq 0 ] || [ "$1" == "region" ] || [ "$1" == "timezone" ]
then
  echo "title: Current Date and Time"
  echo "display:"
  echo "  - note: |"
  echo "      Please ensure date and time are correct (or approximately so)"
  echo "      before you try and start the NTPD service. If the time is too far"
  echo "      out, then the NTPD service will stop again."
  echo "  - text: |"
  date | sed 's/^/      /'
  if [ -f /etc/localtime ]
  then
    ls -all /etc/localtime | sed 's/^.*zoneinfo\//      timezone: /'
  else
    echo "      No timezone set"
  fi
  d=`date "+%Y-%m-%d"`
  t=`date "+%H:%M:%S"`
  echo "  - form:"
  echo "      title: Set Date or Time"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: set"
  echo "        - label: Date (yyyy-mm-dd)"
  echo "          type: date"
  echo "          default: ${d}"
  echo "        - label: Time (hh:mm:ss)"
  echo "          type: time"
  echo "          default: '${t}'"
  echo "      submit: Set"
fi

if [ $# -eq 0 ] || [ "$1" == "timezone" ]  # no params or timezone set
then
  echo "  - form:"
  echo "      title: Configure Timezone"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: region"
  echo "        - label: Region"
  echo "          select: Africa America Antarctic Arctic Asia Atlantic Australia Europe Indian Pacific"
  echo "          delimiter: '\s'"
  echo "      submit: Next"
elif [ "$1" == "region" ]
then
  echo "  - form:"
  echo "      title: Configure Timezone"
  echo "      submit: Set Timezone"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: timezone"
  echo "        - label: Region"
  echo "          select: [${2}]"
  echo "        - label: Location"
  echo "          delimiter: '\n'"
  echo "          select: |"
  ls -1 /usr/share/zoneinfo/${2}/ | sed 's/^/            /'
elif [ "$1" == "set" ]
then
  echo "title: Changing date and time"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  if [ -n $2 ]
  then
    date -f "%Y-%m-%d" "$2"
  fi
  if [ -n $3 ]
  then
    date -f "%H:%M:%S" "$3"
  fi
fi
