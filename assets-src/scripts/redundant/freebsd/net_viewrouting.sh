# -----------------------------
# name: Routing Configuration
# tags: Network
# info: Displays network routing information (netstat -r)
# -----------------------------

# Main Script
# ---------------

echo "title: Network Routing (Internet)"
echo "display:"
echo "  - table:"
echo "      header: [Destination,Gateway,Flags,Refs,Use,Interface,Expire]"
echo "      delimiter: '\s+'"
echo "      data: |"
netstat -rn -f inet | sed -e '1,/^Destination/d' | sed 's/^/        /'
echo "  - table:"
echo "      title: Network Routing (Internet 6)"
echo "      header: [Destination,Gateway,Flags,Interface,Expire]"
echo "      delimiter: '\s+'"
echo "      data: |"
netstat -rn -f inet6 | sed -e '1,/^Destination/d' | sed 's/^/        /'


