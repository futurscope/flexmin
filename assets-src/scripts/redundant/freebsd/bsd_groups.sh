# ----------------------
# name: Groups
# tags: System
# info: Shows all group details in a table.
# -------------------------

# Main Script
# ------------------

if [ $# -eq 0 ]
then
  echo "title: Create New Group"
  echo "display:"
  echo "  - form:"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Group Name"
  echo "          type: text"
  echo "        - label: Members"
  echo "          info: Comma or space separated list of users."
  echo "          type: text"
  echo "      submit: Create Group"
  echo "  - table:"
  echo "      title: Groups"
  echo "      delimiter: ':'"
  echo "      header: [group,-unknown,gid,members]"
  echo "      actions: delete"
  echo "      data: |"
  pw group show -a | sed 's/^/        /'
elif [ "$1" == "create" ]
then
  echo "title: Creating group ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "---"
  echo "Creating group ${2}"
  if [ "$3" == "" ]
  then
    echo "pw group add ${2}"
    pw group add ${2}
  else
    echo "pw group add ${2} -M '${3}'"
    pw group add ${2} -M "${3}"
  fi
elif [ "$1" == "delete" ]
then
  echo "title: Deleting group ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  #echo "status: DELETING"
  echo "---"
  echo "pw group del -n ${2}"
  pw group del -n ${2}
  echo "Deleted group."
fi
