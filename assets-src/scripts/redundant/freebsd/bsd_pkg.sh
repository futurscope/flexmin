# ----------------------
# name: Packages
# tags: System
# info: Shows all installed packages
# -------------------------

# Main Script
# ------------------

if [ $# -eq 0 ]
then
    echo "title: Find and Install Package"
    echo "display:"
    echo "  - form:"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: find"
    echo "        - label: Search String (Package Name)"
    echo "          type: text"
    echo "      submit: Find Packages"
    echo "  - table:"
    echo "      title: Installed Packages"
    echo "      delimiter: '\s{2,}'"
    echo "      header: [pkg, description]"
    echo "      actions: [info,'!delete']"
    echo "      data: |"
    pkg info | sed 's/^/        /'
elif [ "$1" == "info" ]
then
    echo "title: 'Package Info : ${2}'"
    echo "method: popup"
    echo "---"
    pkg info "$2"
elif [ "$1" == "delete" ]
then
    echo "title: Deleting ${2}"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: DELETE"
    echo "..."
    pkg delete ${2}
    echo "Deleted"
elif [ "$1" == "find" ]
then
    # this will time out if local package data is out of date
    # need elegent way to handle this
    echo "title: Searching ${2}"
    echo "method: popup"
    echo "table:"
    echo "  delimiter: ' '"
    echo "  header: [package, version]"
    echo "  actions: install"
    echo "  data: |"
    pkg search "${2}" | sed -e 's/^/    /' -e 's/\-\([0-9|\.|\_|\,]\{1,\}\)$/ \1/'
elif [ "$1" == "install" ]
then
    echo "title: Installing ${2}"
    echo "method: popup"
    echo "category: INSTALL"
    echo "next_action: reload_pane"
    echo "..."
    pkg install ${2}
    echo "Installed"
else
    echo "title: Unrecognized parameters"
    echo "method: popup"
    echo "status: ERROR"
    echo "text: |"
    echo "  Unrecognised parameter(s)"
fi
