# ----------------------
# name: Users
# tags: System
# info: Shows all user details in a table.
# -------------------------

# Main Script
# ------------------

if [ $# -eq 0 ]
then
  echo "title: Create New User"
  echo "display:"
  echo "  - form:"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Username"
  echo "          type: text"
  echo "        - label: Password"
  echo "          type: password"
  echo "        - label: Primary Group"
  echo "          type: text"
  echo "          default: 'same as username'"
  echo "        - label: Additional Groups"
  echo "          info: Comma or space separated list of groups. 'wheel' allows switch to root."
  echo "          type: text"
  echo "      submit: Create User"
  echo "  - table:"
  echo "      title: User Accounts"
  echo "      delimiter: ':'"
  echo "      header: [username,-pw,uid,gid,-class,-pw chg,exp,user inf,home,shell]"
  echo "      actions: [reset_pw,'!delete']"
  echo "      data: |"
  pw user show -a | sed 's/^/        /'
elif [ "$1" == "create" ]
then
  echo "title: Creating user ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  if [ "$4" == "same as username" ] 
  then
    grp=$2
  else
    grp=$4
  fi
  echo "Creating group ${grp}"
  pw group add ${grp}
  echo "Creating user ${2}"
  if [ "$5" == "" ]
  then
    echo "pw user add ${2} -h 0 -m -g ${grp}"
    echo "${3}" | pw user add ${2} -h 0 -m -g ${grp}
  else
    echo "pw user add ${2} -h 0 -m -g ${grp} -G '${5}'"
    echo "${3}" | pw user add ${2} -h 0 -m -g ${grp} -G "${5}"
  fi
elif [ "$1" == "reset_pw" ]
then
  echo "title: Reset Password - ${2}"
  echo "method: popup"
  echo "form:"
  echo "  submit: Change Password"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: change_pw"
  echo "    - type: hidden"
  echo "      default: ${2}"  # username
  echo "    - label: Password"
  echo "      type: password"
  echo "    - label: Retype Password"
  echo "      type: password"
elif [ "$1" == "change_pw" ]
then
  echo "title: Changing Password - ${2}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  if [ "$3" == "$4" ]
  then
    echo "$3" | pw user mod ${2} -h 0
    echo "Password changed"
  else
    echo "Error - passwords did not match"
  fi
  echo "---"
elif [ "$1" == "delete" ]
then
  echo "title: Deleting user ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  #echo "status: DELETING"
  echo "..."
  echo "pw user del -n ${2} -r"
  pw user del -n ${2} -r
  echo "Deleted. Check groups in case user group needs deleting."
fi
