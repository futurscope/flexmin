# -----------------------------
# name: Services
# tags: System
# info: Control Services
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 0 ]
then
  echo "title: System"
  echo "display:"
  echo "  - form:"
  echo "      submit: Proceed"
  echo "      fields:"
  echo "        - label: Action"
  echo "          select: [none,shutdown,restart]"
  echo "        - type: hidden"
  echo "          default: system"
  echo "  - table:"
  echo "      title: Services"
  echo "      actions: per_row"
  echo "      delimiter: ','"
  echo "      header: [Service, Description, Startup, Status]"
  echo "      data: |"
  for h in ./helpers/bsd_services_*.sh
  do
    $h | sed 's/^/        /'
  done
  echo "  - table:"
  echo "      title: Service Parameters"
  echo "      actions: edit"
  echo "      delimiter: '\t'"
  echo "      header: [Parameter, Value]"
  echo "      data: |"
  for h in ./helpers/bsd_services_*.sh
  do
    rcp=`${h} params`
    for p in $rcp
    do
      tab=$'\t'
      rc=`cat /etc/rc.conf`
      curr=`echo "${rc}" | grep -i "^${p}=" | sed "s/^${p}=//"`
      echo "        ${p}${tab}${curr}"
    done
  done
elif [ "$1" == "restart" ] && [ "$2" == "system" ]
then
  echo "title: System ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "done_pattern: '^ERROR.*No matching task id.*$'"  # recognise this as sign that task is done
  echo "..."
  echo "System restart, please wait"  # warn user of delay
  sleep 2
  daemon -f shutdown -r now
  sleep 300 # pause job to keep task update going during restart
  echo ""
elif [ "$1" == "shutdown" ] && [ "$2" == "system" ]
then
  echo "title: System ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  echo "System shutdown initiated. You will need to restart manually."
  sleep 2
  daemon -f shutdown -p now
elif [ "$1" == "none" ]
then
  echo "title: System"
  echo "method: popup"
  echo "---"
  echo "No action specified."
elif [ "$1" == "edit" ]
then
  # allow edit of rc.conf parameters
  echo "title: Set service parameters"
  echo "method: popup"
  echo "no_post_yaml: yes"
  echo "form:"
  echo "  submit: Save"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: setparam"
  echo "    - type: hidden"
  echo "      default: ${2}"
  echo "    - label: ${2}"
  default=`cat /etc/rc.conf | grep -i "^${2}=" | sed -e "s/^${2}=\"//" -e 's/\"$//'`
  curr=${default:="''"}  # use '' if no value found to stop pyYAML putting 'None' there
  echo "      default: ${curr}"
  echo "      type: text"
  
elif [ "$1" == "setparam" ]
then
  echo "title: Setting parameter - ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  echo "${2}=\"${3}\""
  if cat /etc/rc.conf | grep -i -q "^${2}="
  then
    # entry already exists, modify it, sed uses : for replace command instead of / to allow for paths in sub string
    newrc1=`cat /etc/rc.conf | sed "s:^${2}=.*:${2}=\"${3}\":"`
    echo "${newrc1}" > /etc/rc.conf
    echo "Updated"
  else
    # add entry to end of rc.conf
    echo "${2}=\"${3}\"" >> /etc/rc.conf
    echo "Added"
  fi
  
elif [ "$1" == "enable" ]
then
  echo "title: Enabling ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  if cat /etc/rc.conf | grep -i -q "${2}_enable"
  then
    # replace any existing entry
    cat /etc/rc.conf | sed "s/^${2}_enable.*/${2}_enable=\"YES\"/" > ${FM_TASK_TMP}rc.conf.tmp
    cp ${FM_TASK_TMP}rc.conf.tmp /etc/rc.conf
    rm ${FM_TASK_TMP}rc.conf.tmp
  else
    # append to rc.conf
    echo "${2}_enable=\"YES\"" >> /etc/rc.conf
  fi
  echo "Enabled"

elif [ "$1" == "disable" ]
then
  echo "title: Disabling ${2}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  # remove any existing entry
  cat /etc/rc.conf | grep -v -i "^${2}_enable=" > ${FM_TASK_TMP}rc.conf.tmp
  cp ${FM_TASK_TMP}rc.conf.tmp /etc/rc.conf
  rm ${FM_TASK_TMP}rc.conf.tmp
  echo "Disabled"
  
elif [ "$1" == "view_log" ]
then
  echo "title: Service Log for ${2} (last 100 lines)"
  echo "method: popup"
  echo "..."
  if [ -f /var/log/${2}.log ]
  then
    tail -n 100 /var/log/${2}.log | cat -v | sed -E 's/\^\[\[[0-9]+m//g'
  elif [ -f /var/log/${2} ]
  then
    tail -n 100 /var/log/${2} | cat -v | sed -E 's/\^\[\[[0-9]+m//g'
  else
    echo "Log not found."
  fi
  
  
elif [ $# -eq 2 ]
then
  echo "title: Service ${2} ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "done_pattern: '^ERROR.*No matching task id.*$'"  # recognise this as sign that task is done
  echo "..."
  if [ "$2" == "uwsgi" ] || [ "$2" == "nginx" ]
  then
      # if restarting services the the flexmin GUI relies on, then prepare for
      # interruption by giving web services 2 seconds to return the yaml section
      # and be ready for updates.
      echo "please wait"  # warn user of delay
      sleep 2
      echo ""  # make sure we go to the next line after line with wait dots
      service ${2} ${1}
  elif [ "$2" == "flexmin" ] && [ "$1" == "restart" ]
  then
      # handle fdlexmin restart by using daemon command to separate restart 
      # process from flexmin so it can restart flexmin service okay
      echo "flexmin service restart, please wait"  # warn user of delay
      sleep 2
      #nohup service flexmin restart >/dev/null 2>&1 &
      daemon -f service ${2} ${1}
      echo ""  # make sure we go to the next line after line with wait dots
  else
      service ${2} ${1}
  fi
  # postgresql start request doesn't end in a way that subporcess.Popen 
  # recognises, so this text is used to force the end of the Popen task.
  echo "flexmin-force-exit"  
fi
