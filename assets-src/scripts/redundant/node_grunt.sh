# -------------------
# name: Grunt Configuration
# tags: Node.js
# info: Grunt configuration
# ---------------------

if command -v grunt >/dev/null 2>&1
then
  if [ $# -eq 1 ] && [ "$1" == "~default~" ]
  then
    echo "title: Grunt Configuration"
    echo "edit:"
    echo "  filename: Gruntfile.js"
    echo "  mode: javascript"
    echo "  data: |"
    { cat ${FM_NODE_ROOT}/Gruntfile.js || echo ""; } 2>/dev/null | sed 's/^/    /'
  elif [ "$2" == 'save' ]
  then
    echo "title: Saving Grunt Configuration"
    echo "method: popup"
    echo "category: CHANGE"
    echo "---"
    mkdir ${FM_NODE_ROOT} 2>/dev/null # make sure folder exists
    cp ${FM_SCRIPT_PARAM}_3 ${FM_NODE_ROOT}/Gruntfile.js
    echo "Saved ${1}"
  fi
else
  echo "title: Grunt Configuration"
  echo "node: Grunt command is NOT installed"
fi
