# -------------------
# name: Node Package Manager
# tags: Node.js
# info: Node.js configuration
# ---------------------

if command -v npm >/dev/null 2>&1 && [ ! -z $FM_NODE_ROOT ]
then
  cd ${FM_NODE_ROOT}
  if [ $# -eq 1 ] && [ "$1" == "~default~" ]
  then
    echo "title: Node Package Manager"
    echo "display:"
    echo "  - text: |"
    npm ls -g --depth=0 | sed 's/^/      /'
    npm ls --depth=0 | sed 's/^/      /'
    echo "  - form:"
    echo "      title: Install Node Package"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: install"
    echo "        - label: Package Name"
    echo "          info: Leave this blank to install all packages specified in package.json"
    echo "          type: text"
    echo "        - label: Switches"
    echo "          selectformat: linesep"
    echo "          select: |"
    echo "             -g (global/command install)"
    echo "             --save (save as dependency in package.json)"
    echo "      submit: Install Packages"
  elif [ "$2" == 'install' ]
  then
    if [ -z $1 ]
    then
      npmpackage=""
    else
      npmpackage="${1}"
    fi
    if [ "$3" == "" ]
    then
      npmopts=""
    else
      npmopts=`echo "${3}" | sed -e 's/([^)]*)//'`  # needs space after it
    fi
    echo "title: Installing ${npmpackage}"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: INSTALL"
    echo "---"
    npm install ${npmopts}${npmpackage}
  fi
else
  echo "title: Node.js Configuration"
  echo "note: Node Package Manager is NOT installed"
fi
