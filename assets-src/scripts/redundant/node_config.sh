# -------------------
# name: Node Configuration
# tags: Node.js
# info: Node.js configuration
# ---------------------

if command -v node >/dev/null 2>&1
then
  if [ $# -eq 1 ] && [ "$1" == "~default~" ]
  then
    echo "title: Node.js Packages Configuration"
    echo "edit:"
    echo "  filename: package.json"
    echo "  mode: json"
    echo "  data: |"
    { cat ${FM_NODE_ROOT}/package.json || echo ""; } 2>/dev/null | sed 's/^/    /'
  elif [ "$2" == 'save' ]
  then
    echo "title: Saving Node Packages Configuration"
    echo "method: popup"
    echo "category: CHANGE"
    echo "---"
    mkdir ${FM_NODE_ROOT} 2>/dev/null # make sure folder exists
    cp ${FM_SCRIPT_PARAM}_3 ${FM_NODE_ROOT}/package.json
    echo "Saved ${1}"
  fi
else
  echo "title: Node.js Configuration"
  echo "note: Node is NOT installed"
fi
