# -----------------------------
# name: Folders
# tags: Filesystem
# info: Folder Information (Size)
# -----------------------------

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Folder"
  echo "table:"
  echo "  actions: per_row"
  echo "  delimiter: ' '"
  # show_sub specifies regex for first table column to show only last folder in path
  # echo "  show_sub: ['(\/.*\/|\/(?=[^\/]))','']"
  #echo "  show_sub: ['(\*.*\*|\*(?=[^*]))','']"
  echo "  header: [Folder]"
  echo "  data: |"
  find / -maxdepth 1 -mindepth 1 -type d | sort | sed -r 's/\///' | sed -e 's/^/    /' -e 's/$/ ?files.~subfolders.?backup.!delete/'
  #du -d 1 -h / | tail -r | sort -k1 -h -r -s | awk '{ print $2 " " $1 }' | sed -e '1 s/^/    /' -e '2,$ s/^/    \~/' -e '1 s/$/ files.create_folder.backup/' -e '2,$ s/$/ files.subfolders.backup.!delete/'
  
elif [ $# -eq 1 ]
then
  folder=$(echo ${1} | sed -e 's/\^/\//g')
  echo "title: Folder ${folder}"
  echo "display:"
  echo "  - text: ${1}"
  echo "  - table:"
  echo "      actions: per_row"
  echo "      delimiter: ':'"
  # show_sub specifies regex for first table column to show only last folder in path
  #echo "      show_sub: ['(\/.*\/|\/(?=[^\/]))','']"
  echo "      header: [Folder]"
  echo "      data: |"
  if [ "${folder%/*}" == "" ] && [ "$1" != "/" ]
  then
    echo "        ^:parent"
  elif [ "${folder%/*}" != "" ]
  then
    echo "        ..:~parent" 
  fi
  find /${folder} -maxdepth 1 -mindepth 1 -type d | sort | sed -e 's/^\/.*\///' | sed -e 's/^/        /' -e 's/$/:?files.~subfolders.backup.!delete/'
  #du -d 1 -h ${2} | tail -r | sort -k1 -h -r -s | awk '{ print $2 " " $1 }' | sed -e '1 s/^/        \~/' -e '2,$ s/^/        \~\~/' -e '1 s/$/ files.create_folder.backup/' -e '2,$ s/$/ files.subfolders.backup.!delete/'

elif [ "$2" == "backup" ]
then
  echo "title: Backup ${1}"
  echo "method: popup"
  if [ ! -d ${FM_BACKUP} ]
  then
    echo "error: Folder ${FM_BACKUP} does not exist. Please create this folder."
    exit 1
  fi
  echo "display:"
  echo "  - note: |"
  echo "      This process will remove any existing backup file of the same name before the zip process begins."
  echo "      It will not add to an existing zip file."
  echo "  - text: |"
  size=`du -sh /${1} | cut -f 1`
  echo "      Folder to backup is /${1} (size is ${size})"
  avail=`df -h ${FM_BACKUP} | grep -vi "filesystem" | sed -r 's/^[^ ]+\s+[^ ]+\s +[^ ]+\s+([^ ]+).*$/\1/'`
  echo "      There is ${avail} available space on backup destination."
  echo "      Please check there is sufficient space before continuing."
  echo "  - form:"
  echo "      submit: Backup Folder"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: /${1}"
  echo "        - type: hidden"
  echo "          default: backup_confirmed"
  echo "        - label: Backup Name"
  echo "          info: The name will have .folder.zip added to it to make up the backup file."
  echo "          type: text"
  echo "          default: $(basename ${1})"
  echo "        - label: Exclusions"
  echo "          info: Specify files/folders to exclude. This must follow the format accepted by the zip -x switch"
  echo "          type: text"

elif [ "$2" == "backup_file" ]
then
  echo "title: Backup File ${1}"
  echo "method: popup"
  echo "category: BACKUP"
  filename=$(basename /$1)
  dirname=$(dirname /$1)
  parentname=$(basename $dirname)
  cp /${1} ${FM_BACKUP}/${filename}
  echo "text: ${1} backed up to ${FM_BACKUP}/${filename}."

elif [ "$2" == "backup_confirmed" ]
then
  echo "title: Backing Up /${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: BACKUP"  # backup separate from normal logged actions
  echo "comment: Processing folder /${1}"
  echo "restore: sys_folders.sh restore_folder '${3}' '/${1}'"
  echo "..."
  cd $(dirname /$1)
  backupfile="${3}.folder.zip"
  if [ -z "$4" ]
  then
      # backup folder to zip file, folder itself is included in zipped paths
      zip -r --filesync ${FM_BACKUP}/${backupfile} $(basename /$1)/*
  else
      # backup folder to zip file, folder itself is included in zipped paths
      zip -r --filesync ${FM_BACKUP}/${backupfile} $(basename /$1)/* -x ${4}
  fi
  cd -
  echo "Backup file created/updated:"
  du -h ${FM_BACKUP}/${backupfile}

elif [ "$2" == "restore_folder" ]
then
  echo "title: Restoring /${1}"
  echo "method: popup"
  echo "comment: Processing folder ${1}"
  echo "restore: sys_folders.sh restore_folder '${3}' '${1}'"
  echo "..."
  echo "Restoring /${1} to ${3}"
  cd $(dirname $3)
  rm -R $(basename $3)  # remove existing folder if it exists
  backupfile="${1}.folder.zip"
  if [ -f "${FM_BACKUP}/${backupfile}" ]  # check backup file available
  then
      # backup folder to zip file, folder itself is included in zipped paths
      unzip -d ./ ${FM_BACKUP}/${backupfile}
  fi
  cd -
  echo "Permissions not part of restore job, verify ownership and permissions."
  echo "Restored"

elif [ "$2" == "create_folder" ]
then
  echo "title: New folder in ${1}"
  echo "method: popup"
  echo "form:"
  echo "  submit: Create Folder"
  echo "  fields:"
  echo "    - type: hidden"
  echo "      default: ${1}"
  echo "    - type: hidden"
  echo "      default: create"
  echo "    - label: Folder Name"
  echo "      type: text"
  echo "    - label: Ownership (Optional)"
  echo "      info: user:group ownership as used by chown command"
  echo "      type: text"
  echo "    - label: Permission (Optional)"
  echo "      info: Permission in the form rwxrw-r-- as used by chmod"
  echo "      pattern: '([r-][w-][x-]){3}'"
  echo "      type: text"

elif [ "$2" == "create" ]
then
  echo "title: New folder in /${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  echo "Creating folder"
  mkdir -p /${1}/${3}
  [ "$4" != "" ] && OWN=${4}
  # convert mod string to number
  [ "$5" != "" ] && MODINT=`echo $5 | sed -e 's/rwx/7/g' -e 's/rw-/6/g' -e 's/r-x/5/g' -e 's/r--/4/g' -e 's/-wx/3/g' -e 's/-w-/2/g' -e 's/--x/1/g' -e 's/---/0/g'` 
  # If ownership specified set ownership
  [ -n "$OWN" ] && echo "Changing ownership"
  [ -n "$OWN" ] && chown $OWN /${1}/${3}
  # if permissions parameters specified then set permissions
  [ -n "$MODINT" ] && echo "Changing permissions"
  [ -n "$MODINT" ] && chmod $MODINT /${1}/${3}
  echo "Created /${1}/${3}"
  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting /${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  echo "---"
  rm -R /${1}
  echo "Deleted /${1}"
  
elif [ "$2" == "files" ]
then
  echo "title: Files in /${1}"
  echo "method: popup"
  echo "table:"
  echo "  delimiter: '\s+'"
  echo "  actions: 'backup_file.!delete'"
  echo "  show_sub: ['(\/.*\/|\/(?=[^\/]))','']"
  echo "  header: [Name,Mode,Links,User,Group,Size,DateTime]"
  echo "  data: |"
  #                                 exclude total line  exclude directories
  #ls -laSh -D "%Y-%m-%d-%H:%M" -d ${2}/* \
  ls -laSh --time-style +"%Y-%m-%d-%H:%M" -d /${1}/* \
          | grep -v "^total" | grep -v "^d" | sort -k9,9 \
          | awk '{ print $7 "\t" $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 }' \
          | sed 's/^/    /'
  #ls -all -d $PWD/* | awk '{ print $9 "\t" $1 "\t" $3 }'
fi
