# -------------------
# name: Databases
# tags: PostgreSQL
# info: Lists PostgreSQL DBs and allows deletion
# ---------------------

# Check whether postgress installed and initialised
./helpers/postgres_check.sh || exit

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Databases"
  echo "display:"
  echo "  - note: |"
  echo "      The recreate option here is a quick way to empty out a database."
  echo "      Only the name and owner information is retained in the new database."
  echo "      The force delete option closes any existing connections to the "
  echo "      database, and prevents reconnection during the delete process."
  echo "  - table:"
  echo "      header: [Database, Owner, Size]"
  echo "      actions: 'recreate.!delete.!force_delete'"
  echo "      delimiter: '[\s|]+'"
  echo "      data: |"
  q="SELECT d.datname, u.usename, pg_size_pretty(pg_database_size(d.datname)) FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid AND d.datistemplate = false);"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "${q}" | sed 's/^/       /'

  echo "  - form:"
  echo "      title: Create New Database"
  echo "      submit: Create New Database"
  echo "      fields:"
  echo "        - label: Database Name"
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: create"
  echo "        - label: Owner"
  echo "          selectformat: linesep"
  echo "          select: |"
  sudo -u ${FM_PSQL_SU} ${FM_PSQL} -t -c "SELECT usename FROM pg_user;" | sed s'/^/           /'

  echo "  - form:"
  echo "      title: Schedule Database Size Check"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: cron"
  echo "        - label: Frequency"
  echo "          select: daily"
  echo "          delimiter: '\s'"
  echo "        - label: Time (hh:mm)"
  echo "          type: text"
  echo "      submit: Schedule Check"

elif [ "$1" == "cron" ]
then
  # import cron helper functions
  . ./helpers/cron.sh
  echo "title: Cron Entry"
  echo "method: popup"
  echo "text: |"
  cron_set "${FM_SCRIPTS}/common.sh 0 '${0}' log" "${2}" "${3}" | sed 's/^/  /'
  echo "  Scheduled"
  echo "category: CHANGE"

elif [ "$1" == "log" ]
then
  logtime=$(date +"%Y-%m-%d-%H-%M-%S")
  if [ ! -f ${TASK_LOG} ]
  then
    echo "title: Database Size History" > $TASK_LOG
    echo "table:" >> $TASK_LOG
    echo "  separate: Database" >> $TASK_LOG
    echo "  graph: [Size]" >> $TASK_LOG
    echo "  title: Database %separate%" >> $TASK_LOG
    echo "  delimiter: '\s*\|\s*'" >> $TASK_LOG
    echo "  header: [DateTime, Database, Owner, Size]" >> $TASK_LOG
    echo "  data: |" >> $TASK_LOG
  fi
  q="SELECT d.datname, u.usename, pg_database_size(d.datname) FROM pg_database d JOIN pg_user u ON (d.datdba = u.usesysid AND d.datistemplate = false);"
  ${FM_PSQL} -U ${FM_PSQL_SU} -t -c "${q}" | sed "s/^ /    ${logtime} \| /" >> $TASK_LOG
  
elif [ "$2" == "create" ]
then
  echo "title: 'Creating database : ${1}'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CREATE"
  echo "..."
  ${FM_PSQL} -U ${FM_PSQL_SU} -c "CREATE DATABASE ${1} WITH OWNER = ${3};" 2>&1
  
elif [ "${2}" == "recreate" ]
then
  # delete database and create again using same name and owner
  echo "title: 'Recreating Database [${1}]'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "..."
  ./helpers/postgres_recreatedb.sh "${1}"
  echo "Done"

else # delete or force_delete

  echo "title: 'Deleting Database ${1}'"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: DELETE"
  echo "..."
  if [ "${2}" == "delete" ]
  then
    echo "Delete"
    q="DROP DATABASE ${1};"
    ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q}" | sed s'/^/  /'
  elif [ "${2}" == "force_delete" ]
  then
    echo "Force delete"
    q1="UPDATE pg_database SET datallowconn = 'false' WHERE datname = '${1}';"
    q2="SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${1}';"
    q3="DROP DATABASE ${1};"
    ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q1}" | sed s'/^/  /'
    ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q2}" | sed s'/^/  /'
    ${FM_PSQL} -U ${FM_PSQL_SU} -c "${q3}" | sed s'/^/  /'
  fi

fi
  
