# name: Date and Time
# tags: System
# info: Show or Set Date and Time
# -----------------------------

# Main Script
# ---------------


# if parameters are 'timezone' Region Country
if [ "$1" == "timezone" ]
then
  timedatectl set-timezone "${2}/${3}"
fi

if [ $# -eq 1 ] && [ "$1" == "~default~" ] || [ "$1" == "region" ] || [ "$1" == "timezone" ]
then
  echo "title: Current Date and Time"
  echo "display:"
  echo "  - note: |"
  echo "      Please ensure date and time are correct (or approximately so)"
  echo "      before you try and start the NTPD service. If the time is too far"
  echo "      out, then the NTPD service will stop again."
  echo "  - table:"
  echo "      title: Time and Date"
  echo "      delimiter: ';'"
  echo "      header: [property,value]"
  echo '      data: "'
  #timedatectl | sed 's/^ *//' | sed 's/: /;/' | sed 's/^/        /' | sed 's/$/\n/'
  mkfifo mypipe
  timedatectl status | sed 's/^ *//' | sed 's/: /;/' > mypipe &
  while read i
  do
    if [ "$i" == *"NTP Service;active"* ]
    then
      NTP=1
    fi
    echo "        ${i}"
    echo ""
  done < mypipe
  rm mypipe
  echo '        "'

  echo "  - table:"
  echo "      title: Time Sync"
  echo "      delimiter: '>'"
  echo "      header: [property,value]"
  echo '      data: "'
  timedatectl show-timesync | grep -v '^NTPMessage=' | sed 's/=/>/' | sed 's/^/        /' | sed 's/$/\n/'
  echo '        "'
  
  # If NTP not active then allow setting of time and date
  if [ "$NTP" == "1" ]
  then
    d=`date "+%Y-%m-%d"`
    t=`date "+%H:%M:%S"`
    echo "  - form:"
    echo "      title: Set Date or Time"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: set"
    echo "        - label: Date (yyyy-mm-dd)"
    echo "          type: date"
    echo "          default: ${d}"
    echo "        - label: Time (hh:mm:ss)"
    echo "          type: time"
    echo "          default: '${t}'"
    echo "      submit: Set"
  fi
  
fi

if [ $# -eq 1 ] && [ "$1" == "~default~" ] || [ "$1" == "timezone" ]  # no params or timezone set
then
  echo "  - form:"
  echo "      title: Configure Timezone"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: region"
  echo "        - label: Region"
  echo "          select: Africa America Antarctic Arctic Asia Atlantic Australia Europe Indian Pacific"
  # timedatectl list-timezones | sed 's/\/.*//' | sort --unique
  echo "          delimiter: '\s'"
  echo "      submit: Next"
elif [ "$1" == "region" ]
then
  echo "  - form:"
  echo "      title: Configure Timezone"
  echo "      submit: Set Timezone"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: timezone"
  echo "        - label: Region"
  echo "          select: [${2}]"
  echo "        - label: Location"
  echo "          delimiter: '\n'"
  echo "          select: |"
  timedatectl list-timezones | grep "${2}/" | sed 's/^.*\///' | sed 's/^/            /'
  #ls -1 /usr/share/zoneinfo/${2}/ | sed 's/^/            /'
elif [ "$1" == "set" ]
then
  echo "title: Changing date and time"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  if [ -n $2 ]
  then
    echo "Setting date"
    timedatectl set-time "$2"
  fi
  if [ -n $3 ]
  then
    echo "Setting time"
    timedatectl set-time "$3"
  fi
  echo "Done"
fi
