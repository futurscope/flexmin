# ---------------
# name: Logs
# tags: Flexmin
# info: Flexmin Log Viewer
# ---------------------

name="Flexmin Logs"   # used in titles
folder_path='/var/log/flexmin'   # path of the configuration files to list
file_pattern="*.log"      # specifies what filenames to list as configuration files in path
list_headers="[Log,Modified,Size]"
list_format="%P;%Tc;%s\n" # file_ext=".nmconnection"           # what extension to put on newly created files
list_title="Script Logs"
# edit_mode="ini"   # specify edit mode for ACE editor
# create="yes"       # allow creating new configuration files
# yaml="yes"         # is file a flexmin YAML that generates the real file?
delete="yes"       # should we allow delete of existing files
# edit="yes"         # should we provide an edit option
view_tail=200

# ---- Default Action ----
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then 
  echo "title: $name"
  echo "display:"
  echo "  - table:"
  echo "      title: Flexmin Details"
  echo "      delimiter: ';'"
  echo "      header: [property,value]"
  echo "      data: |"
  echo "        Flexmin Version; $(flexmin version)"
  echo "  - table:"
  echo "      title: ${list_title}"
  echo "      header: ${list_headers}"
  echo "      actions: ?view${edit_action}${delete_action}"
  echo "      delimiter: ;"
  echo "      data: |"
  #find $folder_path/ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/        /'
  cd "${folder_path}"
  if [ ! -z "${primary_file}" ]
  then
    find $(dirname ${primary_file}) -name "$(basename ${primary_file})" -readable -printf "$list_format" | sed -r 's/^\.\///' | sed 's/^/        /'
  fi
  find ./ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /'
  cd - >/dev/null

  list_title="Install and upgrade logs"
  file_pattern="*.md"
  
  echo "  - table:"
  echo "      title: ${list_title}"
  echo "      header: ${list_headers}"
  echo "      actions: ?view${edit_action}${delete_action}"
  echo "      delimiter: ;"
  echo "      data: |"
  #find $folder_path/ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^.*\/([^/]+)$/\1/' | sed 's/^/        /'
  cd "${folder_path}"
  if [ ! -z "${primary_file}" ]
  then
    find $(dirname ${primary_file}) -name "$(basename ${primary_file})" -readable -printf "$list_format" | sed -r 's/^\.\///' | sed 's/^/        /'
  fi
  find ./ -name "${file_pattern}" -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /'
  cd - >/dev/null

# ---- View Action ----
elif [ "$2" == "view" ]
then
  if [ ! -z "${view_tail}" ]
  then
    echo "title: View ${1} (last $view_tail lines)"
  else
    echo "title: View ${1}"
  fi
  echo "method: popup"
  echo "code: |2"  # explicit indent specified to allow for extra spaces on first line
  # if tail length specified in view_tail parameter then use tail command
  if [ ! -z "${view_tail}" ]
  then
    tail -n $view_tail ${folder_path}/"${1}" | sed -e "s/\x1b//g" | sed 's/^/  /'
    #    ^ get contents                        ^ replace known control char used by py4web in some output
  else
    cat ${folder_path}/"${1}" | sed -e "s/\x1b//g" | sed 's/^/  /'
    #    ^ get contents         ^ replace known control char used by py4web in some output
  fi
 
 
fi
