 # ----------------------
# name: General
# tags: System
# info: General system management
# -------------------------

# Main Script
# ------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: System Management"
  echo "display:"
  echo "  - table:"
  echo "      title: System Details"
  echo "      delimiter: ';'"
  echo "      header: [property,value]"
  echo "      data: |"
  uname -r | sed -e 's/^/        Version;/'
  if [ $(command -v hostname) ]
  then
    hostname | sed -e 's/^/        Hostname;/'
  else
    echo '        Hostname; not found'
  fi
  date | sed -e 's/^/        Date and Time;/'
  uptime -s | sed -e 's/^/        Last boot;/'
  uptime -p | sed -e 's/^/        Running time;/'
  echo "  - table:"
  echo "      title: CPU Details"
  echo "      delimiter: ';'"
  echo "      header: [property,value]"
  echo "      data: |"
  cat /proc/cpuinfo | grep -E "MHz|name|cache size" | head -n 3 | sed -e 's/:/;/' | sed -e 's/^/        /'
  echo "  - table:"
  echo "      title: Memory Details"
  echo "      last_right: 1"
  echo "      delimiter: ';'"
  echo "      header: [property,value]"
  echo "      data: |"
  vmstat -s | grep -E "memory" | sed -E 's/^(.*)K(.*)$/        \2;\1KB/' | sed -E 's/([0-9])([0-9]{3})([ ,])/\1,\2\3/' | sed -E 's/([0-9])([0-9]{3})([ ,])/\1,\2\3/' | sed -E 's/([0-9])([0-9]{3})([ ,])/\1,\2\3/'
  echo "  - form:"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: shutdown"
  echo "      submit: Power Down"
  echo "  - form:"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: restart"
  echo "      submit: Restart"
  
elif [ "$1" == "shutdown" ]
then
  echo "title: Shutting down system"
  echo "method: popup"
  echo "---"
  echo "System shutdown begun."
  shutdown -P now
  
elif [ "$1" == "restart" ]
then
  echo "title: Restarting system"
  echo "method: popup"
  echo "..."
  shutdown -r now

fi
  

  
  
