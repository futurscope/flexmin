# -------------------
# name: Scheduled Tasks
# tags: System
# info: Lists Scheduled Tasks
# ---------------------

if [ ! $(command -v crontab) ]
then
  echo "title: Scheduled Tasks"
  echo "note: Crontab command not found on this system."
  exit 1
fi

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Scheduled Tasks"
  echo "table:"
  echo "  actions: '!delete'"
  echo "  header: [Line,Minutes,Hours,Date,Month, Weekday, Command]"
  echo "  delimiter: '\s+'"
  echo "  data: |"
  crontab -l 2>&1 | grep -v "no crontab" \
             | grep -n -v "^[[:space:]]*$" \
             | grep -v -E "^[0-9]+:#" \
             | grep -v -E "^[0-9]+:\w+=" \
             | sed -r 's/^([0-9]+)\:/    \1 /' \
             | sed "s|${FM_SCRIPTS}/common.sh [0-9]\{1,\}|(flexmin)|"

elif [ "$2" == "delete" ]
then
  echo "title: Deleting Crontab Entry"
  echo "method: popup"
  echo "category: DELETE"
  echo "next_action: reload_pane"
  echo "..."
  newcron=`crontab -l | sed "${1}d"`
  echo "${newcron}" | crontab -
fi
