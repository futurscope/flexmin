# -----------------------------
# name: Backup Files
# tags: Filesystem
# info: Folder Information (Size)
# -----------------------------

# Check backup user and home folder exist
./helpers/backup_check.sh || exit

list_format="%P;%Tc;%s\n"
file_pattern='.*\.(zip|gz)'

# Main Script
# ---------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  if [ "$FM_BACKUP" == "$FM_DOWNLOAD" ]
  then
    full_actions='~download.upload.!delete'
  else
    full_actions='upload.!delete'
  fi
  echo "title: Backup Files"
  echo "display:"
  echo "  - note: |"
  echo "      The contents of the ${FM_BACKUP} folder are shown below."
  echo "      You can upload these files to a FTP server by clicking on the "
  echo "      upload button to show the upload form."
  echo "      Unlocking these files makes them downloadable without authentication, use with caution!"
  echo "  - table:"
  echo "      actions: per_row"
  #echo "      actions_bulk: yes"
  echo "      header: [Filename,Last Updated,Size (Bytes)]"
  echo "      delimiter: ';'"
  echo "      data: |"
  cd ${FM_BACKUP}
  find ./ -regextype posix-extended -regex "${file_pattern}" | while read -r line
  do
    # look for the file checking it has read access for others
    res=$(find ./ -wholename "$line" -perm -o=r -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /')
    if [ -n "$res" ]
    then
        echo "$res;$full_actions" 
    else
        res=$(find ./ -wholename "$line" -readable -printf "$list_format" | sed -r 's/^\.\///' | sort | sed 's/^/        /')
        echo "$res;upload.!delete"
    fi
  done
  cd - >/dev/null 2>&1
  if [ "$FM_BACKUP" == "$FM_DOWNLOAD" ]
  then
    echo "  - form:"
    echo "      submit: Unlock All"
    echo "      fields:"
    echo "      - type: hidden"
    echo "        default: action"
    echo "      - type: hidden"
    echo "        default: unlock"
    echo "  - form:"
    echo "      submit: Lock All"
    echo "      fields:"
    echo "      - type: hidden"
    echo "        default: action"
    echo "      - type: hidden"
    echo "        default: lock"
  fi
  
elif [ "$2" == "unlock" ] || [ "$2" == "lock" ]
then
  [ "$2" == "unlock" ] && action="Unlocking"
  [ "$2" == "lock" ] && action="Locking"
  echo "title: $action Backups"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."  
  cd ${FM_BACKUP}
  [ "$2" == "unlock" ] && echo "$action folder $FM_BACKUP (giving 'other' rx permission)" && chmod o+rx . 
  [ "$2" == "lock" ] && echo "$action folder $FM_BACKUP (removing 'other' rx permission)" && chmod o-rx .
  find ./ -regextype posix-extended -regex "${file_pattern}" | while read -r line
  do
    echo "$action $line"
    [ "$2" == "unlock" ] && chmod o+r "$line"
    [ "$2" == "lock" ] && chmod o-r "$line"
  done
  cd - >/dev/null 2>&1

  
elif [ "$2" == "delete" ]
then
  echo "title: Deleting Selected Backups"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  cd ${FM_BACKUP}
  echo "Deleting ${1}"
  rm ${1}
  # Bulk delete
  #for f in ${1}
  #do
  #  rm ${f}
  #done
  cd - >/dev/null
  
elif [ "$2" == "upload" ]
then
  echo "title: Upload Selected Backups"
  echo "method: popup"
  echo "display:"
  echo "  - table:"
  echo "      header: [Files to Upload]"
  echo "      row_delimiter: '\s'"
  echo "      data: $1"
  echo "  - form:"
  echo "      submit: Upload Files"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: ${1}"
  echo "        - type: hidden"
  echo "          default: upload_confirmed"
  echo "        - label: Destination"
  echo "          info: Must be an ftp server in the form ftp://example.servername.org/folder1/folder2"
  if [ -n "$FM_REMOTE_UPLOAD" ]
  then
    echo "          default: ${FM_REMOTE_UPLOAD}"
  fi
  echo "          type: text"
  echo "        - label: Login name"
  echo "          type: text"
  echo "        - label: Password"
  echo "          type: password"

elif [ "$2" == "upload_confirmed" ]
then
  echo "title: Uploading Selected Backups"
  echo "method: popup"
  # NOTE: Copying stuff off this system is not a system change
  #       no status = no log of this action
  echo "..."
  uplist="lcd ${FM_BACKUP}"
  NL=$'\n'
  uplist="$1"
  # Bulk upload
  #for f in ${1}
  #do
  #  uplist="${uplist}${NL}put ${f}"
  #done
  echo "FTP Comamnds"
  echo "${uplist}"
  type=`echo "${3}" | sed -r 's/(^[^\:]+).*/\1/'`
  echo "Destination type is ${type}"
  if  [ "${type}" == "ftp" ]
  then
    echo "Using FTP"
    dest=`echo "${3}" | sed -r "s/\:\/\//\:\/\/${4}\:${5}\@/"`
    echo "Connecting to ${dest}"
    cat <<END | ftp $dest
${uplist}
END
  echo "Completed upload"
  fi
fi
