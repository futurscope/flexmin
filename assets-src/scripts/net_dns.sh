# ---------------------
# name: DNS Configuration
# tags: Network
# info: Display DNS server details
# --------------------

# Main Script
# ------------

# find out what network management system is being used
source ./helpers/net_manager.sh

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: $name - DNS Configuration"
  if [ "$name" == "Dhcpcd" ]
  then
    echo "note: DNS Configuration cannot be done here for dhcpcd client, edit dhcpcd.conf in Network Manager"
    exit 0
  else
    echo "display:"
    echo "  - note: WARNING, this feature is not complete only tested for Network Manager (nmcli) managed connections using IPv4."
    echo "  - code: |"
    cat /etc/resolv.conf | sed 's/^/      /'
    echo "  - form:"
    echo "      title: Set nameservers"
    echo "      fields:"
    echo "        - label: Select Connection"
    echo "          select:"
    echo "$list_conn" | sed 's/^/          - /'
    echo "        - type: hidden"
    echo "          default: set_dns"
    echo "        - label: Select IP Version"
    echo "          select: $ip_opt"
    echo "        - label: DNS Servers"
    echo "          info: Enter space separated list of DNS server addresses here, leave blank to have auto-assigned (DHCP)"
    echo "          type: text_long"
    dns_servers=$(cat /etc/resolv.conf | grep nameserver | sed -r 's/^nameserver[ ]+//' | sed -z 's/\n/ /g')
    echo "          default: ${dns_servers}"
    echo "      submit: Set DNS Servers"
    echo "  - form:"
    echo "      title: Set Search Domain"
    echo "      fields:"
    echo "        - type: hidden"
    echo "          default: set_search"
    echo "        - label: Domain suffix"
    echo "          info: Enter the domain suffix for your local domain"
    echo "          type: text"
    dns_search=$(cat /etc/resolv.conf | grep -E '^search .*$' | sed -r 's/^search[ ]+//')
    echo "          default: ${dns_search}"
    echo "      submit: Set DNS Search"
  fi
  
elif [ "$2" == "set_dns" ]
then
  conn=$1
  ip_ver=$3
  dns=$4
  echo "title: Setting DNS Servers"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  echo "Attempting to setting DNS Server(s): $4"
  
  if [ "$name" == "Network Manager" ]
  then
    ip_method=$(nmcli -t -g ${ip_ver}.method connection show "$conn")
    if [ -z "$dns" ]
    then
      # Clearing DNS server entry, check that DHCP will replace manual setting
      if [ "$ip_method" == "auto" ]
      then
        # revert to auto-assigned DNS (from DHCP)
        echo "Removing manual DNS server setting, DHCP will assign a DNS server."
        nmcli connection modify "${conn}" -${ip_ver}.dns "${dns}"
        nmcli connection modify "${conn}" ${ip_ver}.ignore-auto-dns no
      else
        # DHCP no active, no DNS server will be assigned, 
        echo "Cannot remove manual DNS configuration unless DHCP is assigning configuration."
      fi
    else
      # DNS entry is set, will ignore auto assigned DNS
      echo "DNS servers being set, if DHCP is in use the DHCP assigned DNS server will be ignored."
      nmcli connection modify "${conn}" ${ip_ver}.dns "${dns}"
      nmcli connection modify "${conn}" ${ip_ver}.ignore-auto-dns yes
    fi
    nmcli connection down "${conn}"
    nmcli connection up "${conn}"
  else    
    target='/etc/resolv.conf'
    tmp_file="${FM_TASK_TMP}/net_dns_resolv.conf"
    cat /etc/resolv.conf | grep -v "nameserver" > $tmp_file
    for d in $2
    do
        echo "nameserver $d" >> $tmp_file
    done
    cp $tmp_file $target
    rm $tmp_file
  fi
  echo "Done."

elif [ "$1" == "set_search" ]
then
  echo "title: Setting DNS Servers"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  echo "Search: $2"
  target='/etc/resolv.conf'
  tmp_file="${FM_TASK_TMP}/net_dns_resolv.conf"
  cat /etc/resolv.conf | sed -r "s/^search[ ]+.*$/search $2/" > $tmp_file
  cp $tmp_file $target
  rm $tmp_file
  echo "Done."

fi
  


