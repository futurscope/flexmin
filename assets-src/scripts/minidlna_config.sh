# -------------------------------
# name: Generic Configuration Script
# ------------------------------

name=MiniDLNA
app_name='minidlnad'
config='/etc/minidlna.conf'  # actual app config path
config_yaml='minidlna.yaml'  # flexmin yaml config path

source ./helpers/generic_yaml_editconfig.sh
