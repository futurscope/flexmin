# ---------------
# name: Package Flexmin
# tags: Flexmin
# info: Package this instance of flexmin service and upload
# ---------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Package Flexmin Service"
  echo "display:"
  echo "  - note: |"
  echo "      This process allows you to package up this flexmin instance with "
  echo "      its settings into a zip file in the backup area, ready for upload"
  echo "      to a server.The configs folder is excluded as this has system "
  echo "      specific configurations. However, the flexmin parameters and "
  echo "      menu yaml files from the configs folder are included."
  echo "  - form:"
  echo "      submit: Backup Flexmin Service"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: backup"
  echo "        - label: Filename"
  echo "          type: text"
  echo "          default: flexmin_service.zip"
  echo "  - form:"
  echo "      title: Backup Flexmin Settings"
  echo "      submit: Backup Flexmin Settings"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: backup_etc"
  echo "        - label: Filename"
  echo "          type: text"
  echo "          default: flexmin_etc.zip"
elif [ "$2" == "backup" ]
then
  echo "title: Backing Up Flexmin Service"
  echo "method: popup"
  echo "category: BACKUP"
  echo "..."
  servicedir=$(dirname ${FM_SCRIPTS})
  if [ -f ${FM_BACKUP}/${1} ]
  then
    rm ${FM_BACKUP}/${1}  # remove existing file if it exists
  fi
  echo "Zipping $servicedir to ${FM_BACKUP}/${1}"
  cd ${servicedir}/..
  # zip up contents, exclude configs folder
  zip -r ${FM_BACKUP}/${1} flexmin/* -x flexmin/configs/**\*
  # add in parameters.yaml from the configs folder
  #zip ${FM_BACKUP}/${2} flexmin/configs/parameters.yaml
  # add in menu.yaml from the configs folder
  #zip ${FM_BACKUP}/${2} flexmin/configs/menu.yaml
  cd -
elif [ "$2" == "backup_etc" ]
then
  echo "title: Backing Up Flexmin Settings"
  echo "method: popup"
  echo "category: BACKUP"
  echo "..."
  servicedir=$(dirname ${FM_SCRIPTS})
  if [ -f ${FM_BACKUP}/${1} ]
  then
    rm ${FM_BACKUP}/${1}  # remove existing file if it exists
  fi
  echo "Zipping ${FM_CONFIGS} to ${FM_BACKUP}/${1}"
  config_parent=$(dirname ${FM_CONFIGS})
  config_folder=$(basename ${FM_CONFIGS})
  cd ${config_parent}
  # zip up contents
  zip -r ${FM_BACKUP}/${2} ./${config_folder}/*
  cd -
fi
