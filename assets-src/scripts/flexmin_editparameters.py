# --------------------
# name: Edit Parameters
# tags: Flexmin
# info: Edit the flexmin parameters file (yaml), automatically propogate changesx
# ------------------------------------------------------------------------------
# -*- coding: utf-8 -*-
import os, shutil
import flexmin.utils.yamlconfigs as yamlconfigs

def run(vars,fm_vars,lines=None):
    if lines == None:
        retval = True  # return text
        lines = ["title: Edit Parameters"]
    else:
        lines.append("title: Edit Parameters")
        retval = False # output list supplied, just append to that

    f = os.path.join(fm_vars['FM_CONFIGS'], 'flexmin.conf')
    
    if len(vars) == 1 and vars[0] == "~default~": # No parameters supplied

        # return file details and content for editing
        lines.append("note: |")
        lines.append("  On save, changes will take effect immediately for shell scripts, python scripts require a ")
        lines.append("  restart of the flexmin service and the flexmin web service.")
        lines.append("edit:")
        lines.append("  filename: flexmin.conf")
        lines.append("  mode: text")
        lines.append("  data: |")

        if not os.path.isfile(f):
            f = os.path.join(fm_vars['FM_DEFAULT_CONFIGS'], 'flexmin.conf')
        with open(f,'r') as original:
            for line in original:
                lines.append("    " + line.strip('\n'))

    elif (len(vars) > 2) and (vars[1] == 'save'):

        # Save file content supplied in parameter 3 (file)
        
        lines.append("method: popup")
        lines.append('category: CHANGE')
        lines.append('next_action: reload_pane')
        lines.append('---')
        #x = yamlconfigs.Config(f, fm_vars)
        #x.native()
        #try:
        #    x = yamlconfigs.Config(f, fm_vars)
        #    x.native()
        try:
            shutil.copyfile(fm_vars['FM_SCRIPT_PARAM']+'_3',f)
            lines.append('Saved')
        except:
            lines.append("Error loading yaml and saving as native file format")
        
    # Return output to action function
    if retval:
        return '\n'.join(lines)
    else:
        return None
