# ----------------------
# name: Download Files
# tags: Filesystem
# info: Allows the download and unzipping of remote zip files
# -------------------------

# Main Script
# ------------------

if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Download File"
  echo "note: |"
  echo "  Download files to specified directory. "
  echo "  If the download is a zip file you can opt to have this "
  echo "  unzipped to the destination."
  echo "form:"
  echo "  fields:"
  echo "    - label: Action"  #1
  echo "      info: |"
  echo "        If unzip is selected, and file cannot be unzipped, then the "
  echo "        file itself gets copied to the destination. Unzip_install unzips"
  echo "        the file to the destination and looks for a setup.py or "
  echo "        to run install.sh"
  echo "      select: [unzip,unzip_install,download]"
  echo "    - label: Source"  #2
  echo "      info: HTTP/FTP download url for file"
  echo "      type: text"
  echo "      default: ${FM_REMOTE_SOURCE}"
  echo "    - label: Destination"  #3
  echo "      type: text"
  echo "    - label: Ownership"   #4
  echo "      info: user:group ownership as used by chown command"
  echo "      type: text"
  echo "      pattern: '[\w]+:[\w]+'"
  echo "    - label: Permission"  #5
  echo "      info: Permission in the form rwxrw-r-- as used by chmod"
  echo "      type: text"
  echo "      pattern: '[r-][w-][x-][r-][w-][x-][r-][w-][x-]'"
  echo "  submit: Download"
  
# handle download 
elif [ "$1" == "unzip" ] || [ "$1" == "unzip_install" ] || [ "$1" == "download" ]
then
  echo "method: popup"
  echo "title: Downloading ${2}"
  echo "category: CREATE"
  echo "..."
  echo "Downloading to temporary file ${FM_TASK_TMP}/task${FM_TASKID}.download"
  DFILE=${FM_TASK_TMP}/task${FM_TASKID}.download
  
  if [ "${2%%/*}" == "." ]  # if text up to first / is .
  then   # add default source path if supplied path begins ./
    SRC=${FM_REMOTE_SOURCE}/${2#*/}   # add default remote source to specified source
  else   # othersiw use source as supplied
    SRC=$2
  fi
  
  fetch -o $DFILE $SRC
  if [ $? -ne 0 ]  # check if fetch failed
  then
    echo "Download failed, aborting."
    exit 1
  fi
fi

# handle unzip of downloaded file if required
if [ "$1" == "unzip" ] || [ "$1" == "unzip_install" ]
then
  if [ -d ${3} ]
  then
    # convert mod string to number
    [ "$5" != "" ] && MODINT=`echo $5 | sed -e 's/rwx/7/g' \
                                            -e 's/rw-/6/g' \
                                            -e 's/r-x/5/g' \
                                            -e 's/r--/4/g' \
                                            -e 's/-wx/3/g' \
                                            -e 's/-w-/2/g' \
                                            -e 's/--x/1/g' \
                                            -e 's/---/0/g'`
    # convert mod string to number for directories
    # (ensure x is included if r or w are included)
    [ "$5" != "" ] && DIRINT=`echo $5 | sed -e 's/rwx/7/g' \
                                            -e 's/rw-/7/g' \
                                            -e 's/r-x/5/g' \
                                            -e 's/r--/5/g' \
                                            -e 's/-wx/3/g' \
                                            -e 's/-w-/3/g' \
                                            -e 's/--x/1/g' \
                                            -e 's/---/0/g'` 
    [ "$4" != "" ] && OWN=${4}

    # attempt to unzip downloaded file
    unzip -d ${3} $DFILE
    if [ $? -ne 0 ]
    then
      # if unzip fails then indicate this so download can be treated as ordinary file
      echo "Can't unzip, treating as ordinary file."
      UNZIPPED="no"
    else
      cd ${3}
      [ -n "$OWN" ] && echo "Changing ownership"
      [ -n "$OWN" ] && chown $OWN `unzip -l -q $DFILE | grep -E '[ ]+[0-9]+' | cut -w -f5`
      
      [ -n "$MODINT" ] && echo "Changing permissions"
      # chmod all extracted files and folder
      [ -n "$MODINT" ] && chmod $MODINT `unzip -l -q $DFILE | grep -E '[ ]+[0-9]+.*$' | cut -w -f5`
      # chmod folders only (only paths ending with /) 
      # folders need x access
      [ -n "$MODINT" ] && echo "Changing permissions (folders)"
      [ -n "$MODINT" ] && chmod $DIRINT `unzip -l -q $DFILE | grep -E '[ ]+[0-9]+.*[\/]$' | cut -w -f5`
    fi
  fi
fi

# handle case where install process to be run
if [ "$1" == "unzip_install" ]
then
  # find name of directory
  subdir=`unzip -l -q $DFILE | grep -E '[ ]+[0-9]+' | cut -w -f5 | sed -r 's/\/.*$//' | uniq`
  [ -d "${3}/${subdir}" ] && searchpath="${3}/${subdir}"  # idenitifed unique subdir look here
  [ -d "${3}/${subdir}" ] || searchpath="${3}"  # no subdir look in destination
  
  # look for suitable install script (python or shell)
  if [ -f "${searchpath}/setup.py" ]
  then
    cd ${searchpath}
    python setup.py install
    cd -
    #rm -R ${searchpath}
  elif [ -f "${searchpath}/install.sh" ]
  then
    cd ${searchpath}
    ./install.sh
    cd -
    #rm -R ${searchpath}
  fi
fi

# handle no unzip (download only) or if unzip failed
if [ "$1" == "download" ] || [ "$UNZIPPED" == "no" ]
then
  # not meant to unzip or unzip failed
  # copy file to destination folder (uses last part or url as filename)
  NEWFILE=${3}/$(basename "${2}")  # path to the new file
  echo "Copying file to ${NEWFILE}"
  cp $DFILE $NEWFILE
  # Change ownership if ownership string specified
  [ -n "$OWN" ] && echo "Changing ownership"
  [ -n "$OWN" ] && chown $OWN $NEWFILE
  # Change permissions if they are specified
  [ -n "$MODINT" ] && echo "Changing permissions"
  [ -n "$MODINT" ] && chmod $MODINT $NEWFILE
fi

# remove temp download file
[ ! -z ${DFILE} ] && rm $DFILE

