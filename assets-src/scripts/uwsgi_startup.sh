# -------------------------------
# name: Startup Options
# tags: uWSGI
# info: Lists available uWSGI config files for editing
# ------------------------------

# Main Script
# --------------

if [ -z $FM_UWSGI_ROOT ]
then
  echo "title: uWSGI Configuration"
  echo "text: |"
  echo "  Error - FM_UWSGI_ROOT is not configured"
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -d $FM_UWSGI_ROOT ]
then
  echo "title: uWSGI Startup Options"
  echo "display:"
  echo "  - text: |"
  cat /etc/rc.conf | grep "^uwsgi_" | sed 's/^/      /'
  echo "  - form:"
  echo "      long: yes"
  echo "      title: Set options"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: set"
  echo "        - label: ini file path"
  echo "          selectformat: linesep"
  echo "          select: |"
  if [ -d "${FM_UWSGI_ROOT}" ]
  then
    cd ${FM_UWSGI_ROOT}
    find ./ -maxdepth 1 -name '*.ini' | sed -r -e 's/^\.\///' -e s'/^/            /'
    cd -
  fi
  #ls ${FM_UWSGI_ROOT} | sed 's/^/            /'
elif [ "$1" == "set" ]
then
  echo "title: Setting uWSGI flags"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  if cat /etc/rc.conf | grep -i -q '^uwsgi_flags='
  then
    # entry already exists, modify it, sed uses : for replace command instead of / to allow for paths in sub string
    newrc1=`cat /etc/rc.conf | sed "s:^uwsgi_flags=.*:uwsgi_flags=\"--ini ${FM_UWSGI_ROOT}\/${2}\":"`
    echo "${newrc1}" > /etc/rc.conf
  else
    # add entry to end of rc.conf
    echo "uwsgi_flags=\"--ini ${FM_UWSGI_ROOT}/${2}\"" >> /etc/rc.conf
    echo "Changed"
  fi
fi
