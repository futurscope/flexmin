# ----------------------
# name: Projects
# tags: Django
# info: Django Project Management
# -------------------------

# Main Script
# ------------------

NL=$'\n'
# Check whether postgress installed and initialised
./helpers/django_check.sh || exit
django_version=`python -c "import django; print django.get_version()" 2>/dev/null`

#PROJECT=`cat /etc/rc.conf | grep "djangod_path" | sed -e 's/djangod_path=\"\(.*\)\"/\1/' | xargs basename`
project=$(dirname $1)
project_file=$(basename $1)
[ "$project" == "." ] && project=$project_file && project_file=''
project_folder=${FM_DJANGO_ROOT}/${project}

if [ -z $project ]
then
  echo "title: Django Project Details"
  echo "text: Project not specified"
  exit 1
fi

if [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ ! -z $project ]
then
  echo "title: Django Projects"
  echo "display:"
  echo "  - table:"
  echo "      actions: '~view'"
  echo "      header: [Project]"
  echo "      data: |"
  cd ${FM_DJANGO_ROOT}
  for a in */
  do
      echo "        ${a%/}"
  done
  cd - >/dev/null  # switch back to original folder

elif [ $# -eq 1 ] # project selected
then
  # Manage Apps within specified project
  [ -z $project ] && PROJECT=$2
  echo "title: Django Project - ${project}"
  echo "display:"
  echo "- table:"
  echo "    title: Key Files"
  echo "    actions: ['?view','?edit']"
  echo "    header: [File, Import]"
  echo "    delimiter: ';'"
  echo "    data: |"
  cd ${FM_DJANGO_ROOT}/${project}/${project}
  for nc in `find ./ -name "*settings*" | grep -v __pycache__`
  do
    dj_import=$(grep -E "^\s*from .*settings.* import" ${nc} | grep settings)
    echo "      ${nc};${dj_import}"
  done
  cd - >/dev/null # switch back to original folder
  echo "- table:"
  echo "    title: URLs"
  echo "    delimiter: '\s+'"
  echo "    header: [URL, View Func, Name]"
  echo "    data: |"
  cd ${FM_DJANGO_ROOT}/${project}
  python3 ./manage.py show_urls | grep -v "^\/$project\/admin" | sed 's/^/      /'
  cd - >/dev/null # switch back to original folder
  
elif [ "$2" == "view" ]
then
  # 1=file 2=view
  echo "title: View Configuration - ${project_file}"
  echo "method: popup"
  echo "---"
  cat ${project_folder}/${project}/${project_file}

elif [ "$2" == "edit" ]
then
  # 1=file 2=view
  echo "title: Edit Configuration - ${project_file}"
  echo "method: popup"
  echo "edit:"
  echo "  filename: ${1}"
  echo "  mode: python"
  echo "  data: |"
  cat ${project_folder}/${project}/${project_file} | sed 's/^/    /'
  
# ---- Save Action ----
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "..."
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 "${project_folder}/${project}/${project_file}"
  echo "Saved"


elif [ -d ${project_folder}/${project} ]
then
  if grep 'django_extensions' ${project_folder}/${project}/settings.py >/dev/null
  then
    dj_ext=1
  else
    echo "title: Django Project Details"
    echo "note: |"
    echo "  django-extensions needed (https://pypi.org/project/django-extensions/)."
    echo "  If you wish to use this feature please install django-extensions and"
    echo "  enable it in this project by inserting into your install applications list."
    exit 1
  fi

fi
