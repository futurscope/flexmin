# ---------------------
# name: Hostname Configuration
# tags: Network
# info: Display Hostname details
# --------------------

# Main Script
# ------------
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
    echo "title: Hostname Configuration"
    echo "display:"
    echo "  - table:"
    echo "      header: [Property,Value]"
    echo "      delimiter: ';'"
    echo "      data: |"
    hostnamectl | sed 's/^ *//' | sed 's/: /;/' | sed -e 's/^/        /'
    echo "  - form:"
    echo "      title: Set Hostname"
    echo "      fields:"
    echo "        - label: Hostname"
    echo "          type: text"
    echo "          default: $HOSTNAME"
    echo "        - type: hidden"
    echo "          default: set"
    echo "      submit: Set Hostname"
    echo "  - table:"
    echo "      title: Hosts file"
    echo "      header: [Hosts]"
    echo "      delimiter: ','"
    echo "      data: |"
    cat /etc/hosts | sed -e "s/^/        /"
    echo ""
    echo "  - table:"
    echo "      title: Edit hosts file"
    echo "      actions: '?edit'"
    echo "      header: [File]"
    echo "      data: |"
    echo "        /etc/hosts"
    
elif [ "$2" == "set" ]
then
    echo "title: Changing hostname"
    echo "method: popup"
    echo "next_action: reload_pane"
    echo "category: CHANGE"
    echo "---"
    if [ -n $1 ]
    then
        echo "Setting hostname to $1"
        hostnamectl set-hostname "$1"
    fi
    echo "Done"
    
elif [ "$2" == "edit" ]
then
    echo "title: Edit ${1}"
    [ ! -f /${1} ] && echo "error: File /${1} does not exist" && exit 1
    case "$1" in
    'etc/hosts'|'etc/hostsx' )
        echo "edit:"
        echo "  filename: ${1}"
        echo "  mode: text"
        echo "  data: |"
        cat /${1} | sed 's/^/    /'
        ;;
    * )
        echo "method: popup"
        echo "error: ${1} is not a valid file to edit"
        ;;
    esac
    
elif [ "$2" == "save" ]
then
  echo "title: Saving ${1}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "category: CHANGE"
  echo "---"
  # copy saved file to destination
  cp ${FM_SCRIPT_PARAM}_3 ${1}
  echo "Saved"
fi

