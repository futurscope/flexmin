# ----------------------
# name: Backup and Restore
# tags: Web2py
# info: Web2pY Application Management
# -------------------------

# Main Script
# ------------------
NL=$'\n'


if [ -z  ${FM_WEB2PY_ROOT} ]
then
  echo "title: Web2py"
  echo "text: FM_WEB2PY_ROOT is not defined, please define this in Flexmin parameters."
  exit
elif [ $# -eq 1 ] && [ "$1" == "~default~" ] && [ -f ${FM_WEB2PY_ROOT}/VERSION ]
then
  echo "title: Web2Py Applications"
  echo "display:"
  echo "  - table:"
  echo "      delimiter: '\s+'"
  echo "      actions: per_row"
  echo "      header: [Application, Status]"
  echo "      data: |"
  cd ${FM_WEB2PY_ROOT}/applications
  for a in */
  do
      echo "        ${a%/} unknown backup.!delete"
  done
  cd -  # switch back to original folder
  echo "  - note: |"
  echo "      The backup option allows you to backup a zipped copy of your web2py"
  echo "      application. This backup excludes local data as the backup is for"
  echo "      the application, not the data."
  echo "      The database, cache, errors, sessions, and uploads folder contents"
  echo "      are excluded from the zipped file. These folder are included as"
  echo "      empty folders though."
  echo "  - table:"
  echo "      title: Local Web2Py Backups"
  echo "      actions: '!restore.!delete'"
  echo "      delimiter: '\s+'"
  echo "      header: [Backup File]"
  echo "      data: |"
  cd ${FM_BACKUP}
  find ./ -type f -name '*.*.w2p.zip' | sed -e 's/^\.\///' -e 's/^/        /'
  #ls *.*.w2p.zip | sed 's/^/        /'
elif [ "$1" == "delete" ]
then
  echo "title: Deleting ${2}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "next_action: reload_pane"
  echo "---"
  if [ -f ${FM_BACKUP}/${2} ]
  then
    echo "Deleting backup file ${2}"
    rm -R ${FM_BACKUP}/${2}
  else
    echo "Deleting application ${2}"
    rm -R ${FM_WEB2PY_ROOT}/applications/${2}
  fi
  echo "Deleted."
elif [ "$1" == "backup" ]
then
  echo "title: Backup Application ${2}"
  echo "method: popup"
  echo "display:"
  echo "  - note: |"
  echo "      The web2py backup is separated into application (app.w2p.zip) and "
  echo "      local content and configuration (local.w2p.zip) files."
  echo "      The fields below allow you to specify what is saved into the "
  echo "      local.w2p.zip file as content."
  echo "  - form:"
  echo "      submit: Backup App"
  echo "      fields:"
  echo "        - type: hidden"
  echo "          default: backup_app"
  echo "        - type: hidden"
  echo "          default: ${2}"
  echo "        - label: DB Connector (file)"
  echo "          type: text"
  echo "          info: |"
  echo "            This is the file that defines the connection to the"
  echo "            database. It may be specific to this installation."
  echo "            If this file doesn't exist it will be ignored, clear this "
  echo "            entry if you don't want this file backed up, or change it if"
  echo "            a different file holds local initialization details."
  echo "          default: models/0_dbconnect.py"
  echo "        - label: Optional"
  echo "          info: |"
  echo "            These folders may contain data specific to this local "
  echo "            application (e.g. database files, uploaded files). Select"
  echo "            those folders that contain such data so they can be backed up."
  echo "          type: multiselect"
  echo "          options: |"
  echo "            uploads/*"
  echo "            databases/*"
  echo "            cache/*"
  echo "            errors/*"
  echo "            sessions/*"
elif [ "$1" == "backup_app" ] #&& [ -n "$FM_REMOTE_UPLOAD" ]
then
  # 1=zipandupload 2=appfolder 3=destinationfolder 4=dest-filename 5=login 6=password
  echo "title: Backup to Zip ${2}"
  echo "method: popup"
  echo "category: BACKUP"
  echo "next_action: reload_pane"
  echo "comment: Processing web2py application ${2}"
  backupfile="${2}.app.w2p.zip"
  backupopt="${2}.local.w2p.zip"
  # get owner (group) of existing application folder ($3 would get user)
  group=`ls -ld ${FM_WEB2PY_ROOT}/applications/${2} | awk '{print $4}'` 
  echo "restore: web2py_apps.sh restore_app ${backupfile}"
  # NOTE: Backup up stuff is not considered a system change
  #       no status = no log of this action
  echo "..."
  cd ${FM_WEB2PY_ROOT}/applications
  echo "Zipping Application ${2} to ${FM_BACKUP}/${backupfile}"
  # --filesync ensures zip file is updated to reflect added, changed, and deleted files in source
  zip -r --filesync ${FM_BACKUP}/${backupfile} ${2}/* -x ${2}/sessions/**\* ${2}/databases/**\* ${2}/cache/**\* ${2}/errors/**\* ${2}/uploads/**\*
  echo "Zipping Local Config + Content ${2} to ${FM_BACKUP}/${backupopt}"
  if [ -n "$3" ]
  then
    echo "Backing up ${3}"
    zip -r --filesync ${FM_BACKUP}/${backupopt} ${2}/${3}
  fi
  [ -n "$4" ] && list=`echo "${4}" | sed -r -e 's/(^\[|\]$)//g' -e 's/,/ /g'`
  for folder in ${list}
  do
    echo "Backing up contents of ${folder}"
    zip -r --filesync ${FM_BACKUP}/${backupopt} ${2}/${folder}
  done
  echo "Completed"
  cd -
elif [ "$1" == "restore_app" ] || [ "$1" == "restore" ]
then
  # identifies the type of web2py backup and restores application or content
  # as required.
  echo "title: Restoring from ${3}"
  echo "method: popup"
  echo "next_action: reload_pane"
  echo "..."
  echo "Restoring web2py application from ${2} (group: ${3})"
  echo "..."
  if [ -f ${FM_BACKUP}/${2} ]
  then
    if [ "${2#*.app.w2p.zip}" != "$2" ] # is app.w2p.zip file
    then
      # assume folder will match backup name prefix
      appfolder=`echo ${2} | sed 's/\.app\.w2p\.zip$//'`
      echo "Deleting existing ${appfolder}"
      rm -R ${FM_WEB2PY_ROOT}/applications/${appfolder}  # clear existing application folder
      echo "Installing application ${appfolder}"
      ./helpers/web2py_appinstall.sh ${FM_BACKUP}/${2}
      # extraction and permission setting here, should be same as install process
    elif [ "${2#*.local.w2p.zip}" != "$2" ] # is local.w2p.zip file
    then
      # check application folder exists before resting content
      if [ -d ${FM_WEB2PY_ROOT}/applications/${appfolder} ]
      then # application folders exists
        echo "Restoring local content + config to ${appfolder}"
        unzip -d ${FM_WEB2PY_ROOT}/applications/${appfolder}/ ${FM_BACKUP}/${2}
      else # no application folder exists
        echo "Application folder does not exist aborting."
        echo "Restore application before trying to restore content."
      fi
    fi
  else
    echo "Backup file ${2} not found. Aborting."
  fi
  echo "Completed"
else
  echo "title: Web2py"
  echo "No recognised inputs found, or web2py not installed"
fi
