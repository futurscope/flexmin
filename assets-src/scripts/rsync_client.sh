# ----------------------
# name: Client
# tags: Rsync
# info: Rsync Client Tasks
# -------------------------

# Main Script
# ------------------

logfile="rsync_client.log"

if ! command -v rsync >/dev/null 2>&1
then
  echo "title: Rsync Client"
  echo "text: Rsync not installed."
elif [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Rsync Client"
  echo "display:"
  echo "  - note: |"
  echo "      This is where rsync client commands can be executed or scheduled."
  echo "      These commands are executed as root user."
  echo "  - form:"
  echo "      submit: Run"
  echo "      fields:"
  echo "        - label: Destination" #1
  echo "          info: |"
  echo "            Path to copy files to, end with / to ensure multiple files "
  echo "            copied to folder. Leave blank to list files from source."
  echo "          type: text"
  echo "        - type: hidden"
  echo "          default: sync" #2
  echo "        - label: Source" #3
  echo "          info: |"
  echo "            server::module server:/path/to/file (path should end with"
  echo "            / to copy contents to destination)."
  echo "          type: text"
  echo "        - label: Method"  #4
  echo "          info: |"
  echo "            rsync option requires service running on source machine,"
  echo "            rsync-ssh requires a specified user which has been set up to "
  echo "            accept SSH connections and perform rsync over SSH."
  echo "          select: [rsync-ssh,rsync]"
  echo "        - label: Mirror" #5
  echo "          info: |"
  echo "            Mirror removes files/folders from the destination if they no"
  echo "            longer exist at the source"
  echo "          select: ['yes','no']"
  echo "        - label: User" #6
  echo "          info: |"
  echo "            Specify a user to Rsync over SSH, or to authenticate using"
  echo "            rsync credentials if connection to rsync daemon (service) is"
  echo "            attempted."
  echo "          type: text"
  echo "        - label: Password"  #7
  echo "          type: password"
  echo "        - label: Frequency" #8
  echo "          select: ['now','daily']"
  echo "        - label: Time (hh:mm or blank if now)" #9
  echo "          type: text"
elif [ "$2" == "sync" ]
then
  echo "title: Running Rsync"
  # only a system change if action is being scheduled instead of run now
  [ "$8" != "now" ] && echo "category: CHANGE"
  echo "method: popup"
  # classed as a system change if job scheduled, otherwise ignore
  [ "$8" != "now" ] && [ -n "$8" ]  && echo "category: CHANGE"
  echo "..."
  if [ "$8" == "now" ] || [ -z "$8" ]  # now or no frequency defined
  then
    if [ "$4" == "rsync-ssh" ]  # user defined use ssh
    then
      # destination defined
      if [ -n "$1" ]
      then
        # mirror=yes
        echo "----------------------------------------------------" \
            | tee -a ${FM_LOGROOT}/${logfile}
        date | sed 's/^/Begun: /' | tee -a ${FM_LOGROOT}/${logfile}
        echo "Source: ${3}  User: ${6}" | tee -a ${FM_LOGROOT}/${logfile}
        echo "Destination: ${1}" | tee -a ${FM_LOGROOT}/${logfile}
        echo "Mirror: ${5}  Method: ${4}" | tee -a ${FM_LOGROOT}/${logfile}
        echo "----------------------------------------------------" \
            | tee -a ${FM_LOGROOT}/${logfile}
        [ "$5" == "yes" ] &&\
          rsync -avz --delete -e "ssh -l ${6}" ${3} ${1} \
          | tee -a ${FM_LOGROOT}/${logfile}
        # mirror=no
        [ "$5" == "no" ] &&\
          rsync -avz -e "ssh -l ${6}" ${3} ${1} \
          | tee -a ${FM_LOGROOT}/${logfile}
        date | sed 's/^/Completed: /' | tee -a ${FM_LOGROOT}/${logfile}
        echo "" >> ${FM_LOGROOT}/${logfile}
      else
        # no destination, just list files (no logging for this, interactive only)
        echo "----------------------------------------------------"
        echo "Listing files or rsync shares"
        echo "----------------------------------------------------"
        rsync -avz -e "ssh -l ${6}" ${3}
      fi
    else
      echo "Not implemented"
    fi
  elif [ "$8" == "daily" ] && [ -n "$9" ] # scheduled
  then
    # import cron helper functions
    . ./helpers/cron.sh
    if [ -n "$7" ]
    then
      cron_set "${FM_SCRIPTS}/common.sh 0 '$0' $1 '$2' '$3' '$4' $5 '$6' $7" "${8}" "${9}"
    else
      cron_set "${FM_SCRIPTS}/common.sh 0 '$0' $1 '$2' '$3' '$4' $5 '$6'" "${8}" "${9}"
    fi
    echo "Done"
  else
    echo "Schedule details not recognised"
  fi
fi
