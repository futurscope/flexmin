name='Certbot Renewal Configuration'   # used in titles
folder_path='/etc/flexmin/private'   # path of the configuration files to list
file_pattern="certbot_*"      # specifies what filenames to list as configuration files in path
file_ext=""           # what extension to put on newly created files
edit_mode="txt"   # specify edit mode for ACE editor
create="no"       # allow creating new configuration files
#create_filename="certbot_cli"   # force a specific filename on the end user
# yaml="yes"         # is file a flexmin YAML that generates the real file?
delete="yes"       # should we allow delete of existing files
edit="yes"         # should we provide an edit option
# primary_file='/etc/dnsmasq.conf'   # allow specify a file outside folder_path to include
list_headers="[Filename,Modified]"  # headers for the list of matched files
list_format="%p;%Tc;%s\n"   # From find command's printf format options
#templates_path=$FM_CONFIG_TMPLT
#templates_filter="*.certbot_ini.template"

source ./helpers/generic_viewedit_list.sh

