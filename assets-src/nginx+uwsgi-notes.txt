/etc/uwsgi/vassals/py4web.ini:

[uwsgi]
socket = :8081
chdir = /home/flexmin/webapp
pythonpath = /home/flexmin/webapp
file = app.py
plugins = python
;master = true
;processes = 1
uid = http
gid = http
;chmod-socket = 770
;chown-socket = http:http
;vacumm = true

/etc/systemd/system/uwsgi-socket@py4web.service:

[Unit]
Description=uWSGI service unit
After=syslog.target

[Service]
PIDFile=/run/%I.pid
RemainAfterExit=yes
ExecStart=/usr/bin/uwsgi --ini /etc/uwsgi/vassals/%I.ini
ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill -INT $MAINPID
Restart=always
StandardError=syslog
KillSignal=SIGQUIT

[Install]
WantedBy=multi-user.target

Enable and start this uWSGI service
> systemctl enable uwsgi-socket@py4web.service
> systemctl start uwsgi-socket@py4web.service

Create WSGI app launcher in py4web app folder:

/home/flexmin/webapp/app.py
-----------------------------
import os
from py4web.core import wsgi
password_file = os.path.abspath(os.path.join(os.path.dirname(__file__),"password.txt"))
application = wsgi(password_file = password_file, dashboard_mode = "full",      apps_folder=os.path.abspath(os.path.dirname(__file__)))


to check uWSGI component works okay, in /etc/uwsgi/vassals run this command:
> uwsgi py4web.ini

Add this location in the NGINX yaml config to allow NGINX to handle the flexmin app

NGNIX yaml config entry
------------------------
        - comment: flexmin
          path: /flexmin
          uwsgi_pass: 127.0.0.1:8081
          include: uwsgi_params
      
      
This gets flexmin running as a py4web app as a single uWSGI process running as user http (same as NGINX)

Running uWSGI in emperor mode with py4web run as a vassal seems to cause 
problems. It appears to run in to problems trying to communicate with the
flexmin service via the 127.0.0.1:8001 port as nothing happens after this 
is attempted within the ction controller.
