#!/bin/bash

pause_time=5

# copy scripts from this system into scripts subfolder
rm -r ./scripts/   # deleting because --safe-links ignores symlinks but doesn't remove from destination
echo "Retrieving scripts from this system"
rsync -r --safe-links --exclude '__pycache__' --delete /home/flexmin/scripts/ ./scripts/

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

echo "Creating scripts zip file"
# zip scripts sub-folder contents into assets/scripts.zip file
rm -f ../flexmin/assets/scripts.zip
cd scripts
zip -r ../../flexmin/assets/scripts.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

echo "Retrieving py4web app from this system"
# zip py4web sub-folder contents into assets/py4web.zip file
# --copy-links ensures links to development versions of Fragmenty and TreeNav are included
rsync -r --copy-links --delete /home/flexmin/py4web/ ./py4web/

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

echo "Clearing out system specific files"
rm -r ./py4web/__pycache__/
rm -r ./py4web/.service/
rm ./py4web/password.txt
rm -r ./py4web/flexmin/databases/*
rm -r ./py4web/flexmin/__pycache__/
rm -r ./py4web/flexmin/modules/__pycache__/
rm -r ./py4web/flexmin/.service/
rm -fr ./py4web/flexmin/.git/
rm -f ./py4web/flexmin/.gitignore
rm -f ../flexmin/assets/py4web.zip

echo "Creating py4web zip file"
cd py4web
zip -r ../../flexmin/assets/py4web.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

# copy current flexmin configs and zip them up into assets folder
echo "Retrieving system config files and creating configs.zip file"
# excludes any files explicity defined as local e.t. menu.local.yaml
rsync -r --exclude '*.local.*' --delete /etc/flexmin/ ./configs/

# Remove references to certificates as they won't work on other system
#sed -i -r 's/ - 443 / #- 443 /' ./configs/*.nginx.yaml
#sed -i -r 's/ ssl_certificate\:.*$/ #ssl_certificate: \/path\/to\/certificate\/file/' ./configs/*.nginx.yaml
#sed -i -r 's/ ssl_certificate_key\:.*$/ #ssl_certificate_key: \/path\/to\/certificate\/key_file/' ./configs/*.nginx.yaml

rm -f ../flexmin/assets/configs.zip
cd configs
zip -r ../../flexmin/assets/configs.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

# copy scripts from this system into scripts subfolder
echo "Retrieving config-templates from this system"
rsync -r --delete /home/flexmin/config-templates/ ./config-templates/

echo "Creating config-templates zip file"
rm -f ../flexmin/assets/config-templates.zip
cd config-templates
zip -r ../../flexmin/assets/config-templates.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""


echo "Creating config-examples zip file"
rm -f ../flexmin/assets/config-examples.zip
cd config-examples
zip -r ../../flexmin/assets/config-examples.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""

echo "Copying system configuration files and creating system zip file"
# Check these files still needed, if changes here then check setup.sh script also
# systemd flexmin service unit file
cp /etc/systemd/system/flexmin.service ./system/
# systemd flexmin web service unit file (not needed if NGINX and uWSGI being used)
# cp /etc/systemd/system/flexmin-web.service ./system/
# cp /etc/nginx/sites-available/flexmin ./system/flexmin.nginx
# systemd uWSGI service file for py4web apps 
cp /etc/systemd/system/uwsgi-socket@flexmin.service ./system
# uWSGI config file for py4web flexmin app
cp /etc/uwsgi/flexmin.ini ./system

rm -f ../flexmin/assets/system.zip
cd system
zip -r ../../flexmin/assets/system.zip ./
cd ..

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""
