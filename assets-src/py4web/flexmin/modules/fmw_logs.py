#!/usr/bin/env python
# -*- coding: utf-8 -*-
#from gluon import *
import os

# Python Web App script, needs to return text output (yaml) and a boolean indicating whether this 
# task has finished (True) or whether more need to be done by the taskd version of the script (False)

def run(vars,fm_vars,db=None):
    lines = []
    
    if len(vars) == 1 and vars[0] == "~default~":
        actions = db( (db.action_log.id>0 ) &
                      (db.action_log.category != "BACKUP")
                     ).select(orderby=db.action_log.datetime)[-20:]
        lines.append("title: Task History (last 20 entries)")
        lines.append("display:")
        lines.append("  - table:")
        lines.append("      header: [id,datetime,command,category]")
        #lines.append("      actions: 'package.!delete'")
        #lines.append("      actions_bulk: yes")
        lines.append("      long: yes")
        lines.append("      delimiter: '\t'")
        lines.append("      data: |")
        for item in actions:
            if item.cmd != 'fmw_logs.py':  # exclude this script from any action log list
                lines.append('        ' + str(item.id) + '\t' + str(item.datetime) + '\t' + str(item.cmd) + '\t' + str(item.category))
        return '\n'.join(lines), True
    
    # Obsolete
    elif vars[1] == 'package' and vars['2'] == "":
        lines.append("title: Package Error")
        lines.append("method: popup")
        lines.append("text: No actions have been selected.")
        return '\n'.join(lines), True
    
    # Obsolete
    elif vars[1] == 'package':
        if not vars.has_key('3'):  # no package name specified
            lines.append("title: Package Actions")
            lines.append("note: |")
            lines.append("  Please check the action list below, and enter a filename and")
            lines.append("  description for your package.")
            lines.append("form:")
            lines.append("  long: yes")
            lines.append("  fields:")
            lines.append("    - type: hidden")
            lines.append("      default: " + vars['1'])
            lines.append("    - type: hidden")
            lines.append("      default: " + vars['2'])
            lines.append("    - label: Package Name")
            lines.append('      info: Only use alphnumeric characters and _ or - please')
            lines.append("      pattern: '[\w\d_-]+'")
            lines.append("      type: text")
            lines.append("    - label: Package Description")
            lines.append("      type: textbox")
            lines.append("table:")
            lines.append("  header: [Action ID,Action Command]")
            lines.append("  delimiter: '\t'")
            lines.append("  data: |")
            items = vars[2].split()
            actions = db(db.action_log.id.belongs(items)).select()
            for a in actions:
                lines.append("    " + str(a.id) + "\t" + a.cmd)
            return "\n".join(lines), True
        else:
            items = vars[2].split()
            actions = db(db.action_log.id.belongs(items)).select()
            lines = []
            #fileout.append(str(vars['1']))
            for a in actions:
                lines.append(str(a.id) + " " + a.cmd)
            return '\n'.join(lines), False

    # Obsolete
    elif vars[1] == 'delete':
        items = vars[2].split()
        actions = db(db.action_log.id.belongs(items)).delete()
        return "Actions deleted from database.", False

    else:
        
        return "title: Flexmin Logs\nnote: Nothing done", True
 
