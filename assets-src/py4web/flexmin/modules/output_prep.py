import re, yaml
import traceback


def getYaml(text, default_title=None):
    """
    Convert text output to a yaml form, using --- to separate yaml from plain text
    
    Supply default_title to use as a title if yaml doesn;t find a title
    """
    try:
        outList = re.split(r'\n[-\.]{3}', text, 1, re.MULTILINE)   # split into 2 with --- as separator
        out = outList[0]    # get yaml part
        output = yaml.load(out, Loader=yaml.SafeLoader)   # convert yaml
        # if --- in out then handle all output after --- as text
        if len(outList) > 1:
            if len(outList[1].strip()) > 0:
                output['code'] = outList[1]
    except:
        out = str(text) + '\n' + traceback.format_exc()
        output = {'code': out}
    # just text, no structure
    if not isinstance(output,dict):
        output = { 'text': output }
        
    if not 'title' in output:
        output['title'] = default_title
        
    # return dictionary from yaml in text
    return output
        
    


def display(output):
    """
    Convert outputs without a display attribute into a display attribute
    """
    if not 'display' in output:
        display = []
        for item in ['error','note','table','text','code','task','edit','form']:
            if item in output:
                display.append( { item : output[item] })
                del output[item]
        output['display'] = display

    for item in output['display']:
        form(item)
        table(item)

def authenticate(request,output):
    """
    Check output to see if login is required, adjust output dictionary 
    accordingly
    """
    # Handle authentication errors
    if 'error' in output:
        if 'ERROR - Login Required' in output['error']:
            output['method'] = 'popup'
            output['title'] = 'Flexmin Login'
            output['text'] = "Authentication Required"
            output['login'] = 'flexmin_login.py'
            if "flexmin_login" in request.path:
                # it is a login prompt
                # reload (based on browser path, as pane won't be updated yet).
                output['next_action'] = "reload"
            elif request.forms:
                # don't reload pane if form submit action was attempted as
                # form content will be lost forcing end user to re-enter
                output['next_action'] = "nothing"
            else:
                # no form submit, so no data to lose by reload
                output['next_action'] = "reload"
        else: # not an error prompting for login
            output['method'] = 'popup'
            output['title'] = 'Flexmin Error'
            output['text'] = output['error']

def form(output):
    """
    Process data in output['form'] so it is ready for view
    """
    # help out with some of the yaml in forms
    if 'form' in output:
        for field in output['form']['fields']:
            if 'select' in field and 'delimiter' in field:
                field['select'] = re.split(field['delimiter'],field['select'])
            if 'type' in field and field['type'] == "multiselect":
                # convert text field to list of check box values and labels
                cl = field['options'].split('\n')
                data = []
                for c in cl:
                    vl = c.split(";")
                    if len(vl)>1:
                        vd = {'value': vl[0],
                              'label': vl[1]}
                    else:
                        vd = {'value': vl[0],
                              'label': vl[0]}
                    data.append(vd)
                field['options'] = data
            # TODO selectformat is deprecated, to be removed when no scripts need it
            if 'selectformat' in field:
                if field['selectformat'] == 'spacesep':
                    field['select'] = re.split(r'\s',field['select'])
                elif field['selectformat'] == 'linesep':
                    field['select'] = re.split(r'\s*\n',field['select'])

def table(output):
    """
    Process data in output['table'] so it is ready for view
    """
    if 'table' in output:
        table = output['table']
        # get table rows (records)
        if 'delimiter_row' in table:
            table['data'] = re.split(table['delimiter_row'],table['data'])
        else:
            table['data'] = table['data'].splitlines()

        # Convert actions_bulk boolean into actions list for view processing
        if 'actions_bulk' in table and 'actions' in table and table['actions_bulk']:
            table['actions_bulk'] = table['actions'].split('.')
            
        # Take headers from first line of data if not specified in table properties
        if (not 'header' in table) and ('data' in table) and (len(table['data']) > 0):
            # look at first line for header, removing first line from data
            h = re.split(r'(\w+\s+)',table['data'].pop(0))
            headers = list(filter(lambda h: (h != ''), h))  # strip out empty strings
            header = list(map(str.strip, headers))   # strip trailing spaces
            table['header'] = header
        else:
            headers = []
            header = []

        if not 'delimiter' in table and headers:
            header_lengths = list(map(len, headers))  # find length of each column
        else:
            header_lengths = []

        # split rows into fields
        nd = []  # new data : list of table rows
        for r in table['data']:
            if 'delimiter' in table:
                # divide up data line based on delimiter
                if 'header' in table:
                    cn = len(table['header'])  # no. of columns
                    if 'actions' in table and table['actions'] == 'per_row':
                        pass  # allow for actions entry
                    else:
                        cn = cn - 1  # reduce split count as no actions entry
                    nr = re.split(table['delimiter'],r,cn)
                    nr = nr + [''] * (cn + 1 - len(nr))  # pad out to match no. of columns
                    # filter out excluded columns (header begins with -)
                    #for i,h in enumerate(table['header']):
                    #    if h[0] == '-':
                    #        del nr[i]
                else:
                    nr = re.split(table['delimiter'],r)
            elif header_lengths:
                # Extract columns based on header size
                col_start = 0
                nr = []
                for length in header_lengths:
                    nr.append(r[col_start:col_start+length])  # append columnd
                    col_start += length  # move start point forward
                if col_start < len(r):  # whoops we haven't got everything from the row
                    nr[-1] = nr[-1] + r[col_start:]   # extend last item to include rest of row
                    
            else:
                nr = [ r ]  # put in list on it's own

            if 'actions' in table:
                if table['actions'] == 'per_row':  # actions specified in row
                    # actions should be a list, but might be a string separated by .
                    if isinstance(nr[-1], str):
                        acts = nr[-1].strip().split('.')
                        nr[-1] = acts
                else:  # actions same for all rows
                    if isinstance(table['actions'], str):
                        acts = table['actions'].strip().split('.')
                    else:
                        acts = table['actions']
                    nr.append(acts)
            nd.append(nr)

        # filter out excluded columns
        # removed
        
        # Set up column show status
        col_show = []
        new_header = []
        for h in table['header']:
            if h[-2] == '!':
                col_show.append(h[-1])
                h = h[0:-2]  # take extra chars off
                new_header.append(h)
            else:
                col_show.append(3)
                new_header.append(h)
        table['header'] = new_header
                
        table['column_show'] = col_show

        table['data'] = nd
