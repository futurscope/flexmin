import re, datetime

def dateToInt(date):
    """
    Convert a date string yyyy-mm-dd-hh-mm to an integer
    (seconds since 1970-01-01 00:00)

    If no date found in string then return None
    """
    reDate = re.compile("""
        (?P<year>\d\d\d\d)-     # year
        (?P<month>\d\d)-        # month
        (?P<day>\d\d)-          # day
        (?P<hour>\d\d)-         # hour
        (?P<min>\d\d)-          # minute
        (?P<sec>\d\d)           # second
    """, re.VERBOSE)

    f = reDate.search(date)
    if f:
        g = f.groupdict()
        d = datetime.datetime(int(g['year']),int(g['month']),int(g['day']),
                              int(g['hour']),int(g['min']),int(g['sec']))
        origin = datetime.datetime(1970,1,1,0,0,0)
        delta = d - origin
        jt = (delta.days * 24 * 60 * 60) + delta.seconds
        return jt*1000
    else:
        return None
 
