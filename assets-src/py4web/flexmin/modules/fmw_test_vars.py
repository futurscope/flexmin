# --------------------
# name: Test Variables (Flexmin Python)
# tags: Test
# info: Output flexmin variables
# ------------------------------------------------------------------------------
# -*- coding: utf-8 -*-

import os, shutil, time

def run(vars,fm_vars,db=None):
    lines = []
        
    lines.append("title: Test Flexmin Task (Python) ")
    lines.append("method: pane")
    lines.append("---")
    lines.append("Running within Flexmin GUI")
    lines.append("This module is in the Flexmin web app's modules folder.")
    lines.append("No login required as Flexmin service is not being used.")
    lines.append("")
    for i, v in enumerate(vars):
        lines.append(str(i) + " = " + str(v))
    
    lines.append("")
    for key in fm_vars:
        lines.append(key + " = " + str(fm_vars[key]))
        
    return '\n'.join(lines), True
