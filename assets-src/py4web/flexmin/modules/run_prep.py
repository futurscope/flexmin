from base64 import decodebytes
import re

def getInputs(base_script, forms, service, taskid, myobject=None):
    """
    Process form dictionary to build up these outputs
    - script (shell command string for running script)
    - log_script (command to be logged, removing sensitive password fields)
    - params (python parameter dictionary)
    
    forms is a Bottle forms dictionary
    service is the external service (used to write data to file for larger data blocks)
    """
    
    #params = {}
    #script = base_script
    #log_script = base_script
    myscript = Script(base_script)
    
    #if 'back' in forms and forms.back == 'cancel':
    #    return myscript, myscript, {'1': '~default~'}

    # list inputs to be handled as files
    if forms.file_inputs:
        file_inputs = forms.file_inputs.split(' ')
        print("File inputs: " + str(file_inputs))
    else:
        file_inputs = []
        
    print("Parameters: " + str(forms))

    # list inputs to be handled as passwords (to avoid logging actual password)
    if forms.password_inputs:
        password_inputs = forms.password_inputs.split(' ')
    else:
        password_inputs = []

    # append variables 1,2,3... in order
    if myobject:  # object from url is supplied
        myscript.add_param('1', str(myobject), str(myobject))
        variable = 2
    elif not '1' in forms:  # if no object in url, then check posted form
        myscript.add_param('1', '~default~', '~default~')
        variable = 2
    else:  # otherwise supply 'default' as first parameter'
        variable = 1
    while str(variable) in forms:
        log_value = None
        # save variables as strings to go in script command or files
        if str(variable) in password_inputs:
            value = forms[str(variable)]
            # if this is a password parameter, and a password is specified swap it
            # with a password substitution string for security
            if variable>1 and forms[str(variable)] \
                and (not str(variable-1) in password_inputs):
                # log the password substitute string which is based on the previous
                # variable (assumed to be the login name)
                log_value = '{{pw@' + forms[str(variable-1)] + '}}'
            elif variable>2 and forms[str(variable)] \
                    and (not str(variable-2) in password_inputs):
                # handle case where two password fields used for password confirmation
                # assumes confirmation of same password where one password input immediately follows another
                log_value = '{{pw@' + forms[str(variable-2)] + '}}'
        if forms[str(variable)][0:16] == "data:application":
            # is base64 encoded text version of binary file
            bFile = forms[str(variable)].split(',')[1]
            # Do this via service for full access rights
            service.writeTaskDataFile(taskid, base_script, variable, 'wb', decodebytes(bFile))
            value = 'file'  # send string 'file' as script parameter so script knows to look for file
        elif str(variable) in file_inputs:  # input has been specified as a file
            # Do this via service for full access rights
            service.writeTaskDataFile(taskid, base_script, variable, 'w', forms[str(variable)])
            value = 'file'  # send string 'file' as script parameter so script knows to look for file
        else:
            value = forms[str(variable)]

        if not log_value:
            log_value = value  # if no alternative value for log set then this will be the same

        # store value for passing to python function or script
        
        
        """
        params[str(variable)] = value  # for python
        if re.match(r'^[\w_\-\.]+$',str(value)):  # safe string, no need to quote
            script = script + " " + str(value)
            log_script = log_script + " " + str(log_value)
        else:  # may have some awkward characters, quote value just in case
            script = script + " '" + str(value) + "'"  # for shell scripts
            log_script = log_script + " '" + str(log_value) + "'"
        """
        myscript.add_param(str(variable), value, log_value)

        variable = variable + 1 

    return myscript.get_script(), myscript.get_logscript(), myscript.get_params()

class Script(object):
    
    def __init__(self, base_script):
        self.params = []   # paramter dictionary for python scripts
        self.script = base_script  # script command including parameters pass to script
        self.log_script = base_script  # script command as recorded in log
        
    def add_param(self, name, value, log_value):
        self.params.append(value)  # for python
        if re.match(r'^[\w_\-\.]+$',str(value)):  # safe string, no need to quote
            self.script = self.script + " " + str(value)
            self.log_script = self.log_script + " " + str(log_value)
        else:  # may have some awkward characters, quote value just in case
            self.script = self.script + " '" + str(value) + "'"  # for shell scripts
            self.log_script = self.log_script + " '" + str(log_value) + "'"
            
    def get_params(self):
        return self.params
    
    def get_script(self):
        return self.script
    
    def get_logscript(self):
        return self.log_script
        
