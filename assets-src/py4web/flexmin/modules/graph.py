import yaml, re

class Table(object):
    
    def __init__(self, text):
        self.data = yaml.load(text, Loader=yaml.SafeLoader)
        if 'table' in self.data:
            self.table = self.data['table']
        if 'title' in self.data:
            self.title = self.data['title']
        else:
            self.title = ''
        
    def to_chartjs(self, name=[], exclude='tmpfs|loop[0-9]'):
        """
        Convert table date to data for Chart.js
        TODO: Check graph entries match up with header entries
        
        Takes data in the form:
        date       obj  A  B  C  D
        2020-07-21 ObjA 34 45 65 45
        2020-07-21 ObjB 45 32 45 32
        2020-07-22 ObjA 34 45 65 45
        2020-07-22 ObjB 45 32 45 32
        
        And converts it to set of labels and datasets suitable for use in ChartJS 
        where ObjA and ObjB are separate datasets in the same chart and A, B, C, D
        are separate charts.
        """
        chart = {}
        chart_datasets = {}
        name_list = self.table['graph']  # just one name for now
        for n in name_list:
            chart[n] = {'labels': [],
                        'datasets': [],
                        'dataset_max': 0,
                        'dataset_min': 0}
            chart_datasets[n] = {}

        columns = {}  # dictionary of header names to column index
        for i, head in enumerate(self.table['header']):
            columns[head] = i
            if i == 0 and not name:
                name = head
            if head == self.table['separate']:
                dataset_column = i
                
        print("dataset_column:" + str(dataset_column))
                
        datetime_list = []  # list of places on x (time) axis
        datetime_index = {} # position in list of specific datetime strings 
        dataset_list = []   # list of datasets to show in each graph
        delimiter = self.table['delimiter']
        exclude_re = re.compile("^.*(" + exclude + ").*$")
                
        for row in self.table['data'].split('\n'):
            if not re.match(exclude_re, row):
                row_list = re.split(delimiter, row)
                if len(row_list) == len(self.table['header']):
                    print("row: " + row)
                    # get date from first column and add to date_time_list if not already there
                    dt = row_list[columns['DateTime']] # get date from 'DateTime' column (header)
                    if not dt in datetime_list:
                        dt_index = len(datetime_list)
                        datetime_list.append(dt)
                        datetime_index[dt] = dt_index
                        
                    # identify dataset name and add to dataset_list and chart_datasets if not already there
                    dataset_name = row_list[dataset_column]
                    if not dataset_name in dataset_list:
                        dataset_list.append(dataset_name)
                        for name in name_list:
                            chart_datasets[name][dataset_name] = []
                    
                    # get value of the 'name'd column in this row and add to this dataset
                    for name in name_list:
                        # Should be an integer or float value
                        try:
                            val = int(row_list[columns[name]])
                        except ValueError:
                            val = float(row_list[columns[name]])
                        
                        # Insert value into dataset at the appropriate place
                        # index in dataset list should track index in datetime list
                        dataset_len = len(chart_datasets[name][dataset_name])
                        if not dataset_len == dt_index:
                            # pad dataset with empty values to make it match datetime list length
                            chart_datasets[name][dataset_name] += [None]*(dt_index-dataset_len)
                        chart_datasets[name][dataset_name].append(val)
                            
                        # Check and modify min and max values
                        if val > chart[name]['dataset_max']:
                            chart[name]['dataset_max'] = val
                        if val < chart[name]['dataset_min']:
                            chart[name]['dataset_min'] = val

        for name in name_list:
            chart[name]['labels'] = datetime_list
            for ds in dataset_list:
                dataset_obj = {'label': ds,
                        'data': chart_datasets[name][ds]}
                chart[name]['datasets'].append(dataset_obj)
            
        return {'title': self.title,
                'charts': chart}
        

if __name__ == '__main__':

    import os, pprint, json
    pp = pprint.PrettyPrinter(indent=2, sort_dicts=False)
    with open(os.path.join('test_graph.txt'),'r') as f:
        table = f.read()
    t = Table(table)
    data = t.to_chartjs()
    pp.pprint(data)
    print(json.dumps(data))
