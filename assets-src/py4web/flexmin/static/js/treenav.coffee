###
Tree Navigation utility.
- Allows expanding and collapsing of nodes within a 'tree'
- Uses Fragmenty to load tree branch data on the fly
- Tree can exist within a table (e.g. first column has tree entries in to expand and show data in table
- Tree should be able to take the form of a structured list, but not tested

Requirements:
- jQuery
- Fragmenty (optional, for dynamicly loading branch entries)

Usage:
treenav = new TreeNav ({
    debug: 2,
    fragmenty: frag    # Fragmenty object for appending branch data into tree
});
###

class window.TreeNav

    constructor: (@opt) ->
        @_opt = # set default values
            # instance variables (ref using @_opt.variable_name
            debug: 0                # 0 = no debuggin info, 1 = some debugging info
            activeItemClass: 'is-active'
            autoClose: true         # automatically close other groups when one is opened
            closedHTML: '<i class="twisty-closed-hvr"></i>'
            closedClass: "twisty-closed-hvr"
            openHTML: '<i class="twisty-open-hvr"></i>'
            openClass: "twisty-open-hvr"
            sitePath: null
            openlevels: 1                 # How many levels to expand when opening a node */
            fragmenty: null             # Fragmenty object to handle dynamic loading of tree content if neccessary
        $.extend(true, @_opt, @opt) # overwrite defaults with those provided
        @init()


    init: (opt) ->
        # handle navigation tree actions, incl. links
        # assumes all .treenav containers already exist on the page
        # new content loaded within these .treenav containers will be handled by the event registered here
        # new .treenav containers won't be handled.
        $(document.body).on("click", ".treenode .treeitem", @openclose)

        if @_opt.sitePath.startsWith('/')
            match = window.location.href.trim().match(/^((http[s]?|ftp):\/\/)?\/?([^\/\s]+)/i);
            site = match[1]+match[3];
            @_opt.debug > 0 and console.log("TreeNav.init site: " + site);
            @_opt.sitePath = site + @_opt.sitePath
            @_opt.debug > 0 and console.log("TreeNav.init updated sitePath: " + @_opt.sitePath);

        $(window).bind('popstate', @navigate)
        $(window).bind('fragmenty:load', @showActive)   # Custom event to allow interaction with Fragmenty
        @_opt.debug > 0 and console.log("TreeNav.init")
        do @showActive


    findSiblings: (el) =>
        # Find those tree rows that are within the same section as this element
        # This does not yet serve a practical purpose, but will be used to load
        # related content (siblings) above and below the main content
        el = $(el).closest('.treerow')  # work at treerow level where relavent data is held
        next = $(el).nextUntil('.treenode')
        prev = $(el).prevUntil('.treenode')
        siblings = prev.add(el).add(next)
        reflist = siblings.map(-> $(@).attr('-data-ref')).get()
        @_opt.debug > 1 and console.log('sibling refs: ' + reflist.toString())


    navigate: (e) =>
        # Handle change due to browser navigation buttons (forward and back)
        link = location.href
        @_opt.debug > 0 and console.log "TreeNav.navigate new location: " + link
        @_opt.debug > 0 and console.log "TreeNav.navigate sitePath: " + @_opt.sitePath
                
        # Check if still within the 'master' area we are responsible for
        if link.startsWith @_opt.sitePath
            if not @_opt.fragmenty
                do @showActive   # if linked to fragmenty, let fragmenty trigger showActive function
            else
                @_opt.debug > 0 and console.log("TreeNav.navigate letting Fragmenty trigger showActive")
        else
            @_opt.debug > 0 and console.log("TreeNav.navigate outside our site, ignoring")

        
    showActive: =>
        # Finds the anchor element which contains the current path and marks it
        # as active using the class 'active'. Also opens up the parent in the 
        # navigation tree to ensure this link item is visible in the navigation tree
        @_opt.debug > 0 and console.log "TreeNav.showActive"
        link = location.href
        relative = location.href.replace(location.origin,'').replace(location.hash,'').replace(location.search,'').replace(/\/*$/,'')
        @_opt.debug > 1 and console.log "TreeNav.showActive relative: " + relative
        
        # remove active class from tn-linkitem object not matching current relative href
        $('.tn-linkitem a').not('[href|="' + relative + '"]').closest(".tn-linkitem").removeClass(@_opt.activeItemClass)
        
        # find linkitems that do match the current href (beginning of it)
        query = $('.tn-linkitem a[href|="' + relative + '"]').closest(".tn-linkitem")
        
        # not found try with /index.html added
        if not $(query).length > 0  # not found try again with /index.html added to the end
            @_opt.debug > 1 and console.log "TreeNav.showActive looking again: " + relative + '/index.html'
            query = $('.tn-linkitem a[href|="' + relative + '/index.html"]').closest(".tn-linkitem")
        if not $(query).length > 0  # not found try trimming the url and looking again
            trimmed = relative.split('/').slice(0,-1).join('/')
            @_opt.debug > 1 and console.log "TreeNav.showActive looking again: " + trimmed
            query = $('.tn-linkitem a[href|="' + trimmed + '"]').closest(".tn-linkitem")
        while $(query).length = 0 and ( trimmed.split('/').length > 1) # not found try trimming the url and looking again
            trimmed = trimmed.split('/').slice(0,-1).join('/')
            @_opt.debug > 1 and console.log "TreeNav.showActive looking again: " + trimmed
            query = $('.tn-linkitem a[href|="' + trimmed + '"]').closest(".tn-linkitem")
        $(query).addClass(@_opt.activeItemClass)

        @_opt.debug > 1 and console.log "TreeNav.showActive query: " + query.length
        
        @_opt.debug > 1 and console.log "TreeNav.showActive about to open parent"
        @openParent($(query).first())  # assumes only one TreeNav linkitem will be found for the current address


    openParent: (el) =>
        # Get parent of a tree node and set it to open
        # TODO: Start by opening top level node if closed, then move down descendant treenodes in turn
        rowId = $(el).attr("id")
        @_opt.debug > 1 and console.log "TreeNav.openParent of " + rowId

        # get treerow
        row = $(el).closest('.treerow')
        rowLevel = parseInt $(row).attr('data-tnlevel')
        if rowLevel > 0
            openLevel = rowLevel-1  # level we are looking for parent node in
        rowId = $(row).attr("id")
        action = false
        openList = []
        
        while rowLevel > 0 
                
            # Find next treenode further up the hierarchy
            row = $(row).prevAll('.treenode[data-tnlevel="' + openLevel + '"]:first')
            rowLevel = parseInt $(row).attr('data-tnlevel')
            if rowLevel > 0
                openLevel = rowLevel-1  # level we are looking for parent node in
            rowId = $(row).attr("id")
            openList.push(row)

            # Trigger opening of this node if closed
            if $(row).hasClass("tn-closed")
                # Trigger this treenode to open
                action = true                
                @_opt.debug > 1 and console.log "TreeNav.openParent opening rowLevel: " + rowLevel + " id: " + rowId
                $(row).addClass('tn-opening')
                    #.addClass('maxlevel' + (rowLevel + @_opt.openlevels))
                    .removeClass('tn-closed')


        if action
            $(openList).each (i, el) =>
                @_opt.debug > 1 and console.log "TreeNav.openParent open node " + $(el).attr('id')
            @autoClose(openList)
            do @actionOC  # trigger action that opens or closes part of tree
            
    
    autoClose: (openList) =>
        # given a list of treenode elements that should be open, close all other elements at the same tn-level
        if @_opt.autoClose
            while el = openList.pop()
                rowLevel = parseInt $(el).attr('data-tnlevel')
                rowId = $(el).attr("id")
                $('.treenav .treenode.tn-open[data-tnlevel="' + rowLevel + '"][id!="' + rowId + '"]')
                    .addClass("tn-closing").removeClass("tn-open")
            
            
    openclose: (e) =>
        # Trigger the opening or closing of a branch of the tree
        @_opt.debug > 0 and console.log("TreeNav.openclose " + e.type)
        if e.type != "click"  
            @_opt.debug > 1 and console.log("TreeNav.openclose type=" + e.type + "abandoned")
            return  # ignore touch events
        el = e.target
        row = $(el).closest('.treerow')  # make sure we are dealing with treerow, not a child element
        
        if $(row).hasClass('tn-open')    # currently open, set to closing
        
            $(row).addClass('tn-closing')
                  .removeClass('tn-open')
            do @actionOC
            
        else if $(row).hasClass('tn-closed')  # currently closed, set to opening
            @_opt.debug > 1 and console.log("TreeNav.openclose opening closed branch")
            rowLevel = parseInt $(row).attr('data-tnlevel')
            $(row).addClass('tn-opening')
                  #.addClass('maxlevel' + (rowLevel + @_opt.openlevels))
                  .removeClass('tn-closed')
                  
            @autoClose([row])  # close other open nodes at this level

            if $(row).hasClass('tn-load') and @_opt.fragmenty != null
                # if need to load tree branch then go and do this
                href = $(row).attr("data-branch")
                if href
                    $('body').addClass('wait')
                    $.ajax
                        type: 'GET'
                        url: href
                    .done (content, status, xhr) =>
                        @_opt.debug > 1 and console.log("TreeNav.openclose branch load done")
                        @_opt.fragmenty.append content, status, xhr
                        $(row).removeClass("tn-load")
                        do @actionOC  # need to trigger opening if just loaded tree branch
                    .fail =>
                        @_opt.debug > 1 and console.log("TreeNav.openclose branch load failed")
                        $(row).addClass("tn-load tn-closed").removeClass("tn-opening")
                    @_opt.debug > 1 and console.log("TreeNav.openclose loading branch: " + href)
            else
                # branch already exists, begin opening process straight away
                do @actionOC  # trigger action that opens or closes part of tree

            
    # TreeNav function 
    actionLevel: =>
        level = parseInt $(this).attr('data-tnlevel')
        @_opt.debug > 1 and console.log "TreeNav.actionlevel tn-level: " + level
        $('.treenode.tn-open[data-tnlevel="' + level + '"]').removeClass('tn-open').addClass('tn-closing')
        if level > 0
            i = 0
            while i < level
                $('.treenode.tn-closed.tn-level' + i).removeClass('tn-closed')
                                                     .addClass('tn-opening')
                i++
        @actionOC
        return
    

    actionOC: =>
        # control repeated call of action function until not needed        
        #console.log "Fragmenty actionOC"
        carryon = do @action
        # If true returned then keep going 
        c = window.setTimeout(@actionOC, 25) if carryon
        return

  
    action: ->
        # Look for treenodes with class 'tn-closing' or 'tn-opening' and
        # hide the next row due to be hidden. Returns true if an action was
        # carried out which indicates that this may need to be called again.
        # If false is returned, then opening/closing action can be deemed complete.

        carryon = false # Assume we won't have to run again
        #console.log "Fragmenty action"
        
        # Act on closing branches 
        closing = $(".treenode.tn-closing")
        closing.each (index, el) =>
            re = /tn-level(\d+)/
            rowLevel = parseInt $(el).attr("data-tnlevel")
            @_opt.debug > 1 and console.log "TreeNav.action tn-level: " + rowLevel
            next = $(el).nextAll().not(".treehide").first()     # next unhidden row
            nextLevel = parseInt $(next).attr("data-tnlevel") or 0  # if no more elements this nextLevel = 0
          
            # If no rows left or next row is same level as closing row then finished closing 
            if nextLevel <= rowLevel
                # no more elements, finished closing 
                @_opt.debug > 1 and console.log "TreeNav.action find open icon: i." + @_opt.openClass
                $(el)
                    .removeClass("tn-closing")
                    .addClass("tn-closed")
                    .find("i." + @_opt.openClass)
                    .removeClass('twisty-open-hvr')
                    .addClass(@_opt.closedClass)
            else
                # carry on closing 
                if $(next).hasClass("tn-open")
                  
                    # don't hide open nodes, close them first 
                    $(next).addClass("tn-closing").removeClass "tn-open"
            
                # not open and not closing, so hide 
                else $(next).addClass "treehide"  unless $(next).hasClass("tn-closing")
                carryon = true
            return

        # Act on opening branches 
        opening = $(".treenode.tn-opening")
        opening.each (index, el) =>
          
            # Find level of this row 
            rowLevel = parseInt $(el).attr("data-tnlevel")
            maxLevel = parseInt $(el).attr("data-tnmaxlevel")
          
            # Look for next hidden sibling at one level down from this element 
            next = $(el).next()
            tryagain = false
            endofsublevel = false
            waitforopen = false
            loop
                # Find level of this next element, if no next element level is 0
                nextLevel = parseInt $(next).attr("data-tnlevel") or 0
                @_opt.debug > 1 and console.log("TreeNav.action loop nextLevel: " + nextLevel)
                if nextLevel <= rowLevel
                    # finished looking through branch's lower levels 
                    endofsublevel = true
                else if $(next).hasClass("tn-opening")
                    waitforopen = true
                    tryagain = false # Don't look beyond node while waiting for it to open
                else if (nextLevel > rowLevel + 1) or (not $(next).hasClass("treehide"))
                    # If next element is below correct level or not hidden then skip to one after 
                    next = $(next).next() # move to next element
                    tryagain = true # moved to next element so repeat checks
                else
                    # We have the next element we need 
                    tryagain = false
                break unless tryagain and not endofsublevel
            if endofsublevel # finished opening
                # only works for compact html as string based replace
                @_opt.debug > 1 and console.log "TreeNav.action find closed icon: i." + @_opt.closedClass
                $(el)
                    .removeClass("tn-opening")
                    #.removeClass("tn-maxlevel" + maxLevel)
                    .addClass("tn-open")
                    .find("i." + @_opt.closedClass)
                    .removeClass('twisty-closed-hvr')
                    .addClass(@_opt.openClass)
            else if waitforopen
                # Do nothing while next element is still expanding 
                carryon = true
            else
                # unhide this next element and carry on (run again) 
                if nextLevel is rowLevel + 1
                    $(next).removeClass "treehide"  
                if ($(next).hasClass("tn-closed")) and (nextLevel < maxLevel)
                    $(next)
                        .addClass("tn-opening")
                        .addClass("tn-maxlevel" + maxLevel)
                        .removeClass "tn-closed"  
                carryon = true
            return
        return carryon
