jQuery.fn.reverse = [].reverse;

window.utilities =
  
    # Set up AJAX for orderable tables
    #       table needs class 'orderable'
    #       button to save order has id 'aspectsSaveOrder'
    #       data saved to same URL using postListOrder function above
    #    
    setUpSortable: ->
        console.log("Fragmenty.setUpSortable run")
    
        # Keep table columns same width while element dragged 
        $("table.orderable thead tr:last-child th").each ->
            w = $(this).width() + 1
            $(this).css width: w + "px"
            return

        $("table.orderable tbody").sortable
            placeholder: "sortable-placeholder"
            forcePlaceholderSize: true
            cursor: "move"
            scroll: true
            update: (event, ui) ->
                table = $(ui.item).closest("tbody")
                content = $(table).find("span.record_id").text()
                $("input[name=\"order\"]").val content
                return

        $("ul.orderable").sortable
            placeholder: "sortable-placeholder"
            forcePlaceholderSize: true
            cursor: "move"
            scroll: true
            update: (event, ui) ->
                list = $(ui.item).closest("ul")
                content = $(list).find("span.record_id").text()
                $("input[name=\"order\"]").val content
                return

        return
        

class window.Fragmenty

    # class level variables (ref using @constructor.variable_name)
    @navs: []  # list of navigation container ids
    # keep track of URL for @navigate function, @actionLink and @load should update this
    # the address stored here will lag behind browser state changes, allowing us
    # to check where we were before the navigation event (forward or back)
    @currHref: ''  

    constructor: (@opt) ->
        @_opt = # set default values
            # instance variables (ref using @_opt.variable_name
            debug: 0                # 0 = no debuggin info, 1 = some debugging info
            popupId: 'popup'        # id of element for popup content
            activeLinkClass: 'is-active'  # class to assign to any link that is considered 'active' (relates to current content)
            handleAllLinks: false   # Fragmenty handles all links preventing default action only on those within sitePath
            pushState: true         # does this code handle pushing the new state (location) to browser history?
            rePushTest: /.*/        # capture the url path needed to compare and decide whether to push state
            sitePath: null          # URL must begin with this to handle using Fragmenty
            popupHeightClearance: 160    # Used to specify how much space left above and below popup 'window'
            pushstate_on_update: true     # push new state to browser history only if content updated on load
            tasks: []               # list of task ids for recurring status updates
            taskid_prefix: 'task_output_'  # prefix to task update element id
            injectFunction: null    # optional javascript function to inject content into element instead of replaceing content
        $.extend(true, @_opt, @opt) # overwrite defaults with those provided
        @init()


    init: (opt) ->
        # handle navigation tree actions, incl. links
        # stop IE 11 touch events triggering page navigation
        # (for some reason preventDefault doesn't stop link navigation)
        
        #@_opt.debug > 0 and console.log("Fragmenty.init : ", this);
        
        @currHref = window.location.href
        
        if @_opt.sitePath.startsWith('/')
            match = window.location.href.trim().match(/^((http[s]?|ftp):\/\/)?\/?([^\/\s]+)/i);
            site = match[1]+match[3];  # site equals scheme + domain/hostname
            @_opt.debug > 0 and console.log("Fragmenty.init site: " + site);
            @_opt.sitePath = site + @_opt.sitePath  # make sitePath parameter a complete URI e.g. http://mysite.domain:80/
            @_opt.debug > 0 and console.log("Fragmenty.init updated sitePath: " + @_opt.sitePath);

        
        # init content loading links for whole page
        @initContent($('body'))
        
        # Return to top of scrollable element when content refreshed
        $(document.body).on 'DOMSubtreeModified', '.fr-toprefresh', @showTop
        #$(document.body).on 'DOMSubtreeModified', '.fr-scrollbottom', @showBottom

        # universal popup/overlay close icon action
        #$(document.body).on "click touchend pointerup",".jn_close",@popupClose
        $(document.body).on "click",".fr-close",@popupClose


        if @_opt.handleAllLinks
            @_opt.debug > 0 and console.log("Fragmenty.init handling all links in page")
            # intercept all links
            # TODO: Add touch screen capability here if needed
            $(document.body).on "click", "a" ,@link
        else
            @_opt.debug > 0 and console.log("Fragmenty.init handling explicitly defined fragment links in page")
            # intercept fragmenty action links (these don't change browser address)
            # TODO: Add touch screen capability here if needed (touchend or pointerup events)
            $(document.body).on "click", "a.fr-action", @actionLink
            # intercept other explicit fragmenty links
            $(document.body).on "click", "a.fr-link" ,@openLink
        
        # experimental pseudoForm feature
        $(document.body).on "click", ".fr-pseudoform-parent .fr-pseudoform", @pseudoForm
        
        # intercept submits in new content
        $(document.body).on "click", "form input[type=submit]", @post
            
        # will need to be separate from tree nav stuff if more than one treenav used
        $(window).on('popstate', @navigate)
        path = window.location.pathname
        href = window.location.href
        query = window.location.search
        
        # This section caters for path not fully matching main path and loads
        # the full master path on first load, this prevents some funny behaviour
        # such as reloading whole page into content pane
        #@_opt.debug > 0 and console.log("Fragmenty.init href: " + href)
        #@_opt.debug > 0 and console.log("Fragmenty.init sitePath: " + @_opt.sitePath)
        #@_opt.debug > 0 and console.log("Fragmenty.init difference: " + (href.length - @_opt.sitePath.length))
        
        if href.startsWith(@_opt.sitePath)
            @_opt.debug > 0 and console.log("Fragmenty.init in managed site.")

            urlParams = new URLSearchParams(window.location.search)
            if urlParams.has('fragoto')
                @_opt.debug > 0 and console.log("Fragmenty.init fragoto parameter specified, loading new location.")
                #currUrl = location.href.replace(/https?:\/\/[^\/]+/i, "") # browser relative url
                @load(urlParams.get('fragoto'),null)    # load content specified by path
                history.replaceState(null,"", urlParams.get('fragoto'))  # replace address in browser history, forgetting the fragoto query
            else
                $('.fr-hidden').removeClass('fr-hidden')
            
        @_opt.debug > 0 and console.log("register Esc to close")
        $("body").on "keyup.popupClose", (e) =>
            # 27 == "esc"
            @_opt.debug > 1 and console.log("keyup")
            if e.which == 27
                # close popup if Esc key pressed
                e.preventDefault()
                do @esc #@popupClose  # without event then close all open popups
        @_opt.debug > 0 and console.log("Fragmenty.init : " + path)
        do @showActive
        

    esc: ->
        @_opt.debug > 1 and console.log("Fragmenty.esc")
        res = do @revertTempSwap
        @_opt.debug > 1 and console.log("Fragmenty.esc revertTempSwap result ", res)
        if not res
            do @popupClose

        
    showTop: (e) =>
        # Make sure we are at the top of scrollable container when content updated
        @_opt.debug > 0 and console.log("Fragmenty.showTop")
        el = e.target
        setTimeout ->
            $(el).closest(".fr-toprefresh").scrollTop(0)
        , 5
        
    showBottom: (e) =>
        # Make sure we are at the top of scrollable container when content updated
        @_opt.debug > 0 and console.log("Fragmenty.showTop")
        setTimeout ->
            el = $(e.target).closest(".fr-scrollbottom")
            goto = $(el).scrollToMax()
            el.scrollTop(goto)
        , 5


    initContent: (element) =>
        # Initialise part of page that has been loaded
        # register actions for form submits, action links, and normal links
        # checks for task_updater element and kicks off update checks
        #if window.smartforms
        #    smartforms.init(element)
            
        @_opt.debug > 0 and console.log("Fragmenty.initContent " + $(element).attr('class'))
        if element instanceof HTMLElement or element.length  # an element or jquery result set >= 1
                            
            # stop IE 11 touch events triggering page navigation
            # (for some reason preventDefault doesn't stop link navigation)
            #$(element).find("a").css("touch-action","none")

            first_focus = $(element).find('.fr-focus:input:enabled:visible')
            if first_focus?
                @_opt.debug > 1 and console.log("Fragmenty.initContent focus explicitly requested on ", first_focus)
                first_focus.removeClass("fr-focus").focus()
            else
                first_input = $(element).find(':input:enabled:visible:first')
                @_opt.debug > 1 and console.log("Fragmenty.initContent set focus on first suitable input ", first_input)
                first_input.focus()
            
            $(element).find('.fr-taskupdater').each (i, tu) =>
                tuid = $(tu).data('taskid')
                @_opt.debug > 0 and console.log("Fragmenty.initContent tuid: " + tuid)
                $(tu).attr('data-count',3600)  # set timeout count
                @_opt.tasks.push(tuid)
            do @update  # begin update process

           
    update: =>
        # send update request to server to get updated output for task 
        @_opt.debug > 0 and console.log("Fragmenty.update tasks: " + @_opt.tasks)

        while @_opt.tasks.length
        
            tid = @_opt.tasks.shift() # take first item from list
            @_opt.debug > 1 and console.log("Fragmenty.update: id " + @_opt.taskid_prefix + tid)
            to = $('#'+ @_opt.taskid_prefix + tid)
            @_opt.debug > 1 and console.log("Fragmenty.update found: " + to.length)
            
            if to.length # check relevant output element still exists
                to = $('#'+ @_opt.taskid_prefix + tid).first()
                href = to.attr('data-href')
                line = to.attr('data-line')
                href = href + '?line=' + line
                @_opt.debug > 1 and console.log("Updater href: " + href)
                done = to.attr('data-done')
                $(document).css('cursor','wait')
                $.ajax 
                    url: href
                    type: 'POST'
                    dataType: 'json'
                    success: @taskUpdate
                    error: =>
                        @TUError tid
                    timeout: 5000  # 5 second timeout


    TUError: (task_id) =>
        # Elegantly handle failed calls to a task update page on the server
        # this was originally in place to handle task updates for tasks that will
        # interrupt web service, such as nginx restart requests.
        
        @_opt.debug > 0 and console.log("Fragmenty.TUError: task update call failed.")
        to = $('#' + @_opt.taskid_prefix + task_id)
        done = false  # assume not done
        text = to.html()  # existing content of task output element
        text = text + "."   # add dot for each failed request
        to.html(text)  # reload full task output text

        # keep checking for updates
        @_opt.tasks.push(task_id)  # add back to the list
        timeoutID = window.setTimeout(@update, 500)

        return  # return without any data
    
    pushState: (newHref) =>
        # Update our browser history and track Href of last loaded content
        # This should be called after new content has been loaded and record the new Href
        @_opt.debug > 1 and console.log("Fragmenty.pushState newHref = " + newHref)
        @currHref = newHref  # keep our record of currently loaded page up to date
        history.pushState(null,"", newHref)  # add to browser history
        return

    
    taskUpdate: (data, status, xhr) =>
        # Handle returned content from task update request
        
        # data has these attributes
        # - taskid  (taskid)
        # - output  (array of text lines)
        # - line  (how many lines read from state so far)
        # - done (boolean - whether task completed or not)
        # - donepattern (regex to regognise line that indicates task is done)
        
        to = $('#' + @_opt.taskid_prefix + data['taskid'])
        request = ''
        donePattern = to.attr('data-donepattern')
        if donePattern
            doneRegExp = new RegExp (donePattern)
        done = false  # assume not done
        text = to.html()  # existing content of task output element
        @_opt.debug > 1 and console.log("Fragmenty.taskUpdate current text: " + text )
        if text.trim().length > 0
            newline = "\n"
            @_opt.debug > 1 and console.log("Fragmenty.taskUpdate we already have text, setting newline ")
        else
            newline = ""
            @_opt.debug > 1 and console.log("Fragmenty.taskUpdate no text yet, no newline set")
        req_cmd = 'fragmenty_request: '

        @_opt.debug > 1 and console.log("Fragmenty.taskUpdate processing output " + data['output'] )
        while data['output'].length > 0
        
            i = data['output'].shift()   # pop first item of remain output 
        
            # look for any request commands in output
            if i.startsWith(req_cmd)
                request = i.replace(req_cmd,'').trim()
                @_opt.debug > 0 and console.log("Fragmenty.taskUpdate found request " + request + " in " + i)
                
            # look for indicate that task is done
            else if donePattern and i.match(doneRegExp)
                @_opt.debug > 1 and console.log("Fragmenty.taskUpdate found done pattern in " + i)
                done = true
                text = text + newline + "Task Done."
                newline = "\n"
                continue

            else  # otherwise add line to output
                @_opt.debug > 1 and console.log("Fragmenty.taskUpdate adding '" + newline + i + "' to output")
                text = text + newline + i
                newline = "\n"
                
                
        if not done  # not already recognised done using donePattern
            done = data['done']   # get done status of returned task data to see if task complete
            
        @_opt.debug > 1 and console.log('Fragmenty.taskUpdate new update text: ' + text)
        to.html(text)  # reload full task output text
        to.attr('data-line',data['line'].toString())  # store details of lines returned so far
        to.attr('data-done',done.toString())  # store status (done or not)

        # count down from original count value and record as done if we get to zero
        # precaution against tasks running excessivley long
        count = to.attr('data-count')
        count = count - 1
        to.attr('data-count',count)
        if count <= 0
            done = true
        
        # auto scroll down on .visible element as content added
        sPane = $(to).closest('.fr-scroll')
        if sPane.length > 0
            $(sPane).scrollTop($(sPane)[0].scrollHeight)
            @_opt.debug > 1 and console.log("Fragmenty.taskUpdate adjusting scrollHeight: " + $(sPane)[0].scrollHeight)
            
        @_opt.debug > 0 and console.log("task done status: " + done)
        
        # arrange another (delayed) call to the update request
        # only works if one task to update at a time
        
        # TODO adjust to handle more than one task id in tasks list
        # currently this would generate another update for each task update done
        # but each run of @update should handle all the tasks and so should
        # only be handled once, for all tasks, not once for each tasks as 
        # this code does at the moment.
        if done
            # check if follow on action defined within this subpane and act on it
            subpane = $(to).closest('.fr-content')
            @_opt.debug > 0 and console.log("Fragmenty taskUpdate: task done, looking for next action in this content")
            @nextAction(subpane)
        else
            # keep checking for updates
            @_opt.tasks.push(data['taskid'])  # add back to the list
            timeoutID = window.setTimeout(@update, 500)
            
        if request
            @_opt.debug > 0 and console.log("Fragmenty.taskUpdate: making request " + request + " using load method")
            @load(request, null)  # make a http request, don't change browser address
        
        return
    

    navigate: (e) =>
        # Handle change due to browser navigation buttons (forward and back)
        # TODO Handle navigation involving # hashes (doesn't navigate within document currently)
        link = location.href
        @_opt.debug > 0 and console.log "Fragmenty.navigate new location: " + link
        @_opt.debug > 0 and console.log "Fragmenty.navigate sitePath: " + @_opt.sitePath
                
        # Check if still within the 'master' area we are responsible for
        if link.startsWith @_opt.sitePath
            @popupClose()  # close popup/dialog elements before navigating elsewhere
            @load(link,null)
            e.originalEvent.preventDefault() # Prevent default back action if we have handled it here
        else
            @_opt.debug > 0 and console.log("Fragmenty.navigate outside our site, ignoring")


    link: (e) =>
        # universal link handler, determines what action to take or whether to allow default action
        @_opt.debug > 0 and console.log("Fragmenty.link triggered")
        el = e.target
        if not $(el).is('a')
            el = $(el).closest('a')
        href = $(el).prop('href')   #.replace(/https?:\/\/[^\/]+/i, "") # link relative url
        if /https?:\/\/[^\/]+/i.test(href)  # no scheme and server address
            sitePath = @_opt.sitePath
        else
            @_opt.debug > 1 and console.log("Fragmenty.link href is on same server")
            sitePath = @_opt.sitePath.replace(/https?:\/\/[^\/]+/i, "")  # strip scheme and address off sitePath for comparison
        @_opt.debug > 1 and console.log("Fragmenty.link href: " + href + " sitePath: " + sitePath)
        if href.startsWith(sitePath)  # within Fragmenty managed site
            @_opt.debug > 1 and console.log("Fragmenty.link within Fragmenty site: " + sitePath)
            if $(el).hasClass('fr-action')
                @actionLink(e)
            else
                @openLink(e)
        else
            return

            
    actionLink: (e) =>
        # Handle a link that performs action within the content panes context
        # e.g. not moving to other content, but adding content below this or as popup
        # doesn't change browser path like openLink, allows double check before continue
        
        @_opt.debug > 0 and console.log("Fragmenty.actionLink " + e.type)
        a = e.target
        
        if not $(a).is('a')
            a = $(a).closest('a')
            
        e.preventDefault()

        if e.type != "click" and e.type != "touchend"
            @_opt.debug > 1 and console.log("Fragmenty.actionLink type=" + e.type + "abandoned")
            return  # ignore touch events
        
        href = $(a).attr('href')
        @_opt.debug > 1 and console.log("Fragmenty.actionLink link ", href)
        if href and href.length > 0 and href.startsWith("#")
            @_opt.debug > 1 and console.log("Fragmenty.actionLink in page navigation to " + href)
            $(href)[0].scrollIntoView()
            return

        if $(a).hasClass('double_check')
            item = $(a).closest('.fr-actionitem').addClass('selected')
            c = confirm($(a).text() + "?")
            $(item).removeClass('selected')
        else
            c = true
        if c
            #href = $(a).attr("href")
            $('body').addClass('wait')
            if $(a).hasClass('post')
                @_opt.debug > 0 and console.log("Fragmenty.actionLink using POST")
                action = $(a).data('action')
                method = 'POST'
            else
                method = 'GET'
                action = ''
            $.ajax
                type: method
                url: href
                data: action
                success: @append
            @_opt.debug > 0 and console.log("Fragmenty.actionLink done")
        else
            @_opt.debug > 0 and console.log('Fragmenty.actionLink cancelled')
        return


    openLink: (e) =>
        # Load new content into pane based on the navigation link just clicked on
        # TODO recognise links outside the 'managed' ajax pages and let the browser handle those normally
        
        # attempts to get IE11 to handle preventDefault properly
        if typeof e.preventDefault == 'function'
            @_opt.debug > 0 and console.log("Fragmenty.openLink event has preventDefault")
        if !e
            @_opt.debug > 0 and console.log("Fragmenty.openLink event not found, using window event")
            e = window.event
                
        e.preventDefault()
        el = e.target
        
        # make sure we are looking at anchor not child element
        if not $(el).is('a')
            el = $(el).closest('a')

        href = $(el).attr("href").replace(/https?:\/\/[^\/]+/i, "") # link relative url
        curr = location.href.replace(/https?:\/\/[^\/]+/i, "") # browser relative url
        @load(href, curr)
        return false

        
    load: (newHref, currHref) =>
        # load content for newHref
        # newHref and currHref should not include protocol, host, or port part or URL
        # if load is triggered by browser navigation (back and forward) then currRef = null
        # push state should not be triggered if currRef = null
        @_opt.debug > 0 and console.log "Fragmenty.load going to component: " + newHref
        if not newHref
            # use current browser location path if newHref not specified
            @_opt.debug > 0 and console.log("Fragmenty load: no newHref supplied taking browsers location path")
            newHref = window.location.pathname

        # new way URLs called as normal, web server application chooses to deliver fragments
        loadHref = newHref
        
        # use object's record of current Href if currHref not provided
        #if (currHref == null)
        #    currHref = @currHref
    
        $('body').addClass('wait')
        
        # Make request for new content
        @_opt.debug > 1 and console.log("Fragmenty.load request to " + loadHref)
        $.ajax
            type: 'GET'
            headers: {"X-Fragmenty": "load"}
            url: loadHref
        .done (content, status, xhr) =>
            @_opt.debug > 1 and console.log("Fragmenty.load content load done")
            updates = @append content, status, xhr
            
            # if url is different then push new state into browser history
            if (currHref != null)
                cmpCurr = currHref.match(@_opt.rePushTest)[0]
            else
                cmpRef = null
            cmpNew = newHref.match(@_opt.rePushTest)[0]
            @_opt.debug > 1 and console.log("Fragmenty.load cmpCurr=" + cmpCurr)
            @_opt.debug > 1 and console.log("Fragmenty.load cmpNew=" + cmpNew)
            
            # Update browser state if we weren't going back/forward (currHref=null) and
            # loaded page actually updated the contents of this page (updates > 0)
            if @_opt.pushState and (currHref != null) and (cmpCurr != cmpNew)  # if different to current URL
                if @_opt.pushstate_on_update # Check if option for push state to depend on updated content is on
                    if updates > 0   # some updates made
                        @_opt.debug > 0 and console.log("Fragmenty.load: pushing new state to browser history")
                        @pushState(newHref)  # push state (and any other associated actions)
                        #history.pushState(null,"", newHref)  # add to browser history
                    else
                        @_opt.debug > 0 and console.log("Fragmenty.load: no updates, no new browser history state")
                else  # pushstate anyway regardless of whether updated content or not
                    @_opt.debug > 0 and console.log("Fragmenty.load: pushing new state to browser history")
                    @pushState(newHref)  # push state (and any other associated actions)
                    #history.pushState(null,"", newHref)  # add to browser history                        
            else
                @_opt.debug > 0 and console.log("Fragmenty.load no change of location")
                
            if updates > 0
                do @showActive     # update links to indicate current active link
                # trigger any other libraries listening for Fragmenty page load events
                $(window).trigger("fragmenty:load")
                @_opt.debug > 1 and console.log("Fragmenty.load triggered fragmenty:load event")

        .fail =>
            @_opt.debug > 1 and console.log("Fragmenty.load content load failed")


    
    showActive: =>
        # current content to help users see where they are in the site.
        @_opt.debug > 0 and console.log("Fragmenty.showActive")
        link = location.href
        # work out location (within site) without # or ? paramters to ensure a good match with anchor links
        relative = location.href.replace(location.origin,'').replace(location.hash,'').replace(location.search,'')
        @_opt.debug > 1 and console.log("Fragmenty.showActive relative: " + relative)
        $('.fr-linkitem a').not('[href="' + relative + '"]').closest(".fr-linkitem").removeClass(@_opt.activeLinkClass)
        $('.fr-linkitem a[href="' + relative + '"]').closest(".fr-linkitem").addClass(@_opt.activeLinkClass)
        #$('a[href$="' + relative + '"]').closest("li").addClass("is-active").siblings().removeClass("is-active")
        
        # handle sections (fr-section) where only part of the path may match indicating it is in this section
        sections = $('.fr-section a')
        $(sections).each (i, el) =>
            href = $(el).attr("href").replace('index.html','')
            @_opt.debug > 1 and console.log("Fragmenty.showActive checking section " + href)
            if relative.startsWith(href)
                @_opt.debug > 1 and console.log("Fragmenty.showActive matches section " + href)
                $(el).closest(".fr-section").addClass(@_opt.activeLinkClass)
    

    post: (e) =>
        # like load but post form data and then load response into new div
        # appended to main pane.
        @_opt.debug > 0 and console.log("Fragmenty.post")
        button = e.target
        e.preventDefault()

        if e.type != "click" and e.type != "touchend" 
            @_opt.debug > 0 and console.log("Fragmenty.post type=" + e.type + " abandoned")
            return  # ignore touch events

        if $(button).hasClass('double_check')
            # throw up confirmation dialog
            item = $(button).closest('.fr-actionitem').addClass('selected')
            c = confirm($(button).attr("value") + "?")
            $(item).removeClass('selected')
        else
            # no check, continue anyway
            c = true
        if c
            curr = location.href.replace(/https?:\/\/[^\/]+/i, "") # browser relatiuve url
            form = $(button).closest('form')
            bname = $(button).attr('name')
            bvalue = $(button).attr('value')
            @post_form(form, bname, bvalue)

            
    post_form: (form, bname, bvalue) =>
        # post specified form (el), including button name (bname) and value (bvalue) in
        # the formData sent to web server
        
        formMethod = $(form).attr("method")
        
        # Check if we are meant to kee the popup open after form submit
        if $(form).hasClass("fr-keep-popup")
          closePopup = false
        else
          closePopup = true
          
        if !formMethod
          formMethod = "POST" # default to POST method if none specified
        else
          formMethod = formMethod.toUpperCase()
          
        href = $(form).attr("action").replace(/https?:\/\/[^\/]+/i, "") # link relative url
        formData = $(form).serialize()
        
        # include any element holding content to be treated as input
        # (used for container presenting editable html)
        @_opt.debug > 1 and console.log("Fragmenty.post looking for .form_input in form")
        
        # .form_input is the old class, fr-form-input is the preferred usage
        $(form).find('.form_input, .fr-form-input').each (i, el) =>
            @_opt.debug > 1 and console.log("Fragmenty.post processing .form_input")
            name = $(el).attr("data-name")
            @_opt.debug > 1 and console.log("Fragmenty.post name=" + name)
            data = $(el).html()
            @_opt.debug > 1 and console.log("Fragmenty.post data=" + data)
            formData = formData + '&' + encodeURIComponent(name) + '=' + encodeURIComponent(data)
        
        # include clicked button name and value in form data
        # assumes name and value are already URL compliant
        # TODO make more robust and convert to url complaint text
        if bname and bvalue
            formData = formData + '&' + bname + '=' + bvalue

        # close popup if form submitted from within pop up?
        # TODO stop this depending on popupId
        @_opt.debug > 1 and console.log("Fragmenty.post form: ", form, " closePopup", closePopup)
        container = $(form).closest('#' + @_opt.popupId)
        if container.attr('id') == @_opt.popupId and closePopup
            do @popupClose

        $('body').addClass('wait')
        $.ajax
            type: formMethod
            headers: {"X-Fragmenty": "form"}
            url: href
            data: formData,
            success: @append
        return
    
    pseudoForm: (e) =>
        # Simple form data submission for a specific action on specific instance (id)
        @_opt.debug > 0 and console.log("Fragmenty.pseudoForm")
        clicked = e.target
        e.preventDefault()
        item = $(clicked).closest('.fr-pseudoform')
        obj_id = item.data('id')
        action = item.data('action')
        form = item.parents('.fr-pseudoform-parent').children('form:first-child')
        if form?
            href = form.attr("action").replace(/https?:\/\/[^\/]+/i, "") # link relative url
            formData = form.serialize() + "&action=" + action + "&id=" + obj_id
            @_opt.debug > 1 and console.log("Fragmenty.pseudoForm " + href + " data: ", formData)
            $('body').addClass('wait')
            $.ajax
                type: "post"
                headers: {"X-Fragmenty": "form"}
                url: href
                data: formData,
                success: @append
        else
            @_opt.debug > 0 and console.log("Fragmenty.pseudoForm master form not found")
        return

    
    modify: (list) =>
        # process AJAX response, looking for updates to data attributes
        @_opt.debug > 1 and console.log("Fragmenty.modify processing " + $(list).length + " element(s)")
        $(list).each (i, el) =>
            @_opt.debug > 1 and console.log("Fragmenty.modify processing .fr-attribute element")
            destId = $(el).attr('id')  # get destination id
            dest = $("#" + destId)
            attributes = $(el).prop("attributes")
            $.each attributes, ->
                if @name.startsWith('data-')
                    $(dest).attr(@name, @value)
        return
  
  
    append: (content, status, xhr) =>
        # Process response to AJAX request.
        
        @_opt.debug > 0 and console.log("Fragmenty.append processing returned content " + xhr.status)
        $('body').removeClass('wait')
        
        # place returned content into element so jquery can work on it
        rhtml = $('<div></div>').html(content)
        nav = $(rhtml).find('.navigation')
        
        if nav
            @_opt.debug > 1 and console.log('Fragmenty.append navigation found')
            if nav.first().data('navigation') == 'back'
                @_opt.debug > 1 and console.log('Fragmenty.append navigation back')
                window.history.back()
                return  # go back using browser history, then we are done.
        
        updates = 0  # begin count of update fragments at zero

        # Process each fr-place element in the returned content
        $(rhtml).find('.fr-content').not('.fr-content .fr-content').each (i, el) =>
        
            @_opt.debug > 1 and console.log("Fragmenty.append processing .fr-content element")
            
            # destination id should match that of original
            destId = $(el).attr('id')  # get destination id
            dest = $('#' + destId)
            currSrc = $(dest).attr('data-src') 

            if not currSrc?
                currSrc = ""
            if currSrc == 'none'
                currSrc = ""
            newSrc = $(el).attr('data-src')
            if not newSrc?
                newSrc = "none"  # deliberately different from currSrc of neither specified
            if newSrc != currSrc
                changed = true
                updates += 1  # increment fragment updates counter
            else
                changed = false
                
            @_opt.debug > 1 and console.log("Fragmenty.append currSrc=" + currSrc + " newSrc=" + newSrc)
                
            @_opt.debug > 1 and console.log("Fragmenty.append destId=" + destId)

            # TODO: handle non-existent methods
            method = $(el).attr('data-method')
            dest_layout = $(dest).attr('data-layout')
            src_layout = $(el).attr('data-layout')
            @_opt.debug > 0 and console.log("Fragmenty.append method=" + method)
            done = false   # assume not done
            if method == 'close'
                # close dialog 'window' (aka overlay)
                @overlayClose(dest)
                done = true

            else if method == 'insert_after'
                # simple insert of content after specified element
                @_opt.debug > 1 and console.log("Fragmenty.append inserting content")
                @initContent(el)
                $(el).children().reverse().each (index, child) =>
                    dest.after(child)
                dest.removeClass('fr-temp-swapped')
                done = true
                
            else if method == 'append'
                @_opt.debug > 0 and console.log("Fragmenty.append appending content")
                @initContent(el)    # initialise contents to be inserted
                lastChild = dest.children().last()
                dest.attr('data-src', "")  # leave data-src empty as it has been changed from whatever is was before
                $(el).children().reverse().each (index, newChild) =>
                    lastChild.after(newChild)
                @activateOverlay(dest) # Check if destination is to be overlaid onto page
                dest.removeClass('fr-temp-swapped')
                done = true
                
            else if method == 'remove'
                @_opt.debug > 0 and console.log("Fragmenty.append removing content")
                dest.remove()
                done = true
                
            else if method == 'inject' and dest_layout == src_layout
                # new and old content layouts should match for this more
                # sophisticated insertion
                @_opt.debug > 0 and console.log("Fragmenty.append injecting content")
                @_opt.debug > 1 and console.log("Fragmenty.append injecting content using ", @_opt.injectFunction)
                
                # insert operation may fail checks (return false), mark not done if it fails
                done = @_opt.inserter.staticInsert(el, dest)
                
                dest.attr('data-src', newSrc)
                @activateOverlay(dest) # Check if destination is to be overlaid onto page
                @initContent(dest)
                dest.removeClass('fr-temp-swapped')
                done = true

            else if method == 'temp_swap'
                @_opt.debug > 0 and console.log("Fragmenty.append temporarily swapping content")
                do @revertTempSwap   # undo last temporary swap ready for next one
                console.log("reverted")
                temp = $('#fr-temp-swap')
                $(temp).empty().html($(dest).html())  # put current contents into temp element
                $(temp).data('originalid', $(dest).attr('id'))
                dest.empty().html($(el).html())  # put new content in destination
                @activateOverlay(dest) # Check if destination is to be overlaid onto page
                @initContent(dest)
                dest.addClass('fr-temp-swapped')
                done = true
                        
            if (not done) and changed   
                # if not covered (done) by above steps use this one
                @_opt.debug > 0 and console.log("Fragmenty.append replacing content")
                dest.attr('data-src', newSrc)
                if dest.closest('.fr-scroll').hasClass("smoothscroll")
                    smoothscroll = true
                else
                    smoothscroll = false
                dest.closest('.fr-scroll').removeClass("smoothscroll")
                dest.closest(".fr-scroll").scrollTop(0)
                dest.empty().html($(el).html())
                if smoothscroll
                    dest.closest('.fr-scroll').addClass("smoothscroll")
                @activateOverlay(dest) # Check if destination is to be overlaid onto page
                @initContent(dest)
                dest.removeClass('fr-temp-swapped')
            else
                @_opt.debug > 1 and console.log("Fragmenty.append no action")

        # modify attributes of elements where response elements has fr-attribute class
        @modify $(rhtml).find('.fr-attribute')
                
        # handle follow up action if specified
        next_action = $(rhtml).find('.fr-nextaction').first()
        task_updater = $(rhtml).find('.fr-taskupdater').first()
        if next_action.length and task_updater.length == 0  # next action and no updater
            @_opt.debug > 1 and console.log("Fragmenty.append calling nextAction")
            @nextAction(next_action)

        @_opt.debug > 1 and console.log("Fragmenty.append # of updates: " + updates)
        
        # ensure newly updated content is visible
        $('.fr-hidden').removeClass('fr-hidden')

        return updates
    
    revertTempSwap: ->
        @_opt.debug > 0 and console.log("Fragmenty.revertTempSwap")
        temp = $('#fr-temp-swap')
        rep_id = temp.data('originalid')
        @_opt.debug > 1 and console.log("Fragmenty.revertTempSwap rep_id: ", rep_id)
        if typeof rep_id is 'string' and rep_id.length > 0
            dest = $('#' + rep_id)
            if dest.hasClass('fr-temp-swapped')
                dest.empty().html($(temp).html())
                temp.data('originalid','')
                temp.empty()
                return true  # we have swapped content back in
            else
                @_opt.debug > 1 and console.log("Fragmenty.revertTempSwap original content changed, not swapping back")
                # fr-temp-swapped class not there, content can't go back
                temp.data('originalid','')
                temp.empty()   # clear temp content as it is no longer valid
                return false  # nothing swapped back
        else
            return false  # nothing swapped back
            
    
    activateOverlay: (el) =>
        content = $(el).html().trim().length
        # If element is within a hidden overlay component, then make visible and set height
        if $(el).closest('.fr-overlay').length > 0 and content > 0
            # Handle update of overlay elements (popup boxes) by setting dimensions and making visible
            container = $(el).closest('.fr-overlay')
            
            @_opt.debug > 1 and console.log("Fragmenty.activateOverlay destination is an overlay element")

            # calculate max height based on browser window height
            popup_hgt = $(window).height() - @_opt.popupHeightClearance

            @_opt.debug > 1 and console.log("Fragmenty.activateOverlay popup height=" + popup_hgt)

            container.find('.fr-popup-content') # go to main content element
                #.css('max-height',popup_hgt)    # set max-height
                .closest(".fr-page-blocker")    # find element that 'hides' the page
                .show()                         # show popup
            container.find('.fr-scroll').scrollTop(0)    # make sure we start at top of scrollables
        
    nextAction: (el) =>
        # Do follow up action on subpane load or task complete
        @_opt.debug > 1 and console.log("Fragmenty.nextAction start")
        if $(el).is("[data-action]")
            action = $(el).data('action')
        else
            action = $(el).find('.fr-nextaction').data('action')
        @_opt.debug > 1 and console.log("Fragmenty.nextAction action=" + action)
        if $(el).is("[data-destination]")
            dest = $(el).data('destination')
        else
            dest = $(el).find('.fr-nextaction').data('destination')
        @_opt.debug > 1 and console.log("Fragmenty.nextAction destination=" + dest)
        if action == 'post_form'
            @post_form(el, "auto", "fr-nextaction")
        if action == 'reload'
            # needs to use attr method as data method seems to get cached by jquery
            # uses href specific to specified destination if available
            # TODO allow calculation of path using browser path and data-trasnform
            path = $('#' + dest).first().attr('data-href')  #window.location.pathname
            @_opt.debug > 0 and console.log("Fragmenty nextAction reload_pane path: " + path)
            if path
                # load content using specific path
                @load(path,null)    # load content using data-href attribute 
            else
                # load content based on main browser URL
                @load(null, null)


    popupClose: (e) =>
        # Close the overlaying element by emptying out this element and hiding
        # it's parent element.
        @_opt.debug > 0 and console.log('Fragmenty.popupClose')
        if e?  # get overlay containing the trigger element.
            overlay = $(e.target).closest(".fr-overlay")
        else   # otherwise get any visible overlay container
            overlay = $(".fr-overlay:visible")
        @overlayClose(overlay)
        
        
    overlayClose: (el) =>
        # close the overlay element el can be an element or a jQuery query string
        @_opt.debug > 1 and console.log('Fragmenty.overlayClose running')
        
        # empty out content elements (fr-content) leaving pop-up structure in place
        $(el).find(".fr-content")
             .empty()
             .attr('data-src','')   # clear source details so any new content will be placed here and activated
             .closest(".fr-page-blocker")
             .hide()   # hide pop-up element

