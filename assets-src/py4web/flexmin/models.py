"""
This file defines the database models
"""

from .common import db, Field
from pydal.validators import *
from . import settings
import xmlrpc.client


### Define your table below
#
# db.define_table('thing', Field('name'))
#
## always commit your models to avoid problems later
#
# db.commit()
#

# Not sure what this is about?
#response.generic_patterns = ['*'] if request.is_local else ['*.json']

db.define_table('action_log',
                Field('datetime','datetime'),
                Field('cmd','string'),
                Field('restore_cmd','string'),  # restore equivalent of backup
                Field('description','string'),
                Field('client','string'),
                Field('comment','string'),
                Field('status','string'),  # deprecate?
                Field('category','string'),
                Field('response','text'),
                format='%(name)s'
                )

db.commit()

