"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

import re, os, xmlrpc.client, yaml, json
import shlex, subprocess, getpass, shutil, time, datetime, uuid
import pathlib
import importlib
import ombott as bottle

from py4web import action, request, abort, redirect, URL, Session
from py4web.core import Template
from yatl.helpers import A, BEAUTIFY
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated


from . import settings
from .modules import output_prep, run_prep
from .modules.utils import dateToInt
from .modules.graph import Table

session = Session(secret=settings.SESSION_SECRET_KEY,
                  expiration=3600,
                  algorithm='HS256',
                  storage=None,
                  same_site='Strict')


def render_template(temp, data):
    """
    Function to render alternative template
    """
    context = dict(output=data)
    Template(temp).on_success(context)
    return context['output']


@action('index')
@action.uses('index.html')
def page(script=None):
    """
    Main admin page
    """

    #root = os.path.join(request.folder,'private','scripts')
    # scripts = glob.glob('/usr/local/www/osadmin/*.sh')
    tasks = []
    
    if script:
        action_html = myaction(script, internal=True)
    else:
        action_html = None
        
    if 'fragoto' in request.query:
        action_html = '<p></p>'
        
    """
    for s in scripts:
        tasks.append(os.path.basename(s))
    """
    return dict(tasks = tasks,
                appname = settings.APP_NAME, #request.ext.app_name,
                paneId = 'pane',
                menu_html = menu(),
                action_html = action_html,
                taskView = 'action.load')


@action('menu.html')
@action.uses('menu.html')
def menu():
    """
    Generate html for main flexmin menu
    
    Enumerates all scripts and collects details required to create menu
    """
    
    # access to flexmin service
    service = xmlrpc.client.ServerProxy(settings.FLEXMIN_SRV)
    service.load_menu()
    menu = service.get_menu()
        

    return dict(menu = menu,
                mainPage = "index",
                paneId = 'pane',
                taskView = "action.load")


@action('taskupdate/<taskid:int>', method='POST')
@action.uses(db)
def taskupdate(taskid=None):
    """
    Get updated status of a task that is running
    /flexmin/default/taskupdate/taskid
    """
    
    # get current state line if specified
    if 'line' in request.query:
        line = int(request.query.line)
    else:
        line = 0

    service = xmlrpc.client.ServerProxy(settings.FLEXMIN_SRV)
    state, lines = service.stateUpdate(taskid,line,'<h1>')
    done = service.completed(taskid)
    if done:
        # Log full response in action log (db)
        task_record = db.action_log(taskid)
        # if task record exists update with respose
        if task_record:
            task_record.update_record(response="\n".join(service.state(taskid)))
                    
        # remove task from task server list
        service.remove(taskid)
        state.append("")
        state.append("Task " + str(taskid) + " Completed")
        
    req_cmd = 'fragmenty_request: '
    for i, entry in enumerate(state):
        if req_cmd in entry:
            # modify request line to insert correct URL
            req = entry.strip()[len(req_cmd):].strip()
            full_req = URL(req)
            state[i] = req_cmd + full_req

    # just return text to go into a text box
    return dict(output=state,
                 done=done,
                 taskid=taskid,
                 line=lines)



@action('login', method='POST')
@action.uses('login.html',session)
def login():
    """
    Verify password from login form
    """
    
    # access to flexmin service
    service = xmlrpc.client.ServerProxy(settings.FLEXMIN_SRV)

    tasksess_id = session.get('task_session')
    script = 'flexmin_login.py reload'
    #fm_vars = settings.FM_VARS
    #print("fm_vars: " + str(fm_vars))

    if not tasksess_id:
        tasksess_id = str(uuid.uuid4())  # generate random uid
        session['task_session'] = tasksess_id
        
    task_auth = {'session_id': str(tasksess_id),
                 'client_ip': request.remote_addr}
    if request.forms.password:
        task_auth['client_ip'] = request.remote_addr
        task_auth['username'] = 'flexmin'
        task_auth['password'] = request.forms.password
        print("Detected login form")
        
    out = service.runyaml(script, {'FM_TASKID': 0}, task_auth)  # Login not a proper task so provide task id of 0

    output = output_prep.getYaml(out, script)
    
    return dict(output=output,
                dest_title=settings.popup_title,
                dest_content=settings.popup_content
                )

@action('test/<myobject:path>')
def test(myobject=None):
    return(str(myobject))

@action('action/<myaction>/<myobject:path>/')
#@action('action/<myaction>/<myobject:path>')
@action('action/<myaction>/<myobject:path>', method='POST')
@action('action/<myaction>/')
#@action('action/<myaction>')
@action('action/<myaction>', method='POST')
@action.uses('action.html',db,session)
def myaction(myaction=None,internal=False, myobject=None):
    """
    Perform a specified task, or task step as specified by task

    /osadmin/default/action/listusers
    /osadmin/default/action/adduser?username=todd&password=pass123&group=wheel...
    """
    # If reloading an action page, return a basic header to reload the site home page and
    # then load this action.
    if not internal and not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        template = Template("reload.html", delimiters='[[ ]]')
        return render_template("reload.html", dict(
                script=myaction,
                path=None,
                myobject=myobject,
                app=settings.APP_NAME
            ))
    
    # if object specified, provide option to go back to list
    if myobject:
        back_url = URL('action',myaction)
    else:
        back_url = None
        
    #if 'back' in request.forms:
    #    if request.forms.back == 'cancel':
    #        myobject=None
    
    debug = None
    #modroot = os.path.join(request.folder,"modules")
    script = 'No script specified'
    script_type = None
    query = False
    
    # access to flexmin service
    service = xmlrpc.client.ServerProxy(settings.FLEXMIN_SRV)
    
    fm = settings.FM

    # provide step number for action output where each step remains in DOM
    if request.query.step:
        stepNo = request.query.step
    else:
        stepNo = 0
        
    out = ""
    output = None
    err = ""
    mod = None
    if request.fullpath[-1] == '/':
        form_action = request.fullpath[0:-1]
    else:
        form_action = request.fullpath
    
    tasksess_id = session.get('task_session')
    if not tasksess_id:
        tasksess_id = str(uuid.uuid4())  # generate random uid
        session['task_session'] = tasksess_id

    # Verify CSRF Token for all POST requests, reject if no match
    if request.method == 'POST':
        token = session.get('csrf_token','')
        if (request.forms.csrf_token and
            request.forms.csrf_token == token):
            # Token verified, generate new one
            csrf_token = str(uuid.uuid4())  
            csrf_pass = True
        else:
            output = {'error': 'Request rejected, valid CSRF token not found, token expected is ' + token}
            # Token check failed, generate new token
            csrf_token = str(uuid.uuid4())  
            csrf_pass = False
    elif request.method == 'GET':
        if '2' in request.query:
            query = True
            # just a query, keep existing token if possible, or generate new one if not.
            csrf_token = session.get('csrf_token',str(uuid.uuid4()))
        else:
            csrf_token = str(uuid.uuid4())  
        csrf_pass = True
    else:
        csrf_token = str(uuid.uuid4())  
        csrf_pass = True  # not a POST so no check needed, flag as pass

    session['csrf_token'] = csrf_token
        
    if csrf_pass:        
        task_auth = {'session_id': str(tasksess_id),
                    'client_ip': request.remote_addr}
        if request.forms.password:
            task_auth['client_ip'] = request.remote_addr
            task_auth['username'] = 'flexmin'
            task_auth['password'] = request.forms.password
            #print("Detected login form")
        

        if myaction:  # a script is specified
            # log action
            taskid = int(db.action_log.insert(datetime = datetime.datetime.now()))
            task_record = db.action_log(taskid)
            task_vars = {'FM_TASKID': taskid}   # initialise parameters specific to this task starting with id
            #print("Task ID: " + str(taskid))

            if request.forms.long:
                wait = True
            else:
                wait = False

            script = myaction   # request.args(0)      # this version is executed
            
            if script[0:4] == "fmw_":
                script_type = 'python_web'
            elif script[-3:] == ".py" or script[0:3] == "fm_" or script[0:4] == "fmp_":  # python module
                script_type = 'python'
            else:
                script_type = 'shell'
                oscurrent = '.' + os.sep

            # Check if log file exists for this task
            task_vars['FM_TASK_LOG'] = os.path.join(fm.FM_LOGROOT, os.path.splitext(script)[0] + '.log')

            # Prepare command for execution by external service
            # script is full command including parameters (for shell scripts)
            # params is a list of parameters which is provided to python scripts
            # log_script is a version of the full command for logging (passwords replaced with a password placeholder)
            if not query:
                script, log_script, params = run_prep.getInputs(script, request.forms, service, taskid, myobject=myobject)
            else:
                # is a query not a form submission, use query parameters instead
                script, log_script, params = run_prep.getInputs(script, request.query, service, taskid, myobject=myobject)

            # log command to be executed
            #print("About to update record")
            task_record.update_record(cmd=log_script)
            #print("Updated record, about to run script")

            if script_type == 'python_web':
                done = True  # assume done if module can't be found, prevents problem being compounded by follow up actions.
                mod = "flexmin.modules." + os.path.splitext(script)[0]
                #print("About to import " + mod)
                try:
                    mypath = pathlib.Path().absolute()
                    #print("Path: " + str(mypath))
                    task_module = importlib.import_module(mod, package=".")
                except:
                    task_module = None
                    out += "ERROR: Flexmin web application had problem importing module '" + mod + "' (python script)"
                if task_module:
                    # run module function (run) provide output list for
                    # function to update
                    task_vars = {**fm.__dict__, **task_vars}  # merge local task_vars with global flexmin parameters before passing to task method
                    out, done = task_module.run(params,task_vars,db)
                if not done:
                    task_vars['FMW_SHORT_OUT'] = out  # pass output of web2py version of script to service version using FMW_SHORT_OUT parameter
                    # run as long running task via xmlrpc task server and return taskid for client to poll for updates
                    out = service.runyaml(script, task_vars, task_auth)
            else:
                #print("sending task to service")
                out = service.runyaml(script, task_vars, task_auth)

            #print("sent script to service, updating db record again")
            task_record.update_record(response=out)  # update database
            
            """
            print("Raw output:")
            for o in out.split("\n"):
                print(o)
            """
        
            # process yaml in command output to get dictionary output
            output = output_prep.getYaml(out, log_script)

            # Indicate history (log) exists but not for popups.
            if os.path.exists(task_vars['FM_TASK_LOG']):
                if 'method' in output:
                    if output['method'] != "popup":
                        output['logfile'] = task_vars['FM_TASK_LOG']
                else:
                    output['logfile'] = os.path.basename(task_vars['FM_TASK_LOG'])

            # TODO deprecate the status key and database entry
            if 'status' in output:  # task reported status, therefore remains in the database
                task_record.update_record(status=output['status'])
                # remove folders only if no data stored
                service.tidyTaskData(taskid,False)
            elif 'category' in output:  # task reported status, therefore remains in the database
                if 'restore' in output:
                    restore = output['restore']
                else:
                    restore = ''
                comment = (output['comment'] if 'comment' in output else '')
                task_record.update_record(category=output['category'],
                                        restore_cmd=restore,
                                        comment=comment)
                # remove folders only if no data stored
                service.tidyTaskData(taskid,False)
            else:
                db(db.action_log.id == taskid).delete()
                # remove folders
                service.tidyTaskData(taskid,True)

        else:  # No script specified
            output = {'text': "Please click on a task"}

    # Handle authentication errors, put login form entry into output if 
    # error indicates a login is rquired
    output_prep.authenticate(request,output)
    
    # Tidy up output, make sure display item present for consistent template handling
    output_prep.display(output)
    
    #print("Contents of text:")
    #print(output)

    return dict(output=output,
                form_action=form_action,
                download_action=URL('download'),
                back_url=back_url,
                login_action='/flexmin/login',
                script=script,
                paneId='pane',
                csrf_token=csrf_token,
                popup_title=settings.popup_title,
                popup_content=settings.popup_content,
                stepNo=stepNo, # part of id of step div
                task_session=tasksess_id,
                out=out,
                debug=debug,
                submitLabel="Proceed"  # default submit button label unless specified in form
                #params=params   # only used for forms
                )


@action('download/<download_file>')
def download(download_file=None):
    """
    Allow download of files from folder specified in FM_DOWNLOADS parameter
    """
    service = xmlrpc.client.ServerProxy(settings.FLEXMIN_SRV)
    fm_vars = service.get_vars()
    if 'FM_DOWNLOAD' in fm_vars:
        static_folder = os.path.join(fm_vars['FM_DOWNLOAD'])
        # return f"folder: {fm_vars['FM_DOWNLOADS']} file: {download_file}"
        return bottle.static_file(download_file, root=static_folder, download=download_file)
    else:
        return str(fm_vars)


@action('history/<logfile>')
@action.uses('history.html',db,session)
def history(logfile=None, internal=False):
    """
    View history of script if log file exists
    """
    
    # If reloading an action page, return a basic header to reload the site home page and
    # then load this action.
    if not internal and not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        template = Template("reload.html", delimiters='[[ ]]')
        return template.transform(dict(
                path='history/'+logfile,
                app=settings.APP_NAME
            ))

    task_vars = settings.FM.__dict__
    text = "No data found."
    if logfile:  # a file is specified

        # Get data from log file
        # TODO: More elegant way to get parameters from file, maybe in yamlconfigs module
        #logfile = os.path.join(parameters['FM_LOGROOT']['value'],request.vars['logfile'])
        logfile = os.path.join(task_vars['FM_LOGROOT'],logfile)
        with open(logfile,'r') as f:
            data = f.read()
            t = Table(data)
            graph_data=t.to_chartjs(exclude='tmpfs|loop[0-9]|/run/media/')

    if graph_data['title']:
        title = graph_data['title']
    else:
        title = 'History - ' + str(logfile) 
        
    # TODO: do a more robust generation of return to main action URL
    back_url = URL('action', os.path.basename(logfile)[0:-3]+"sh")

    return dict(text=text,
                re=re,
                output=data,
                title=title,
                back_url=back_url,
                paneId='pane',
                graph_list=list(graph_data['charts'].keys()),
                graph_data=json.dumps(graph_data['charts']),
                )
