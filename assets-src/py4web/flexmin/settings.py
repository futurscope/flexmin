"""
This is an optional file that defined app level settings such as:
- database settings
- session settings
- i18n settings
This file is provided as an example:
"""
import os
import xmlrpc.client
from argparse import Namespace

# db settings
APP_FOLDER = os.path.dirname(__file__)
APP_NAME = os.path.split(APP_FOLDER)[-1]
# DB_FOLDER:    Sets the place where migration files will be created
#               and is the store location for SQLite databases
DB_FOLDER = os.path.join(APP_FOLDER,'databases')
DB_URI = "sqlite://storage.db"
DB_POOL_SIZE = 1

# send email on regstration
VERIFY_EMAIL = False

# email settings
SMTP_SERVER = None
SMTP_SENDER = "you@example.com"
SMTP_LOGIN = "username:password"
SMTP_TLS = False

# session settings
MEMCACHE_CLIENTS = ["127.0.0.1:11211"]
REDIS_SERVER = "localhost:6379"

# logger settings
LOGGERS = [
    "warning:stdout"
]  # syntax "severity:filename" filename can be stderr or stdout

# single sign on Google (will be used if provided)
OAUTH2GOOGLE_CLIENT_ID = None
OAUTH2GOOGLE_CLIENT_SECRET = None

# single sign on Google (will be used if provided)
OAUTH2FACEBOOK_CLIENT_ID = None
OAUTH2FACEBOOK_CLIENT_SECRET = None

# enable PAM
USE_PAM = False

# enable LDAP
USE_LDAP = False
LDAP_SETTING = {
    "mode": "ad",
    "server": "my.domain.controller",
    "base_dn": "ou=Users,dc=domain,dc=com",
}

# i18n settings
T_FOLDER = os.path.join(APP_FOLDER, "translations")

# Celery settings
USE_CELERY = False
CELERY_BROKER = 'redis://localhost:6379/0'


# these should be overridden in setting_private.py
SESSION_TYPE = "cookies"
SESSION_SECRET_KEY = "<my_secret_key>"
FLEXMIN_SRV = 'http://localhost:8001'

## try import private settings
try:
    from .settings_private import *
except:
    pass  # should work okay without private settings

# Flexmin Specific Settings
server = xmlrpc.client.ServerProxy(FLEXMIN_SRV)
FM = Namespace(**server.get_vars())
popup_title = 'popup-title'
popup_content = 'popup-content'
pane_title = 'pane-title'
pane_content = 'pane'
