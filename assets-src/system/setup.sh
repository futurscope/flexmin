#!/bin/bash

# Configure system for flexmin, needs to be supplied flexmin password
# This script is designed to install Flexmin components on the local system
# or upgrade Flexmin on the system.
# usage 
#   setup.sh <flexmin_home_folder>


echo "Flexmin system installation script"
echo "=================================="
echo ""
echo "Running set up script: $(realpath $0)"
echo ""

# Switch to system folder 
cd $(dirname $(realpath $0))

source ./setup_prereq_checks.sh

# Find flexmin.conf and load parameters
# This code copied from scripts/common.sh
[ -d "/usr/local/etc/flexmin/" ] && export FM_CONFIGS="/usr/local/etc/flexmin"
[ -d "/etc/flexmin/" ] && export FM_CONFIGS="/etc/flexmin"
set -o allexport
source "${FM_CONFIGS}/flexmin.conf"
set +o allexport

# Check common Flexmin paths
source ./setup_pathcheck.sh

# set home folder
[ $# -eq 0 ] && flexmin_home='/home/flexmin'   # default if none specified
[ $# -eq 0 ] && [ ! -z "$FM_HOME" ] && flexmin_home=${FM_HOME}  # use FM_HOME if specified
[ $# -eq 1 ] && flexmin_home=${1}  # use specified folder is available

# set FM_HOME parameter in flexmin.conf to match specified path
sed -i "s@^FM_HOME=.*@FM_HOME='$flexmin_home'@" ${FM_CONFIGS}/flexmin.conf

# If flexmin user doesn't exist, and no password specified, warn user
# [ ! $(cat /etc/passwd | grep 'flexminrf:') ] && [ -z "$1" ] && echo "WARNING: No flexmin password has been specified";echo "" && sleep 5

# set uwsgi plugins path
[ -d /usr/lib/uwsgi/plugins ] && uwsgi_plugins=/usr/lib/uwsgi/plugins
[ -d /lib/uwsgi ] && uwsgi_plugins=/lib/uwsgi

echo ""
echo "Setting up flexmin application"
echo "------------------------------"

echo ""
if [ -d ${FM_CONFIGS} ]
then
  echo "Flexmin configuration folder ${FM_CONFIGS} already exists."
else
  echo "Creating flexmin configuration folder ${FM_CONFIGS}"
  mkdir ${FM_CONFIGS} || exit 1
  echo "Copying main flexmin config files"
  cp ../configs/flexmin.conf ${FM_CONFIGS}/ || exit 1
  cp ../configs/menu.yaml ${FM_CONFIGS}/ || exit 1
  cp ../configs/py4web.nginx.yaml ${FM_CONFIGS}/ || exit 1
  cp ../configs/py4web_loc_flexmin.nginx.yaml ${FM_CONFIGS}/ || exit 1
  echo "Setting permissions on ${FM_CONFIGS}"
  chown -R root:root ${FM_CONFIGS} || exit 1
  chmod -R 774 ${FM_CONFIGS} || exit 1
  chmod 775 ${FM_CONFIGS} || exit 1
fi

echo ""
if [ -d ${FM_CONFIGS}/private ]
then
  echo "Flexmin configuration private folder ${FM_CONFIGS}/private already exists."
else
  echo "Creating flexmin configuration private folder ${FM_CONFIGS}/private"
  mkdir ${FM_CONFIGS}/private || exit 1
  echo "Setting permissions on ${FM_CONFIGS}/private"
  chown -R root:root ${FM_CONFIGS}/private || exit 1
  chmod -R 770 ${FM_CONFIGS}/private || exit 1
  chmod 770 ${FM_CONFIGS}/private || exit 1
fi


if [ ! -f ${FM_CONFIGS}/menu.local.yaml ]
then
  echo "No menu.local.yaml file, creating a minimum copy"
  echo "source: []" > ${FM_CONFIGS}/menu.local.yaml || exit 1
else
  echo "Existing menu.local.yaml file found, leaving it alone"
fi

echo "Making sure there is a flexmin.local.conf file."
[ ! -f ${FM_CONFIGS}/flexmin.local.conf ] && touch ${FM_CONFIGS}/flexmin.local.conf
[ -f ${FM_CONFIGS}/flexmin.local.conf ] && chmod ug+x -f ${FM_CONFIGS}/flexmin.local.conf

echo "Making sure there is a 'local' scripts folder."
[ ! -d ${FM_HOME}/local/scripts ] && mkdir -p ${FM_HOME}/local/scripts


echo ""
echo "Configuring $flexmin_home permissions"

echo "Securing flexmin scripts = root:root 774 (rwxrwxr--)"
chown -R root:root $flexmin_home/scripts/* || exit 1
chmod -R 774 $flexmin_home/scripts/* || exit 1

#echo "Securing flexmin local scripts = root:root 774 (rwxrwxr--)"
#chown -R root:root $flexmin_home/local/scripts/* || exit 1
#chmod -R 774 $flexmin_home/local/scripts/* || exit 1

echo "Securing flexmin configs = root:root 664 (rw-rw-r--)"
chown -R root:root $flexmin_home/configs/* || exit 1
chmod -R 664 $flexmin_home/configs/* || exit 1

echo "Securing flexmin config templates = root:root 664 (rw-rw-r--)"
chown -R root:root $flexmin_home/config-templates/* || exit 1
chmod -R 664 $flexmin_home/config-templates/* || exit 1

echo "Securing flexmin system files = root:root 664 (rw-rw-r--)"
chown -R root:root $flexmin_home/system/* || exit 1
chmod -R 664 $flexmin_home/system/* || exit 1
chmod 774 $flexmin_home/system/*.sh

echo ""

echo "Generating new 32 character secret key for cookie encryption"
secret=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32;echo;)
echo "Putting secret key into $flexmin_home/py4web/flexmin/settings_private.py"
sed -i -r "s/<my_secret_key>/${secret}/" $flexmin_home/py4web/flexmin/settings_private.py

echo ""
echo "Securing flexmin web app, giving access to local web server account"
# check which web service user account is available before setting relevant ownership
cat /etc/passwd | grep -E '^www-data:' && echo "web account is www-data" && FM_WEB_USER="www-data"
cat /etc/passwd | grep -E '^www:' && echo "web account is www" && FM_WEB_USER="www"
cat /etc/passwd | grep -E '^http:' && echo "web account is http" && FM_WEB_USER="http"
chown -R $FM_WEB_USER:$FM_WEB_USER $flexmin_home/py4web
# adjust main settings to register FM_WEB_USER to correct acount
sed -i -e "s/^FM_WEB_USER[ ]*=[ ]*'http'/FM_WEB_USER='${FM_WEB_USER}'/" $FM_CONFIGS/flexmin.conf

# give rwx access only to owner and group, and no access to others
chmod -R 0770 $flexmin_home/py4web || exit 1

# make all folders eXecutable for other, to allow switching to folders
find $flexmin_home -type d -exec chmod 775 {} \;

echo ""
if [ -d ${FM_TASK_TMP} ]
then
  echo "Flexmin task temporary folder ${FM_TASK_TMP} already exists."
else
  echo "Creating Flexmin task temporary folder ${FM_TASK_TMP}"
  mkdir ${FM_TASK_TMP} || exit 1
fi

echo ""
if [ $(cat /etc/passwd | grep 'flexmin:') ]
then 
  echo "flexmin user already exists" 
else
  echo "Creating flexmin user account"
  useradd flexmin || exit 1
  echo "Generating random password"
  flexmin_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 10;echo;)
  echo "Setting flexmin password"
passwd flexmin << EOD
${flexmin_password}
${flexmin_password}
EOD
fi

echo ""
if [ -d ${FM_LOG_ROOT} ]
then
  echo "Flexmin log folder ${FM_LOG_ROOT} already exists."
else
  echo "Creating Flexmin log folder **${FM_LOG_ROOT}**"
  mkdir ${FM_LOG_ROOT}
  echo "Setting ownership to root:root"
  chown root:root ${FM_LOG_ROOT}
  echo "Setting permissions to 774 to folder and contents"
  chmod -R 774 ${FM_LOG_ROOT}
  echo "Setting permissions to 755 to folder itself"
  chmod 775 ${FM_LOG_ROOT}
fi

echo ""
if [ -f /etc/systemd/system/flexmin.service ]
then
  echo "flexmin.service already exists."
else
  echo "Setting up and enabling flexmin.service (systemd)"
  [ -f '/usr/bin/flexmin' ] && sysd_dest='/etc/systemd/system/' && sysd_unit='flexmin.service'
  [ -f '/usr/local/bin/flexmin' ] && sysd_unit='flexmin.service.debian' && sysd_dest='/etc/systemd/system/flexmin.service'
  echo "Copying **$sysd_unit** systemd unit file to **$sysd_dest**"
  cp ${sysd_unit} ${sysd_dest}
  echo "Reloading systemd daemon to recognise new unit file"
  systemctl daemon-reload || exit 1
  echo "Enabling **flexmin.service**"
  systemctl enable flexmin.service
  systemctl restart flexmin.service
fi

echo ""
echo "Setting up and enabling uWSGI"
echo "-----------------------------"

if [ -d /etc/uwsgi/apps-available ]
then
  echo "uWSGI apps-available folder exists already"
else
  mkdir -p /etc/uwsgi/apps-available
fi

if [ -d /etc/uwsgi/apps-enabled ]
then
  echo "uWSGI apps-enabled folder exists already"
else
  mkdir -p /etc/uwsgi/apps-enabled
fi

if [ -d /var/log/uwsgi/app ]
then
  echo "uWSGI log folder exists already"
else
  mkdir -p /var/log/uwsgi/app
fi

if [ -f /etc/uwsgi/flexmin.ini ]
then
  echo "uWSGI flexmin config exists."
  echo "Checking if we need to change default web user account of **http**"

  # Set gid and uid to correct user in flexmin uwsgi config
  sed -i -r "s/(^.id)[ ]*=[ ]*http/\1 = ${FM_WEB_USER}/g" /etc/uwsgi/flexmin.ini
  
  # Adjust any uWSGI template files to refer correct local web user account
  if [ -d $flexmin_home/config-templates ]
  then
    cd $flexmin_home/config-templates
    for uc in `find ./ -mindepth 1 -maxdepth 1 -name "uwsgi.*.template"`
    do
      sed -i -r "s/(.id[ ]*=[ ]*)http/\1${FM_WEB_USER}/g" ${uc}
      sed -i -r "s/(chown-socket[ ]*=[ ]*)http/\1${FM_WEB_USER}/g" ${uc}
    done
    cd - >/dev/null
  fi
  
  echo "Checking if we need to specify python3 for uWSGI plugins"
  if [ ! -f $uwsgi_plugins/python_plugin.so ]
  then
    sed -i -e 's/plugins = python/plugins = python3/g' /etc/uwsgi/flexmin.ini
    echo "- we do"
  else
    echo "- we don't"
  fi
  
fi
if [ -f /etc/systemd/system/uwsgi-socket@flexmin.service ]
then
  echo "uwsg-socket@flexmin.service already exists"
else
  echo "Setting up systemd unit file for uwsgi socket for flexmin"
  cp $flexmin_home/system/uwsgi-socket@flexmin.service /etc/systemd/system/ || exit 1
  echo "Reloading systemd unit files to process new unit file"
  systemctl daemon-reload || exit 1
  
  # Only enable the flexmin uWSGI service on installation, leave alone during upgrade
  if [ ! -f /etc/flexmin/installed ]
  then
    echo "Enabling uwsgi-socket@flexmin.service"
    systemctl enable uwsgi-socket@flexmin.service || exit 1
  else
    echo "Upgrade: skipping enable flexmin uWSGI service"
  fi
fi

echo ""
echo "Setting up NGINX configuration"
echo "------------------------------"

if [ -d ${FM_NGINX_CONFIG}/certificates ]
then
  echo "NGINX certificates folder exists already"
else
  echo "Creating ${FM_NGINX_CONFIG}/certificates folder"
  mkdir -p ${FM_NGINX_CONFIG}/certificates
fi

if [ -d ${FM_NGINX_CONFIG}/sites-available ]
then
  echo "NGINX sites-available folder exists already"
else
  echo "Creating ${FM_NGINX_CONFIG}/sites-available folder"
  mkdir -p ${FM_NGINX_CONFIG}/sites-available
fi

if [ -d ${FM_NGINX_CONFIG}/sites-enabled ]
then
  echo "NGINX sites-enabled folder exists already"
  # echo "Disabling existing enabled sites (to avoid conflict)"
  # rm ${FM_NGINX_CONFIG}/sites-enabled/*
else
  echo "Creating ${FM_NGINX_CONFIG}/sites-enabled folder"
  mkdir -p ${FM_NGINX_CONFIG}/sites-enabled
fi

if [ -d ${FM_NGINX_CONFIG}/locations-available ]
then
  echo "NGINX locations-available folder exists already"
else
  echo "Creating ${FM_NGINX_CONFIG}/locations-available folder"
  mkdir -p ${FM_NGINX_CONFIG}/locations-available
fi

if [ -d ${FM_NGINX_CONFIG}/locations-enabled ]
then
  echo "NGINX locations-enabled folder exists already"
else
  echo "Creating ${FM_NGINX_CONFIG}/locations-enabled folder"
  mkdir -p ${FM_NGINX_CONFIG}/locations-enabled
fi

echo ""
if [ -f ${FM_SSL_ROOT}/flexmin.crt ] && [ -f ${FM_SSL_ROOT}/flexmin.key ]
then
  echo "SSL certificates exist for flexmin at ${FM_SSL_ROOT}"
else
  echo "Creating SSL certificates for flexmin at ${FM_SSL_ROOT}"
  # Generate self-signed certificate using ssl_selfcert.sh
  cd $flexmin_home/scripts
  ./common.sh 0 ./ssl_selfcert.sh flexmin create 365 AA private private flexmin flexmin
  cd - >/dev/null
fi
echo ""

if [ -f ${FM_NGINX_CONFIG}/certificates/flexmin.crt ]
then
  echo "NGINX certificate/flexmin.crt already exists"
else
  echo "Adding link to certificate for Flexmin"
  ln -s ${FM_SSL_ROOT}/flexmin.crt ${FM_NGINX_CONFIG}/certificates/flexmin.crt
fi

if [ -f ${FM_NGINX_CONFIG}/certificates/flexmin.key ]
then
  echo "NGINX certificate/flexmin.key already exists"
else
  echo "Adding link to certificate key for Flexmin"
  ln -s ${FM_SSL_ROOT}/flexmin.key ${FM_NGINX_CONFIG}/certificates/flexmin.key
fi
  
site_config=flexmin
if [ -f ${FM_NGINX_CONFIG}/sites-enabled/${site_config} ]
then
  echo "NGINX required config ${site_config} already exists and is enabled."
  # TODO Copy new configs and regenerate nginx configs anyway? In case the default config have been updated
else
  # flexmin_cli with install option handles copying flexmin.nginx.yaml and generating NGINX config in
  # sites-available
  
  # If installing, enable flexmin site, otherwise leave alone
  if [ ! -f /etc/flexmin/installed ]
  then
    echo "Enabling site ${site_config}" 
    ln -s ${FM_NGINX_CONFIG}/sites-available/${site_config} ${FM_NGINX_CONFIG}/sites-enabled
  else
    echo "Upgrade: skipping enable site flexmin"
  fi
fi

echo ""
echo "(Re)starting Flexmin, uWSGI and NGINX services"
systemctl restart flexmin
systemctl restart uwsgi-socket@flexmin.service
systemctl restart nginx.service

# Confirm completion of installation, existence of this file used to trigger upgrade behaviour
echo "Install completed $(date +'%Y-%m-%d %H:%M')" > /etc/flexmin/installed

echo ""
if [ ! -z "${flexmin_password}" ]
then
    echo "A password was generated for the flexmin user account. Please make a "
    echo "note of it as you will need it to log in to the Flexmin web interface."
    echo ""
    echo "password: ${flexmin_password}"   # format of this line is important as it stops this being logged
    echo ""
else
    echo "The flexmin user account already exists, no change made."
    echo ""
fi
echo "If the flexmin password is not shown here, or you want to use another password,"
echo "please use the command: sudo passwd flexmin"
echo ""
echo "Your Flexmin web interface should be available at: https://$(hostname).$(dnsdomainname):8003/flexmin/"
flexmin_password=""   # clear variable to keep password secure
