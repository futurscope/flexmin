# Check common paths required by Flexmin

if [ ! -d ${FM_SSL_ROOT} ]
then
    [ -d /etc/ssl ] && FM_SSL_ROOT='/etc/ssl'
    [ -d /usr/local/etc/ssl ] && FM_SSL_ROOT='/etc/ssl'
fi

# If we can see the existing flexmin.conf file and FM_CONF_CHECK=save
# then update the flexmin.conf file with the current set of parameters
if [ -f ${FM_CONFIGS}/flexmin.conf ]
then
  if [ "${FM_CONF_CHECK}" == "save" ]
  then
    printenv | grep ^FM_ | sed -r "s/=/='/" | sed -r "s/[ ]*$/'/" > ${FM_CONFIGS}/flexmin.conf
  fi
fi


