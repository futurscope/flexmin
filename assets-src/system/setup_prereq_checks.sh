# Make pre-requisite checks and abandon if not all met
#
# Checks all pre-requisites and generates error message before abandoning
# this ensures all can be dealt with before next attempt

# check flexmin installed, exit if not
if [ ! $(command -v flexmin) ]
then
  echo "Error: flexmin python module is not installed (flexmin command not available)"
  abandon="yes"
fi

# check NGINX installed, exit if it isn't
if [ ! $(command -v nginx) ] 
then
  echo "Error: NGINX is not installed"
  abandon="yes"
fi

# check uWSGI installed, exit if it isn't
if [ ! $(command -v uwsgi) ] 
then
  echo "Error: uWSGI is not installed"
  abandon="yes"
fi

# check openssl installed, exit if it isn't
if [ ! $(command -v openssl) ] 
then
  echo "Error: OpenSSL is not installed"
  abandon="yes"
fi

user=$(whoami)
if [ ! "$user" == "root" ]
then
  echo "Error: This script should be run as root"
  abandon="yes"
fi

# abandon install if NGINX or uWSGI not installed
[ "$abandon" == "yes" ] && echo "exiting" && exit 1
 
