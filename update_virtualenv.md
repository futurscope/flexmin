# Update to virtual env

## Flexmin

Create /home/flexmin/pyvenv folder
Activate it
use pip to install flexmin

Update flexmin.service to:

```
ExecStart=/home/flexmin/pyvenv/bin/python -m flexmin.flexmin_cli run
```

Update `/etc/uwsgi/flexmin.ini`

Add

```
virtualenv = /home/flexmin/pyvenv
```

## New process to install Flexmin

```sh
sudo mkdir /home/flexmin/pyvenv
chown -R <curr_user> /home/flexmin/
sudo apt-get install python3.12-venv
python -m venv /home/flexmin/pyvenv
source /home/flexmin/pyvenv/bin/activate
sudo ln -s /home/flexmin/pyvenv/bin/flexmin /usr/local/bin/flexmin
sudo flexmin install
sudo useradd flexmin || exit 1
flexmin_password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 10;echo;)
sudo passwd flexmin << EOD
${flexmin_password}
${flexmin_password}
EOD
```
