==================
Flexmin Parameters
==================

Defined key

- **Cfs** calculated in flexmin_srv.py
- **Py** defined in parameters.yaml file
- **Sc** and **Sv** set in common.sh and 0_vars.py
- **Cca** calculated by flexmin service client app (e.g. Web application)
- **Ccs** calculated within common.sh script


System parameters
-----------------

General parameters required by the Flexmin system.

==================  ==========  =======
Parameter           Defined     Purpose
==================  ==========  =======
FM_SERVICE_ROOT     Cfs,Ccs     Root directory of flexmin service
FM_SCRIPTS          Cfs,Ccs     Flexmin scripts folder
FM_LOGROOT          Py(Sc+Sv)   Location for logging task data
FM_CONFIGS          Cfs         Location of flexmin system configuration files
FM_DEFAULT_CONFIGS  Cfs,Ccs     Location for default flexmin configuration files (included with service)
FMS_PARAMETER       Cfs         Parameter provided when launching flexmin service ('debug' or 'display')
FM_TASK_DATA        Cfs         Location of data relating to task instances, sits within log root normally.
FM_TASK_TMP         Py(Sc+Sv)   Location for temporary extraction of batch task data (scripts and associated data files)
FM_BACKUP           Py(Sc+Sv)   Location for scripts to use when backing up data 
FM_USER             Cfs,Ccs     Account used to run the tasks on this system
==================  ==========  =======

Script parameters
-----------------

These parameters are needed for specific scripts or sets of scripts, normally those managing particular applications.

These are defined in the parameters.yaml file, the *flexmin_editparameters.py* script is used to edit this parameters file and 
update the *common.sh* and *0_vars.py* file to make these parameters available to shell and python scripts.

==================  ==========  =======
Parameter           Defined     Purpose
==================  ==========  =======
FM_WEB2PY_ROOT      Py(Sc+Sv)   Path to local web2py installation
FM_NODE_ROOT        Py(Sc+Sv)   Path to directory containing local node.js installation 
FM_REMOTE_UPLOAD    Py(Sc+Sv)   Path to ftp server where batch tasks can be uploaded e.g. 'ftp://ftp.ideaflowsolutions.com/public_html/websnap/'
FM_DJANGO_ROOT      Py(Sc+Sv)   Path to local Django instllation
FM_PSQL             Py(Sc+Sv)   PostgreSQL command on this system
FM_PSQL_SU          Py(Sc+Sv)   PostgreSQL super user account name e.g. 'pgsql'
FM_REMOTE_SOURCE    Py(Sc+Sv)   Path to source of this Websnap installation e.g. 'http://static.ideaflowsolutions.com/websnap/'
FM_SSL_ROOT         Py(Sc+Sv)   Path to SSL certificate folder e.g. '/usr/local/etc/ssl'
FM_UWSGI_ROOT       Py(Sc+Sv)   Path to system UWSGI configuration files e.g. '/usr/local/etc/uwsgi'

==================  ==========  =======


Task parameters
---------------

==================  ==========  =======
Parameter           Defined     Purpose
==================  ==========  =======
FM_TASK_ID          Cca,Ccs     Task id number, each task has a unique id for reference
FM_SCRIPT           Cfs,Ccs     Script name
FM_PARAMS           Cfs,Ccs     Location of parameters for this specific task instance (within FM_TASK_DATA location)
FM_SCRIPT_PARAM     Cfs,Ccs     Generic path to this scripts parameter files (append '_<varnum>' to get path to data saved for this task)
FM_TASK_LOG         Cfs,Ccs     Path to log file for this script (historical data for this script)
FM_LOGIN_SUCCESS    Cfs         Only needed by flexmin_login.py (not available to shell scripts)
==================  ==========  =======
