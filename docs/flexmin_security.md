# Flexmin Security

The Flexmin service runs as root user in order to be able to perform the 
various administration tasks required of it. This means the flexmin system
must take security seriously to protect the system.

Currently, it is recommended that you do not use Flexmin for systems on
public networks. Despite the precautions described in this document Flexmin
is untested in a public network enviroment and you are advised to only use it
on a secure private network. If placed behind a firewall you may be able to
retsrict access to the Flexmin interface to specific networks or IP addresses
to avoid the added risk.

The security measures currently in place are described here.

## Flexmin Login

The flexmin service will not run any script unless the correct password has
already been provided. Flexmin uses the password of the *flexmin* account on
the local system, a python PAM module is used to verify the password provided
against the linux account. This use of a standard system account was a
deliberate decision to use the built in security of the local system accounts.

When it validates a password, the Flexmin web application generates a UUID
(using the Python uuid library) for this session. This UUID represents a 
valid current *service session id*. This UUID is passed to the flexmin service
along with the supplied password. If the password is correct then the UUID is
recorded as a valid session id.

Future requests passed to the service from the Flexmin web application are
authorised by passing this *service session id* and the client IP address 
along with any requests to the service. 

> The Flexmin service only checks the *service session id*, the IP address
is not checked.

The web application in turn stores the *service session id* and associates 
this with the user's current session. A cookie in the browser tracks the user
session using a session id, the web application links that session to the
*service session id*.

As the password is sent to the web application over the network, HTTPS with
a suitable certificate is essential to the secure use of the Flexmin web 
interface.

### Lockouts : Failed Authentication

To proctect against brute force password attacks there should be a mechanism
to prevent repeated attempts to login. Flexmin implements an account lockout
mechanism which will restrict the rate at which attackers can try passwords.

Three login failures will trigger a lockout, each login failure is registered
and expires after 30 minutes. Therefore, three failures within 30 minutes will
prevent further login attempts until 30 minutes after the first failure. Login
attempts blocked by the lockout are not registered as the password is not
checked. This effectively limits any attacker to a login attempt every 10 
minutes which should be sufficient to protect Flexmin against a brute force
password attacke provided you use a suitably long and secure password.

In order to prevent password attacks locking a genuine system administrator 
out of the Flexmin interface, Flexmin remembers up to 5 IP addresses of 
previous successful logins. Anyone attempting to login from one of these addresses
is permitted **five** login failures before a lockout occurs. If an attacker
has caused a lockout, a genuine administrator will still be able to login 
by connecting from one of the five most recent IP addresses as they have 2 more
opportunities to sign-in compared to an attacker from an unrecognised IP 
address.

Be aware that if you are connecting to the Flexmin system from another network
that is behind a NAT firewall or gateway, then Flexmin will recognise your 
**public** IP address and give anyone from that network (with the same public
address) 5 attempts to login and allow them to lock you out of the system.

See the *check_session* method within the *flexmin_srv* module if you wish
to see the code used to implement the lockouts.

### Flexmin Sessions

Once a password has been provided and validated the end user can now make 
requests to Flexmin without supplying a password while the session remains
active. There are two sessions to consider.

#### Service Session

The Flexmin service uses the *service session id* provided by the web application, this
session times out after an hour of inactivity. Further (valid) requests to 
the service renew the session, extending the timeout to an hour after the 
request.

Restarting the Flexmin service will clear all session and login history.

#### Web Session

The web application stores the *service session id* in the user's own session,
this also expires after an hour of inactivity.

The web session data is stored in a cookie on the users browser. This cookie 
uses the **strict** same site setting. See 
[SameSite Cookies Explained](https://web.dev/samesite-cookies-explained/)

The Flexmin setup script (setup.sh) automatically generates a secret key
during installation using the following code:

`secret=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;)`

This code is inserted into the settings_private.py file for the local 
web application.

## CSRF Protection

All requests to the /flexmin/action URL generate a CSRF token which goes into 
any forms in the resulting HTML. This token is then submitted along with
the form and validated to ensure the form has originated from a current
client session. Only one token is held at any one time, so multiple tabs or
windows could end up causing confusion as they both will have different
tokens, so only the tab or window with the most recently loaded form will
be able to submit the form and have it accepted.

When presenting forms from Flexmin scripts please bear in mind:

* Once a form is submitted, the CSRF token will no longer be valid
* A `next_action: reload_pane` entry should be used to generate a new token
if the form is to be used again.
* If a table action requires a pop-up form, ensure you prefix the action with
`?`, this will make the pop-up a query and avoid generating a new CSRF token.

This last point is important so that if a pop-up form is shown and then 
closed without submitting the form, any form in the original page will still
hold a valid CSRF token. The pop-up form will have the same token as the main
page, so if the form is submitted a *reload_pane* action must be triggered
to ensure the main page (pane) gets a new (valid) CSRF token).


## Allowed Script Protection

The Flexmin service holds a list of scripts which it is allowed to execute.
This list is generated as startup using the menu.yaml configuration file,
and a check of which sripts are available in the Flexmin scripts folder.

The menu will only show tasks if the script to execute those tasks exists 
on the system, and the checks for the specified group have also passed 
(see Flexmin menu documentation). If these tasks are not shown in the menu
the Flexmin service will reject any attempt to execute the scripts associated
with those tasks. This means that any scripts that might exist on the system 
cannot be executed by the flexmin service if they are not listed within the 
Flexmin menu.

