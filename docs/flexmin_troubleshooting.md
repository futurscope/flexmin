# Flexmin Troubleshooting

## Flexmin Service

You can stop the service and run it in the shell in debug mode to find out 
what's going on in the service.

as root:

```bash
systemctl stop flexmin.service
flexmin run -V debug
```

## Flexmin scripts

You can run flexmin scripts from the command line using

```bash
./common.sh 0 '<flexmin_script>' [parameter1] [parameter2] ...
```

Scripts will expect `~default~` as the first paramter is testing the initial
display behaviour.




