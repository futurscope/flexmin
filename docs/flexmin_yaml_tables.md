# YAML For Table Output

This document describes how tables can be presented using the YAML output 
format.

## Basic Table Format

Tables can take the form:

```
table:
  header: [Column A, Column B!0, Column C]
  delimiter: ';'
  actions: per_row
  object_col: 1
  second_col: 0
  show_sub: ['_','&nbsp;']
  data: |"
    Cell 1A;Cell 1B;Cell 1C ?view.disable.!delete
    Cell 2A;Cell 2B;Cell 2C ?view.disable
```

## Properties

### header

A list of headers for the table columns, should be as a comma separated list
in square brackets. Although you could have an indented list of headers under
the header property line if you wish (see YAML format references).

You can specify that a column is not displayed by appending `!0` onto the end
of the column name, this adds the class *show_0* to the header and cells for 
that column, which is hidden via css rules. This might be useful if you want 
to send information along with an action request, but do not want, or need, 
that information to be displayed to the end user.

### delimiter

Specifies what character should be used to split a single line of text into separate columns

### show_sub

Allows you to substitute specific characters or strings with other characters 
or strings when displaying the table in the web page. This is useful if you 
need to ensure some cell data does not get split over more than one line. You
can put the text 'my_cell' into the table data, and replace the _ with 
`&nbsp;`.

### cell_newline

Sometimes data in a table cell is too long to fit comfortably into the table 
on the web page. It may help to define where this data should be split into
separate lines. the *cell_newline* property specifies what character 
(or string) is used to define a line break. This string is replaced with a 
`<br/>` tab to force the cell content onto separate lines.

### last_right

This allows you to specify that the last column in the table is right justified

This is suitable for columns showing numbers, or just for aesthetic reasons.

```
last_right: 1
```

### data

The data property is the text output that makes up the data for the table, 
this should be appropriately indented so it is processed as the contents of
the *data* property.

## Action Tables

An action table allows you to place buttons in the final column for each row.
When clicking on this button the value in the first column of that row is
passed back to the script as parameter 1 and the the name of the action is
passed as parameter 2. The script can then read these parameters to carry out
the specified action on the item represented by that row.

### actions (per_row or a list)

By specifying `actions: per_row` you must add an extra entry to each line in
the data specifying the actions available for this row object. This is useful
if the actions available depend on some property or state of that object.

If all rows should have the same actions available, you can specify:

`actions: '?view.~edit.!delete'`

> The action column is added automatically, you do not need to list it in
> the header property.

The actions should be unbroken text, separated by . with each action taking 
the form:

`[?|!|~][prefix+]<action>`

You can use an underscore (_) where you need a space in the button or link
name, this is replaced by a non-breaking space character in the web page.

The first optional character (?,! or ~) specifies the type of action link.


|  Type  | Behaviour |
| ------ | --------- |
| ?      | The action is a query that will add the *object* to the URL |
| !      | Brings up a confirmation prompt before the action is submitted |
| ~      | Makes the action a link instead of a form submission, but doesn't change the URL |


#### The Query Type (?)

If your action does not make any changes on the system, and will not require a 
refresh of the current view, then you should specify a query. 

This will show a link rather than a form submit button, the link will
have the *object* added to the URL, and the action as a query parameter
(with secondary column parameter if specified).

Should be used if your main view has a form and you wish to pop up another
form. This new form will have the same CSRF token and won't break your main
form. See Flexmin security documentation.

#### The Link Type (~)

This type makes a link to a *sub view* of the main script, with
the *object* from the table the subject of this view. Your script should 
handle a single parameter (not with value '~default~') and show details
of that object with forms if desired.

#### The Prefix (+)

The second optional part of the action is a prefix, followed by the + 
character. This prefix is not shown on the web page, but is added to the 
action command sent to the script, with the + character replaces by a space.

The action `?cb+view` will show as a link labelled `view` in the page 
and pass the parameter `cb view` to the script if clicked on. This is useful 
if you need to offer actions with the same name in the same page, but handle 
them differently.

### object_col and second_col

When you have an action table, the first column is taken to represent the 
object the row represents. However, sometimes this may not be the column that
holds the identifying data. If this is the case you can specify which column
should be used to find the *object* identifier that is passed to the script
as parameter 1. If you need more than a single identifier then you might
specify a second column, which holds the additional information that will be
passed to the script as parameter 3 (parameter 2 is by convention the action
verb e.g. view, edit or delete).

For example:

`object_col: 0` is the default behaviour, no need to specify this.

`object_col: 1` would specify the second column as holding the identifier, and
is passed to the script as parameter 1.

`second_col: 2` would specify the third column, the value of which is passed
to the script as parameter 3.
  
