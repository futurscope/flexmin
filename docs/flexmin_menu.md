# Flexmin Menu

The Flexmin menu is defined using a YAML file.

The YAML file looks a bit like this:

```YAML
destination:
  - type: fm-menu
    path: '{FM_CONFIGS}/menu.conf'
source:
  - group: System;arch.png
    tasks:
    - item: General;sys_general.sh;System management
    - item: Date and Time;sys_time.sh;Show or Set Date and Time
  - group: Network;network.png
    tasks:
    - item: Adaptor Configuration;net_editconfig.sh;Lists network interfaces to select which one 
        view config.
    - item: Network Manager;netman_editconfig.sh;Edit Network Manager Configurations
    - item: Hostname;net_hostname.sh;Show or Set Hostname
  - group: Samba;samba.png
    check_file:
      - '/etc/samba/smbd.conf'
      - '/etc/samba/smb.conf'
    check_executable: samba
    tasks:
    - item: Samba Dummy;samba_dummy.sh;Test
    - item: Samba Config;samba_config.sh;Configure Samba Server
    - item: Samba Users;samba_users.sh;Manage Users
```

## Groups

Menu items are groups together, normally all tasks relating to a particular
application or service will be groups together. The *group* items in the YAML
*source* entry define these top level groups.

Groups should have a name and an icon defined as follows:

`- group: <name>;<icon_file>`

If the icon is not avialable then you can omit the *icon file* entry but you 
must keep the semi-colon ; after the *name* entry

Each group entry can have additional properties defined, see the *Samba* group
in the example above. The properties available are:

* check_executable - *this specifies an executable that must be present on the system to enable this group*
* check_file - *this specifies a list of files, any one of which must exist on the system to enable this group*
* check_folder - *this specifies a list of folders, any one of which must exist on the system to enable this group*

All specified checks must be satisfied before the group and it's tasks entries will be enabled in the menu.

Each group also has a set of defined *tasks*

## Tasks

Tasks are entries that allow the end user to perform useful tasks such as 
editing files, configuring applications and so on. Each task relies on a script
that can be run by the Flexmin service. Each item in the task list therefore
specifies a name, script, and description, in that order as follows:

`- item: <name>;<script>;[description]`

The description is optional, but leave the semi-colon after the script entry
in place.

The specified script must exist in the Flexmin scripts folder in order to be 
included in the final task menu.

## Local menu file

You can define a *local* menu file (menu.local.yaml) if you wish to add menu 
settings independently of Flexmin's own default menu configuration. This is 
useful if you wish to restrict menu access, or add your own menu groups and 
items which will not be replaced during the next Flexmin upgrade.

The main Flexmin *menu.yaml* and the *menu.local.yaml* are combined as follows.

### General Principals

The *local* menu settings override the Flexmin menu settings.

### Groups

Local menu groups that do not match a main menu group (first parameter in the
group line is the identifier) get appended to the end of the main menu group 
list.

Local menu groups that do match a main menu group can override the name and
description (description not actually used currently) by providing these
details in a group entry. For example, this entry in your local menu file:

`- group: system;My System;My local system menu`

Will override the name and description values of the main menu group entry
with the identifier of `system`.

However, this entry in your local menu file:

`- group: system`

Will not change the name or description of the group, it merely acts as a
group for tasks you wish to include for this system only.

#### Group: active

If specified, this setting overides the main menu configuration. If not 
specified, the main menu configuration takes effect.

#### Group: check_*

If any of the group checks are specified in the local menu file, then the
local menu file checks are used and the main menu file checks are ignored.

#### Group Tasks

Within a group defined in your local menu file task items are processed as
follows:

* If the task identifier (script name) matches one in the same group in the
main menu file, then you override the name and description.
* If the task identifier does not match one in the same group in the main
menu file, then this task item is added to the end of the tasks in that group
* All tasks defined in a group which doesn't have a matching group identifier
in the main menu file get listed within an additional group after the groups
defined in the main menu file.

#### Local Configuration Indictaors

Where a group or task item configuration are modified by the local menu 
configuration file, or are configured only in the local file a small L is
appended to the group or task item name. This helps to indicate that the
item is being defined in the local menu file, not the main menu file.

## Other Notes

The menu.yaml file is processed when the Flexmin service starts, so the options
available should reflect the state of the system at startup. If applications 
are installed, or the menu configuration updated then a restart of the Flexmin
service may be required for changes to take effect. Alternatively, re-saving 
either menu configuration file should regenerate the menu based on what 
applications and folders exist on the system at that time.

