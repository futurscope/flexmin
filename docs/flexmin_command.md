# Flexmin Command

The *flexmin* command has three possible actions:

* install - extracts the associated files from the flexmin assets folder
* run - runs the flexmin service
* yamlconfig - provides access to the flexmin yaml to config file conversion

## Install

After installing Flexmin there are other files that need to be extracted
to get it up and running. See install instructions for further details.

`sudo flexmin install`

## Run

If you are having problems with the Flexmin service, it can be run in a
command console using the parameters *display* or *debug* so you can see
what is going on as requests are processed by the service. This must be
run as root so it has the access required.

`sudo flexmin run [display|debug]`

## Yamlconfig 

Flexmin uses YAML configuration files extensively because these fil;es are
easy to generate in shell scripts, easy to read and flexible. In some cases
YAML configuration files are preferred to an applications native config file
format, in which case it becomes necessary to convert the YAML configuration
into the applications native config file when these YAML files are modified.

The *amlconfig* option is used to allow shell scripts to carry out the 
conversion of YAML files to produce the applications native configuration file.

`flexmin yamlconfig <yaml_config_file>`

This process makes available all the variables defined in the Flexmin 
parameters file (flexmin.conf) so these parameters are substituted where they
appear in the YAML file enclosed in curly braces {}

The YAML configuration file contains details of where the native configurarion
file should be put, so there is no need to specify the name or path of the
file to be generated.

The specified YAML config file is assumed to be in the flexmin config folder,
as specified by the FM_CONFIGS parameter.


