sudo # Flexmin Installation

How to install the Flexmin service and web gui.

## Prerequisites

Flexmin requires the following:

- OpenSSL (openssl)
- Python 3 and Pip (python3 and python-pip or python3-pip)
- NGINX (nginx)
- uWSGI and uWSGI Python Plugins (uwsgi, uwsgi-plugin-python or uwsgi-plugin-python3)

> Debian based distros (Raspbian and Ubuntu for example) seem to need packages
> python3-pip and uwsg-plug-python3, they also require the use of the `pip3` 
> command instead of pip.

Please install these packages using your distribution's package manager before
proceeding with Flexmin installation.

Python modules required (pip, or pip3 command for debian and ubuntu based distro):

- wheel (install first, needed for subsequent installs)
- pyyaml
- py4web
- click

These components will be installed automatically by the pip installer, although
it is sometimes worth making sure the *wheel * module is installed first on 
less powerful systems (such as a Raspberry Pi) as this speeds up the install
process for subsequent modules.

You can make sure whell is installed using: `sudo pip install wheel`

## Installation

Cconfirm you have Python 3 using `python -V`, if this shows version 2 then try `python3 -V`. 
Install Python 3 if it is not already installed.

Check you have pip installed and that it is the package installer for Python 3 
using `pip -V`. If this displays python 3.x somwhere in the display string 
then use the `pip` command. Otherwise check you have the version for Python 3 
by using `pip3 -V`. Use the `pip3` command if this is available or install 
**python3-pip** package to make it available.

Install the Flexmin package from PyPi using the command:

`sudo pip install flexmin`
or `sudo pip3 install flexmin` on some Linux distributions

this will install Flexmin and it's dependencies within the system-wide python site-packages.

Once this package is installed you need to run the following command:

```
sudo flexmin install
```

The install command above sets up most of the system folders required for 
Flexmin and installs the flexmin system files into these folders. It then 
launches the setup script.

The setup.sh script will put individual configuration files into various 
system folders as required to get the Flexmin system up and running.

The setup script takes the following precautions to avoid messing up your system:
- Checks you are running as root
- Exits on error for most commands to avoid the rest of the script running into
  problems
- Avoids changing existing folders

### Troubleshooting Installation

#### 502 Bad Gateway

If you get this message it normally means NGINX is up and running okay but 
uWSGI is not. Check you have uWSGI **and** the uWSGI Python plugin package 
installed.

If you think you have found the uWSGI problem and fixed it, restart the uWSGI
Flexmin service: `sudo systemctl restart uwsgi-socket@flexmin`

#### 500 Internal Server Error

This indicates an issue with the py4web application. A reboot after the 
installation of flexmin may fix this, especially if you are re-installing
flexmin after an OS upgrade.

Otherwise, you can check the py4web application is working by doing the 
following:

```bash
sudo py4web run -H 0.0.0.0 /home/flexmin/py4web/
```

Then trying to connect to http://server:8000/flexmin/

You should see any errors starting up the application, or responding to the web request, in the console window.


## Further Details ##

### Flexmin Service/Daemon ###

The Flexmin service is a python based program that runs in the background as 
root to process system administration tasks via the Flexmin GUI (a Web GUI) 
to make system administration easier.

### File and Folders

#### Python packages

```bash
├── site-packages
    └── flexmin
        ├── __init__.py
        ├── flexmin_cli.py
        ├── flexmin_srv.py
        ├── flexmin_task.py
        ├── assets
        └── utils
```

#### Scripts, Configurations and Templates

Scripts are stored in Flexmin user home directory: **/home/flexmin/scripts**. Helpers are scripts used to perform
common functions within the main scripts.

```bash
/
└── home
    └── flexmin
        ├── scripts
        │   └── helpers
        ├── configs
        ├── config-templates
        ├── system
        └── local
            ├── script
            │   └── helpers
            ├── configs
            ├── config-templates
```

Other folders here are:

* configs - *original or default configuiration files for copying to /etc/flexmin folder*
* config-templates - *templates for creating various new configuration files*
* system - *holds all files used to set up or update flexmin on the system*
* local - *is used to store system specific scripts, configs or templates*

The *local* folder is used for system specific files; they will not be 
deleted during an upgrade. When an upgrade is complete, links will be 
(re)created to files within the *local* area to make them available to the 
upgraded version of Flexmin.

#### Live Configurations and Logs

Configurations are in **/etc/flexmin**

Logs are stored in **/var/log/flexmin**, these will be logs generated by 
individual task scripts.

Task data is stored in **/var/log/flexmin/task_data**. Task data consists
of files used to pass data to scripts, for example when editing a file and
saving this file via a script, the new copy is save here for access by the
script. A copy is kept here in case you need to view the details of what was
changed.

```bash
/
├── etc
│   └── flexmin
│       ├── flexmin.config
│       └── menu.yaml
└── var
    ├── log
    │   └── flexmin
    │       └── task_data
    └── tmp
        └──flexmin        
```
       
#### Flexmin Web GUI

The Web GUI is based on [PY4WEB](http://www.py4web.com)

The py4web apps folder is /home/flexmin/py4webp (just the flexmin app here)

```bash
/
└── home
    └── flexmin
        └── py4web
            └── flexmin
```
 
