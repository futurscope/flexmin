# Flexmin and DNSMASQ

Flexmin does not rely on DNSMASQ, but it does provide tools to help you manage
it.

The configuration view allows you to edit any DNSMASQ configuration files.
