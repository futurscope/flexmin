# YAML Base Configurations

Flexmin makes extensive use of YAML due to it's simplicity and ease of
creating and editing. One use for YAML is to store the configuration files 
managed by Flexmin. This use will be covered here.

## Why Not Application Configuration Files?

Why add the extra complexity of editing YAML files and then converting 
these to the application's own native format?

The idea of Flexmin is to save end users or administrators from dealing with
the various different configuration files and their own idiosyncrasies. Also
editing of configuration files is done using the ACE code editor, which can't 
necessarily handle the different formats involved.

One future development for Flexmin is to allow reconfiguration of various
applications and service via user friendly forms. YAML is one way of allowing
a consistent interface between these forms and configuration files, and YAML
will also allow plenty of metadata to be associated with various configuration
options without complicating the application configuration file.

## Basic Format

The main requirements for a YAML configuration file is a destination and source
section. Here is an example for a NGINX site configuration:

```YAML
destination:
  - type: nginx
    path: {FM_NGINX_CONFIG}/sites-available/py4web
source:
  server:
    - listen:
        - 443 default_server ssl
        - 80 default_server

      server_name: localhost $hostname
      access_log: /var/log/nginx/py4web_access.log
      error_log: /var/log/nginx/py4web_error.log
      keepalive_timeout: 70
      ssl_certificate: /etc/letsencrypt/live/flexmin.creativeedgedigital.co.uk/fullchain.pem
      ssl_certificate_key: /etc/letsencrypt/live/flexmin.creativeedgedigital.co.uk/privkey.pem
      client_max_body_size: 10M

      include: locations-enabled/py4web_*
```

Destination takes the form of a list of destinations. There should be at least 
one destination entry in this list, the dash inidcates a new list item. The
destination entry in the example above has *type* and *path* properties 
defined. The *type* property determines what type of configuration file we are
generating which is used to identify the method used to convert. The *path*
property indicates where the converted configuration file should be saved.

The *source* section is where the actual configuration is defined.

## Parameters

YAML configuration files handled by Flexmin can all use Flexmins own set of 
parameters (as defined in flexmin.conf). Simply insert the parameter name
within a pair or curly braces e.g. {FM_CONFIGS} and this paramter will be 
inserted during the conversion process.

## Access via Shell Scripts

YAML Configuration files can be modified and processed from shell scripts,
see the Flexmin command guide for details on how to access the *yamlconfig*
function which will handle the conversion process.

## Gotchas

PyYAML will interpret `yes`, `no`, `true`, `false`, `on` and `off` values
as *boolean* values resulting in `True` or `False` values in your 
generated configuration. To avoid this include single quotes `'` around
these values to force pyYAML to interpret them as strings.

## Specific Formt Guide

### Samba YAML

A Samba YAML configuration file looks like the example shown below.

The groups seciton holds a list of all groups. Each group can have
these properties:

* name - *the name that will fill the section header in square brackets []*
* info - *a comment for use only within YAML*
* active [yes|no] - *indicate whether to include this group in the output*
* settings - *a list of all settings for this group in the ini file*

The settings exactly match the settings you can use in a Samba configuration
file.

```YAML
destination:
  - type: ini
    path: {FM_SAMBA_CONFIG}
    separator: ' = '
source:
  groups:
    - name: global
      info: Server wide settings
      settings:
        - workgroup: Workgroup
        - server string: Samba Server
        - server role: standalone server
        #- hosts allow: 192.168.0.0/16
        #- guest account: guest
        - log file: '/usr/local/samba/var/log.%m'
        - max log size: 50
        #- interfaces: 0.0.0.0
    - name: homes
      active: no
      info: Home Directories
      settings:
        - browseable: 'no'
        - writeable: 'yes'
    - name: shared
      active: no
      settings:
        - path: '/home/<user>/shared'
        - writeable: 'yes'
        - create mask: '0777'
        - directory mask: '0777'
        - public: yes
```

## Metadata (Future Development)

THis is not currently implemented, but future development of Flexmin may
well use a metadata section of the YAML to hold details abotu what each 
configuration option does, in a similar way extensive commented lines are
used in many configuration file to expalin what each option does.



