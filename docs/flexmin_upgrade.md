# Flexmin Upgrades

As Flexmin has access to root privileges on your machine, it is important to
keep Flexmin and the applications it relies on up to date.

## Flexmin Upgrade Process

Flexmin can be upgraded using the following command:

`sudo pip install --upgrade flexmin`

> or `pip3` if the default pip command is for Python 2

followed by:

`sudo flexmin install`

The `flexmin install` command runs the same process as the original 
installation. 

This process is designed to avoid replacing your own configuration and data 
files (see details below). You will be prompted to allow each step of the 
installation process to complete. Provided you have followed the correct
process to add your own custom scripts and custom configurations, you should
be able to accept all the default options.

### Replace and Preserve

All the folders within the /home/flexmin folder (or whichever folder you 
define as FM_FLEXMIN_HOME) are deleted and replaced, subject to user 
confirmation.

The exception is the **py4web** folder, here the `./database` subfolder 
and it's contents are preserved, and the `passwords.txt` file is kept.

### Selective Copying

The following files are copied to the flexmin configuration folder
`/etc/flexmin/`.

File               | Check existing   | User Confirmation | YAML Config 
------------------ | ---------------- | ----------------- | -------
menu.yaml          | yes              | yes               | yes*
flexmin.conf       | yes              | yes               | no
flexmin.nginx.yaml | yes              | yes               | yes

If we are checking for an existing file, then a user confirmation may be 
required to overwrite the cofiguration file. The YAML config column
indicates whether this is a YAML configuration file, which is used to
generate a native configuration file. If the file is treated as a YAML
config, and the user allows it to be overwritten, then the native 
configuration file will be generated and overwrite any existing native 
configuration file.

Additionally, the **flexmin.ini** uWSGI configuration file is copied to the
uWSGI configuration folder `/etc/uwsgi/`. If the file already exists, user 
confirmation is required to overwrite it.

### Protecting Your Configurations

The Flexmin NGINX and uWSGI files (sites-available/flexmin and flexmin.ini) 
**will** be replaced during an upgrade. If you want to use your own 
configuration for NGINX or uWSGI, you should create new configurations and
disable the NGINX flexmin configuration, or disable the uWSGI service that
handle the default configurations. During an upgrade process the relevant
configurations and services will not be re-enabled.

Flexmin assumes that a valid certificate is available in the NGINX 
configuration folder with these paths:

* certificates/flexmin.crt (public certificate)
* certificates/flexmin.key (private key)

These should be symbolic links, not the actual certificate files (which should
be stored securely elsewhere on your system). If the links and the target files
exist then they will be left alone. Therefore your certificates for Flexmin
will remain in use after an upgrade.

You can set the FM_UPGRADE_BLOCK parameter as follows:

`FM_UPGRADE_BLOCK=flexmin_conf|menu.yaml|config-templates`

Each item you wish to prevent the upgrade of is listed, separated by the pipe
`|` symbol. Other options you can list are:

* scripts
* py4web
* configs

These will protect the relevant folder in your flexmin home folder, or the
specific configuration files from being overwritten in the flexmin 
configuration folder (/etc/flexmin).

By setting this parameter you are implicitly giving consent to other files and
folders being overwritten during the upgrade process. If this parameter is not 
specified, you will be prompted for each folder and file every time you 
upgrade.

### Reporting

A log is created by default an `/var/log/flexmin_setup.log.md`.

The log is in markdown format, so with a suitable viewer you can view 
a nicely presented log with headings and sections.



