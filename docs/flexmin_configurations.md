# Flexmin Configurations

Flexmin stores various configuration files within the `/etc/flexmin` folder.

The various categories of configuration file are described here.

## Essential Configurations

### Menu

The *menu.yaml* and *menu.conf* files hold the configuration of the Flexmin 
menu. This is likely to be customised to suit the needs of the relevant
system administrators. The *menu.yaml* is the pimrary configuration and is the
one that will be edited to meet each administrators requirements. The 
*menu.conf* file should be generated each time the *menu.yaml* file is saved.

The Flexmin service may need restarting when changes are made to the *menu 
confguration files.

### Flexmin Parameters

The *flexmin.conf* file holds various parameters for Flexmin, and is likely to
be configured to reflext settings specific to the local system.

## YAML Configurations

Flexmin has a number of YAML configuration files within the configuration 
folder which are used to generate configuration files for various applications
on the system. As they are files managed by Flexmin the original YAML versions
sit in the flexmin configuration folder. All these files end in *.yaml*

These YAML configurations files will potentially include:

* NGINX Configurations
* SAMBA Configuration
* MiniDLNA Configuration

The NGINX configurations are essential to the functioning of Flexmin as
they configure NGINX which is required for Flexmin to function. Some of
these may therefore be replaced during an upgrade.

## Permission Sets

The Filesystem group includes a section whioch allows you to define ownership 
and permissions for various files and folder within the local filesystem. 
Multiple sets of permissions can be defined, each specifying the required
permissions for a different set of files and folders. These files have the 
extension *.perms.ini*.

These permission sets should not be affected by the Flexmin upgrade process,
unless the format is changed and, then a conversion process may carried out on
them.

## Other Files

Other files may be stored here, which will normally be unaffected by a Flexmin
upgrade. At the time of writing these include:

* certbot_cli.ini - *holds settings for certbot client to handle certificate requests*

