# Flexmin Certificates

As a system administration tool, Flexmin needs to accept the required password
in a secure manner; a secure HTTP connection (HTTPS) is essential.

During installation the script will try to generate a self-signed certificate
for Flexmin. This will trigger dire warnings from your browser of choice about
not being able to verify the self-signed certificate, you will need to select
the *adavnced* option and choose to continue despite the risk. 

When you first log in to Flexmin setting up your own certificate that will be
accepted by the browser should be one of your first tasks. For this you need 
the following:

* Public web domain (e.g. example.com)
* Public web server (for the required domain)
* FTP access to the web server (secured using TLS)
* A DNS server you control on your own private network
* Some understanding of domains, web servers and DNS

## Lets Encrypt and Certbot

[Let's Encrypt](https://letsencrypt.org/) offer free certificates, the only 
catch is that the certificates only last for 90 days. No verification other
than ownership of the domain is done, so the certficates only confirm that
the domain name is genuine and that your conection is encrypted, which is
sufficient for most requirements.

The [Certbot ](https://certbot.eff.org/) tool is recommended as the way to
request and manage Let's Encrypt certificates. Flexmin has the facility to
request Let's Encrypt certificates via the Certbot tool.

### Certbot Installation

First you will need to install Certbot, this can probably be done using your
distribution's package manager and normally **certbot** would be the package
name. If you have problems with your usual package manager, you might see 
problems with python dependencies for example, then PyPi can be
used as an alternative installation method:

`pip install certbot`

### Certbot Usage

Certbot is used to help verify to Let's Encrypt that you are the owner of
the domain you a requesting a certificate for. This can be done by:

* Setting a DNS record within the domain
* Placing a specific file in a path on an existing web server already serving
pages for the domain.

Either can be a fiddly process, although Certbot can automate the latter 
option if it is running on the web server concerned.

It is likely (and strongly recommended currently) that you are not using 
Flexmin on a public web server, so it is not possible to set up the file
on the server where Let's Encrypt's systems can access it and confirm domain 
ownership.

One way around this is to use an existing public web server and configure
that server to accept requests to a sub-domain for which you can then request
a certificate. This often creates a folder specificly for that sub-domain.
So a sub-domain **flexmin.example.com** might be handled by a folder on the web
server called **flexmin.example.com**. In order to satisfy Let's Encrypt that
you own that domain you can then place the required *challenge* file in this 
folder. This is the method Flexmin is designed to use.

### Flexmin and Certbot

The first thing you need to do in Flexmin is to go to the *Certificates* group
and look for the Certbot configuration option. Here you can create a new 
Certbot configuration file from a template which has most of the configuration
already defined. All you need to add is your email address.

Once the Certbot configuration has been set up you can proceed to requesting
a certificate. You need to find the Certbot Certificates entry in the menu.

The Cerbot Certificates view will show you any Let's Encrypt certificates
set up on your system. Here you can also request a new certificate.

Flexmin assumes your public web server has FTP access enabled (with TLS for 
security), and that you have set up the domain folder as described above, and
that folder is a sub-folder within the default folder used by the FTP client.

Flexmin asks you for your FQDN entry (e.g. flexmin.example.com) for which you
require a certficate. You should also enter your email address (this is already
filled in if you set your email address in the Certbot configuration). Finally,
FTP server name, username and password are required, before clicking the 
*Proceed* button.

You should see the progress of the request, which can take a while, it goes 
through the following steps:

* Request sent to Let's Encrypt
* Let's Encrypt respond with a request to put a specific string inside a file
of a specified name at a standard path on your server
* Certbot passes these deatisl to a Flexmin helper script which then connects 
to your FTP server using the details you provided
* Flexmin switches to the folder matching the request domain name (FQDN), 
creates the required sub-folders and places the required file here
* Let's Encrypt then tries to access this file from the web server handling 
the domain specified (which will hopefull be the one we've just place the file)
* If Let's Encrypt's servers can access the required file on the public web 
server the certificate request is successful and the certificate is saved onto
your server (the one running Flexmin). You should see in the task output the 
details of where this certificate is saved
* Flexmin then runs a tidy up script which connects to the FTP server again,
deletes the file it put there earlier and deletes the sub-folders it created

Make a note of the path of the certificates created by Let's Encrypt and 
Certbot. You will then need to go to the NGINX group and edit the Flexmin
configuration file to enable the port on which SSL is used (port 8003 by 
default), edit the lines that specify the path to certificates and enter
the correct path to the Let's Encrypt certificates. Restart NGINX (or maybe 
reload). See Flexmin NGINX documentation for more details.

Now you should be able to connect to the Flexmin interface using a public 
domain name and publicly verifiable SSL certficate. *Note, you should have
the required entry in your local DNS server so this domain name resolves to
you local Flexmin server's IP address (you can use DNSMASQ for this)*

