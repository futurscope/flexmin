# Flexmin and uWSGI

Flexmin uses uWSGI to make itself available viw the NGINX server. The uWSGI
protocol is more efficient that the HTTP protocol, so using uWSGI instead
of the NGINX *proxy_pass* avoids the extra overhead of processing the HTTP 
request twice (once with NGINX, and once with the py4web web server).

On installation flexmin has it's own uWSGI configuration file called 
flexmin.ini in the main uWSGI configuration folder.

Flexmin provides a uWSGI management funtion to help with managing uWSGI 
applications such as py4web.

## Configuration Folders, Emperors and Vassals

Flexmin uses *apps-available* and *apps-enabled* folders to switch uWSGI
applications on and off. 

This assumes an *emperor* master configuration
file is defined and active. The emperor would be configured to use 
*apps-enabled* as it's source of *vassal* configurations. The *emperor*
will then automatically start up applications it finds defined in the
apps-enabled folder (using symbolic links to the actual configurations in the
apps-available folder). If we remove the symbolic links fro the apps-enabled
folder (using the disable function in Flexmin for example) then the emperor
will automatically shutdown that application.

The benefit of using this approach to handling vassal configurations files is
that Flexmin can also offer a *reload* option which simply disables and then 
enables the vassal causing a reload of the application allowing any updates
in the application code to take effect at the touch of a button.

## Overview

The Overview view within the uWSGI group gives you a useful overview of the 
uWSGI configurations available on the system and lets you switch any *optional*
configurations on and off.

The advantage of the *optional* configurations, if managed by an *emperor*
configuration is that they can be easily reloaded.

This view also lists the sockets defined in each uWSGI configuration file and
displays this in a list to make it easy to see at a glance how uWSGI is making
your applications available. This can be cross referenced with a similar view
for NGINX configurations to confirm NGINX is passing requests correctly to
uWSGI.


## Edit Configurations

The *edit configuration* view in the uWSGI task group shows you a list of all
uWSGI configuration files, along with the option to edit or delete any of these
files. Configuration files are found within subfolders of the uWSGI 
configuration folder (which defined using the FM_UWSGI_ROOT parameter).

The option to create a new uWSGI configuration file is here, it defaults to
using the `vassals` sub folder as Flexmin assumes an emperor/vassal uWSGI
usage mode. This has the advantage of having uWSGI manage all the vassals and
restart them automatically if they fail.

## Templates

No templates are defined for uWSGI configurations at the moment.

## Logs

Flexmin also provides easy access to uWSGI logs, those that are located in the
`/var/logs/uwsgi` folder. You can view the contents of these logs (limited to
the last set of entries to avoid huge log files becoming unwieldy via the web
interface), or delete them from with the Flexmin interface.


