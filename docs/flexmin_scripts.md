# Flexmin Scripts

Flexmin scripts are ordinary shell scripts, and sometimes python scripts.
These scripts can take input from the web interface and generate output to 
be displayed in the web interface.

## A Simple Example

A simple flexmin script could be something like this:

```
if [ $# -eq 1 ] && [ "$1" == "~default~" ]
then
  echo "title: Test Variables (Shell)"
  echo "text: |"
  printenv | grep ^FM_ | sed 's/^/  /'
fi
```

The first line checks that a parameter has been provided and that it is the
*default* value which means no object or action is specified.

The *echo* statements output partial YAML text, then the *printenv* statement
outputs the environment variables. The *grep* command ensures only Flexmin
variables are shown (all flexmin environment variables are prefixed with **FM_**).
The *sed* command inserts some extra spacing at the beginning of each output 
line to ensure the layout fits within the YAML format and can be processed for
the Flexmin interface.

## Communicating with Flexmin web interface

### Script output (YAML)

Flexmin scripts need to output text in YAML format in order that the Flexmin 
interface can handle it. The YAML gets converted into a dictionary and
this dictionary is porcessed to generate the HTML present to the end user
via the Flexmin web application.

At the top level of the YAML structure you can have any of these items:

* display
* error
* note
* table
* text
* task
* edit
* form

To have control over the order in which items are displayed, use the display
item at the top level, and include any of the other items as list items
within display. For example:

```
display:
  - form:
      title: My form
      fields:
        etc...
  - text: |
      A message in plain text format
```

If there is no display item then Flexmin will create it after processing the
YAML, and place any of the other items listed in the order shown above. If the
predefined order suits your purposes you can omit the display item and just
have:

```
note: This is an example
form:
  title: My form
  fields:
    etc...
text: |
  A long block of text
```

### Script parameters

Flexmin will run the specified script with a series of command line parameters.
There is a convention for how each parameter is used:

- The first parameter is normally the object (a user, host or file for example)
- The second parameter is normally an action (*delete*, *save* or *create* for
  example)
- The third parameter may be a secondary parameter passed to a table action

The first parameter is special, because the script does not have complete
control over what value is provided. In particular actions made available
within a table will always provide the first parameter as the object specific
to that row in the table, and the action as the second parameter. If the
script doesn't specify a value for the first parameter, it gets set to
'~default~' by the Flexmin web interface so your script should handle
this scenario.

### Tables

Example output to present a table:

```
table:
  title: My Table
  actions: 'view.!delete'"
  object_col: 0"   # column 0 passed as parameter #1
  second_col: 1"   # column 1 passed as parameter #3
  header: [A, B]"
  delimiter: ' '"
  data: |"
    1 2
    2 5
    3 56
```

#### Actions

Actions can be specified across the entire table, or *per_row*. When the 
*per_row* option is specified then each line of *data* must have a single
string added that has a . separated list of action commands. 

Each *action* takes the form of a text string which can be prefixed to
modify it's behaviour within the HTML page:

* ! - confirmation is required before proceeding with the action
* ~ - the action is a link to the object represented by this row 
(this adds the object reference from the row to the URL, resulting in
a URL for that specific *object* within the script)
* ? - the action is a query to request data (no change of address or state)

Actions within the table are POST requests except for these prefixes:

* ~ - Anchor element (GET request) with the row object added to URL
* ? - Like above, but with an action added to the URL as query parameter.

### Forms

A form can be presented for a user to fill in, see the example below.

```
form:"
  title: Create New User"
  fields:"
    - label: Username"
      info: Name of bew user account"
      pattern: '[a-zA-Z0-9_]+'"
      type: text"
    - label: Type
      selectformat: linesep
      select: |
        End User
        Administrator
    - type: hidden"
      default: create"
    - label: Password"
      type: password"
```

The various fields to be filled in are in the form of a YAML list (prefixed 
with the -). The dash indicateing a list item is only added to the first 
property or key, subsequent keys without the - therefore below to the same
item in the list.

Here the first field has *label*, *info*, *pattern*, and *type* defined.
It is recommended that label is the first property defined for any field
(where applicable) so that it is easy to interpret the field list.

The order of the fields matters, the fields are assigned numbers in the 
order they are presented in the YAML output, these numbers correspond to
the command parameters provided back to the script when the form is
submitted (e.g. shell variables $1 $2 $3 etc.).

Flexmin convention dictates that the first field is an *object* (i.e. user,
filename, hostname etc.). The second field should be the action to be taken
(i.e. create, delete, update etc.). Generally a form is presented for a 
particular action so the second field will be of type hidden, with the 
default value specifiying the action to be taken. The script will usually
check the action parameter ($2 in shell scripts) to determine what action
is to be taken and the object parameter ($1) to determine on what object
this action is to be taken.

### Form Fields

The various properties that can be specified for each form field are:

* label - the label shown next to the field in the web form.
* info - the details shown when you hover over the *info icon* for this field.
* type - what type of information is input here [text|password|hidden] [TBC: Check other types]
* default - provide a default value for this field
* pattern - a regular expression that can be used to validate the input

#### Field Types

The type property of a field is used to determine how the field will be 
presented among other things.

The **password** type is important for confidential details, the field will
be presented as a password field, meaning the text typed into this field
will not be visible to onlookers. This type also means that the value of this
field (and the parameter passed to the script) is not logged within Flexmin,
it will be replaced by a placeholder (e.g. <password>).  Scripts should also
ensure they do not record passwords anywhere.

The **hidden** field type is not shown to the user at all, it is used to pass
information back to the script when the form is submitted. The *default*
property should be defined for hidden fields as there is no other way for the
value to be specified.

The **text** and **text_long** field type is for simple text fields, the long
variant specified an 80 character length in the HTML for longer text input.
A **textbox** field type presents a text box of 80 characters, with 10 lines.

#### Select Field

If the property *select* is defined for a form field this presents a drop down
list to the end user. Add property `selectformat: linesep` to specify that the
select property is a line separated list:

```
selectformat: linesep
select:
  Option1
  Option2
```

### Editing and saving files

A script can output the following to present an edit file form, which will 
bring up the file in an edit box with a *save* button.

```
title: Editing authorized_key file for ${1}"
method: popup"
edit:"
  filename: /home/${1}/.ssh/authorized_keys"
  mode: text"
  data: |2"
    Some text to edit
    And another line
    
    More...
```

This will create a form that will submit the following parameters when the 
*save* button is pressed.

1. ```<filename>``` (full path of file to be saved)
2. ```save```       (action parameter)
3. ```<file_contents>```
4. ```<filename>``` (same as 1, needed in case parameter 1 is replaced with an object from the URL)

There is another parameter called *file_inputs*, which contains a list of which
parameters (numbered) represent file contents (text). When a parameter is marked 
as a *file input* flexmin saves the contents of the parameter as a file so that
the flexmin script can access the contents of the file submitted by the form.

An example of how a script might handle a save action from a file edit is shown 
here:

```
if [ "$2" == 'save' ]
then
  [ ! -z ${4} ] && filepath="${4}"
  echo "title: Saving ${filepath}"
  echo "method: popup"
  echo "category: CHANGE"
  echo "..."
  if [ -z ${filepath} ]
  then
    echo "Filepath not found to save file"
  else
    mkdir $(dirname ${filepath}) 2>/dev/null # make sure folder exists
    cp ${FM_SCRIPT_PARAM}_3 ${filepath} 2>/dev/null   # save file
    echo "Saved ${filepath}"
  fi
```

# Testing flexmin scripts

Your script should run from any bash console, simply enter the directory where the scripts are located and run the script with any required parameters.



