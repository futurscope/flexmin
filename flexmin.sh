#!/bin/sh

# PROVIDE: taskd
# REQUIRE: DAEMON FILESYSTEMS 
# KEYWORD: shutdown

PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin"

. /etc/rc.subr 

name="flexmin" 
rcvar=${name}_enable 

load_rc_config $name 

start_cmd="service_start" 
stop_cmd="service_stop"
status_cmd="service_status"
restart_cmd="service_restart"
extra_commands="status"
pidfile=/var/run/${name}.pid

service_start() {
  echo "Starting ${name}"
  cd /usr/local/share/${name}
  daemon -f -p ${pidfile} -u root ./flexmin_srv.py
}

service_stop() {
  echo "Stopping ${name}"
  pgrep -F ${pidfile} | xargs kill -s TERM
}

service_restart() {
  service_stop
  sleep 1
  service_start
}

service_status() {
  local retcode=0
  if [ -f $pidfile ] && cat $pidfile | xargs ps -p > /dev/null
  then
    echo $name is running as pid `cat $pidfile`
  else
    echo $name is not running.
    retcode=1
  fi
  return $retcode
}

run_rc_command "$1"
