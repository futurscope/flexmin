pause_time=5

echo "----------------------------------------------"
echo "- Creating new PyPi distribution packages    -"
echo "----------------------------------------------"

echo ""
echo "IMPORTANT: Please ensure you have updated the package version"
echo ""
echo "Current version string is:"
cat flexmin/__init__.py | grep __version__
echo "Enter new version number or press Return to continue with existing version"
read -p ">" newversion 
if [ ! -z $newversion ]
then
    sed -i  -e "s/^__version__[0-9.]*.*$/__version__ = '$newversion'/" flexmin/__init__.py
fi
echo ""
echo "Updated version string is:"
cat flexmin/__init__.py | grep __version__
read -n 1 -s -r -p "Press any key to continue or Ctrl+C to cancel"
echo "" && echo ""


echo "Clearing out existing distributions"
rm ./dist/*

cd ./assets-src
./update.sh
cd -

echo "Creating source distribution"
python setup.py sdist 

echo ""
read -t $pause_time -n 1 -s -r -p "Press any key to continue (or wait $pause_time seconds)"
echo "" && echo ""


echo "Creating Pure Python Wheel (python 2 OR python 3, not both)"
python setup.py bdist_wheel

echo ""
echo "New distribution packages created:"
ls -all ./dist/*
echo ""
echo "Use this command to upload to test repository:"
echo "$ twine upload --repository-url https://test.pypi.org/legacy/ -u <username> -p <password> dist/*"
echo "Use this command to upload to live repository:"
echo "$ twine upload -u <username> -p <password> dist/*"
