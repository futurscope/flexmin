A simple xmlrpc server implemented using python. 

This is provided to allow certain flexmin tasks to be carried out with root 
privileges, but with some authentication (pam) for security. The task is run as 
the user specified in the rc.d script, not the user used to authenticate.

The simplepam.py module is used to authenticate requests against a particular 
user.

Only make this available on 'localhost' for security reasons.

USE AT YOUR OWN RISK, CARELESS USE COULD OPEN YOUR SYSTEM TO UNAUTHORISED USERS.

FreeBSD Install Notes:

install to /usr/local/sbin/flexmin
flexmin is the rc.d script, it should be copied to /usr/local/etc/rc.d
/etc/rc.conf will need the line: xmlrpcd_enable="YES" to enable this service.

as root:

> cd /usr/local/sbin/
> hg clone https://ideaflows@bitbucket.org/ideaflows/xmlrpcd
> cp xmlrpcd/xmlrpcd /usr/local/etc/rc.d/
> chmod ug+x /usr/local/etc/rc.d/flexmin
> echo 'flexmin_enable="YES"' >> /etc/rc.con
> service flexmin start